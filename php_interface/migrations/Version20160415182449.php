<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160415182449 extends Version {

	protected $description = "переименование раздела «Общие контакты» в ИБ Contacts (sidebar) Investor relations";

	public function up()
	{
		$dbSections = \CIBlockSection::GetList(
			array("SORT"=>"ASC"),
			array("IBLOCK_ID" => INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN, "NAME" => "Общие контакты"),
			false,
			array(),
			false
		);
		if ($arSection = $dbSections->Fetch())
		{
			$obSection = new \CIBlockSection;
			$obSection->Update($arSection["ID"], array("NAME" => "Shared contacts"));
		}
	}

	public function down()
	{
		$dbSections = \CIBlockSection::GetList(
			array("SORT"=>"ASC"),
			array("IBLOCK_ID" => INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN, "NAME" => "Shared contacts"),
			false,
			array(),
			false
		);
		if ($arSection = $dbSections->Fetch())
		{
			$obSection = new \CIBlockSection;
			$obSection->Update($arSection["ID"], array("NAME" => "Общие контакты"));
		}
	}

}
