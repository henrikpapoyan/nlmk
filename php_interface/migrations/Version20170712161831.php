<?php

namespace Sprint\Migration;

use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20170712161831 extends Version
{

    protected $description = "Меняет настройки модуля поиска";

    const OLD_SETTINGS_STRING = '/index.php;/bitrix/*;/404.php;/en/404.php;/ru/404.php;/upload/*;*/.hg/*;*/.svn/*;*/.git/*;*/cgi-bin/*;/local/*;/html/*;/pdf/*;/en/polls/*;/ru/polls/*;/requests/*;/assets/*';

    const NEW_SETTINGS_STRING = '/index.php;/bitrix/*;/404.php;/en/404.php;/ru/404.php;/upload/*;*/.hg/*;*/.svn/*;*/.git/*;*/cgi-bin/*;/local/*;/html/*;/pdf/*;/en/polls/*;/ru/polls/*;/requests/*;/assets/*;*/detail.php';

    const SEARCH_MASK_EXCLUDE_CODE = 'exclude_mask';

    const AGENT_STEMMING = 'agent_stemming';

    const MODULE = 'search';

    public function up()
    {
        if (\COption::SetOptionString(self::MODULE, self::SEARCH_MASK_EXCLUDE_CODE, self::NEW_SETTINGS_STRING)
            && \COption::SetOptionString(self::MODULE, self::AGENT_STEMMING, 'N') && \CModule::IncludeModule('search')) {
            \CSearch::ReIndexAll(true);
            $this->outSuccess("Настройки модуля изменены");
        } else {
            $this->outError("Не удалось изменить настройки мудуля");
        }
    }

    public function down()
    {
        if (\COption::SetOptionString(self::MODULE, self::SEARCH_MASK_EXCLUDE_CODE, self::OLD_SETTINGS_STRING)
            && \CModule::IncludeModule('search')) {
            \CSearch::ReIndexAll(true);
            \COption::SetOptionString(self::MODULE, self::AGENT_STEMMING, 'Y');
            $this->outSuccess("Настройки модуля изменены");
        } else {
            $this->outError("Не удалось изменить настройки мудуля");
        }
    }
}
