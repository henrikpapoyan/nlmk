<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160415190421 extends Version {

	protected $description = "Решение возможных проблем со свойством COMPANY";

	public function up(){

		$arCompaniesRu = array();
		$dbCompaniesRu = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => COMPANIES_IBLOCK_ID_RU), false, false, array("ID", "IBLOCK_ID", "CODE"));
		while ($arCompanyRu = $dbCompaniesRu->Fetch())
		{
			$arCompaniesRu[$arCompanyRu["ID"]] = $arCompanyRu["CODE"];
		}

		$arCompaniesEn = array();
		$dbCompaniesEn = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => COMPANIES_IBLOCK_ID_EN), false, false, array("ID", "IBLOCK_ID", "CODE"));
		while ($arCompanyEn = $dbCompaniesEn->Fetch())
		{
			$arCompaniesEn[$arCompanyEn["CODE"]] = $arCompanyEn["ID"];
		}

		$arIblocks = array(
			PERFORMANSES_IBLOCK_ID_EN,
			NEWS_SMI_IBLOCK_ID_EN,
			NEWS_COMPANIES_IBLOCK_ID_EN,
			PHOTOS_IBLOCK_ID_EN,
			VIDEOS_IBLOCK_ID_EN
		);
		foreach ($arIblocks as $iblock)
		{

			$CIBlockProperty = new \CIBlockProperty;
			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblock, "CODE" => "COMPANY")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty->Update($arProp["ID"], array("LINK_IBLOCK_ID" => COMPANIES_IBLOCK_ID_EN));
			}

			$dbElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => $iblock), false, false, array("ID", "IBLOCK_ID", "PROPERTY_COMPANY", "NAME"));
			while ($arElement = $dbElements->Fetch())
			{
				if (in_array($arElement["PROPERTY_COMPANY_VALUE"], array_keys($arCompaniesRu)))
				{
					// вывод элементов привязанных к предприятиям из русских ИБ
					// на второй запуск миграции будут выведены только элементы, привязанные к русским компаниям у которых нет соотв. англ компании
					// echo "<pre>"; print_r($arElement); echo " - {$companyIdEn}</pre>";
					$companyCode = $arCompaniesRu[$arElement["PROPERTY_COMPANY_VALUE"]];
					$companyIdEn = $arCompaniesEn[$companyCode];
					if ($companyIdEn)
					{
						\CIBlockElement::SetPropertyValuesEx($arElement["ID"], false, array("COMPANY" => $companyIdEn));
					}
					else
					{
						$this->outError('Требуется проверить значение свойтва "COMPANY" элемента #'.$arElement["ID"].' в инфоблоке #'.$iblock);
					// 	\CIBlockElement::SetPropertyValuesEx($arElement["ID"], false, array("COMPANY" => ""));
					}
				}
			}

		}


	}

	public function down(){
		$helper = new IblockHelper();
	}

}
