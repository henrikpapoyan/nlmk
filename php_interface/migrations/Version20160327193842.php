<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160327193842 extends Version {

    protected $description = "Обновление настроек ИБ на использование SITE_DIR";

    public function up(){
        $iBlockFix = new \CIBlock;
        $iBlockFix->Update(141, array(
            'LIST_PAGE_URL' => '#SITE_DIR#media-center/news-companies/',
            'SECTION_PAGE_URL' => '#SITE_DIR#media-center/news-companies/',
            'DETAIL_PAGE_URL' => '#SITE_DIR#media-center/news-companies/#ELEMENT_CODE#/'
        ));

        $arIblocks = array();
        $dbIblocks = \CIBlock::GetList(array(), array());
        while($arIblock = $dbIblocks->Fetch()) {
            if ($arIblock['LID'] == 's2') {
                $listPageUrl = preg_replace('/^\/en\//', '#SITE_DIR#', $arIblock['LIST_PAGE_URL']);
                $sectionPageUrl = preg_replace('/^\/en\//', '#SITE_DIR#', $arIblock['SECTION_PAGE_URL']);
                $detailPageUrl = preg_replace('/^\/en\//', '#SITE_DIR#', $arIblock['DETAIL_PAGE_URL']);
            }
            if ($arIblock['LID'] == 's1') {
                $listPageUrl = preg_replace('/^\//', '#SITE_DIR#', $arIblock['LIST_PAGE_URL']);
                $sectionPageUrl = preg_replace('/^\//', '#SITE_DIR#', $arIblock['SECTION_PAGE_URL']);
                $detailPageUrl = preg_replace('/^\//', '#SITE_DIR#', $arIblock['DETAIL_PAGE_URL']);
            }
            $arIblocks[] = array(
                'ID' => $arIblock['ID'],
                'LIST_PAGE_URL' => $listPageUrl,
                'SECTION_PAGE_URL' => $sectionPageUrl,
                'DETAIL_PAGE_URL' => $detailPageUrl
            );
        }

        $iBlock = new \CIBlock;
        foreach ($arIblocks as $arIblock) {
            $iBlock->Update($arIblock['ID'], array(
                'LIST_PAGE_URL' => $arIblock['LIST_PAGE_URL'],
                'SECTION_PAGE_URL' => $arIblock['SECTION_PAGE_URL'],
                'DETAIL_PAGE_URL' => $arIblock['DETAIL_PAGE_URL']
            ));
        }
    }

    public function down(){
        $iBlockFix = new \CIBlock;
        $iBlockFix->Update(141, array(
            'LIST_PAGE_URL' => '/en/media-center/news-companies/',
            'SECTION_PAGE_URL' => '/en/media-center/news-companies/',
            'DETAIL_PAGE_URL' => '/en/media-center/news-companies/#ELEMENT_CODE#/'
        ));

        $arIblocks = array();
        $dbIblocks = \CIBlock::GetList(array(), array());
        while($arIblock = $dbIblocks->Fetch()) {
            if ($arIblock['LID'] == 's2') {
                $listPageUrl = preg_replace('/^#SITE_DIR#/', '/en/', $arIblock['LIST_PAGE_URL']);
                $sectionPageUrl = preg_replace('/^#SITE_DIR#/', '/en/', $arIblock['SECTION_PAGE_URL']);
                $detailPageUrl = preg_replace('/^#SITE_DIR#/', '/en/', $arIblock['DETAIL_PAGE_URL']);
            }
            if ($arIblock['LID'] == 's1') {
                $listPageUrl = preg_replace('/^#SITE_DIR#/', '/', $arIblock['LIST_PAGE_URL']);
                $sectionPageUrl = preg_replace('/^#SITE_DIR#/', '/', $arIblock['SECTION_PAGE_URL']);
                $detailPageUrl = preg_replace('/^#SITE_DIR#/', '/', $arIblock['DETAIL_PAGE_URL']);
            }
            $arIblocks[] = array(
                'ID' => $arIblock['ID'],
                'LIST_PAGE_URL' => $listPageUrl,
                'SECTION_PAGE_URL' => $sectionPageUrl,
                'DETAIL_PAGE_URL' => $detailPageUrl
            );
        }

        $iBlock = new \CIBlock;
        foreach ($arIblocks as $arIblock) {
            $iBlock->Update($arIblock['ID'], array(
                'LIST_PAGE_URL' => $arIblock['LIST_PAGE_URL'],
                'SECTION_PAGE_URL' => $arIblock['SECTION_PAGE_URL'],
                'DETAIL_PAGE_URL' => $arIblock['DETAIL_PAGE_URL']
            ));
        }
    }

}
