<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160622101152 extends Version
{

	protected $description = "Изменение метода генерации выпусков автоматической рассылки (перевод на cron)";

	public function up()
	{
		\COption::SetOptionString("subscribe", "subscribe_template_method", "cron");
		\CAgent::RemoveAgent("CPostingTemplate::Execute();", "subscribe");
	}

	public function down()
	{
		\COption::SetOptionString("subscribe", "subscribe_template_method", "agent");
		\CAgent::AddAgent("CPostingTemplate::Execute();", "subscribe", "N", 60, "", "Y", "", 30);
	}
}
