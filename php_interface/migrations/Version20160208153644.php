<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
use CIBlockProperty;

class Version20160208153644 extends Version {

	protected $description = "Обновление свойства \"Фильтр\" для инфоблоков \"Совет директоров\" и \"Члены правления\"";

	public function up(){

		$arIblocks = array(
			DIRECTORS_IBLOCK_ID_RU,
			DIRECTORS_IBLOCK_ID_EN,
			MANAGEMENT_IBLOCK_ID_RU,
			MANAGEMENT_IBLOCK_ID_EN
		);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = CIBlockProperty::GetPropertyEnum(
				"FILTER",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["XML_ID"] == "i-ico_user") {
					$arEnumProp['VALUE'] = "Комитет по кадрам, вознаграждениям и социальной политике";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}

	}

	public function down(){

		$arIblocks = array(
			DIRECTORS_IBLOCK_ID_RU,
			DIRECTORS_IBLOCK_ID_EN,
			MANAGEMENT_IBLOCK_ID_RU,
			MANAGEMENT_IBLOCK_ID_EN
		);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = CIBlockProperty::GetPropertyEnum(
				"FILTER",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["XML_ID"] == "i-ico_user") {
					$arEnumProp['VALUE'] = "Комитет по кадрам, вознаграждениям, стратегической и социальной политик";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
	}

}
