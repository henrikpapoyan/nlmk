<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
/**
 * Для запуска из консоли:
 * php local/php_interface/migrations/migrate.php execute Version20171213160845 --up
 * php local/php_interface/migrations/migrate.php execute Version20171213160845 --down
 */
class Version20171213160845 extends Version {

    protected $description = "Меняем дату начала активности в ИБ \"Документы\" на основании названия элементов RU и EN версии";

    protected $langs = array(
        "RU",
        "EN"
    );
    
    protected $sections = array(
        'kvartalnye_otchety',
        'facts',
        'list_affeli_faces',
        'emiss-doc'
    );

    public function up(){
        $el = new \CIBlockElement;
        foreach ($this->langs as $lang) {
            $err = false;
            $sections = array();
            $arSelect = Array("ID", "CODE");
            $arFilter = Array("IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang));

            $res = \CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);
            while ($elem = $res->fetch()) {
                if (in_array($elem["CODE"], $this->sections)) {
                    $sections[$elem["CODE"]] = $elem["ID"];
                }
            }
            if (count($sections) > 0) {
                $arElems = array();
                $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
                $arFilter = Array(
                    "IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang),
                    "SECTION_ID" => $sections,
                    "INCLUDE_SUBSECTIONS" => "Y",
                );

                $res = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                while ($elem = $res->fetch()) {
                    if (isset($elem['DATE_ACTIVE_FROM']) && strlen($elem['DATE_ACTIVE_FROM']) > 0) {
                        $date = explode(' ', $elem['DATE_ACTIVE_FROM']);
                        $elem['DATE_ACTIVE_FROM'] = $date[0];
                        $yearActive = date_create_from_format("d.m.Y", $date[0])->format("Y");
                        $md = date_create_from_format("d.m.Y", $date[0])->format("d.m");
                    } else {
                        $yearActive = '2017';
                        $md = date('d.m');
                    }
                    if (preg_match('#(\d{6})#isu', $elem['NAME'], $resStr)) {
                        $elem['NAME'] = str_replace($resStr[1], "", $elem['NAME']);
                    }
                    if (preg_match('#(\d{5})#isu', $elem['NAME'], $resStr)) {
                        $elem['NAME'] = str_replace($resStr[1], "", $elem['NAME']);
                    }
                    if (preg_match('#(\d{4})#isu', $elem['NAME'], $resStr)) {
                        $nameYear = $resStr[1];
                    } elseif (preg_match('#\d{2}.\d{2}.(\d{3})#isu', $elem['NAME'], $resStr)) {
                        $nameYear = '2' . $resStr[1];
                    } elseif (preg_match('#\d{2}.\d{2}.(\d{2})#isu', $elem['NAME'], $resStr)) {
                        $nameYear = '20' . $resStr[1];
                    } else {
                        continue;
                    }
                    if ($nameYear != $yearActive) {
                        $arElems[$elem['ID']] = array(
                            'OLD_DATE' => $md . '.' . $yearActive,
                            'NEW_DATE' => $md . '.' . $nameYear,
                        );
                    }
                }
            } else {
                $this->outError("Не удалось найти нужные разделы");
                
                continue;
            }
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/migrations/migration_Version20171213160845_' . $lang . '.txt', var_export(json_encode($arElems),true));
            foreach($arElems as $id => $element){
                if ($el->Update($id, array("DATE_ACTIVE_FROM" => $element["NEW_DATE"]))) {
                    $this->outSuccess('Элемент id='. $id . ' обновлен');
                } else {
                    $err = true;
                    $this->outError('Не получилось обновить элемент id='. $id);
                }
            }
            if ($err === false) {
                $this->outSuccess('Все элементы обновлены');
            }
        }
    }

    public function down(){
        $el = new \CIBlockElement;
        foreach ($this->langs as $lang) {
            $err = false;
            $file = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/migrations/migration_Version20171213160845_' . $lang . '.txt');
            if ($file) {
                $elements = json_decode(substr($file,1,-1), true);

                foreach ($elements as $id => $element) {
                    if ($el->Update($id, array("DATE_ACTIVE_FROM" => $element["OLD_DATE"]))) {
                        $this->outSuccess('Элемент id='. $id . ' обновлен');
                    } else {
                        $err = true;
                        $this->outError('Не получилось обновить элемент id='. $id);
                    }
                }
                if ($err === false) {
                    $this->outSuccess('Все элементы обновлены');
                }
                unlink($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/migrations/migration_Version20171213160845_' . $lang . '.txt');
            } else {
                $this->outError('Файл migration_Version20171213160845_' . $lang . '.txt' . ' не удалось найти');
                
                return false;
            }
        }
    }

}
