<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160517083047 extends Version
{

	protected $description = "Настройки индексации для инфоблоков";

	public function up()
	{
		$obIblock = new \CIBlock;
		$arIblockFields = array(
			GEOGRAPHY_DIVISION_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
			),
			DOCUMENTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			INFOGRAPHIC_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			MISSION_STORIES_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			HISTORY_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "Y",
				"LIST_PAGE_URL" => "#SITE_DIR#about/history/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/history/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/history/",
			),
			KEY_FACTORS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#about/key-factors/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/key-factors/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/key-factors/",
			),
			COMPANIES_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			PURCHASES_AND_SALES_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "Y",
				"LIST_PAGE_URL" => "#SITE_DIR#about/strategy/purchases-and-sales/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/strategy/purchases-and-sales/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/strategy/purchases-and-sales/",
			),
			FIVE_YEAR_RESULTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#about/five-year-highlights/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/five-year-highlights/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/five-year-highlights/",
			),
			HOME_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			KEY_FACTORS_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			MISSION_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			MISSION_SLIDER_LEADERSHIP_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			DIRECTORS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			DOCUMENTS_BOTTOM_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			STRATEGY_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "Y",
				"LIST_PAGE_URL" => "#SITE_DIR#about/strategy/strategy-in-action/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/strategy/strategy-in-action/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/strategy/strategy-in-action/",
			),
			FUNCTIONAL_DIVISION_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
			),
			MANAGEMENT_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),


			PRODUCTION_TYPE_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
				"SECTION_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
			),
			PRODUCTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
				"SECTION_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
			),
			INNOVATIONS_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			PRODUCTION_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			PRODUCTS_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			APPLICATION_SCOPE_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/application-fields/",
				"SECTION_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/application-fields/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/application-fields/",
			),


			ANNUAL_REPORTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#investor-relations/reporting-center/annual-reports/",
				"SECTION_PAGE_URL" => "#SITE_DIR#investor-relations/reporting-center/annual-reports/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#investor-relations/reporting-center/annual-reports/",
			),
			SHAREHOLDER_CENTRE_DOCS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			INVESTOR_CONTACTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			REPORTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			PRESENTATIONS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			AGM_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			INVESTOR_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			FINANCIAL_CALENDAR_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),


			CHARITY_PROGRAM_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#responsibility/charity/",
				"SECTION_PAGE_URL" => "#SITE_DIR#responsibility/charity/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#responsibility/charity/",
			),
			CHARITY_PROJECTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			ENV_PROGRAMME_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#responsibility/ecology/environmental-programme-2020/",
				"SECTION_PAGE_URL" => "#SITE_DIR#responsibility/ecology/environmental-programme-2020/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#responsibility/ecology/environmental-programme-2020/",
			),
			ECOLOGY_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			ENERGY_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			CHARITY_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			ECOLOGY_CASES_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			ENERGY_CASES_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),


			VIDEOS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			QUESTIONS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			FAQ_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#media-center/questions-and-answers/",
				"SECTION_PAGE_URL" => "#SITE_DIR#media-center/questions-and-answers/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#media-center/questions-and-answers/",
			),
			AWARDS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#media-center/corporate-media/",
				"SECTION_PAGE_URL" => "#SITE_DIR#media-center/corporate-media/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#media-center/corporate-media/",
			),
			PERFORMANSES_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			MC_CONTACTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			PRESS_CONTACTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			CORPORATE_MAGAZINE_IBLOCK_ID => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			EVENTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			MULTIMEDIA_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			NEWS_COMPANIES_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			PRESS_KIT_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#media-center/press-kit/",
				"SECTION_PAGE_URL" => "#SITE_DIR#media-center/press-kit/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#media-center/press-kit/",
			),
			NEWS_GROUP_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			MC_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			EVENTS_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			GLOSSARY_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			NEWS_SMI_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			MC_FILES_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			PHOTOS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),

			COMPETITIONS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#career/personnel-development/",
				"SECTION_PAGE_URL" => "#SITE_DIR#career/personnel-development/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#career/personnel-development/",
			),
			CAREER_CONTACTS_SIDEBAR_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			OUR_PROFESSIONS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			RESUME_IBLOCK_ID => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			CAREER_SLIDER_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),


			CONTACTS_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#contacts/",
				"SECTION_PAGE_URL" => "#SITE_DIR#contacts/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#contacts/",
			),
			CONTACTS_SIDEBAR_IBLOCK_ID_RU => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),


			GEOGRAPHY_DIVISION_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
			),
			DOCUMENTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			INFOGRAPHIC_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			MISSION_STORIES_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			HISTORY_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "Y",
				"LIST_PAGE_URL" => "#SITE_DIR#about/history/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/history/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/history/",
			),
			KEY_FACTORS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#about/key-factors/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/key-factors/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/key-factors/",
			),
			COMPANIES_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			PURCHASES_AND_SALES_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "Y",
				"LIST_PAGE_URL" => "#SITE_DIR#about/strategy/purchases-and-sales/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/strategy/purchases-and-sales/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/strategy/purchases-and-sales/",
			),
			FIVE_YEAR_RESULTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#about/five-year-highlights/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/five-year-highlights/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/five-year-highlights/",
			),
			HOME_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			KEY_FACTORS_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			MISSION_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			MISSION_SLIDER_LEADERSHIP_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			DIRECTORS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			DOCUMENTS_BOTTOM_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			STRATEGY_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "Y",
				"LIST_PAGE_URL" => "#SITE_DIR#about/strategy/strategy-in-action/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/strategy/strategy-in-action/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/strategy/strategy-in-action/",
			),
			FUNCTIONAL_DIVISION_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
				"SECTION_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#about/map-of-assets/",
			),
			MANAGEMENT_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),


			PRODUCTION_TYPE_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
				"SECTION_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
			),
			PRODUCTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
				"SECTION_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/products/",
			),
			INNOVATIONS_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			PRODUCTION_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			PRODUCTS_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			APPLICATION_SCOPE_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/application-fields/",
				"SECTION_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/application-fields/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#our-business/products-and-innovations/application-fields/",
			),


			ANNUAL_REPORTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#investor-relations/reporting-center/annual-reports/",
				"SECTION_PAGE_URL" => "#SITE_DIR#investor-relations/reporting-center/annual-reports/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#investor-relations/reporting-center/annual-reports/",
			),
			SHAREHOLDER_CENTRE_DOCS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			INVESTOR_CONTACTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			REPORTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			PRESENTATIONS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			AGM_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			INVESTOR_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			FINANCIAL_CALENDAR_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),


			CHARITY_PROGRAM_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#responsibility/charity/",
				"SECTION_PAGE_URL" => "#SITE_DIR#responsibility/charity/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#responsibility/charity/",
			),
			CHARITY_PROJECTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			ENV_PROGRAMME_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#responsibility/ecology/environmental-programme-2020/",
				"SECTION_PAGE_URL" => "#SITE_DIR#responsibility/ecology/environmental-programme-2020/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#responsibility/ecology/environmental-programme-2020/",
			),
			ECOLOGY_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			ENERGY_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			CHARITY_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			ECOLOGY_CASES_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			ENERGY_CASES_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),


			VIDEOS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			QUESTIONS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			FAQ_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#media-center/questions-and-answers/",
				"SECTION_PAGE_URL" => "#SITE_DIR#media-center/questions-and-answers/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#media-center/questions-and-answers/",
			),
			AWARDS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#media-center/corporate-media/",
				"SECTION_PAGE_URL" => "#SITE_DIR#media-center/corporate-media/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#media-center/corporate-media/",
			),
			PERFORMANSES_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			MC_CONTACTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			PRESS_CONTACTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			EVENTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			MULTIMEDIA_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			NEWS_COMPANIES_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			PRESS_KIT_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#media-center/press-kit/",
				"SECTION_PAGE_URL" => "#SITE_DIR#media-center/press-kit/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#media-center/press-kit/",
			),
			NEWS_GROUP_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			MC_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			EVENTS_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			GLOSSARY_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			NEWS_SMI_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			MC_FILES_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			PHOTOS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),

			COMPETITIONS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#career/personnel-development/",
				"SECTION_PAGE_URL" => "#SITE_DIR#career/personnel-development/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#career/personnel-development/",
			),
			CAREER_CONTACTS_SIDEBAR_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			OUR_PROFESSIONS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
			),
			RESUME_IBLOCK_ID => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),
			CAREER_SLIDER_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),


			CONTACTS_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "Y",
				"INDEX_SECTION" => "N",
				"LIST_PAGE_URL" => "#SITE_DIR#contacts/",
				"SECTION_PAGE_URL" => "#SITE_DIR#contacts/",
				"DETAIL_PAGE_URL" => "#SITE_DIR#contacts/",
			),
			CONTACTS_SIDEBAR_IBLOCK_ID_EN => array(
				"INDEX_ELEMENT" => "N",
				"INDEX_SECTION" => "N",
			),

		);
		foreach ($arIblockFields as $iblockId => $arFields)
		{
			$res = $obIblock->Update($iblockId, $arFields);
		}
	}

	public function down()
	{

	}

}
