<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
use CIBlockProperty;

class Version20160224163225 extends Version {

	protected $description = "Убрана обязательность свойства \"Описание\" для ИБ Совет директоров и Правление";

	public function up(){
		$arIblocks = array(
			DIRECTORS_IBLOCK_ID_RU,
			DIRECTORS_IBLOCK_ID_EN,
			MANAGEMENT_IBLOCK_ID_RU,
			MANAGEMENT_IBLOCK_ID_EN
		);
		foreach ($arIblocks as $iblockId) {
			$dbProp = CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "DESCRIPTION")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("IS_REQUIRED" => "N"));
			}
		}
	}

	public function down(){
		$arIblocks = array(
			DIRECTORS_IBLOCK_ID_RU,
			DIRECTORS_IBLOCK_ID_EN,
			MANAGEMENT_IBLOCK_ID_RU,
			MANAGEMENT_IBLOCK_ID_EN
		);
		foreach ($arIblocks as $iblockId) {
			$dbProp = CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "DESCRIPTION")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("IS_REQUIRED" => "Y"));
			}
		}
	}

}
