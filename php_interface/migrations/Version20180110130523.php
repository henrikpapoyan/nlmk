<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
/**
 * Для запуска из консоли:
 * php local/php_interface/migrations/migrate.php execute Version20180110130523 --up
 * php local/php_interface/migrations/migrate.php execute Version20180110130523 --down
 */
class Version20180110130523 extends Version {
    protected $description = "Создание раздела \"Контакты Службы снабжения\" в ИБ \"Контакты\" RU версия";
    protected $props = array(
        "RU" => array(
            'CODE' => 'contacts-supply-services',
            'NAME' => 'Контакты Службы снабжения',
        )
    );

    public function up(){
        $helper = new IblockHelper();

        foreach ($this->props as $lang => $prop) {
            $section = \CIBlockSection::GetList(array(),array("IBLOCK_ID" => constant("CONTACTS_IBLOCK_ID_" . $lang), "CODE" => $prop["CODE"]))->fetch();
            if (!$section) {
                $arSection = array("NAME" => $prop["NAME"], "CODE" => $prop["CODE"]);

                if ($helper->addSection(constant("CONTACTS_IBLOCK_ID_" . $lang), $arSection)) {
                    $this->outSuccess('Раздел '. $prop['NAME'] . ' добавлен');
                } else {
                    $this->outError("Не удалось добавить раздел " . $prop['NAME']);
                }
            } else {
                $this->outSuccess('Раздел '. $prop['NAME'] . ' уже существует');
            }
        }
    }

    public function down(){
        $arSectionsId = array();
        foreach ($this->props as $lang => $prop) {
            $dbSections = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => constant("CONTACTS_IBLOCK_ID_" . $lang), "CODE" => $prop["CODE"]), false, array("ID"));
            while ($arSection = $dbSections->GetNext()) {
                $arSectionsId[] = $arSection["ID"];
            }
        }

        foreach ($arSectionsId as $sectionId) {
            if (\CIBlockSection::Delete($sectionId)) {
                $this->outSuccess('Раздел id='. $sectionId . ' удален');
            } else {
                $this->outError("Не удалось удалить раздел id=" . $sectionId);
            }
        }
    }
}
