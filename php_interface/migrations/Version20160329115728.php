<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160329115728 extends Version {

	protected $description = "Изменение названий сайтов (русской и анлийской версии)";

	public function up() {
		$obSite = new \CSite;
		$obSite->Update("s1", array("NAME" => "RU"));
		if (strlen($obSite->LAST_ERROR) > 0)
			echo $obSite->LAST_ERROR;
		$obSite->Update("s2", array("NAME" => "EN"));
		if (strlen($obSite->LAST_ERROR) > 0)
			echo $obSite->LAST_ERROR;
	}

	public function down(){
		$obSite = new \CSite;
		$obSite->Update("s1", array("NAME" => "НЛМК"));
		if (strlen($obSite->LAST_ERROR) > 0)
			echo $obSite->LAST_ERROR;
		$obSite->Update("s2", array("NAME" => "Английская версия НЛМК"));
		if (strlen($obSite->LAST_ERROR) > 0)
			echo $obSite->LAST_ERROR;
	}

}
