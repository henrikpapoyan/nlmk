<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171211114631 extends Version {

    protected $description = "Создаем св-во \"Синонимы для проставления ссылок\" для русской версии";

    protected $property = [
        'CODE' => 'NAME_VARIANTS',
        'SORT' => 1500,
        "PROPERTY_TYPE" => "S",
        'NAME' => 'Синонимы для проставления ссылок',
        "MULTIPLE" => "Y"
    ];

    protected $ibIds = [\DIRECTORS_IBLOCK_ID_RU, \MANAGEMENT_IBLOCK_ID_RU, \GOVMANAGEMENT_IBLOCK_ID_RU];

    public function up()
    {
        $helper = new IblockHelper();
        foreach ($this->ibIds as $ibId) {
            if ($helper->addPropertyIfNotExists($ibId, $this->property)) {
                $this->outSuccess('Добавлено свойство "Ссылки на персоны проставлены" в инфоблок ' . $ibId);
            }
        }
    }

    public function down()
    {
        $helper = new IblockHelper();
        foreach ($this->ibIds as $ibId) {
            if ($helper->deleteProperty($ibId, $this->property['CODE'])) {
                $this->outSuccess('Удалено свойство "Ссылки на персоны проставлены" из инфоблока ' . $ibId);
            }
        }
    }
}