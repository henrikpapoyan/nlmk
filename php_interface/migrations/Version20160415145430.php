<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160415145430 extends Version {

    protected $description = "изменение XML_ID для NLMK Europe Strip";

    public function up()
    {
		$arIblocks = array(FUNCTIONAL_DIVISION_IBLOCK_ID_EN, FUNCTIONAL_DIVISION_IBLOCK_ID_RU);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = \CIBlockProperty::GetPropertyEnum(
				"DIVISION_CODE",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["VALUE"] == "NLMK Europe Strip") {
					$arEnumProp['XML_ID'] = "strip";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"], "XML_ID" => $arEnumProp["XML_ID"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new \CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}

    }

    public function down() {
		$arIblocks = array(FUNCTIONAL_DIVISION_IBLOCK_ID_EN, FUNCTIONAL_DIVISION_IBLOCK_ID_RU);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = \CIBlockProperty::GetPropertyEnum(
				"DIVISION_CODE",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["VALUE"] == "NLMK Europe Strip") {
					$arEnumProp['XML_ID'] = "flat-rolled2";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"], "XML_ID" => $arEnumProp["XML_ID"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new \CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
    }

}
