<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160413154415 extends Version {

	protected $description = "Изменение пользовательских настроек формы редактирования элементов инфоблоков (для англ. инфоблоков) (Our Business)";

	public function up()
	{
		$helper = new IblockHelper();
		$arIblocks = array(
			PRODUCTION_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTION_SLIDER_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTION_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
					)
				)
			),
			PRODUCTION_TYPE_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTION_TYPE_IBLOCK_ID_EN, "CATALOG_LINK"), "TITLE" => "Link to product catalog"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
					)
				)
			),
			APPLICATION_SCOPE_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
					)
				)
			),
			PRODUCTS_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTS_SLIDER_IBLOCK_ID_EN, "SUBTITLE"), "TITLE" => "*Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTS_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "*Link"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTS_SLIDER_IBLOCK_ID_EN, "LINK_NAME"), "TITLE" => "Link name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
					)
				)
			),
			PRODUCTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTS_IBLOCK_ID_EN, "SHOW_HOME"), "TITLE" => "Show on home page"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTS_IBLOCK_ID_EN, "TYPE"), "TITLE" => "*Type of products"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTS_IBLOCK_ID_EN, "SCOPE"), "TITLE" => "Products and use"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTS_IBLOCK_ID_EN, "PERCENT"), "TITLE" => "Percent"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRODUCTS_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
					)
				)
			),
			INNOVATIONS_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INNOVATIONS_SLIDER_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "*Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INNOVATIONS_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "*Link"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INNOVATIONS_SLIDER_IBLOCK_ID_EN, "LINK_NAME"), "TITLE" => "Link name"),
					)
				)
			),
		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}

	}

	public function down()
	{
	}

}

