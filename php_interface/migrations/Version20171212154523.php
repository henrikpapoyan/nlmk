<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171212154523 extends Version {
    protected $description = "Изменение сортировки элементов в ИБ \"Документы\" EN и RU";
    protected $props = array(
        "RU",
        "EN"
    );

    public function up(){
        $el = new \CIBlockElement;
        foreach ($this->props as $lang) {
            $corp = 0;
            $err = false;
            $arSelect = Array("ID", "CODE");
            $arFilter = Array("IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang));
            
            $res = \CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);
            
            while ($elem = $res->fetch()) {
                if ($elem["CODE"] == "CORPORATE_DOCUMENTS") {
                    $corp = $elem["ID"];
                }
            }
            if($corp != 0){
                $sort = 1;
                $arSortElems = array();
                $arSelect = Array("ID", "SORT", "IBLOCK_SECTION_ID");
                $arFilter = Array("IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang));
                $res = \CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
                while ($elem = $res->fetch()) {
                    if ($elem["IBLOCK_SECTION_ID"] == $corp) {
                        $arSortElems[$elem["ID"]]["NEW_SORT"] = $sort;
                        $arSortElems[$elem["ID"]]["OLD_SORT"] = (int)$elem["SORT"];
                        $sort++;
                    } elseif($elem["SORT"] < 100) {
                        $arSortElems[$elem["ID"]]["NEW_SORT"] = (int)$elem["SORT"] + 400;
                        $arSortElems[$elem["ID"]]["OLD_SORT"] = (int)$elem["SORT"];
                    }
                }

                file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/migrations/migration_Version20171212154523_' . $lang . '.txt', var_export(json_encode($arSortElems),true));
//echo"<pre>";var_dump($arSortElems);echo"</pre>";
                foreach($arSortElems as $id => $element){
                    //echo $id; var_dump($element);
                    if ($el->Update($id, array("SORT" => $element["NEW_SORT"]))) {
                        $this->outSuccess('Элемент id='. $id . ' обновлен');
                    } else {
                        $err = true;
                        $this->outError('Не получилось обновить элемент id='. $id);
                    }
                }
                if ($err === false) {
                    $this->outSuccess('Все элементы обновлены');
                }
            } else {
                $this->outError('Элемент '. $element['NAME'] . ' не удалось перенести');
            }
        }
    }

    public function down(){
        $el = new \CIBlockElement;
        foreach ($this->props as $lang) {
            $err = false;
            $file = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/migrations/migration_Version20171212154523_' . $lang . '.txt');
            if ($file) {
                $elements = json_decode(substr($file,1,-1), true);

                foreach ($elements as $id => $element) {
                    if ($el->Update($id, array("SORT" => $element["OLD_SORT"]))) {
                        $this->outSuccess('Элемент id='. $id . ' обновлен');
                    } else {
                        $err = true;
                        $this->outError('Не получилось обновить элемент id='. $id);
                    }
                }
                if ($err === false) {
                    $this->outSuccess('Все элементы обновлены');
                }
                unlink($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/migrations/migration_Version20171212154523_' . $lang . '.txt');
            } else {
                $this->outError('Файл migration_Version20171212154523_' . $lang . '.txt' . ' не удалось найти');
            }
        }
    }

}
