<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160427123548 extends Version {

	protected $description = "Обновление состояния активности для разделов ИБ Документы (ru и en) из-за бага GLOBAL_ACTIVE";

	public function up(){
		$obSection = new \CIBlockSection;
		$dbSections = \CIBlockSection::GetList(
			array(),
			array("IBLOCK_ID" => array(DOCUMENTS_IBLOCK_ID_EN, DOCUMENTS_IBLOCK_ID_RU)),
			false,
			array(),
			false
		);
		while ($arSection = $dbSections->GetNext())
		{
			if ($arSection["ACTIVE"] == "Y")
			{
				$obSection->Update($arSection["ID"], array("ACTIVE" => "N"));
				$obSection->Update($arSection["ID"], array("ACTIVE" => "Y"));
			}
			else {
				$obSection->Update($arSection["ID"], array("ACTIVE" => "Y"));
				$obSection->Update($arSection["ID"], array("ACTIVE" => "N"));
			}
		}
	}

	public function down(){
		$helper = new IblockHelper();
	}

}
