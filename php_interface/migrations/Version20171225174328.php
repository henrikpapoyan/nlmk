<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171225174328 extends Version {

    protected $description = "проставляет новостям теги (русская версия)";

    protected $iblockId = [
        QUESTIONS_IBLOCK_ID_RU,MC_CONTACTS_IBLOCK_ID_RU,PRESS_CONTACTS_IBLOCK_ID_RU,PRESS_KIT_IBLOCK_ID_RU,
        NEWS_GROUP_IBLOCK_ID_RU,NEWS_COMPANIES_IBLOCK_ID_RU,MULTIMEDIA_IBLOCK_ID_RU,NEWS_SMI_IBLOCK_ID_RU,
        PERFORMANSES_IBLOCK_ID_RU, GLOSSARY_IBLOCK_ID_RU,EVENTS_SLIDER_IBLOCK_ID_RU,EVENTS_IBLOCK_ID_RU,
        AWARDS_IBLOCK_ID_RU
    ];

    protected $personIblock = [DIRECTORS_IBLOCK_ID_RU, MANAGEMENT_IBLOCK_ID_RU, GOVMANAGEMENT_IBLOCK_ID_RU];

    public function up(){
        foreach ($this->personIblock as $id) {
            $arFilter = ["IBLOCK_ID" => $id];
            $arSelectFields = [
                "ID",
                "NAME",
                'PROPERTY_NAME_VARIANTS'
            ];
            $res = \CIBlockElement::GetList([], $arFilter, false, false, $arSelectFields);
            while ($arPerson = $res->GetNext()) {
                $persons[$arPerson['ID']]['NAME'] = $arPerson['NAME'];
                if ($arPerson['PROPERTY_NAME_VARIANTS_VALUE'])
                    $persons[$arPerson['ID']]['NAME_VARIANTS'][] = $arPerson['PROPERTY_NAME_VARIANTS_VALUE'];
            }
        }

        if (!empty($persons)) {
            foreach ($this->iblockId as $id) {
                $arFilter = ["IBLOCK_ID" => $id];
                $arSelectFields = [
                    "ID",
                    "DETAIL_TEXT",
                    "TAGS"
                ];
                $res = \CIBlockElement::GetList([], $arFilter, false, false, $arSelectFields);

                while ($r = $res->Fetch()) {
                    $newTags = [];
                    if (!empty($r['DETAIL_TEXT'])) {
                        foreach ($persons as $person) {
                            $text = $r['DETAIL_TEXT'];
                            $oldText = $r['DETAIL_TEXT'];
                            $pattern = '/(?<!class="person-popup-link">)(#NAME#)(?!<\/a>)/su';
                            $text = $this->replacePersonInText($text, $person, $pattern);

                            if (!empty($person['NAME_VARIANTS'])) {
                                foreach ($person['NAME_VARIANTS'] as $variant) {
                                    $tmpPerson = $person;
                                    $tmpPerson['NAME'] = $variant;
                                    $text = $this->replacePersonInText($text, $person, $pattern);
                                }
                            }

                            if ($oldText !== $text) {
                                $newTags[$person['NAME']] = $person['NAME'];
                            }
                        }

                        if (!empty ($newTags)) {
                            $this->updateElementTags($r['TAGS'], $newTags, $r['ID']);
                        }
                    }
                }
            }
        }
    }

    private function replacePersonInText($text, $person, $pattern)
    {
        $replace = '<a href="" class="person-popup-link">$1</a>';

        $pattern = str_replace('#NAME#', $person['NAME'], $pattern);

        return preg_replace($pattern, $replace, $text);
    }

    private function updateElementTags($oldTags, $newTags, $productId)
    {
        $el = new \CIBlockElement;
        $tags = [];

        if (!empty($oldTags)) {
            foreach (explode(',', $oldTags) as $tag) {
                $tags[$tag] = $tag;
            }
        }

        $tags = implode(',', array_merge($tags, $newTags));
        $el->Update($productId, ['TAGS' => $tags]);
    }

    public function down(){
    }
}
