<?
namespace Sprint\Migration;

use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171212163722 extends Version
{

    protected $description = 'Добавление инфоблока по вопросам оплаты';

    const IB_NAME = 'Вопросы оплаты';
    const IB_CODE = 'PAYMENT_QUESTIONS_RU';

    public function up() {
        $IblockHelper = new IblockHelper();

        $iblockId = $IblockHelper->addIblockIfNotExists(['ACTIVE'          => 'Y',
                                                         'NAME'            => self::IB_NAME,
                                                         'CODE'            => self::IB_CODE,
                                                         'LIST_PAGE_URL'   => '',
                                                         'DETAIL_PAGE_URL' => '',
                                                         'IBLOCK_TYPE_ID'  => 'activities',
                                                         'SITE_ID'         => ['s1'],
                                                         'SORT'            => 500,
                                                         'DESCRIPTION'     => '']);

        if ($iblockId) {
            $this->outSuccess("Добавлен ИБ \"" . self::IB_NAME . "\"");
            $properties = [['IBLOCK_ID'     => $iblockId,
                            'NAME'          => 'Деятельность подразделения',
                            'ACTIVE'        => 'Y',
                            'SORT'          => '100',
                            'CODE'          => 'SUBDIVISION_BUSINESS',
                            'PROPERTY_TYPE' => 'S',
                            'ROW_COUNT'     => '3',
                            'COL_COUNT'     => '45',
                            'LIST_TYPE'   => 'L',
                            'MULTIPLE'    => 'N',
                            'IS_REQUIRED' => 'N',
                            'FILTRABLE'   => 'N',],
                           ['IBLOCK_ID'      => $iblockId,
                            'NAME'           => 'Родительское подразделение',
                            'ACTIVE'         => 'Y',
                            'SORT'           => '200',
                            'CODE'           => 'PARENT_SUBDIVISION',
                            'PROPERTY_TYPE'  => 'E',
                            'LINK_IBLOCK_ID' => $iblockId,
                            'ROW_COUNT'      => '1',
                            'COL_COUNT'      => '30',
                            'LIST_TYPE'      => 'L',
                            'MULTIPLE'       => 'N',
                            'IS_REQUIRED' => 'N',
                            'FILTRABLE'   => 'N',]];
            foreach ($properties as $property) {
                if ($IblockHelper->addPropertyIfNotExists($iblockId, $property)) {
                    $this->outSuccess('Добавлено свойство \'' . $property['NAME'] . '\' в ИБ \'' . self::IB_NAME . '\'');
                }
            }

            $elements = ['000-nlmk-teaching-center'         => ['FIELDS'     => ['NAME'              => 'ООО "НЛМК - УЧЕБНЫЙ ЦЕНТР"',
                                                                                'IBLOCK_SECTION_ID' => false,
                                                                                'CODE'              => '000-nlmk-teaching-center',
                                                                                'ACTIVE'            => 'Y',
                                                                                'PREVIEW_TEXT'      => '',
                                                                                'DETAIL_TEXT'       => ''],
                                                               'PROPERTIES' => ['SUBDIVISION_BUSINESS' => '',
                                                                                'PARENT_SUBDIVISION'   => '']],
                        'otdel-zakupok-ouz'                => ['FIELDS'     => ['NAME'              => 'Отдел учета закупок (ОУЗ)',
                                                                                'IBLOCK_SECTION_ID' => false,
                                                                                'CODE'              => 'otdel-zakupok-ouz',
                                                                                'ACTIVE'            => 'Y',
                                                                                'PREVIEW_TEXT'      => '',
                                                                                'DETAIL_TEXT'       => ''],
                                                               'PROPERTIES' => ['SUBDIVISION_BUSINESS' => '',
                                                                                'PARENT_SUBDIVISION'   => ''],
                                                               'PARENT'     => '000-nlmk-teaching-center'],
                        'gruppa-po-rabote-s-postavchikami' => ['FIELDS'     => ['NAME'              => 'Группа по работе с поставщиками',
                                                                                'IBLOCK_SECTION_ID' => false,
                                                                                'CODE'              => 'gruppa-po-rabote-s-postavchikami',
                                                                                'ACTIVE'            => 'Y',
                                                                                'PREVIEW_TEXT'      => '',
                                                                                'DETAIL_TEXT'       => ''],
                                                               'PROPERTIES' => ['SUBDIVISION_BUSINESS' => 'Подбор и проверка документов НЛМК для отражения в учете, уточнение вопросов и запрос исправленных документов по всем компаниям Группы НЛМК.',
                                                                                'PARENT_SUBDIVISION'   => ''],
                                                               'PARENT'     => 'otdel-zakupok-ouz'],
                        'gruppa-registrazii-nlmk'          => ['FIELDS'     => ['NAME'              => 'Группа регистрации НЛМК',
                                                                                'IBLOCK_SECTION_ID' => false,
                                                                                'CODE'              => 'gruppa-registrazii-nlmk',
                                                                                'ACTIVE'            => 'Y',
                                                                                'PREVIEW_TEXT'      => '',
                                                                                'DETAIL_TEXT'       => ''],
                                                               'PROPERTIES' => ['SUBDIVISION_BUSINESS' => 'Отражение в учете, оплата',
                                                                                'PARENT_SUBDIVISION'   => ''],
                                                               'PARENT'     => 'otdel-zakupok-ouz'],
                        'gruppa-registracii-prochie'       => ['FIELDS'     => ['NAME'              => 'Группа регистрации – прочие компании',
                                                                                'IBLOCK_SECTION_ID' => false,
                                                                                'CODE'              => 'gruppa-registracii-prochie',
                                                                                'ACTIVE'            => 'Y',
                                                                                'PREVIEW_TEXT'      => '',
                                                                                'DETAIL_TEXT'       => ''],
                                                               'PROPERTIES' => ['SUBDIVISION_BUSINESS' => 'Отражение в учете, оплата.',
                                                                                'PARENT_SUBDIVISION'   => ''],
                                                               'PARENT'     => 'otdel-zakupok-ouz'],
                        'otdel-po-rabote-s-dokumentami'    => ['FIELDS'     => ['NAME'              => 'Отделы по работе с документами (ОПД)',
                                                                                'IBLOCK_SECTION_ID' => false,
                                                                                'CODE'              => 'otdel-po-rabote-s-dokumentami',
                                                                                'ACTIVE'            => 'Y',
                                                                                'PREVIEW_TEXT'      => '',
                                                                                'DETAIL_TEXT'       => ''],
                                                               'PROPERTIES' => ['SUBDIVISION_BUSINESS' => 'Территориально удаленные отделы Учетного центра на площадках компаний.',
                                                                                'PARENT_SUBDIVISION'   => ''],
                                                               'PARENT'     => '000-nlmk-teaching-center']];
            foreach ($elements as $code=>&$element) {
                if ($element['PARENT']) {
                    $element['PROPERTIES']['PARENT_SUBDIVISION'] = $elements[$element['PARENT']]['ID'];
                }
                $element['ID'] = $IblockHelper->addElement($iblockId, $element['FIELDS'], $element['PROPERTIES']);
            }
        }
    }

    public function down() {
        $IblockHelper = new IblockHelper();

        if ($IblockHelper->deleteIblockIfExists(self::IB_CODE)) {
            $this->outSuccess('Удален ИБ по вопросам оплаты');
        }
    }

}
