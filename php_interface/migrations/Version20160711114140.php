<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160711114140 extends Version {

    protected $description = "Добавление инфоблоков \"Ключевые экологические показатели\" и \"Key environmental indicators\"";

    public function up()
    {
        $path = $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/migrations/xml";

        $chartsRu = $path . "/key-indicators_ru.xml";
        $resultRU = ImportXMLFile($chartsRu, "sustainability", "s1", "N", "N");
        if($resultRU)
            $this->outSuccess("Инфоблок \"Ключевые экологические показатели\" добавлен");
        else
            $this->outError("Импорт завершен с ошибкой");

        $chartsEn = $path . "/key-indicators_en.xml";
        $resultEn = ImportXMLFile($chartsEn, "sustainability_en", "s2", "N", "N");
        if($resultEn)
            $this->outSuccess("Инфоблок \"Key environmental indicators\" добавлен");
        else
            $this->outError("Импорт завершен с ошибкой");
    }

    public function down()
    {
        $helper = new IblockHelper();
    }

}
