<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160412140000 extends Version {

	protected $description = "Переименование анлийских инфоблоков и их свойств на английский (откат миграции - перевод на русский)";

	public function up()
	{
		$arIblockInfo = array(
			ENV_PROGRAMME_IBLOCK_ID_EN => array(
				"NAME" => "Air Protection",
				"PROPS" => array(
					"STATUS" => "Status",
					"YEAR" => "Year"
				)
			),
			ECOLOGY_CASES_IBLOCK_ID_EN => array(
				"NAME" => "Ecology cases",
				"PROPS" => array()
			),
			ECOLOGY_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - ecology",
				"PROPS" => array(
					"DESCRIPTION" => "Description",
					"LINK" => "Link"
				)
			),
			CHARITY_PROJECTS_IBLOCK_ID_EN => array(
				"NAME" => "Charity projects",
				"PROPS" => array(
					"SUBTITLE" => "Subtitle"
				)
			),
			CHARITY_PROGRAM_IBLOCK_ID_EN => array(
				"NAME" => "Charity programs",
				"PROPS" => array()
			),
			CHARITY_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - Charity projects",
				"PROPS" => array(
					"TITLE2" => "Title2",
					"TITLE3" => "Title3",
					"TITLE4" => "Title4"
				)
			),
			INNOVATIONS_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - Products and innovations",
				"PROPS" => array(
					"DESCRIPTION" => "Description",
					"LINK" => "Link",
					"LINK_NAME" => "Link name"
				)
			),
			PRODUCTS_IBLOCK_ID_EN => array(
				"NAME" => "Products",
				"PROPS" => array(
					"PERCENT" => "Percent",
					"LINK" => "Link",
					"TYPE" => "Type of products",
					"SCOPE" => "Products and use",
					"SHOW_HOME" => "Show on home page"
				)
			),
			PRODUCTS_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - Products and use",
				"PROPS" => array(
					"SUBTITLE" => "Description",
					"LINK" => "Link",
					"LINK_NAME" => "Link name"
				)
			),
			APPLICATION_SCOPE_IBLOCK_ID_EN => array(
				"NAME" => "Products and use",
				"PROPS" => array()
			),
			PRODUCTION_TYPE_IBLOCK_ID_EN => array(
				"NAME" => "Types of products",
				"PROPS" => array(
					"CATALOG_LINK" => "Link to product catalog"
				)
			),
			PRODUCTION_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - Production",
				"PROPS" => array(
					"DESCRIPTION" => "Description",
					"LINK" => "Link"
				)
			),
			QUESTIONS_IBLOCK_ID_EN => array(
				"NAME" => "Questions",
				"PROPS" => array(
					"NAME" => "Person name",
					"EMAIL" => "Email",
					"QUESTION" => "Question"
				)
			),
			MC_CONTACTS_IBLOCK_ID_EN => array(
				"NAME" => "Media Center’s contacts",
				"PROPS" => array(
					"POST" => "Post",
					"PHONE" => "Phone",
					"EMAIL" => "Address"
				)
			),
			MC_FILES_IBLOCK_ID_EN => array(
				"NAME" => '"About NLMK" files',
				"PROPS" => array(
					"FILE" => "File"
				)
			),
			VIDEOS_IBLOCK_ID_EN => array(
				"NAME" => "Video, audio",
				"PROPS" => array(
					"YOUTUBE_URL" => "Youtube video url",
					"FILE" => "File",
					"DESCRIPTION" => "Description",
					"TAGS" => "Tags",
					"COMPANY" => "Company",
					"TYPE" => "Type",
					"VIDEO_LENGTH" => "Playtime"
				)
			),
			PHOTOS_IBLOCK_ID_EN => array(
				"NAME" => "Photo gallery",
				"PROPS" => array(
					"PICTURES" => "Pictures",
					"DESCRIPTION" => "Description",
					"TAGS" => "Tags",
					"COMPANY" => "Company",
					"TYPE" => "TYPE"
				)
			),
			FAQ_IBLOCK_ID_EN => array(
				"NAME" => "Questions and answers",
				"PROPS" => array(
					"QUESTION" => "Question",
					"ANSWER" => "Answer"
				)
			),
			PRESS_CONTACTS_IBLOCK_ID_EN => array(
				"NAME" => "Personal contacts",
				"PROPS" => array(
					"POST" => "Post",
					"PHONE" => "Phone",
					"EMAIL" => "Email",
					"ADDRESS" => "Address",
				)
			),
			PRESS_KIT_IBLOCK_ID_EN => array(
				"NAME" => "Press-kit",
				"PROPS" => array(
					"FILES" => "Files"
				)
			),
			NEWS_GROUP_IBLOCK_ID_RU => array(
				"NAME" => "Пресс-релизы",
				"PROPS" => array(
					"SUBTITLE" => "Подзаголовок",
					"COMPANY" => "Предприятие",
					"FILES" => "Файлы",
					"FINANCIAL_RELEASES" => "Раскрытие финансовой отчетности",
					"TRADING_UPDATES" => "Раскрытие операционной информации"
				)
			),
			NEWS_GROUP_IBLOCK_ID_EN => array(
				"NAME" => "Press-releases",
				"PROPS" => array(
					"SUBTITLE" => "Subtitle",
					"COMPANY" => "Company",
					"FILES" => "Files",
					"FINANCIAL_RELEASES" => "Financial releases",
					"TRADING_UPDATES" => "Trading updates"
				)
			),
			NEWS_COMPANIES_IBLOCK_ID_EN => array(
				"NAME" => "News of companies",
				"PROPS" => array(
					"SUBTITLE" => "Subtitle",
					"COMPANY" => "Company",
					"FILES" => "Files"
				)
			),
			MULTIMEDIA_IBLOCK_ID_EN => array(
				"NAME" => "Multimedia",
				"PROPS" => array(
					"MULTIMEDIA" => "Multimedia",
					"TYPE" => "Type"
				)
			),
			NEWS_SMI_IBLOCK_ID_EN => array(
				"NAME" => "Mention in the media",
				"PROPS" => array(
					"SUBTITLE" => "Subtitle",
					"COMPANY" => "Company",
					"SOURCE_LINK" => "Source link",
					"SOURCE" => "Source name"
				)
			),
			PERFORMANSES_IBLOCK_ID_EN => array(
				"NAME" => "Interviews",
				"PROPS" => array(
					"SUBTITLE" => "Subtitle",
					"COMPANY" => "Company",
					"PERSON_NAME" => "Person name"
				)
			),
			GLOSSARY_IBLOCK_ID_EN => array(
				"NAME" => "Glossary",
				"PROPS" => array(
					"DESCRIPTION" => "Description"
				)
			),
			EVENTS_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - Events",
				"PROPS" => array(
					"DATE" => "Date",
					"LINK" => "Link"
				)
			),
			EVENTS_IBLOCK_ID_EN => array(
				"NAME" => "Events",
				"PROPS" => array(
					"SUBTITLE" => "Subtitle",
					"COMPANY" => "Company",
					"DATE" => "Date",
					"ADDRESS_STREET" => "Address",
					"ADDRESS_COUNTRY" => "Country",
					"ADDRESS_CITY" => "City"
				)
			),
			AWARDS_IBLOCK_ID_EN => array(
				"NAME" => "Awards",
				"PROPS" => array(
					"YEAR" => "Year"
				)
			),
			MC_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - mediacenter",
				"PROPS" => array(
					"LINK" => "Link"
				)
			),
			FINANCIAL_CALENDAR_IBLOCK_ID_EN => array(
				"NAME" => "Financial calendar",
				"PROPS" => array(
					"DATE" => "Event start date",
					"DATE_END" => "Event end date"
				)
			),
			AGM_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - AGM",
				"PROPS" => array(
					"LINK" => "Link"
				)
			),
			SHAREHOLDER_CENTRE_DOCS_IBLOCK_ID_EN => array(
				"NAME" => "Documents to shareholders",
				"PROPS" => array(
					"FILE" => "File",
					"PERIOD" => "Period"
				)
			),
			INVESTOR_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - Investors",
				"PROPS" => array(
					"DESCRIPTION" => "Description",
					"LINK" => "Link"
				)
			),
			PRESENTATIONS_IBLOCK_ID_EN => array(
				"NAME" => "Presentations for investors",
				"PROPS" => array(
					"FILE" => "Presentation",
					"FILE_TYPE" => "File type"
				)
			),
			ANNUAL_REPORTS_IBLOCK_ID_EN => array(
				"NAME" => "Annual reports",
				"PROPS" => array(
					"REPORTS" => "Reports",
					"FULL_REPORT" => "Annual repor (full version)",
					"COMMENT" => "Comment",
					"PERSON_NAME" => "Person name",
					"POST" => "Post",
					"PHOTO" => "Photo"
				)
			),
			REPORTS_IBLOCK_ID_EN => array(
				"NAME" => "Reports",
				"PROPS" => array(
					"FILE" => "File",
					"PICTURE" => "Picture"
				)
			),
			INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN => array(
				"NAME" => "Contacts (sidebar)",
				"PROPS" => array(
					"POST" => "Post",
					"PHONE" => "Phone",
					"EMAIL" => "Email",
					"PAGE_URL" => "Page url"
				)
			),
			INVESTOR_CONTACTS_IBLOCK_ID_EN => array(
				"NAME" => "IR contacts",
				"PROPS" => array(
					"POST" => "Post",
					"PHONE" => "Phone, Fax",
					"EMAIL" => "Email",
					"ADDRESS" => "Address"
				)
			),
			CONTACTS_IBLOCK_ID_EN => array(
				"NAME" => "Contacts",
				"PROPS" => array(
					"PERSON_NAME" => "Person name",
					"POST" => "Post",
					"PHONE" => "Phone",
					"EMAIL" => "Email",
					"ADDRESS" => "Address",
					"FAX" => "Fax"
				)
			),
			CAREER_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - Career",
				"PROPS" => array(
					"LINK" => "Link"
				)
			),
			OUR_PROFESSIONS_IBLOCK_ID_EN => array(
				"NAME" => "Our professions",
				"PROPS" => array(
					"POST" => "Post",
					"COMPANY" => "Company"
				)
			),
			COMPETITIONS_IBLOCK_ID_EN => array(
				"NAME" => "Competitions",
				"PROPS" => array()
			),
			CAREER_CONTACTS_SIDEBAR_IBLOCK_ID_EN => array(
				"NAME" => "Contacts (sidebar)",
				"PROPS" => array(
					"POST" => "Post",
					"PHONE" => "Phone",
					"EMAIL" => "Email"
				)
			),
			STRATEGY_IBLOCK_ID_EN => array(
				"NAME" => "Strategy",
				"PROPS" => array(
					"YEAR" => "Year",
					"YEAR_RESULT" => "Year result"
				)
			),
			PURCHASES_AND_SALES_IBLOCK_ID_EN => array(
				"NAME" => "Purchases and sales",
				"PROPS" => array(
					"DATE" => "Date",
					"SHOW_MONTH" => "Show only month"
				)
			),
			MISSION_STORIES_IBLOCK_ID_EN => array(
				"NAME" => "Stories of employees",
				"PROPS" => array(
					"POST" => "Post",
					"COMPANY" => "Company"
				)
			),
			MISSION_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - Mission",
				"PROPS" => array(
					"LINK" => "Link",
					"PERSON_NAME" => "Person name",
					"POST" => "Post"
				)
			),
			FUNCTIONAL_DIVISION_IBLOCK_ID_EN => array(
				"NAME" => "Functional Divisions",
				"PROPS" => array(
					"ACTIVITIES" => "Activities",
					"PRODUCTION" => "Products",
					"PRODUCTION_VOLUMES" => "Production volumes",
					"SALES" => "Sales",
					"COMPANIES" => "Companies",
					"COORDINATES" => "Coordinates",
					"DIVISION_CODE" => "Division type"
				)
			),
			GEOGRAPHY_DIVISION_IBLOCK_ID_EN => array(
				"NAME" => "Geography Divisions",
				"PROPS" => array(
					"DESCRIPTION" => "Description",
					"FUNCTIONAL_DIVISIONS" => "Functional divisions",
					"DESCRIPTION2" => "Description2"
				)
			),
			INFOGRAPHIC_IBLOCK_ID_EN => array(
				"NAME" => "Infographic numbers",
				"PROPS" => array(
					"PICTURE" => "Picture",
					"TEXT_TOP" => "Text top",
					"TEXT_BOTTOM" => "Text bottom",
					"DATA" => "Data",
				)
			),
			KEY_FACTORS_IBLOCK_ID_EN => array(
				"NAME" => "Key factors",
				"PROPS" => array()
			),
			KEY_FACTORS_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - Key factors",
				"PROPS" => array(
					"FACT_VALUE" => "Value",
					"FACT_UNIT" => "Unit of measurement"
				)
			),
			FIVE_YEAR_RESULTS_IBLOCK_ID_EN => array(
				"NAME" => "5-year highlights",
				"PROPS" => array(
					"COLUMNS" => "Columns",
					"ROWS" => "Rows",
					"DATA" => "Values",
					"DESCRIPTION" => "Units (in the corner of the table)",
					"PDF" => "Table in pdf"
				)
			),
			DOCUMENTS_BOTTOM_IBLOCK_ID_EN => array(
				"NAME" => "Links",
				"PROPS" => array("FILES" => "Files")
			),
			HOME_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Slider - Home page",
				"PROPS" => array(
					"TEXT" => "Text",
					"LINK" => "Link"
				)
			),
			HISTORY_IBLOCK_ID_EN => array(
				"NAME" => "History",
				"PROPS" => array(
					"DATE" => "Date",
					"SHOW_MONTH" => "Show only month"
				)
			),
			COMPANIES_IBLOCK_ID_EN => array(
				"NAME" => "Companies",
				"PROPS" => array(
					"LINK" => "Link",
					"PICTURE" => "Picture",
					"COUNTRY" => "Country",
					"CITY" => "City",
					"REGION" => "Region",
					"GEOGRAPHIC_DIVISION" => "Geographic division",
					"FUNCTIONAL_DIVISION" => "Functional division",
					"DESCRIPTION" => "Description",
					"INFOGRAPHIC" => "Infographic",
					"PRODUCTION" => "Production",
					"FILES" => "Files",
					"COORDINATES" => "Coordinates",
					"COMPANY_ON_MAP" => "Company on map",
					"HIDE_LINK" => "Hide link",
					"TITLE_COLOR" => "Title color on detail page"
				)
			),
			DOCUMENTS_IBLOCK_ID_EN => array(
				"NAME" => "Documents",
				"PROPS" => array(
					"FILE" => "File",
					"FILE_TYPE" => "File type",
					"PAGE_URL" => "Page"
				)
			),
			MANAGEMENT_IBLOCK_ID_EN => array(
				"NAME" => "Members of the Board",
				array(
					"POST" => "Post",
					"DESCRIPTION" => "Description",
					"BIOGRAPHY_PDF" => "Biography in pdf format",
					"BIOGRAPHY_DOCKX" => "Biography in dockx format",
					"MULTIMEDIA" => "Multimedia",
					"FILTER" => "Filter",
					"ROLE" => "Chairman of the Board"
				)
			),
			DIRECTORS_IBLOCK_ID_EN => array(
				"NAME" => "Board of Directors",
				"PROPS" => array(
					"POST" => "Post",
					"DESCRIPTION" => "Description",
					"BIOGRAPHY_PDF" => "Biography in pdf format",
					"BIOGRAPHY_DOCKX" => "Biography in dockx format",
					"MULTIMEDIA" => "Multimedia",
					"FILTER" => "Filter",
					"ROLE" => "Role"
				)
			),
		);

		$obProp = new \CIBlockProperty;
		$obIblock = new \CIBlock;
		foreach (array_keys($arIblockInfo) as $iblockId)
		{
			$obIblock->Update($iblockId, array("NAME" => $arIblockInfo[$iblockId]["NAME"]));

			$dbProps = \CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockId));
			while ($arProp = $dbProps->GetNext())
			{
				$obProp->Update($arProp["ID"], array("NAME" => $arIblockInfo[$iblockId]["PROPS"][$arProp["CODE"]]));
			}
		}
	}

	public function down()
	{
		$arIblockInfo = array(
			ECOLOGY_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - экология",
				"PROPS" => array(
					"DESCRIPTION" => "Описание",
					"LINK" => "Ссылка"
				)
			),
			ECOLOGY_CASES_IBLOCK_ID_EN => array(
				"NAME" => "Экологические кейсы",
				"PROPS" => array()
			),
			ENV_PROGRAMME_IBLOCK_ID_EN => array(
				"NAME" => "Охрана атмосферного воздуха",
				"PROPS" => array(
					"STATUS" => "Статус",
					"YEAR" => "Год"
				)
			),
			CHARITY_PROJECTS_IBLOCK_ID_EN => array(
				"NAME" => "Благотворительные проекты",
				"PROPS" => array(
					"SUBTITLE" => "Подзаголовок"
				)
			),
			CHARITY_PROGRAM_IBLOCK_ID_EN => array(
				"NAME" => "Благотворительные программы",
				"PROPS" => array()
			),
			CHARITY_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - Благотворительные проекты",
				"PROPS" => array(
					"TITLE2" => "Заголовок2",
					"TITLE3" => "Заголовок3",
					"TITLE4" => "Заголовок4"
				)
			),
			INNOVATIONS_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - Продукция и инновации",
				"PROPS" => array(
					"DESCRIPTION" => "Описание",
					"LINK" => "Ссылка",
					"LINK_NAME" => "Название ссылки"
				)
			),
			PRODUCTS_IBLOCK_ID_EN => array(
				"NAME" => "Продукция",
				"PROPS" => array(
					"PERCENT" => "Процент",
					"LINK" => "Ссылка",
					"TYPE" => "Вид продукции",
					"SCOPE" => "Сфера применения продукции",
					"SHOW_HOME" => "Выводить на главной странице"
				)
			),
			PRODUCTS_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - сферы применения продукции",
				"PROPS" => array(
					"SUBTITLE" => "Описание",
					"LINK" => "Ссылка",
					"LINK_NAME" => "Название ссылки"
				)
			),
			APPLICATION_SCOPE_IBLOCK_ID_EN => array(
				"NAME" => "Сферы применения продукции",
				"PROPS" => array()
			),
			PRODUCTION_TYPE_IBLOCK_ID_EN => array(
				"NAME" => "Виды продукции",
				"PROPS" => array(
					"CATALOG_LINK" => "Ссылка на каталог продукции"
				)
			),
			PRODUCTION_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - Производство",
				"PROPS" => array(
					"DESCRIPTION" => "Описание",
					"LINK" => "Ссылка"
				)
			),
			QUESTIONS_IBLOCK_ID_EN => array(
				"NAME" => "Вопросы",
				"PROPS" => array(
					"NAME" => "Имя и фамилия",
					"EMAIL" => "Адрес электронной  почты",
					"QUESTION" => "Вопрос"
				)
			),
			MC_CONTACTS_IBLOCK_ID_EN => array(
				"NAME" => "Контакты медиацентра",
				"PROPS" => array(
					"POST" => "Должность",
					"PHONE" => "Контактные телефоны",
					"EMAIL" => "Адрес электронной почты"
				)
			),
			MC_FILES_IBLOCK_ID_EN => array(
				"NAME" => 'Файлы "О НЛМК"',
				"PROPS" => array(
					"FILE" => "Файл"
				)
			),
			VIDEOS_IBLOCK_ID_EN => array(
				"NAME" => "Видео, аудио",
				"PROPS" => array(
					"YOUTUBE_URL" => "Ссылка на видео с Youtube",
					"FILE" => "Файл",
					"DESCRIPTION" => "Описание",
					"TAGS" => "Список тегов",
					"COMPANY" => "Предприятие",
					"TYPE" => "Тип",
					"VIDEO_LENGTH" => "Продолжительность видео"
				)
			),
			PHOTOS_IBLOCK_ID_EN => array(
				"NAME" => "Фотогалерея",
				"PROPS" => array(
					"PICTURES" => "Изображения",
					"DESCRIPTION" => "Описание",
					"TAGS" => "Список тегов",
					"COMPANY" => "Предприятие",
					"TYPE" => "Тип"
				)
			),
			FAQ_IBLOCK_ID_EN => array(
				"NAME" => "Вопросы и ответы",
				"PROPS" => array(
					"QUESTION" => "Вопрос",
					"ANSWER" => "Ответ"
				)
			),
			PRESS_CONTACTS_IBLOCK_ID_EN => array(
				"NAME" => "Контакты персон",
				"PROPS" => array(
					"POST" => "Должность",
					"PHONE" => "Телефон, факс",
					"EMAIL" => "Адрес электронной  почты",
					"ADDRESS" => "Адрес",
				)
			),
			PRESS_KIT_IBLOCK_ID_EN => array(
				"NAME" => "Пресс-кит",
				"PROPS" => array(
					"FILES" => "Файлы"
				)
			),
			NEWS_GROUP_IBLOCK_ID_RU => array(
				"NAME" => "Новости группы компаний",
				"PROPS" => array(
					"SUBTITLE" => "Подзаголовок",
					"COMPANY" => "Предприятие",
					"FILES" => "Файлы",
					"FINANCIAL_RELEASES" => "Раскрытие финансовой отчетности",
					"TRADING_UPDATES" => "Раскрытие операционной информации"
				)
			),
			NEWS_GROUP_IBLOCK_ID_EN => array(
				"NAME" => "Пресс-релизы",
				"PROPS" => array(
					"SUBTITLE" => "Подзаголовок",
					"COMPANY" => "Предприятие",
					"FILES" => "Файлы",
					"FINANCIAL_RELEASES" => "Раскрытие финансовой отчетности",
					"TRADING_UPDATES" => "Раскрытие операционной информации"
				)
			),
			NEWS_COMPANIES_IBLOCK_ID_EN => array(
				"NAME" => "Новости предприятий",
				"PROPS" => array(
					"SUBTITLE" => "Подзаголовок",
					"COMPANY" => "Предприятие",
					"FILES" => "Файлы"
				)
			),
			MULTIMEDIA_IBLOCK_ID_EN => array(
				"NAME" => "Мультимедиа",
				"PROPS" => array(
					"MULTIMEDIA" => "Мультимедиа",
					"TYPE" => "Тип"
				)
			),
			NEWS_SMI_IBLOCK_ID_EN => array(
				"NAME" => "Упоминания в СМИ",
				"PROPS" => array(
					"SUBTITLE" => "Подзаголовок",
					"COMPANY" => "Предприятие",
					"SOURCE_LINK" => "Ссылка на первоисточник",
					"SOURCE" => "Источник"
				)
			),
			PERFORMANSES_IBLOCK_ID_EN => array(
				"NAME" => "Интервью и выступления",
				"PROPS" => array(
					"SUBTITLE" => "Подзаголовок",
					"COMPANY" => "Предприятие",
					"PERSON_NAME" => "Персона"
				)
			),
			GLOSSARY_IBLOCK_ID_EN => array(
				"NAME" => "Список терминов",
				"PROPS" => array(
					"DESCRIPTION" => "Описание"
				)
			),
			EVENTS_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - мероприятия",
				"PROPS" => array(
					"LINK" => "Ссылка",
					"DATE" => "Дата"
				)
			),
			EVENTS_IBLOCK_ID_EN => array(
				"NAME" => "Мероприятия",
				"PROPS" => array(
					"SUBTITLE" => "Подзаголовок",
					"COMPANY" => "Предприятие",
					"DATE" => "Дата проведения",
					"ADDRESS_STREET" => "Адресс (Улица, дом и т.д.)",
					"ADDRESS_COUNTRY" => "Страна",
					"ADDRESS_CITY" => "Город"
				)
			),
			AWARDS_IBLOCK_ID_EN => array(
				"NAME" => "Достижения",
				"PROPS" => array(
					"YEAR" => "Год"
				)
			),
			MC_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - медиацентр",
				"PROPS" => array(
					"LINK" => "Ссылка"
				)
			),
			FINANCIAL_CALENDAR_IBLOCK_ID_EN => array(
				"NAME" => "Финансовый календарь",
				"PROPS" => array(
					"DATE" => "Дата события",
					"DATE_END" => "Дата окончания события"
				)
			),
			AGM_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - Акционерам",
				"PROPS" => array(
					"LINK" => "Ссылка"
				)
			),
			SHAREHOLDER_CENTRE_DOCS_IBLOCK_ID_EN => array(
				"NAME" => "Документы акционерам",
				"PROPS" => array(
					"FILE" => "Файл",
					"PERIOD" => "Период"
				)
			),
			INVESTOR_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - Инвесторам",
				"PROPS" => array(
					"DESCRIPTION" => "Описание",
					"LINK" => "Ссылка"
				)
			),
			PRESENTATIONS_IBLOCK_ID_EN => array(
				"NAME" => "Презентации инвесторам",
				"PROPS" => array(
					"FILE" => "Презентация",
					"FILE_TYPE" => "Тип файла"
				)
			),
			ANNUAL_REPORTS_IBLOCK_ID_EN => array(
				"NAME" => "Годовые отчеты",
				"PROPS" => array(
					"REPORTS" => "Отчеты",
					"FULL_REPORT" => "Годовой отчет (полная версия)",
					"COMMENT" => "Комментарий",
					"PERSON_NAME" => "Имя, Фамилия",
					"POST" => "Должность",
					"PHOTO" => "Фото"
				)
			),
			REPORTS_IBLOCK_ID_EN => array(
				"NAME" => "Отчеты",
				"PROPS" => array(
					"FILE" => "Файл",
					"PICTURE" => "Изображение"
				)
			),
			INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN => array(
				"NAME" => "Контакты для инвесторов (сайдбар)",
				"PROPS" => array(
					"POST" => "Должность",
					"PHONE" => "Контактные телефоны",
					"EMAIL" => "Адрес электронной почты",
					"PAGE_URL" => "Страница"
				)
			),
			INVESTOR_CONTACTS_IBLOCK_ID_EN => array(
				"NAME" => "Контакты для инвесторов",
				"PROPS" => array(
					"POST" => "Должность",
					"PHONE" => "Телефон, факс",
					"EMAIL" => "Адрес элестронной почты",
					"ADDRESS" => "Адрес"
				)
			),


			CONTACTS_IBLOCK_ID_EN => array(
				"NAME" => "Контакты",
				"PROPS" => array(
					"PERSON_NAME" => "Имя, фамилия  контактного лица",
					"POST" => "Должность  контактного лица",
					"PHONE" => "Телефон",
					"EMAIL" => "Адрес электронной  почты",
					"ADDRESS" => "Адрес",
					"FAX" => "Факс"
				)
			),
			CAREER_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - Карьера",
				"PROPS" => array(
					"LINK" => "Ссылка"
				)
			),
			OUR_PROFESSIONS_IBLOCK_ID_EN => array(
				"NAME" => "Наши профессии",
				"PROPS" => array(
					"POST" => "Должность",
					"COMPANY" => "Предприятие"
				)
			),
			COMPETITIONS_IBLOCK_ID_EN => array(
				"NAME" => "Конкурсы",
				"PROPS" => array()
			),
			CAREER_CONTACTS_SIDEBAR_IBLOCK_ID_EN => array(
				"NAME" => "Контакты (сайдбар)",
				"PROPS" => array(
					"POST" => "Должность",
					"PHONE" => "Контактные телефоны",
					"EMAIL" => "Адрес электронной почты"
				)
			),
			STRATEGY_IBLOCK_ID_EN => array(
				"NAME" => "Стратегия в действии",
				"PROPS" => array(
					"YEAR" => "Год",
					"YEAR_RESULT" => "Итоги года"
				)
			),
			PURCHASES_AND_SALES_IBLOCK_ID_EN => array(
				"NAME" => "Приобретения и продажи",
				"PROPS" => array(
					"DATE" => "Дата",
					"SHOW_MONTH" => "Выводить только месяц"
				)
			),
			MISSION_STORIES_IBLOCK_ID_EN => array(
				"NAME" => "Истории работников",
				"PROPS" => array(
					"POST" => "Должность",
					"COMPANY" => "Предриятие"
				)
			),
			MISSION_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - Миссия и видение",
				"PROPS" => array(
					"LINK" => "Ссылка",
					"PERSON_NAME" => "Имя, Фамилия персоны",
					"POST" => "Должность"
				)
			),
			FUNCTIONAL_DIVISION_IBLOCK_ID_EN => array(
				"NAME" => "Функциональный/производственный дивизион",
				"PROPS" => array(
					"ACTIVITIES" => "Деятельность",
					"PRODUCTION" => "Продукция",
					"PRODUCTION_VOLUMES" => "Объемы производства",
					"SALES" => "Продажи",
					"COMPANIES" => "Предприятия",
					"COORDINATES" => "Координаты на карте",
					"DIVISION_CODE" => "Тип дивизиона"
				)
			),
			GEOGRAPHY_DIVISION_IBLOCK_ID_EN => array(
				"NAME" => "Географический дивизион",
				"PROPS" => array(
					"DESCRIPTION" => "Описание",
					"FUNCTIONAL_DIVISIONS" => "Функциональные дивизионы",
					"DESCRIPTION2" => "Описание2"
				)
			),
			INFOGRAPHIC_IBLOCK_ID_EN => array(
				"NAME" => "Инфографика числа",
				"PROPS" => array(
					"PICTURE" => "Изображение",
					"TEXT_TOP" => "Подпись сверху",
					"TEXT_BOTTOM" => "Подпись снизу",
					"DATA" => "Данные",
				)
			),
			KEY_FACTORS_IBLOCK_ID_EN => array(
				"NAME" => "Ключевые факты",
				"PROPS" => array()
			),
			KEY_FACTORS_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - ключевые факты",
				"PROPS" => array(
					"FACT_VALUE" => "Значение",
					"FACT_UNIT" => "Еденица измерения"
				)
			),
			FIVE_YEAR_RESULTS_IBLOCK_ID_EN => array(
				"NAME" => "Результаты за 5 лет",
				"PROPS" => array(
					"COLUMNS" => "Колонки",
					"ROWS" => "Строки",
					"DATA" => "Значения",
					"DESCRIPTION" => "Ед. измерения (в углу таблицы)",
					"PDF" => "Таблица в pdf"
				)
			),
			DOCUMENTS_BOTTOM_IBLOCK_ID_EN => array(
				"NAME" => "Ссылки",
				"PROPS" => array("FILES" => "Файлы")
			),
			HOME_SLIDER_IBLOCK_ID_EN => array(
				"NAME" => "Слайдер - главная страница",
				"PROPS" => array(
					"TEXT" => "Текст",
					"LINK" => "Ссылка"
				)
			),
			HISTORY_IBLOCK_ID_EN => array(
				"NAME" => "История",
				"PROPS" => array(
					"DATE" => "Дата",
					"SHOW_MONTH" => "Выводить только месяц"
				)
			),
			COMPANIES_IBLOCK_ID_EN => array(
				"NAME" => "Предприятия",
				"PROPS" => array(
					"LINK" => "Ссылка",
					"PICTURE" => "Изображение",
					"COUNTRY" => "Страна расположения",
					"CITY" => "Город расположения",
					"REGION" => "Регион",
					"GEOGRAPHIC_DIVISION" => "Дивизион",
					"FUNCTIONAL_DIVISION" => "Функциональный  дивизион",
					"DESCRIPTION" => "Описание",
					"INFOGRAPHIC" => "Инфографика",
					"PRODUCTION" => "Продукция",
					"FILES" => "Файлы",
					"COORDINATES" => "Координаты на карте",
					"COMPANY_ON_MAP" => "Предприятие на карте",
					"HIDE_LINK" => "Не отображать ссылку предприятия",
					"TITLE_COLOR" => "Цвет заголовка на детальной странице"
				)
			),
			DOCUMENTS_IBLOCK_ID_EN => array(
				"NAME" => "Документы",
				"PROPS" => array(
					"FILE" => "Файл",
					"FILE_TYPE" => "Тип файла",
					"PAGE_URL" => "Страница"
				)
			),
			MANAGEMENT_IBLOCK_ID_EN => array(
				"NAME" => "Члены правления",
				"PROPS" => array(
					"POST" => "Должность",
					"DESCRIPTION" => "Описание",
					"BIOGRAPHY_PDF" => "Биография в формате pdf",
					"BIOGRAPHY_DOCKX" => "Биография в формате docx",
					"MULTIMEDIA" => "Мультимедиа",
					"FILTER" => "Фильтр",
					"ROLE" => "Председатель Правления"
				)
			),
			DIRECTORS_IBLOCK_ID_EN => array(
				"NAME" => "Совет директоров",
				"PROPS" => array(
					"POST" => "Должность",
					"DESCRIPTION" => "Описание",
					"BIOGRAPHY_PDF" => "Биография в формате pdf",
					"BIOGRAPHY_DOCKX" => "Биография в формате docx",
					"MULTIMEDIA" => "Мультимедиа",
					"FILTER" => "Фильтр",
					"ROLE" => "Роль"
				)
			),
		);

		$obProp = new \CIBlockProperty;
		$obIblock = new \CIBlock;
		foreach (array_keys($arIblockInfo) as $iblockId)
		{
			$obIblock->Update($iblockId, array("NAME" => $arIblockInfo[$iblockId]["NAME"]));

			$dbProps = \CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockId));
			while ($arProp = $dbProps->GetNext())
			{
				$obProp->Update($arProp["ID"], array("NAME" => $arIblockInfo[$iblockId]["PROPS"][$arProp["CODE"]]));
			}
		}
	}

}
