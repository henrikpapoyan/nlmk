<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160415195737 extends Version {

	protected $description = "привязка свойств TYPE и SCOPE ИБ Products (изменять значения свойств не нужно)";

	public function up()
	{

		$arIblocks = array(PRODUCTS_IBLOCK_ID_EN);
		foreach ($arIblocks as $iblock)
		{
			$CIBlockProperty = new \CIBlockProperty;
			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblock, "CODE" => "TYPE")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty->Update($arProp["ID"], array("LINK_IBLOCK_ID" => PRODUCTION_TYPE_IBLOCK_ID_EN));
			}

			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblock, "CODE" => "SCOPE")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty->Update($arProp["ID"], array("LINK_IBLOCK_ID" => APPLICATION_SCOPE_IBLOCK_ID_EN));
			}
		}

	}

	public function down()
	{
		$arIblocks = array(PRODUCTS_IBLOCK_ID_EN);
		foreach ($arIblocks as $iblock)
		{
			$CIBlockProperty = new \CIBlockProperty;
			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblock, "CODE" => "TYPE")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty->Update($arProp["ID"], array("LINK_IBLOCK_ID" => PRODUCTION_TYPE_IBLOCK_ID_RU));
			}

			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblock, "CODE" => "SCOPE")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty->Update($arProp["ID"], array("LINK_IBLOCK_ID" => APPLICATION_SCOPE_IBLOCK_ID_RU));
			}
		}
	}

}
