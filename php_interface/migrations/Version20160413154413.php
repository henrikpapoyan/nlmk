<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160413154413 extends Version {

	protected $description = "Изменение пользовательских настроек формы редактирования элементов инфоблоков (для англ. инфоблоков About NLMK)";

	public function up()
	{
		$helper = new IblockHelper();
		$arIblocks = array(
			STRATEGY_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(STRATEGY_IBLOCK_ID_EN, "YEAR"), "TITLE" => "Year"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(STRATEGY_IBLOCK_ID_EN, "YEAR_RESULT"), "TITLE" => "Year result"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Preview text"),
					)
				),
			),
			MISSION_STORIES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MISSION_STORIES_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MISSION_STORIES_IBLOCK_ID_EN, "COMPANY"), "TITLE" => "Company"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Preview text"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail text"),
					)
				),
			),
			MISSION_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Title"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MISSION_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MISSION_SLIDER_IBLOCK_ID_EN, "PERSON_NAME"), "TITLE" => "Person name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MISSION_SLIDER_IBLOCK_ID_EN, "POST"), "TITLE" => "Post"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
					)
				),
			),
			KEY_FACTORS_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(KEY_FACTORS_SLIDER_IBLOCK_ID_EN, "FACT_VALUE"), "TITLE" => "Value"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(KEY_FACTORS_SLIDER_IBLOCK_ID_EN, "FACT_UNIT"), "TITLE" => "Unit of measurement"),
					)
				),
			),
			HOME_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(HOME_SLIDER_IBLOCK_ID_EN, "TEXT"), "TITLE" => "Text"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(HOME_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
					)
				),
			),
			PURCHASES_AND_SALES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PURCHASES_AND_SALES_IBLOCK_ID_EN, "DATE"), "TITLE" => "*Date"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PURCHASES_AND_SALES_IBLOCK_ID_EN, "SHOW_MONTH"), "TITLE" => "Show only month"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
					)
				),
			),
			MANAGEMENT_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "ROLE"), "TITLE" => "Chairman of the Board"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "FILTER"), "TITLE" => "Filter"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "BIOGRAPHY_PDF"), "TITLE" => "Biography in pdf format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "BIOGRAPHY_DOCKX"), "TITLE" => "Biography in dockx format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "MULTIMEDIA"), "TITLE" => "Multimedia"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					)
				),
			),
			DOCUMENTS_BOTTOM_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_BOTTOM_IBLOCK_ID_EN, "FILES"), "TITLE" => "Files"),
					)
				),
			),
			KEY_FACTORS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
					)
				),
			),
			INFOGRAPHIC_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INFOGRAPHIC_IBLOCK_ID_EN, "DATA"), "TITLE" => "Data"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INFOGRAPHIC_IBLOCK_ID_EN, "TEXT_TOP"), "TITLE" => "Text top"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INFOGRAPHIC_IBLOCK_ID_EN, "PICTURE"), "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INFOGRAPHIC_IBLOCK_ID_EN, "TEXT_BOTTOM"), "TITLE" => "Text bottom"),
					)
				),
			),
			HISTORY_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(HISTORY_IBLOCK_ID_EN, "DATE"), "TITLE" => "*Date"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(HISTORY_IBLOCK_ID_EN, "SHOW_MONTH"), "TITLE" => "Show only month"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
					)
				),
			),
			GEOGRAPHY_DIVISION_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(GEOGRAPHY_DIVISION_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "*Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(GEOGRAPHY_DIVISION_IBLOCK_ID_EN, "FUNCTIONAL_DIVISIONS"), "TITLE" => "*Functional divisions"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(GEOGRAPHY_DIVISION_IBLOCK_ID_EN, "DESCRIPTION2"), "TITLE" => "Description2"),
					)
				),
			),
			FUNCTIONAL_DIVISION_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FUNCTIONAL_DIVISION_IBLOCK_ID_EN, "DIVISION_CODE"), "TITLE" => "*Division type"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FUNCTIONAL_DIVISION_IBLOCK_ID_EN, "PRODUCTION"), "TITLE" => "*Products"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FUNCTIONAL_DIVISION_IBLOCK_ID_EN, "COMPANIES"), "TITLE" => "*Companies"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FUNCTIONAL_DIVISION_IBLOCK_ID_EN, "COORDINATES"), "TITLE" => "*Coordinates"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FUNCTIONAL_DIVISION_IBLOCK_ID_EN, "ACTIVITIES"), "TITLE" => "*Activities"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FUNCTIONAL_DIVISION_IBLOCK_ID_EN, "PRODUCTION_VOLUMES"), "TITLE" => "*Production volumes"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FUNCTIONAL_DIVISION_IBLOCK_ID_EN, "SALES"), "TITLE" => "*Sales"),
					)
				),
			),
			DOCUMENTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_EN, "FILE"), "TITLE" => "File"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_EN, "FILE_TYPE"), "TITLE" => "File type"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_EN, "PAGE_URL"), "TITLE" => "Page url"),
						array("NAME" => "SECTIONS", "TITLE" => "Section"),
					)
				),
			),
			COMPANIES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "TITLE_COLOR"), "TITLE" => "Color header of detailed page"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "HIDE_LINK"), "TITLE" => "Not show URL"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "CITY"), "TITLE" => "City"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "COUNTRY"), "TITLE" => "Country"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "REGION"), "TITLE" => "Region"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "INFOGRAPHIC"), "TITLE" => "Infographic"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "PRODUCTION"), "TITLE" => "Production"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "FILES"), "TITLE" => "Files"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "COORDINATES"), "TITLE" => "Coordinates"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "COMPANY_ON_MAP"), "TITLE" => "Company on map"),
					)
				)
			),
			FIVE_YEAR_RESULTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FIVE_YEAR_RESULTS_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "*Units (in the corner of the table)"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FIVE_YEAR_RESULTS_IBLOCK_ID_EN, "COLUMNS"), "TITLE" => "*Columns"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FIVE_YEAR_RESULTS_IBLOCK_ID_EN, "ROWS"), "TITLE" => "*Rows"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FIVE_YEAR_RESULTS_IBLOCK_ID_EN, "DATA"), "TITLE" => "*Values"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FIVE_YEAR_RESULTS_IBLOCK_ID_EN, "PDF"), "TITLE" => "*Table in pdf")
					)
				)
			),
			DIRECTORS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "ROLE"), "TITLE" => "Role"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "FILTER"), "TITLE" => "Filter"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "BIOGRAPHY_PDF"), "TITLE" => "Biography in pdf format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "BIOGRAPHY_DOCKX"), "TITLE" => "Biography in dockx format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "MULTIMEDIA"), "TITLE" => "Multimedia"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					),
				)
			)
		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}

	}

	public function down()
	{
	}

}

