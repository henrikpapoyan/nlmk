<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160315120022 extends Version {

	protected $description = "Изменение сортировки России и США в ИБ Географический дивизион";

	public function up() {
		$arDivisonsRU = array("russia" => array("SORT" => 1), "usa" => array("SORT" => 3));
		$dbDivisionsRU = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => GEOGRAPHY_DIVISION_IBLOCK_ID_RU), false, false, array("ID", "IBLOCK_ID", "CODE"));
		while ($arDivision = $dbDivisionsRU->GetNext()) {
			if ($arDivision["CODE"] == "russia")
				$arDivisonsRU["russia"]["ID"] = $arDivision["ID"];
			if ($arDivision["CODE"] == "usa")
				$arDivisonsRU["usa"]["ID"] = $arDivision["ID"];
		}
		$arDivisonsEN = array("russia" => array("SORT" => 1), "usa" => array("SORT" => 3));
		$dbDivisionsEN = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => GEOGRAPHY_DIVISION_IBLOCK_ID_EN), false, false, array("ID", "IBLOCK_ID", "CODE"));
		while ($arDivision = $dbDivisionsEN->GetNext()) {
			if ($arDivision["CODE"] == "russia")
				$arDivisonsEN["russia"]["ID"] = $arDivision["ID"];
			if ($arDivision["CODE"] == "usa")
				$arDivisonsEN["usa"]["ID"] = $arDivision["ID"];
		}

		$el = new \CIBlockElement();
		foreach ($arDivisonsRU as $arDivisionRU) {
			$el->Update($arDivisionRU["ID"], array("SORT" => $arDivisionRU["SORT"]));
		}
		foreach ($arDivisonsEN as $arDivisionEN) {
			$el->Update($arDivisionEN["ID"], array("SORT" => $arDivisionEN["SORT"]));
		}
	}

	public function down() {
		$arDivisonsRU = array("russia" => array("SORT" => 3), "usa" => array("SORT" => 1));
		$dbDivisionsRU = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => GEOGRAPHY_DIVISION_IBLOCK_ID_RU), false, false, array("ID", "IBLOCK_ID", "CODE"));
		while ($arDivision = $dbDivisionsRU->GetNext()) {
			if ($arDivision["CODE"] == "russia")
				$arDivisonsRU["russia"]["ID"] = $arDivision["ID"];
			if ($arDivision["CODE"] == "usa")
				$arDivisonsRU["usa"]["ID"] = $arDivision["ID"];
		}
		$arDivisonsEN = array("russia" => array("SORT" => 3), "usa" => array("SORT" => 1));
		$dbDivisionsEN = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => GEOGRAPHY_DIVISION_IBLOCK_ID_EN), false, false, array("ID", "IBLOCK_ID", "CODE"));
		while ($arDivision = $dbDivisionsEN->GetNext()) {
			if ($arDivision["CODE"] == "russia")
				$arDivisonsEN["russia"]["ID"] = $arDivision["ID"];
			if ($arDivision["CODE"] == "usa")
				$arDivisonsEN["usa"]["ID"] = $arDivision["ID"];
		}

		$el = new \CIBlockElement();
		foreach ($arDivisonsRU as $arDivisionRU) {
			$el->Update($arDivisionRU["ID"], array("SORT" => $arDivisionRU["SORT"]));
		}
		foreach ($arDivisonsEN as $arDivisionEN) {
			$el->Update($arDivisionEN["ID"], array("SORT" => $arDivisionEN["SORT"]));
		}
	}

}
