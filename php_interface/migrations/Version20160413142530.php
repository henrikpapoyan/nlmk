<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160413142530 extends Version {

	protected $description = "Переименование русских типов инфоблоков";

	public function up()
	{
		$arIblockTypes = array(
			"activities" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Деятельность",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Деятельность",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"career" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Карьера",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Карьера",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"company" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "О компании",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "О компании",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"contacts" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Контакты",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Контакты",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"investors" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Инвесторам",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Инвесторам",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"mediacenter" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Медиа-центр",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Медиа-центр",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"sustainability" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Устойчивое развитие",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Устойчивое развитие",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			)
		);

		$obIblockType = new \CIBlockType;
		foreach ($arIblockTypes as $iblockCode => $arIblockTypeFields)
		{
			$obIblockType->Update($iblockCode, $arIblockTypeFields);
		}


	}

	public function down()
	{
		$arIblockTypes = array(
			"activities" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Деятельность",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Деятельность",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"career" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Сareer",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Карьера",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"company" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Company",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "О компании",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"contacts" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Contacts",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Контакты",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"investors" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Investor relations",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Инвесторам",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"mediacenter" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Media Center",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Медиацентр",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"sustainability" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Sustainability",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Устойчивое развитие",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			)
		);

		$obIblockType = new \CIBlockType;
		foreach ($arIblockTypes as $iblockCode => $arIblockTypeFields)
		{
			$obIblockType->Update($iblockCode, $arIblockTypeFields);
		}
	}

}
