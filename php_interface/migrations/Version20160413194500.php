<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
use CIBlockProperty;

class Version20160413194500 extends Version {

	protected $description = "Обновление свойства \"Фильтр\" для инфоблоков \"Совет директоров\" и \"Члены правления\" EN";

	public function up(){

		$arIblocks = array(
			DIRECTORS_IBLOCK_ID_EN,
			MANAGEMENT_IBLOCK_ID_EN
		);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = CIBlockProperty::GetPropertyEnum(
				"FILTER",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["XML_ID"] == "i-ico_user") {
					$arEnumProp['VALUE'] = "The Human Resources, Remuneration and Social Policies Committee";
				}
				if ($arEnumProp["XML_ID"] == "i-ico_director") {
					$arEnumProp['VALUE'] = "Independent Board members";
				}
				if ($arEnumProp["XML_ID"] == "i-ico_check") {
					$arEnumProp['VALUE'] = "The Strategic Planning Committee";
				}
				if ($arEnumProp["XML_ID"] == "i-ico_audit") {
					$arEnumProp['VALUE'] = "The Audit Committee";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}

	}

	public function down(){
		$arIblocks = array(
			DIRECTORS_IBLOCK_ID_EN,
			MANAGEMENT_IBLOCK_ID_EN
		);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = CIBlockProperty::GetPropertyEnum(
				"FILTER",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["XML_ID"] == "i-ico_user") {
					$arEnumProp['VALUE'] = "Комитет по кадрам, вознаграждениям и социальной политике";
				}
				if ($arEnumProp["XML_ID"] == "i-ico_director") {
					$arEnumProp['VALUE'] = "Независимые директора";
				}
				if ($arEnumProp["XML_ID"] == "i-ico_check") {
					$arEnumProp['VALUE'] = "Комитет по стратегическому планированию";
				}
				if ($arEnumProp["XML_ID"] == "i-ico_audit") {
					$arEnumProp['VALUE'] = "Комитет по аудиту";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
	}

}
