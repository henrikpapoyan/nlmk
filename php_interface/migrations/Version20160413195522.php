<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
use CIBlockProperty;

class Version20160413195522 extends Version {

    protected $description = "Обновление свойства \"Роль\" для инфоблока \"Совет директоров\" EN";

    public function up(){
		$arIblocks = array(
			DIRECTORS_IBLOCK_ID_EN,
		);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = CIBlockProperty::GetPropertyEnum(
				"ROLE",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["XML_ID"] == "HEAD") {
					$arEnumProp['VALUE'] = "Chairman of the Board of Directors";
				}
				if ($arEnumProp["XML_ID"] == "MEMBER") {
					$arEnumProp['VALUE'] = "Members of the board of directors";
				}
				if ($arEnumProp["XML_ID"] == "INDEPENDENT") {
					$arEnumProp['VALUE'] = "Independent directors";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
    }

    public function down(){
		$arIblocks = array(
			DIRECTORS_IBLOCK_ID_EN,
		);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = CIBlockProperty::GetPropertyEnum(
				"ROLE",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["XML_ID"] == "HEAD") {
					$arEnumProp['VALUE'] = "Председатель СД";
				}
				if ($arEnumProp["XML_ID"] == "MEMBER") {
					$arEnumProp['VALUE'] = "Член СД";
				}
				if ($arEnumProp["XML_ID"] == "INDEPENDENT") {
					$arEnumProp['VALUE'] = "Независимый директор";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
    }

}
