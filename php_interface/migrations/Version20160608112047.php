<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160608112047 extends Version
{

	protected $description = "Добавление свойства (чекбокс) \"Выводить элемент в списках (Раздел \"Руководство\")\" для ИБ \"Совет директоров\" и \"Члены правления\" ru/en";

	public function up()
	{
		$helper = new IblockHelper();

		$arPropertyFields = array(
			"RU" => array(
				"NAME" => "Выводить элемент в списках (Раздел \"Руководство\")",
				"ACTIVE" => "Y",
				"SORT" => "500",
				"CODE" => "SHOW_IN_LISTS",
				"PROPERTY_TYPE" => "L",
				"VALUES" => array(array("VALUE" => "Да", "XML_ID" => "Y")),
				"LIST_TYPE" => "C"
			),
			"EN" => array(
				"NAME" => "Show in lists (\"Governance\" section)",
				"ACTIVE" => "Y",
				"SORT" => "500",
				"CODE" => "SHOW_IN_LISTS",
				"PROPERTY_TYPE" => "L",
				"VALUES" => array(array("VALUE" => "Yes", "XML_ID" => "Y")),
				"LIST_TYPE" => "C"
			)
		);

		$helper->addPropertyIfNotExists(MANAGEMENT_IBLOCK_ID_RU, $arPropertyFields["RU"]);
		$helper->addPropertyIfNotExists(MANAGEMENT_IBLOCK_ID_EN, $arPropertyFields["EN"]);

		$helper->addPropertyIfNotExists(DIRECTORS_IBLOCK_ID_RU, $arPropertyFields["RU"]);
		$helper->addPropertyIfNotExists(DIRECTORS_IBLOCK_ID_EN, $arPropertyFields["EN"]);

		$arIblocks = array(
			MANAGEMENT_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "SHOW_IN_LISTS"), "TITLE" => "Show in lists (\"Governance\" section)"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "ROLE"), "TITLE" => "Chairman of the Board"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "FILTER"), "TITLE" => "Filter"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "BIOGRAPHY_PDF"), "TITLE" => "Biography in pdf format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "BIOGRAPHY_DOCKX"), "TITLE" => "Biography in dockx format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "MULTIMEDIA"), "TITLE" => "Multimedia"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					)
				),
			),
			DIRECTORS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "SHOW_IN_LISTS"), "TITLE" => "Show in lists (\"Governance\" section)"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "ROLE"), "TITLE" => "Role"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "FILTER"), "TITLE" => "Filter"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "BIOGRAPHY_PDF"), "TITLE" => "Biography in pdf format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "BIOGRAPHY_DOCKX"), "TITLE" => "Biography in dockx format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "MULTIMEDIA"), "TITLE" => "Multimedia"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					),
				)
			),
			MANAGEMENT_IBLOCK_ID_RU => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Элемент",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Активность"),
						array("NAME" => "SORT", "TITLE" => "Сортировка"),
						array("NAME" => "NAME", "TITLE" => "*ФИО"),
						array("NAME" => "CODE", "TITLE" => "*Символьный код"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "SHOW_IN_LISTS"), "TITLE" => "Выводить элемент в списках (Раздел \"Руководство\")"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Фото"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "POST"), "TITLE" => "*Должность"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "ROLE"), "TITLE" => "Председатель Правления"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "FILTER"), "TITLE" => "Фильтр"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "BIOGRAPHY_PDF"), "TITLE" => "Биография в формате pdf"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "BIOGRAPHY_DOCKX"), "TITLE" => "Биография в формате docx"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "MULTIMEDIA"), "TITLE" => "Мультимедиа"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "DESCRIPTION"), "TITLE" => "Краткое описание"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Детальное описание")
					)
				),
			),
			DIRECTORS_IBLOCK_ID_RU => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Элемент",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Активность"),
						array("NAME" => "SORT", "TITLE" => "Сортировка"),
						array("NAME" => "NAME", "TITLE" => "*ФИО"),
						array("NAME" => "CODE", "TITLE" => "*Символьный код"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "SHOW_IN_LISTS"), "TITLE" => "Выводить элемент в списках (Раздел \"Руководство\")"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Фото"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "POST"), "TITLE" => "*Должность"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "ROLE"), "TITLE" => "Роль"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "FILTER"), "TITLE" => "Фильтр"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "BIOGRAPHY_PDF"), "TITLE" => "Биография в формате pdf"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "BIOGRAPHY_DOCKX"), "TITLE" => "Биография в формате docx"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "MULTIMEDIA"), "TITLE" => "Мультимедиа"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "DESCRIPTION"), "TITLE" => "Краткое описание"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Детальное описание")
					),
				)
			),
		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}

	}

	public function down()
	{
		$helper = new IblockHelper();
		$helper->deleteProperty(MANAGEMENT_IBLOCK_ID_RU, "SHOW_IN_LISTS");
		$helper->deleteProperty(MANAGEMENT_IBLOCK_ID_EN, "SHOW_IN_LISTS");
		$helper->deleteProperty(DIRECTORS_IBLOCK_ID_RU, "SHOW_IN_LISTS");
		$helper->deleteProperty(DIRECTORS_IBLOCK_ID_EN, "SHOW_IN_LISTS");

		$arIblocks = array(
			MANAGEMENT_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "ROLE"), "TITLE" => "Chairman of the Board"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "FILTER"), "TITLE" => "Filter"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "BIOGRAPHY_PDF"), "TITLE" => "Biography in pdf format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "BIOGRAPHY_DOCKX"), "TITLE" => "Biography in dockx format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "MULTIMEDIA"), "TITLE" => "Multimedia"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					)
				),
			),
			DIRECTORS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "ROLE"), "TITLE" => "Role"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "FILTER"), "TITLE" => "Filter"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "BIOGRAPHY_PDF"), "TITLE" => "Biography in pdf format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "BIOGRAPHY_DOCKX"), "TITLE" => "Biography in dockx format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "MULTIMEDIA"), "TITLE" => "Multimedia"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					),
				)
			),
			MANAGEMENT_IBLOCK_ID_RU => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Элемент",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Активность"),
						array("NAME" => "SORT", "TITLE" => "Сортировка"),
						array("NAME" => "NAME", "TITLE" => "*ФИО"),
						array("NAME" => "CODE", "TITLE" => "*Символьный код"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Фото"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "POST"), "TITLE" => "*Должность"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "ROLE"), "TITLE" => "Председатель Правления"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "FILTER"), "TITLE" => "Фильтр"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "BIOGRAPHY_PDF"), "TITLE" => "Биография в формате pdf"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "BIOGRAPHY_DOCKX"), "TITLE" => "Биография в формате docx"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "MULTIMEDIA"), "TITLE" => "Мультимедиа"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_RU, "DESCRIPTION"), "TITLE" => "Краткое описание"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Детальное описание")
					)
				),
			),
			DIRECTORS_IBLOCK_ID_RU => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Элемент",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Активность"),
						array("NAME" => "SORT", "TITLE" => "Сортировка"),
						array("NAME" => "NAME", "TITLE" => "*ФИО"),
						array("NAME" => "CODE", "TITLE" => "*Символьный код"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Фото"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "POST"), "TITLE" => "*Должность"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "ROLE"), "TITLE" => "Роль"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "FILTER"), "TITLE" => "Фильтр"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "BIOGRAPHY_PDF"), "TITLE" => "Биография в формате pdf"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "BIOGRAPHY_DOCKX"), "TITLE" => "Биография в формате docx"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "MULTIMEDIA"), "TITLE" => "Мультимедиа"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_RU, "DESCRIPTION"), "TITLE" => "Краткое описание"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Детальное описание")
					),
				)
			),
		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}

	}

}
