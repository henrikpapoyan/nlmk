<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160520142353 extends Version {

	protected $description = "Добавление свойства \"Описание\" в поисковый индекс для ИБ \"Фотогалерея\" и \"Видео, Аудио\" (ru/en)";

	public function up()
	{
		$helper = new IblockHelper();
		$arFieldsRu = array(
			"NAME" => "Описание",
			"ACTIVE" => "Y",
			"CODE" => "DESCRIPTION",
			"PROPERTY_TYPE" => "S",
			"SEARCHABLE" => "Y",
		);
		$helper->addPropertyIfNotExists(MULTIMEDIA_IBLOCK_ID_RU, $arFieldsRu);

		$arFieldsEn = array(
			"NAME" => "Description",
			"ACTIVE" => "Y",
			"CODE" => "DESCRIPTION",
			"PROPERTY_TYPE" => "S",
			"SEARCHABLE" => "Y",
		);
		$helper->addPropertyIfNotExists(MULTIMEDIA_IBLOCK_ID_EN, $arFieldsEn);

		$arMultimediaDesc = array();
		$dbPhotosRu = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => PHOTOS_IBLOCK_ID_RU), false, false, array("ID", "IBLOCK_ID", "PROPERTY_DESCRIPTION"));
		while ($arPhoto = $dbPhotosRu->GetNext())
		{
			$arMultimediaDesc[$arPhoto["ID"]] = $arPhoto["PROPERTY_DESCRIPTION_VALUE"]["TEXT"];
		}
		$dbPhotosEn = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => PHOTOS_IBLOCK_ID_EN), false, false, array("ID", "IBLOCK_ID", "PROPERTY_DESCRIPTION"));
		while ($arPhoto = $dbPhotosEn->GetNext())
		{
			$arMultimediaDesc[$arPhoto["ID"]] = $arPhoto["PROPERTY_DESCRIPTION_VALUE"]["TEXT"];
		}

		$dbVideosRu = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => VIDEOS_IBLOCK_ID_RU), false, false, array("ID", "IBLOCK_ID", "PROPERTY_DESCRIPTION"));
		while ($arVideo = $dbVideosRu->GetNext())
		{
			$arMultimediaDesc[$arVideo["ID"]] = $arVideo["PROPERTY_DESCRIPTION_VALUE"];
		}
		$dbVideosEn = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => VIDEOS_IBLOCK_ID_EN), false, false, array("ID", "IBLOCK_ID", "PROPERTY_DESCRIPTION"));
		while ($arVideo = $dbVideosEn->GetNext())
		{
			$arMultimediaDesc[$arVideo["ID"]] = $arVideo["PROPERTY_DESCRIPTION_VALUE"];
		}

		$dbMultimedia = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => array(MULTIMEDIA_IBLOCK_ID_RU, MULTIMEDIA_IBLOCK_ID_EN)), false, false, array("ID", "IBLOCK_ID", "PROPERTY_MULTIMEDIA"));
		while ($arMultimedia = $dbMultimedia->GetNext())
		{
			\CIBlockElement::SetPropertyValuesEx($arMultimedia["ID"], $arMultimedia["IBLOCK_ID"], array("DESCRIPTION" => strip_tags($arMultimediaDesc[$arMultimedia["PROPERTY_MULTIMEDIA_VALUE"]])));
		}

	}

	public function down()
	{
		$helper = new IblockHelper();
		$helper->deleteProperty(MULTIMEDIA_IBLOCK_ID_RU, "DESCRIPTION");
		$helper->deleteProperty(MULTIMEDIA_IBLOCK_ID_EN, "DESCRIPTION");
	}

}
