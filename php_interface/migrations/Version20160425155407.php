<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160425155407 extends Version {

    protected $description = "Свойства \"Обновить при сохранении\" и \"Страницы документа\" для инфоблока \"Документы\"";

    public function up(){
        $helper = new IblockHelper();
        $arFields = array(
            "NAME"          => "Страницы документа",
            "ACTIVE"        => "Y",
            "SORT"          => "500",
            "MULTIPLE"      => "Y",
            "CODE"          => "PDF_PAGES",
            "PROPERTY_TYPE" => "F",
            "FILE_TYPE"     => "jpg, gif, bmp, png, jpeg"
            // "USER_TYPE"     => "FileMan"
        );
        $helper->addPropertyIfNotExists(DOCUMENTS_IBLOCK_ID_RU, $arFields);

        $arFields = array(
            "NAME"          => "Обновить при сохранении",
            "ACTIVE"        => "Y",
            "SORT"          => "500",
            "CODE"          => "IS_UPDATE",
            "PROPERTY_TYPE" => "L",
            "VALUES"         => ["да"],
            "LIST_TYPE"     => "C"
            // "USER_TYPE"     => "Checkbox"
        );
        $helper->addPropertyIfNotExists(DOCUMENTS_IBLOCK_ID_RU, $arFields);

        $arFields = array(
            "NAME"          => "Document pages",
            "ACTIVE"        => "Y",
            "SORT"          => "500",
            "MULTIPLE"      => "Y",
            "CODE"          => "PDF_PAGES",
            "PROPERTY_TYPE" => "F",
            "FILE_TYPE"     => "jpg, gif, bmp, png, jpeg"
            // "USER_TYPE"     => "FileMan"
        );
        $helper->addPropertyIfNotExists(DOCUMENTS_IBLOCK_ID_EN, $arFields);

        $arFields = array(
            "NAME"          => "Update on save",
            "ACTIVE"        => "Y",
            "SORT"          => "500",
            "CODE"          => "IS_UPDATE",
            "PROPERTY_TYPE" => "L",
            "VALUES"         => ["yes"],
            "LIST_TYPE"     => "C"
            // "USER_TYPE"     => "Checkbox"
        );
        $helper->addPropertyIfNotExists(DOCUMENTS_IBLOCK_ID_EN, $arFields);
    }

    public function down(){
        $helper = new IblockHelper();
        $helper->deleteProperty(DOCUMENTS_IBLOCK_ID_RU, "PDF_PAGES");
        $helper->deleteProperty(DOCUMENTS_IBLOCK_ID_RU, "IS_UPDATE");
        $helper->deleteProperty(DOCUMENTS_IBLOCK_ID_EN, "PDF_PAGES");
        $helper->deleteProperty(DOCUMENTS_IBLOCK_ID_EN, "IS_UPDATE");
    }

}
