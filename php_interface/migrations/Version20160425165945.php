<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160425165945 extends Version {

	protected $description = "Вкладка \"Корпоративный журнал\" в инфоблоке \"Документы\"";

	public function up(){
		$helper = new IblockHelper();
		$arIblocks = array(
			DOCUMENTS_IBLOCK_ID_RU => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Элемент",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Активность"),
						array("NAME" => "SORT", "TITLE" => "Сортировка"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Начало активности"),
						array("NAME" => "NAME", "TITLE" => "*Название"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_RU, "FILE"), "TITLE" => "Файл"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_RU, "FILE_TYPE"), "TITLE" => "Тип файла"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_RU, "PAGE_URL"), "TITLE" => "Страница"),
						array("NAME" => "SECTIONS", "TITLE" => "Раздел"),
					)
				),
				// array(
				// 	"CODE" => "edit2",
				// 	"TITLE" => "Корпоративный журнал",
				// 	"FIELDS" => array(
				// 		array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_RU, "IS_UPDATE"), "TITLE" => "Обновить при сохранении"),
				// 		// array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_RU, "PDF_PAGES"), "TITLE" => "Страницы документа"),
				// 	)
				// )
			),
			DOCUMENTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_EN, "FILE"), "TITLE" => "File"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_EN, "FILE_TYPE"), "TITLE" => "File type"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_EN, "PAGE_URL"), "TITLE" => "Page url"),
						array("NAME" => "SECTIONS", "TITLE" => "Section"),
					)
				),
				// array(
				// 	"CODE" => "edit2",
				// 	"TITLE" => "Corporate magazine",
				// 	"FIELDS" => array(
				// 		array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_EN, "IS_UPDATE"), "TITLE" => "Update on save"),
				// 		// array("NAME" => "PROPERTY_" . $helper->getPropertyId(DOCUMENTS_IBLOCK_ID_EN, "PDF_PAGES"), "TITLE" => "Document pages"),
				// 	)
				// )
			)
		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}
	}

		public function down(){
				$helper = new IblockHelper();
		}

}
