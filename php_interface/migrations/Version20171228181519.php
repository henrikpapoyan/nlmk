<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171228181519 extends Version {

    protected $description = "Меняет название раздела контактов";

    public function up(){
        $bs = new \CIBlockSection;
        $bs->Update(2, ['NAME' => 'НЛМК (Липецк)']);
    }

    public function down(){
        $bs = new \CIBlockSection;
        $bs->Update(2, ['NAME' => 'Новолипецкий металлургический комбинат (Липецк)']);
    }

}
