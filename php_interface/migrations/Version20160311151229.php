<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
use CIBlockProperty;

class Version20160311151229 extends Version {

	protected $description = "";

	public function up(){
		$arIblocks = array(
			ANNUAL_REPORTS_IBLOCK_ID_RU,
			ANNUAL_REPORTS_IBLOCK_ID_EN
		);
		foreach ($arIblocks as $iblockId) {
			$dbProp = CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "COMMENT")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("USER_TYPE" => "HTML"));
			}
		}
	}

	public function down(){
		$arIblocks = array(
			ANNUAL_REPORTS_IBLOCK_ID_RU,
			ANNUAL_REPORTS_IBLOCK_ID_EN
		);
		foreach ($arIblocks as $iblockId) {
			$dbProp = CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "COMMENT")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("USER_TYPE" => ""));
			}
		}
	}

}
