<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160327193420 extends Version {

    protected $description = "";

    public function up(){
        $arElements = array();
        $dbElements = \CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => array(
                    DOCUMENTS_IBLOCK_ID_RU,
                    CAREER_CONTACTS_SIDEBAR_IBLOCK_ID_RU,
                    INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_RU
                ),
                '!PROPERTY_PAGE_URL' => false
            ),
            false,
            false,
            array()
        );
        while ($obElement = $dbElements->GetNextElement()) {
            $arFields = $obElement->GetFields();
            $arProperties = $obElement->GetProperties();
            $arElement = array('ID' => $arFields['ID']);
            foreach ($arProperties['PAGE_URL']['VALUE'] as $value) {
                if (strpos($value, '/ru/') === false)
                    $arElement['PAGE_URL'][] = '/ru' . $value;
            }
            $arElements[] = $arElement;
        }
        foreach ($arElements as $arElement) {
            if ($arElement['PAGE_URL']) {
                \CIBlockElement::SetPropertyValuesEx($arElement['ID'], false, array('PAGE_URL' => $arElement['PAGE_URL']));
            }
        }
    }

    public function down(){
        $arElements = array();
        $dbElements = \CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => array(
                    DOCUMENTS_IBLOCK_ID_RU,
                    CAREER_CONTACTS_SIDEBAR_IBLOCK_ID_RU,
                    INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_RU
                ),
                '!PROPERTY_PAGE_URL' => false
            ),
            false,
            false,
            array()
        );
        while ($obElement = $dbElements->GetNextElement()) {
            $arFields = $obElement->GetFields();
            $arProperties = $obElement->GetProperties();
            $arElement = array('ID' => $arFields['ID']);
            foreach ($arProperties['PAGE_URL']['VALUE'] as $value) {
                if (strpos($value, '/ru/') !== false)
                    $arElement['PAGE_URL'][] = str_replace('/ru/', '/', $value);
            }
            $arElements[] = $arElement;
        }
        foreach ($arElements as $arElement) {
            if ($arElement['PAGE_URL']) {
                \CIBlockElement::SetPropertyValuesEx($arElement['ID'], false, array('PAGE_URL' => $arElement['PAGE_URL']));
            }
        }
    }

}
