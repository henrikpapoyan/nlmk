<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160325120716 extends Version {

	protected $description = "Изменение координат для предприятий Европы EN";

	public function up(){
		$offsetX = 141;
		$offsetY = 52;
		$arCoordinates = array(
			array("27", "60"),
			array("41", "66"),
			array("45", "40"),
			array("30", "61"),
			array("44", "74")
		);
		$arCompanies = array();
		$dbCompanies = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => COMPANIES_IBLOCK_ID_EN), false, false, array("ID", "IBLOCK_ID", "PROPERTY_COORDINATES"));
		while ($arCompany = $dbCompanies->Fetch()) {
			$coordinatesXY = explode(",", trim($arCompany["PROPERTY_COORDINATES_VALUE"]));
			if (in_array($coordinatesXY, $arCoordinates)) {
				$arCompanies[] = array(
					"ID" => $arCompany["ID"],
					"COORDINATE_X" => intval($coordinatesXY[0]),
					"COORDINATE_Y" => intval($coordinatesXY[1])
				);
			}
		}
		foreach ($arCompanies as $arCompany) {
			$coordinatesXY = implode(",", array($arCompany["COORDINATE_X"] + $offsetX, $arCompany["COORDINATE_Y"] + $offsetY));
			\CIBlockElement::SetPropertyValuesEx($arCompany["ID"], false, array("COORDINATES" => $coordinatesXY));
		}
	}

	public function down(){
		$offsetX = 141;
		$offsetY = 52;
		$arCoordinates = array(
			array("168", "112"),
			array("182", "118"),
			array("186", "92"),
			array("171", "113"),
			array("185", "126")
		);
		$arCompanies = array();
		$dbCompanies = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => COMPANIES_IBLOCK_ID_EN), false, false, array("ID", "IBLOCK_ID", "PROPERTY_COORDINATES"));
		while ($arCompany = $dbCompanies->Fetch()) {
			$coordinatesXY = explode(",", trim($arCompany["PROPERTY_COORDINATES_VALUE"]));
			if (in_array($coordinatesXY, $arCoordinates)) {
				$arCompanies[] = array(
					"ID" => $arCompany["ID"],
					"COORDINATE_X" => intval($coordinatesXY[0]),
					"COORDINATE_Y" => intval($coordinatesXY[1])
				);
			}
		}
		foreach ($arCompanies as $arCompany) {
			$coordinatesXY = implode(",", array($arCompany["COORDINATE_X"] - $offsetX, $arCompany["COORDINATE_Y"] - $offsetY));
			\CIBlockElement::SetPropertyValuesEx($arCompany["ID"], false, array("COORDINATES" => $coordinatesXY));
		}
	}

}
