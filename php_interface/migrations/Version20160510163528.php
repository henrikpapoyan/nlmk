<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160510163528 extends Version {

	protected $description = "Изменение привязки элементов в медиацентре (замена возможной привязки к русскому предприятию на привязку к NLMK Group)";

	public function up()
	{
		$dbCompaniesRu = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => COMPANIES_IBLOCK_ID_RU, "CODE" => "nlmk-group"), false, false, array("ID", "IBLOCK_ID"));
		if ($arCompany = $dbCompaniesRu->GetNext())
			$companyRuId = $arCompany["ID"];

		$dbCompaniesEn = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => COMPANIES_IBLOCK_ID_EN, "CODE" => "nlmk-group"), false, false, array("ID", "IBLOCK_ID"));
		if ($arCompany = $dbCompaniesEn->GetNext())
			$companyEnId = $arCompany["ID"];

		if (isset($companyRuId) && isset($companyEnId))
		{
			// не работает фильтр вида array("IBLOCK_ID" => array(VIDEOS_IBLOCK_ID_EN, PHOTOS_IBLOCK_ID_EN), "PROPERTY_COMPANY" => $companyRuId)
			// поэтому отдельные GetList
			$dbVideo = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => VIDEOS_IBLOCK_ID_EN, "PROPERTY_COMPANY" => $companyRuId),
				false,
				false,
				array("ID", "IBLOCK_ID", "PROPERTY_COMPANY")
			);
			while ($arVideo = $dbVideo->GetNext())
			{
				\CIBlockElement::SetPropertyValuesEx($arVideo["ID"], false, array("COMPANY" => $companyEnId));
			}

			$dbPhoto = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => PHOTOS_IBLOCK_ID_EN, "PROPERTY_COMPANY" => $companyRuId),
				false,
				false,
				array("ID", "IBLOCK_ID", "PROPERTY_COMPANY")
			);
			while ($arPhoto = $dbPhoto->GetNext())
			{
				\CIBlockElement::SetPropertyValuesEx($arPhoto["ID"], false, array("COMPANY" => $companyEnId));
			}

			$dbNewsGroup = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => NEWS_GROUP_IBLOCK_ID_EN, "PROPERTY_COMPANY" => $companyRuId),
				false,
				false,
				array("ID", "IBLOCK_ID", "PROPERTY_COMPANY")
			);
			while ($arNewsGroup = $dbNewsGroup->GetNext())
			{
				\CIBlockElement::SetPropertyValuesEx($arNewsGroup["ID"], false, array("COMPANY" => $companyEnId));
			}

			$dbNewsCompanies = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => NEWS_COMPANIES_IBLOCK_ID_EN, "PROPERTY_COMPANY" => $companyRuId),
				false,
				false,
				array("ID", "IBLOCK_ID", "PROPERTY_COMPANY")
			);
			while ($arNewsCompanies = $dbNewsCompanies->GetNext())
			{
				\CIBlockElement::SetPropertyValuesEx($arNewsCompanies["ID"], false, array("COMPANY" => $companyEnId));
			}

			$dbNewsSmi = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => NEWS_SMI_IBLOCK_ID_EN, "PROPERTY_COMPANY" => $companyRuId),
				false,
				false,
				array("ID", "IBLOCK_ID", "PROPERTY_COMPANY")
			);
			while ($arNewsSmi = $dbNewsSmi->GetNext())
			{
				\CIBlockElement::SetPropertyValuesEx($arNewsSmi["ID"], false, array("COMPANY" => $companyEnId));
			}

			$dbPerformanses = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => PERFORMANSES_IBLOCK_ID_EN, "PROPERTY_COMPANY" => $companyRuId),
				false,
				false,
				array("ID", "IBLOCK_ID", "PROPERTY_COMPANY")
			);
			while ($arPerformanses = $dbPerformanses->GetNext())
			{
				\CIBlockElement::SetPropertyValuesEx($arPerformanses["ID"], false, array("COMPANY" => $companyEnId));
			}

			$dbEvents = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => EVENTS_IBLOCK_ID_EN, "PROPERTY_COMPANY" => $companyRuId),
				false,
				false,
				array("ID", "IBLOCK_ID", "PROPERTY_COMPANY")
			);
			while ($arEvents = $dbEvents->GetNext())
			{
				\CIBlockElement::SetPropertyValuesEx($arEvents["ID"], false, array("COMPANY" => $companyEnId));
			}

		}
	}

	public function down()
	{
	}

}
