<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160506150751 extends Version {

	protected $description = "Добавление предприятия \"Группа НЛМК\" (ru/en)";

	public function up()
	{
		$arProperties = array("COUNTRY" => "-", "CITY" => "-", "DESCRIPTION" => array("VALUE" => array("TEXT" => "-", "TYPE" => "html")), "PRODUCTION" => "-");
		$arFieldsEn = array(
			"IBLOCK_ID" => COMPANIES_IBLOCK_ID_EN,
			"PROPERTY_VALUES" => $arProperties,
			"NAME" => "NLMK Group",
			"CODE" => "nlmk-group",
			"ACTIVE" => "Y",
			"PREVIEW_PICTURE" => \CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . "/local/templates/nlmk/images/logo-nlmk.png")
		);
		$obElement = new \CIBlockElement;
		if(!$obElement->Add($arFieldsEn))
			echo "Error: " . $obElement->LAST_ERROR;

		$dbCompanies = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => COMPANIES_IBLOCK_ID_RU, "CODE" => "gruppa-nlmk"), false, false, array("ID", "IBLOCK_ID"));
		if ($arCompany = $dbCompanies->GetNext())
		{
			$obElement->Update($arCompany["ID"], array(
				"ACTIVE" => "Y",
				"CODE" => "nlmk-group",
				"PREVIEW_PICTURE" => \CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . "/local/templates/nlmk/images/logo-nlmk.png"),
				"PROPERTY_VALUES" => $arProperties
			));
		}

	}

	public function down()
	{
		$arToDelete = array();
		$dbElements = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => COMPANIES_IBLOCK_ID_EN, "CODE" => "nlmk-group"),
			false,
			false,
			array("ID", "IBLOCK_ID")
		);
		while ($arElement = $dbElements->GetNext())
		{
			$arToDelete[] = $arElement["ID"];
		}

		foreach ($arToDelete as $id)
		{
			\CIBlockElement::Delete($id);
		}
	}

}
