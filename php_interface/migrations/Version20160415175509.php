<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160415175509 extends Version {

    protected $description = "перевод значений свойства TYPE для ИБ Multimedia";

    public function up(){
		$arIblocks = array(MULTIMEDIA_IBLOCK_ID_EN);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = \CIBlockProperty::GetPropertyEnum(
				"TYPE",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch())
			{
				if ($arEnumProp["XML_ID"] == "VIDEO")
				{
					$arEnumProp["VALUE"] = "Video";
				}
				if ($arEnumProp["XML_ID"] == "PHOTO")
				{
					$arEnumProp["VALUE"] = "Photo";
				}
				if ($arEnumProp["XML_ID"] == "AUDIO")
				{
					$arEnumProp["VALUE"] = "Audio";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new \CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
    }

    public function down(){
		$arIblocks = array(MULTIMEDIA_IBLOCK_ID_EN);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = \CIBlockProperty::GetPropertyEnum(
				"TYPE",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch())
			{
				if ($arEnumProp["XML_ID"] == "VIDEO")
				{
					$arEnumProp["VALUE"] = "Видео";
				}
				if ($arEnumProp["XML_ID"] == "PHOTO")
				{
					$arEnumProp["VALUE"] = "Фото";
				}
				if ($arEnumProp["XML_ID"] == "AUDIO")
				{
					$arEnumProp["VALUE"] = "Аудио";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new \CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
    }

}
