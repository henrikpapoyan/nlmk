<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160525164503 extends Version
{

	protected $description = "Настройки ИБ \"Вакансии\"";

	public function up()
	{
		if (defined("JOBS_IBLOCK_ID_RU") && defined("JOBS_IBLOCK_ID_EN"))
		{
			$helper = new IblockHelper;

			$obIblock = new \CIBlock;
			$obIblock->Update(JOBS_IBLOCK_ID_RU, array("LIST_MODE" => "C"));
			$obIblock->Update(JOBS_IBLOCK_ID_EN, array("LIST_MODE" => "C"));

			$arFieldsRu = \CIBlock::getFields(JOBS_IBLOCK_ID_RU);
			$arFieldsRu["CODE"]["IS_REQUIRED"] = "Y";
			$arFieldsRu["CODE"]["DEFAULT_VALUE"]["UNIQUE"] = "Y";
			$arFieldsRu["CODE"]["DEFAULT_VALUE"]["TRANSLITERATION"] = "Y";
			$arFieldsRu["ACTIVE_FROM"]["IS_REQUIRED"] = "Y";
			$arFieldsRu["ACTIVE_FROM"]["DEFAULT_VALUE"] = "=now";
			\CIBlock::setFields(JOBS_IBLOCK_ID_RU, $arFieldsRu);

			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => JOBS_IBLOCK_ID_RU, "CODE" => "REGION")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new \CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("LINK_IBLOCK_ID" => COMPANIES_IBLOCK_ID_RU));
			}

			$arFieldsEn = \CIBlock::getFields(JOBS_IBLOCK_ID_EN);
			$arFieldsEn["CODE"]["IS_REQUIRED"] = "Y";
			$arFieldsEn["CODE"]["DEFAULT_VALUE"]["UNIQUE"] = "Y";
			$arFieldsEn["CODE"]["DEFAULT_VALUE"]["TRANSLITERATION"] = "Y";
			$arFieldsEn["ACTIVE_FROM"]["IS_REQUIRED"] = "Y";
			$arFieldsEn["ACTIVE_FROM"]["DEFAULT_VALUE"] = "=now";
			\CIBlock::setFields(JOBS_IBLOCK_ID_EN, $arFieldsEn);

			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => JOBS_IBLOCK_ID_EN, "CODE" => "REGION")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new \CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("LINK_IBLOCK_ID" => COMPANIES_IBLOCK_ID_EN));
			}

			$arIblocks = array(
				JOBS_IBLOCK_ID_EN => array(
					array(
						"CODE" => "edit1",
						"TITLE" => "Element",
						"FIELDS" => array(
							array("NAME" => "ACTIVE", "TITLE" => "Active"),
							array("NAME" => "SORT", "TITLE" => "Sorting"),
							array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
							array("NAME" => "NAME", "TITLE" => "*Name"),
							array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(JOBS_IBLOCK_ID_EN, "REGION"), "TITLE" => "Region"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(JOBS_IBLOCK_ID_EN, "RESPONSIBILITIES"), "TITLE" => "Responsibilities"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(JOBS_IBLOCK_ID_EN, "REQUIREMENTS"), "TITLE" => "Requirements"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(JOBS_IBLOCK_ID_EN, "OTHER_QUALIFICATIONS"), "TITLE" => "Other qualifications"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(JOBS_IBLOCK_ID_EN, "TYPE_OF_EMPLOYMENT"), "TITLE" => "Type of employment"),
						)
					)
				),
				JOBS_IBLOCK_ID_RU => array(
					array(
						"CODE" => "edit1",
						"TITLE" => "Элемент",
						"FIELDS" => array(
							array("NAME" => "ACTIVE", "TITLE" => "Активность"),
							array("NAME" => "SORT", "TITLE" => "Сортировка"),
							array("NAME" => "ACTIVE_FROM", "TITLE" => "*Начало активности"),
							array("NAME" => "NAME", "TITLE" => "*Название"),
							array("NAME" => "CODE", "TITLE" => "*Символьный код"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(JOBS_IBLOCK_ID_RU, "REGION"), "TITLE" => "Регион"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(JOBS_IBLOCK_ID_RU, "RESPONSIBILITIES"), "TITLE" => "Обязанности"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(JOBS_IBLOCK_ID_RU, "REQUIREMENTS"), "TITLE" => "Требования"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(JOBS_IBLOCK_ID_RU, "OTHER_QUALIFICATIONS"), "TITLE" => "Другие квалификации"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(JOBS_IBLOCK_ID_RU, "TYPE_OF_EMPLOYMENT"), "TITLE" => "Тип занятости"),
						)
					)
				),
			);

			foreach ($arIblocks as $iblockId => $arIblock)
			{
				$tabsString = "";
				foreach($arIblock as $arTab) {
					$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
					foreach($arTab["FIELDS"] as $field) {
						$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
						if (end($arTab["FIELDS"]) == $field) {
							$tabsString .= "--;--";
							continue;
						}
						$tabsString .= "--,--";
					}
				}
				$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
				\CUserOptions::SetOptionsFromArray($arOptions);
			}
		}
	}

	public function down()
	{
		$helper = new IblockHelper();
	}

}
