<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160315122924 extends Version {

	protected $description = "Изменение типа свойств DATE и DATE_END на \"DateTime\" для ИБ Финансовый календарь";

	public function up() {
		$arIblocks = array(FINANCIAL_CALENDAR_IBLOCK_ID_RU, FINANCIAL_CALENDAR_IBLOCK_ID_EN);
		$CIBlockProperty = new \CIBlockProperty;
		foreach ($arIblocks as $iblockId) {
			$dbPropDate = \CIBlockProperty::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $iblockId, "CODE" => "DATE"));
			while($arPropDate = $dbPropDate->GetNext()) {
				$CIBlockProperty->Update($arPropDate["ID"], array("USER_TYPE" => "DateTime"));
			}
			$dbPropDateEnd = \CIBlockProperty::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $iblockId, "CODE" => "DATE_END"));
			while($arPropDateEnd = $dbPropDateEnd->GetNext()) {
				$CIBlockProperty->Update($arPropDateEnd["ID"], array("USER_TYPE" => "DateTime"));
			}
		}
	}

	public function down() {
		$arIblocks = array(FINANCIAL_CALENDAR_IBLOCK_ID_RU, FINANCIAL_CALENDAR_IBLOCK_ID_EN);
		$CIBlockProperty = new \CIBlockProperty;
		foreach ($arIblocks as $iblockId) {
			$dbPropDate = \CIBlockProperty::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $iblockId, "CODE" => "DATE"));
			while($arPropDate = $dbPropDate->GetNext()) {
				$CIBlockProperty->Update($arPropDate["ID"], array("USER_TYPE" => "Date"));
			}
			$dbPropDateEnd = \CIBlockProperty::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $iblockId, "CODE" => "DATE_END"));
			while($arPropDateEnd = $dbPropDateEnd->GetNext()) {
				$CIBlockProperty->Update($arPropDateEnd["ID"], array("USER_TYPE" => "Date"));
			}
		}
	}

}
