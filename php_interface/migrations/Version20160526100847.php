<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160526100847 extends Version
{

	protected $description = "Почтовые шаблоны подтверждения подписки";

	public function up()
	{
		$obMessage = new \CEventMessage;
		$dbMessage = \CEventMessage::GetList($by="site_id", $order="desc", array("TYPE_ID" => "SUBSCRIBE_CONFIRM"));
		while ($arMess = $dbMessage->GetNext())
		{
			$obMessage->Delete(intval($arMess["ID"]));
		}

		$arMessageRu = array(
			"ACTIVE" => "Y",
			"EVENT_NAME" => "SUBSCRIBE_CONFIRM",
			"LID" => array("s1"),
			"EMAIL_FROM" => "ПАО «НЛМК» <info@nlmk.com>",
			"EMAIL_TO" => "#EMAIL#",
			"SUBJECT" => "Уведомление ПАО «НЛМК»: Подтверждение подписки",
			"BODY_TYPE" => "html",
			"MESSAGE" => '
				<table style="border-collapse:collapse; margin-bottom:0; font-family:Verdana,sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none; background-color:#ffffff; text-align:center" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
				<tbody>
				<tr>
					<td width="886">
						<table style="border-collapse:collapse; margin-bottom:0; font-family:Verdana,sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none; text-align:left; background-color:#f8f9fa;" bgcolor="#f8f9fa" align="center" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
							<td colspan="3" height="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								 &nbsp;
							</td>
						</tr>
						<tr>
							<td width="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								 &nbsp;
							</td>
							<td style="vertical-align:top; padding:0; border:none;" valign="top">
								<table width="886" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none;" cellpadding="0" cellspacing="0">
								<tbody>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
									<td width="152" height="106" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
				 <a href="http://nlmk.ru/" title="nlmk" target="_blank"> <img width="152" alt="nlmk" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/logo.jpg" height="106" style="display:block; line-height:1px; border:none;"> </a>
									</td>
									<td width="40" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
									<td width="510" style="vertical-align:middle; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#007ec7" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:13px; line-height:21px; color:#333333; text-transform:uppercase; letter-spacing:0.065px;">подтверждение подписки</span>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="5" width="886" height="331" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
				 <img width="886" alt="nlmk" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/bg.jpg" height="331" style="display:block; line-height:1px; border:none;">
									</td>
								</tr>
								</tbody>
								</table>
								<table width="886" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; line-height:20px; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none;" cellpadding="0" cellspacing="0">
								<tbody>
								<tr>
									<td colspan="3" height="50" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
									<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:20px; line-height:40px; color:#333333;">Уважаемый #UF_NAME#!</span>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="3" height="33" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
									<td height="35" style="vertical-align:top; padding:0; border:none;" valign="top">
				 <img width="60" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/bg-line.png" height="2" alt="" style="display:block; line-height:1px; border:none;">
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
									<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#595959;">Благодарим Вас за оформление подписки на обновления сайта ПАО «НЛМК».</span>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="3" height="20" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
									<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#595959;">Для подтверждения нажмите на кнопку</span>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="3" height="35" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
									<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
				 <a href="http://#SERVER_NAME#/ru/media-center/subscription/?ID=#ID#&CONFIRM_CODE=#CONFIRM_CODE#" title="nlmk" target="_blank"> <img width="181" alt="подтвердить подписку" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/btn-confirm.png" height="30" style="display:block; line-height:1px; border:none;"> </a>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="3" height="90" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								</tbody>
								</table>
								<table width="886" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" cellpadding="0" cellspacing="0">
								<tbody>
								<tr>
									<td colspan="7" height="45" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
									<td width="242" height="106" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
										<div style="display:block; font-family:Arial, FreeSans, sans-serif; font-size:11px; line-height:20px; color:#333333; text-transform:uppercase; letter-spacing:0.825px; padding-bottom:30px;">
											 Единый колл-центр службы продаж ПАО «НЛМК»
										</div>
										<div style="color: #404040; font-family:Arial, FreeSans, sans-serif; font-size: 16px; line-height:20px; font-weight:bold;">
											 +7 (495) 134 44 45
										</div>
									</td>
									<td width="60" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
									<td width="209" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#007ec7" valign="top">
										<div style="display:block; font-family:Arial, FreeSans, sans-serif; font-size:11px; line-height:20px; color:#333333; text-transform:uppercase; letter-spacing:0.825px; padding-bottom:30px;">
											 Присоединяйтесь в социальных сетях
										</div>
										<table align="left" style="border-collapse:collapse;border-spacing:0;margin-bottom:0;-premailer-cellspacing:0;-premailer-cellpadding:0;border:none;text-align:left;" cellpadding="0" cellspacing="0">
										<tbody>
										<tr>
											<td style="padding-right:10px; padding-left:6px; border:none; text-align:center;" valign="middle">
				 <a href="http://facebook.com/nlmk.press" title="facebook" target="_blank" style="color:#0c54a0; text-decoration:none"><img width="8" alt="facebook" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-fb.png" height="16" style="display: block; border: none;"></a>
											</td>
											<td style="border:none;" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;"><a href="http://facebook.com/nlmk.press" title="nlmk.press" target="_blank" style="color:#0c54a0; text-decoration:none">nlmk.press</a></span>
											</td>
										</tr>
										<tr>
											<td colspan="2" height="15" style="vertical-align:top; line-height:15px; padding:0; border:none;" valign="top">
												 &nbsp;
											</td>
										</tr>
										<tr>
											<td style="padding-right:10px; padding-left:2px; border:none; text-align:center;" valign="middle">
				 <a href="http://twitter.com/nlmk" title="twitter" target="_blank" style="color:#0c54a0; text-decoration:none"><img width="15" alt="twitter" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-tw.png" height="13" style="display: block; border: none;"></a>
											</td>
											<td style="border:none;" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;"><a href="http://twitter.com/nlmk" title="@nlmk" target="_blank" style="color:#0c54a0; text-decoration:none">@nlmk</a></span>
											</td>
										</tr>
										<tr>
											<td colspan="2" height="15" style="vertical-align:top; line-height:15px; padding:0; border:none;" valign="top">
												 &nbsp;
											</td>
										</tr>
										<tr>
											<td width="20" style="padding-right:10px; border:none; text-align:center;" valign="middle">
				 <a href="http://vk.com/nlmk_ru" title="vk" target="_blank" style="color:#0c54a0; text-decoration:none"><img width="20" alt="vk" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-vk.png" height="12" style="display: block; border: none;"></a>
											</td>
											<td style="border:none;" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;"><a href="http://vk.com/nlmk_ru" title="nlmk_ru" target="_blank" style="color:#0c54a0; text-decoration:none">nlmk_ru</a></span>
											</td>
										</tr>
										<tr>
											<td colspan="2" height="15" style="vertical-align:top; line-height:15px; padding:0; border:none;" valign="top">
												 &nbsp;
											</td>
										</tr>
										<tr>
											<td width="20" style="padding-right:10px; padding-left:3px; border:none; text-align:center;" valign="middle">
				 <a href="http://instagram.com/nlmk_group" title="instagram" target="_blank" style="color:#0c54a0; text-decoration:none"><img width="13" alt="instagram" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-in.png" height="13" style="display: block; border: none;"></a>
											</td>
											<td style="border:none;" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;"><a href="http://instagram.com/nlmk_group" title="nlmk_group" target="_blank" style="color:#0c54a0; text-decoration:none">nlmk_group</a></span>
											</td>
										</tr>
										<tr>
											<td colspan="2" height="15" style="vertical-align:top; line-height:15px; padding:0; border:none;" valign="top">
												 &nbsp;
											</td>
										</tr>
										<tr>
											<td width="20" style="padding-right:10px; padding-left:2px; border:none; text-align:center;" valign="middle">
				 <a href="http://youtube.com/user/nlmkonair" title="youtube" target="_blank" style="color:#0c54a0; text-decoration:none"><img width="15" alt="youtube" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-yt.png" height="10" style="display: block; border: none;"></a>
											</td>
											<td style="border:none;" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;"><a href="http://youtube.com/user/nlmkonair" title="nlmkonair" target="_blank" style="color:#0c54a0; text-decoration:none">nlmkonair</a></span>
											</td>
										</tr>
										</tbody>
										</table>
									</td>
									<td width="60" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
									<td width="131" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#007ec7" valign="top">
										<div style="display:block; font-family:Arial, FreeSans, sans-serif; font-size:11px; line-height:20px; color:#333333; text-transform:uppercase; letter-spacing:0.825px; padding-bottom:30px;">
											 Отписаться от рассылки
										</div>
										<div style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;">
				 <a href="http://#SERVER_NAME#/ru/media-center/subscription/?ID=#ID#&CONFIRM_CODE=#CONFIRM_CODE#&action=unsubscribe" title="Отписаться от рассылки" target="_blank" style="color:#0c54a0; text-decoration:none">Перейдите по ссылке</a>
										</div>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="7" height="45" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								</tbody>
								</table>
							</td>
							<td width="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								 &nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="3" height="25" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								 &nbsp;
							</td>
						</tr>
						</tbody>
						</table>
					</td>
				</tr>
				</tbody>
				</table>
			'
		);

		$arMessageEn = array(
			"ACTIVE" => "Y",
			"EVENT_NAME" => "SUBSCRIBE_CONFIRM",
			"LID" => array("s2"),
			"EMAIL_FROM" => "Novolipetsk Steel <info@nlmk.com>",
			"EMAIL_TO" => "#EMAIL#",
			"SUBJECT" => "Notification Novolipetsk Steel: Confirm subscription",
			"BODY_TYPE" => "html",
			"MESSAGE" => '
				<table style="border-collapse:collapse; margin-bottom:0; font-family:Verdana,sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none; background-color:#ffffff; text-align:center" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
				<tbody>
				<tr>
					<td width="886">
						<table style="border-collapse:collapse; margin-bottom:0; font-family:Verdana,sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none; text-align:left; background-color:#f8f9fa;" bgcolor="#f8f9fa" align="center" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
							<td colspan="3" height="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								 &nbsp;
							</td>
						</tr>
						<tr>
							<td width="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								 &nbsp;
							</td>
							<td style="vertical-align:top; padding:0; border:none;" valign="top">
								<table width="886" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none;" cellpadding="0" cellspacing="0">
								<tbody>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
									<td width="152" height="106" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
				 <a href="http://nlmk.com/" title="nlmk" target="_blank"> <img width="152" alt="nlmk" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/logo.jpg" height="106" style="display:block; line-height:1px; border:none;"> </a>
									</td>
									<td width="40" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
									<td width="510" style="vertical-align:middle; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#007ec7" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:13px; line-height:21px; color:#333333; text-transform:uppercase; letter-spacing:0.065px;">Subscribe confirmation</span>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="5" width="886" height="331" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
				 <img width="886" alt="nlmk" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/bg.jpg" height="331" style="display:block; line-height:1px; border:none;">
									</td>
								</tr>
								</tbody>
								</table>
								<table width="886" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; line-height:20px; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none;" cellpadding="0" cellspacing="0">
								<tbody>
								<tr>
									<td colspan="3" height="50" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
									<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:20px; line-height:40px; color:#333333;">Dear #UF_NAME#!</span>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="3" height="33" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
									<td height="35" style="vertical-align:top; padding:0; border:none;" valign="top">
				 <img width="60" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/bg-line.png" height="2" alt="" style="display:block; line-height:1px; border:none;">
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
									<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#595959;">Thank you for subscribing to the Novolipetsk Steel website updates.</span>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="3" height="20" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
									<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#595959;">Please press the Confirm subscription button to confirm your subscription.</span>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="3" height="35" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
									<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
				 <a href="http://#SERVER_NAME#/en/media-center/subscription/?ID=#ID#&CONFIRM_CODE=#CONFIRM_CODE#" title="nlmk" target="_blank"> <img width="181" alt="confirm subscription" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/btn-confirm-en.png" height="30" style="display:block; line-height:1px; border:none;"> </a>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="3" height="90" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								</tbody>
								</table>
								<table width="886" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" cellpadding="0" cellspacing="0">
								<tbody>
								<tr>
									<td colspan="7" height="45" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td width="92" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
									<td width="242" height="106" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
										<div style="display:block; font-family:Arial, FreeSans, sans-serif; font-size:11px; line-height:20px; color:#333333; text-transform:uppercase; letter-spacing:0.825px; padding-bottom:30px;">
											Sales service Call-Centre
										</div>
										<div style="color: #404040; font-family:Arial, FreeSans, sans-serif; font-size: 16px; line-height:20px; font-weight:bold;">
											 +7 (495) 134 44 45
										</div>
									</td>
									<td width="60" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
									<td width="209" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#007ec7" valign="top">
										<div style="display:block; font-family:Arial, FreeSans, sans-serif; font-size:11px; line-height:20px; color:#333333; text-transform:uppercase; letter-spacing:0.825px; padding-bottom:30px;">
											 Join us in social media
										</div>
										<table align="left" style="border-collapse:collapse;border-spacing:0;margin-bottom:0;-premailer-cellspacing:0;-premailer-cellpadding:0;border:none;text-align:left;" cellpadding="0" cellspacing="0">
										<tbody>
										<tr>
											<td style="padding-right:10px; padding-left:6px; border:none; text-align:center;" valign="middle">
				 <a href="http://facebook.com/nlmk.press" title="facebook" target="_blank" style="color:#0c54a0; text-decoration:none"><img width="8" alt="facebook" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-fb.png" height="16" style="display: block; border: none;"></a>
											</td>
											<td style="border:none;" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;"><a href="http://facebook.com/nlmk.press" title="nlmk.press" target="_blank" style="color:#0c54a0; text-decoration:none">nlmk.press</a></span>
											</td>
										</tr>
										<tr>
											<td colspan="2" height="15" style="vertical-align:top; line-height:15px; padding:0; border:none;" valign="top">
												 &nbsp;
											</td>
										</tr>
										<tr>
											<td style="padding-right:10px; padding-left:2px; border:none; text-align:center;" valign="middle">
				 <a href="http://twitter.com/nlmk" title="twitter" target="_blank" style="color:#0c54a0; text-decoration:none"><img width="15" alt="twitter" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-tw.png" height="13" style="display: block; border: none;"></a>
											</td>
											<td style="border:none;" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;"><a href="http://twitter.com/nlmk" title="@nlmk" target="_blank" style="color:#0c54a0; text-decoration:none">@nlmk</a></span>
											</td>
										</tr>
										<tr>
											<td colspan="2" height="15" style="vertical-align:top; line-height:15px; padding:0; border:none;" valign="top">
												 &nbsp;
											</td>
										</tr>
										<tr>
											<td width="20" style="padding-right:10px; border:none; text-align:center;" valign="middle">
				 <a href="http://vk.com/nlmk_ru" title="vk" target="_blank" style="color:#0c54a0; text-decoration:none"><img width="20" alt="vk" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-vk.png" height="12" style="display: block; border: none;"></a>
											</td>
											<td style="border:none;" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;"><a href="http://vk.com/nlmk_ru" title="nlmk_ru" target="_blank" style="color:#0c54a0; text-decoration:none">nlmk_ru</a></span>
											</td>
										</tr>
										<tr>
											<td colspan="2" height="15" style="vertical-align:top; line-height:15px; padding:0; border:none;" valign="top">
												 &nbsp;
											</td>
										</tr>
										<tr>
											<td width="20" style="padding-right:10px; padding-left:3px; border:none; text-align:center;" valign="middle">
				 <a href="http://instagram.com/nlmk_group" title="instagram" target="_blank" style="color:#0c54a0; text-decoration:none"><img width="13" alt="instagram" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-in.png" height="13" style="display: block; border: none;"></a>
											</td>
											<td style="border:none;" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;"><a href="http://instagram.com/nlmk_group" title="nlmk_group" target="_blank" style="color:#0c54a0; text-decoration:none">nlmk_group</a></span>
											</td>
										</tr>
										<tr>
											<td colspan="2" height="15" style="vertical-align:top; line-height:15px; padding:0; border:none;" valign="top">
												 &nbsp;
											</td>
										</tr>
										<tr>
											<td width="20" style="padding-right:10px; padding-left:2px; border:none; text-align:center;" valign="middle">
				 <a href="http://youtube.com/user/nlmkonair" title="youtube" target="_blank" style="color:#0c54a0; text-decoration:none"><img width="15" alt="youtube" src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-yt.png" height="10" style="display: block; border: none;"></a>
											</td>
											<td style="border:none;" valign="middle">
				 <span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;"><a href="http://youtube.com/user/nlmkonair" title="nlmkonair" target="_blank" style="color:#0c54a0; text-decoration:none">nlmkonair</a></span>
											</td>
										</tr>
										</tbody>
										</table>
									</td>
									<td width="60" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
									<td width="131" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#007ec7" valign="top">
										<div style="display:block; font-family:Arial, FreeSans, sans-serif; font-size:11px; line-height:20px; color:#333333; text-transform:uppercase; letter-spacing:0.825px; padding-bottom:30px;">
											 Unsubscribe
										</div>
										<div style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#0c54a0;">
				 <a href="http://#SERVER_NAME#/en/media-center/subscription/?ID=#ID#&CONFIRM_CODE=#CONFIRM_CODE#&action=unsubscribe" title="Unsubscribe" target="_blank" style="color:#0c54a0; text-decoration:none">Link</a>
										</div>
									</td>
									<td width="92" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
										 &nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="7" height="45" style="vertical-align:top; padding:0; border:none;" valign="top">
										 &nbsp;
									</td>
								</tr>
								</tbody>
								</table>
							</td>
							<td width="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								 &nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="3" height="25" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								 &nbsp;
							</td>
						</tr>
						</tbody>
						</table>
					</td>
				</tr>
				</tbody>
				</table>
			'
		);

		$obMessage->Add($arMessageRu);
		$obMessage->Add($arMessageEn);

	}

	public function down()
	{
		$helper = new IblockHelper();
	}

}
