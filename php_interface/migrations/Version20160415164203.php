<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160415164203 extends Version {

	protected $description = "описание свойства UF_PICTURES при добавлении раздела (History)";

	public function up(){
		$oUserTypeEntity    = new \CUserTypeEntity();
		$oUserTypeEntity->Update(6, array(
			'EDIT_FORM_LABEL'   => array(
				'ru' => 'Pictures of the year',
				'en' => 'Pictures of the year',
				),
			'LIST_COLUMN_LABEL' => array(
				'ru' => 'Pictures of the year',
				'en' => 'Pictures of the year',
				),
			'LIST_FILTER_LABEL' => array(
				'ru' => 'Pictures of the year',
				'en' => 'Pictures of the year',
			),
		));
	}

	public function down(){
		$oUserTypeEntity    = new \CUserTypeEntity();
		$oUserTypeEntity->Update(6, array(
			'EDIT_FORM_LABEL'   => array(
				'ru' => '',
				'en' => '',
				),
			'LIST_COLUMN_LABEL' => array(
				'ru' => '',
				'en' => '',
				),
			'LIST_FILTER_LABEL' => array(
				'ru' => '',
				'en' => '',
			),
		));
	}

}
