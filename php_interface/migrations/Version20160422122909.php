<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160422122909 extends Version {

	protected $description = "Пользовательские настройки инфоблоков Слайдер - Энергоэффективность (RU и EN) и Энергоэффективные кейсы (RU и EN)";

	public function up()
	{

		$helper = new IblockHelper();
		$arIblocks = array(
			ENERGY_CASES_IBLOCK_ID_RU => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Элемент",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Активность"),
						array("NAME" => "SORT", "TITLE" => "Сортировка"),
						array("NAME" => "NAME", "TITLE" => "*Название"),
						array("NAME" => "CODE", "TITLE" => "*Символьный код"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ENERGY_CASES_IBLOCK_ID_RU, "COMPANY"), "TITLE" => "Предприятие"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Изображение"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Детальное описание"),
					)
				),
			),
			ENERGY_CASES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ENERGY_CASES_IBLOCK_ID_EN, "COMPANY"), "TITLE" => "Company"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail text"),
					)
				),
			),
			ENERGY_SLIDER_IBLOCK_ID_RU => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Элемент",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Активность"),
						array("NAME" => "SORT", "TITLE" => "Сортировка"),
						array("NAME" => "NAME", "TITLE" => "*Название"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ENERGY_SLIDER_IBLOCK_ID_RU, "LINK"), "TITLE" => "Ссылка"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Изображение"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Описание"),
					)
				),
			),
			ENERGY_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ENERGY_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
					)
				),
			),
		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}

	}

	public function down()
	{

	}

}
