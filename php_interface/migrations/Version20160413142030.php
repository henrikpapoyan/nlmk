<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160413142030 extends Version {

	protected $description = "Переименование английских типов инфоблоков";

	public function up()
	{
		$arIblockTypes = array(
			"activities_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Our Business",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Our Business",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"career_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Career",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Career",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"company_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "About NLMK",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "About NLMK",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"contacts_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Contacts",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Contacts",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"investors_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Investor relations",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Investor relations",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"mediacenter_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Media",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Media",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"sustainability_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Sustainability",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Sustainability",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			)
		);

		$obIblockType = new \CIBlockType;
		foreach ($arIblockTypes as $iblockCode => $arIblockTypeFields)
		{
			$obIblockType->Update($iblockCode, $arIblockTypeFields);
		}


	}

	public function down()
	{
		$arIblockTypes = array(
			"activities_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Activities EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Деятельность EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"career_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Career EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Карьера EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"company_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "About NLMK",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "О Компании EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"contacts_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Contacts EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Контакты EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"investors_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Investor relations EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Инвесторам EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"mediacenter_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Media EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Медиацентр EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			),
			"sustainability_en" => array(
				"SECTIONS" => "Y",
				"LANG" => array(
					"en" => array(
						"NAME" => "Sustainability EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					),
					"ru" => array(
						"NAME" => "Устойчивое развитие EN",
						"SECTION_NAME" => "",
						"ELEMENT_NAME" => ""
					)
				)
			)
		);

		$obIblockType = new \CIBlockType;
		foreach ($arIblockTypes as $iblockCode => $arIblockTypeFields)
		{
			$obIblockType->Update($iblockCode, $arIblockTypeFields);
		}
	}

}
