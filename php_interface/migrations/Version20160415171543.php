<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160415171543 extends Version {

	protected $description = "установка инфоблока для связи в свойстве COMPANY для ИБ Stories of employees, Our professions и Events";

	public function up()
	{
		$arIblocks = array(MISSION_STORIES_IBLOCK_ID_EN, OUR_PROFESSIONS_IBLOCK_ID_EN, EVENTS_IBLOCK_ID_EN);
		foreach ($arIblocks as $iblockId) {
			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "COMPANY")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new \CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("LINK_IBLOCK_ID" => COMPANIES_IBLOCK_ID_EN));
			}
		}
	}

	public function down()
	{
		$arIblocks = array(MISSION_STORIES_IBLOCK_ID_EN, OUR_PROFESSIONS_IBLOCK_ID_EN, EVENTS_IBLOCK_ID_EN);
		foreach ($arIblocks as $iblockId) {
			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "COMPANY")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new \CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("LINK_IBLOCK_ID" => ""));
			}
		}
	}

}
