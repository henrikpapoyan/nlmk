<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
use CIBlockProperty;

class Version20160413200856 extends Version {

	protected $description = "Обновление свойства \"Division type\" для инфоблока \"Функциональный дивизион\" EN";

	public function up() {
		$arIblocks = array(FUNCTIONAL_DIVISION_IBLOCK_ID_EN);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = CIBlockProperty::GetPropertyEnum(
				"DIVISION_CODE",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["XML_ID"] == "flat-rolled") {
					$arEnumProp['VALUE'] = "NLMK Russia Flat Steel";
				}
				if ($arEnumProp["XML_ID"] == "materials") {
					$arEnumProp['VALUE'] = "Raw Materials";
				}
				if ($arEnumProp["XML_ID"] == "sort-rolled") {
					$arEnumProp['VALUE'] = "NLMK Russia Long";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
	}

	public function down() {
		$arIblocks = array(FUNCTIONAL_DIVISION_IBLOCK_ID_EN);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = CIBlockProperty::GetPropertyEnum(
				"DIVISION_CODE",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["XML_ID"] == "flat-rolled") {
					$arEnumProp['VALUE'] = "Плоский прокат";
				}
				if ($arEnumProp["XML_ID"] == "materials") {
					$arEnumProp['VALUE'] = "Добыча и производство сырья";
				}
				if ($arEnumProp["XML_ID"] == "sort-rolled") {
					$arEnumProp['VALUE'] = "Сортовой прокат";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
	}

}
