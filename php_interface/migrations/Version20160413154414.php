<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160413154414 extends Version {

	protected $description = "Изменение пользовательских настроек формы редактирования элементов инфоблоков (для англ. инфоблоков) Media";

	public function up()
	{
		$helper = new IblockHelper();
		$arIblocks = array(

			MC_FILES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MC_FILES_IBLOCK_ID_EN, "FILE"), "TITLE" => "*File"),
					)
				),
			),
			AWARDS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(AWARDS_IBLOCK_ID_EN, "YEAR"), "TITLE" => "Year"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Photo"),
					)
				),
			),
			EVENTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(EVENTS_IBLOCK_ID_EN, "SUBTITLE"), "TITLE" => "Subtitle"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(EVENTS_IBLOCK_ID_EN, "COMPANY"), "TITLE" => "*Company"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(EVENTS_IBLOCK_ID_EN, "DATE"), "TITLE" => "*Date"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(EVENTS_IBLOCK_ID_EN, "ADDRESS_COUNTRY"), "TITLE" => "*Country"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(EVENTS_IBLOCK_ID_EN, "ADDRESS_CITY"), "TITLE" => "*City"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(EVENTS_IBLOCK_ID_EN, "ADDRESS_STREET"), "TITLE" => "Address"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					)
				),
			),
			GLOSSARY_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Glossary"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(GLOSSARY_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "*Description"),
						array("NAME" => "SECTIONS", "TITLE" => "Section"),
					)
				),
			),
			PERFORMANSES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
						array("NAME" => "NAME", "TITLE" => "*Interviews"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PERFORMANSES_IBLOCK_ID_EN, "SUBTITLE"), "TITLE" => "*Subtitle"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PERFORMANSES_IBLOCK_ID_EN, "COMPANY"), "TITLE" => "*Company"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PERFORMANSES_IBLOCK_ID_EN, "PERSON_NAME"), "TITLE" => "*Person name"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					)
				),
			),
			MC_CONTACTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Media Center’s contacts"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MC_CONTACTS_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MC_CONTACTS_IBLOCK_ID_EN, "EMAIL"), "TITLE" => "*Address"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MC_CONTACTS_IBLOCK_ID_EN, "PHONE"), "TITLE" => "Phone"),
					)
				),
			),
			NEWS_SMI_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
						array("NAME" => "NAME", "TITLE" => "*Mention in the media"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_SMI_IBLOCK_ID_EN, "SUBTITLE"), "TITLE" => "Subtitle"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_SMI_IBLOCK_ID_EN, "COMPANY"), "TITLE" => "Company"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_SMI_IBLOCK_ID_EN, "SOURCE_LINK"), "TITLE" => "*Source link"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_SMI_IBLOCK_ID_EN, "SOURCE"), "TITLE" => "*Source name"),
					)
				),
			),
			MULTIMEDIA_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
						array("NAME" => "ACTIVE_TO", "TITLE" => "Activity End"),
						array("NAME" => "NAME", "TITLE" => "*Multimedia"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MULTIMEDIA_IBLOCK_ID_EN, "MULTIMEDIA"), "TITLE" => "*Multimedia"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MULTIMEDIA_IBLOCK_ID_EN, "TYPE"), "TITLE" => "*Type"),
					)
				),
			),
			NEWS_COMPANIES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
						array("NAME" => "NAME", "TITLE" => "*News of companies"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_COMPANIES_IBLOCK_ID_EN, "SUBTITLE"), "TITLE" => "Subtitle"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_COMPANIES_IBLOCK_ID_EN, "COMPANY"), "TITLE" => "Company"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_COMPANIES_IBLOCK_ID_EN, "FILES"), "TITLE" => "Files"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					)
				),
			),
			PRESS_CONTACTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Personal contacts"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRESS_CONTACTS_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRESS_CONTACTS_IBLOCK_ID_EN, "PHONE"), "TITLE" => "*Phone"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRESS_CONTACTS_IBLOCK_ID_EN, "EMAIL"), "TITLE" => "*Email"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRESS_CONTACTS_IBLOCK_ID_EN, "ADDRESS"), "TITLE" => "*Address"),
						array("NAME" => "SECTIONS", "TITLE" => "Section")
					)
				),
			),
			PHOTOS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Photo gallery"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PHOTOS_IBLOCK_ID_EN, "PICTURES"), "TITLE" => "*Pictures"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PHOTOS_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "*Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PHOTOS_IBLOCK_ID_EN, "COMPANY"), "TITLE" => "*Company"),
					)
				),
			),
			PRESS_KIT_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Press-kit"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRESS_KIT_IBLOCK_ID_EN, "FILES"), "TITLE" => "*Files")
					)
				),
			),
			NEWS_GROUP_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
						array("NAME" => "NAME", "TITLE" => "*Press-releases"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_GROUP_IBLOCK_ID_EN, "SUBTITLE"), "TITLE" => "*Subtitle"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_GROUP_IBLOCK_ID_EN, "COMPANY"), "TITLE" => "Company"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_GROUP_IBLOCK_ID_EN, "TRADING_UPDATES"), "TITLE" => "Trading updates"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_GROUP_IBLOCK_ID_EN, "FINANCIAL_RELEASES"), "TITLE" => "Financial releases"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(NEWS_GROUP_IBLOCK_ID_EN, "FILES"), "TITLE" => "Files"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					)
				),
			),
			QUESTIONS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Questions"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(QUESTIONS_IBLOCK_ID_EN, "NAME"), "TITLE" => "*Person name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(QUESTIONS_IBLOCK_ID_EN, "EMAIL"), "TITLE" => "*Email"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(QUESTIONS_IBLOCK_ID_EN, "QUESTION"), "TITLE" => "*Question"),
					)
				),
			),
			FAQ_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Questions and answers"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FAQ_IBLOCK_ID_EN, "QUESTION"), "TITLE" => "Question"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FAQ_IBLOCK_ID_EN, "ANSWER"), "TITLE" => "Answer")
					)
				),
			),
			EVENTS_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(EVENTS_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(EVENTS_SLIDER_IBLOCK_ID_EN, "DATE"), "TITLE" => "Date"),

					)
				),
			),
			MC_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Slider - mediacenter"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MC_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),

					)
				),
			),
			VIDEOS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Video, audio"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(VIDEOS_IBLOCK_ID_EN, "FILE"), "TITLE" => "File"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(VIDEOS_IBLOCK_ID_EN, "YOUTUBE_URL"), "TITLE" => "Youtube video url"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(VIDEOS_IBLOCK_ID_EN, "VIDEO_LENGTH"), "TITLE" => "*Playtime"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(VIDEOS_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "*Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(VIDEOS_IBLOCK_ID_EN, "COMPANY"), "TITLE" => "*Company"),

					)
				),
			),

		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}

	}

	public function down()
	{
	}

}

