<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160330120647 extends Version {

    protected $description = "Добавление обязательности свойству \"Тип\" ИБ Мультимедиа";

	public function up(){
		$arIblocks = array(
			MULTIMEDIA_IBLOCK_ID_RU,
			MULTIMEDIA_IBLOCK_ID_EN
		);
		foreach ($arIblocks as $iblockId) {
			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "TYPE")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new \CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("IS_REQUIRED" => "Y"));
			}
		}
	}

	public function down(){
		$arIblocks = array(
			MULTIMEDIA_IBLOCK_ID_RU,
			MULTIMEDIA_IBLOCK_ID_EN
		);
		foreach ($arIblocks as $iblockId) {
			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "TYPE")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new \CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("IS_REQUIRED" => "N"));
			}
		}
	}

}
