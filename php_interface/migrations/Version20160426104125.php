<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160426104125 extends Version {

    protected $description = "Символьные коды раздела \"Корпоративный журнал\" в инфоблоке \"Документы\"";

    public function up(){
        $helper = new IblockHelper();
        $bs = new \CIBlockSection;

        $res = \CIBlockSection::GetList(array(),array(
            'IBLOCK_ID' => DOCUMENTS_IBLOCK_ID_RU,
            'CODE'      => 'NLMK-Company'
        ));
        if($sect = $res->GetNext())
            $bs->Update($sect['ID'], ['CODE'=>'NLMK_COMPANY']);

        $res = \CIBlockSection::GetList(array(),array(
            'IBLOCK_ID' => DOCUMENTS_IBLOCK_ID_EN,
            'CODE'      => 'company-nlmk'
        ));
        if($sect = $res->GetNext())
            $bs->Update($sect['ID'], ['CODE'=>'NLMK_COMPANY']);

    }

    public function down(){
        $helper = new IblockHelper();
        $bs = new \CIBlockSection;

        $res = \CIBlockSection::GetList(array(),array(
            'IBLOCK_ID' => DOCUMENTS_IBLOCK_ID_RU,
            'CODE'      => 'NLMK_COMPANY'
        ));
        if($sect = $res->GetNext())
            $bs->Update($sect['ID'], ['CODE'=>'NLMK-Company']);

        $res = \CIBlockSection::GetList(array(),array(
            'IBLOCK_ID' => DOCUMENTS_IBLOCK_ID_EN,
            'CODE'      => 'NLMK_COMPANY'
        ));
        if($sect = $res->GetNext())
            $bs->Update($sect['ID'], ['CODE'=>'company-nlmk']);

    }

}
