<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160330113123 extends Version {

    protected $description = "Добавление свойства \"Ссылка\" в ИБ \"Предприятия\"";

    public function up(){
        $helper = new IblockHelper();
        $arFields = array(
            "NAME" => "Ссылка",
            "ACTIVE" => "Y",
            "SORT" => "100",
            "CODE" => "LINK",
            "PROPERTY_TYPE" => "S",
            "COL_COUNT" => "50"
        );
        $helper->addPropertyIfNotExists(COMPANIES_IBLOCK_ID_RU, $arFields);
        $helper->addPropertyIfNotExists(COMPANIES_IBLOCK_ID_EN, $arFields);
    }

    public function down(){
        $helper = new IblockHelper();
        $helper->deleteProperty(COMPANIES_IBLOCK_ID_RU, "LINK");
        $helper->deleteProperty(COMPANIES_IBLOCK_ID_EN, "LINK");
    }

}
