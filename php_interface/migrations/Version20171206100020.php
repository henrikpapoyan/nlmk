<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;

class Version20171206100020 extends Version {

    protected $description = "Добавление раздела \"Основные документы\" ИБ Документы (ru и en)";
    protected $props = array(
        "RU" => array(
            'CODE' => 'BASIC_DOCUMENTS',
            'NAME' => 'Основные документы',
        ),
        "EN" => array(
            'CODE' => 'BASIC_DOCUMENTS',
            'NAME' => 'Basic documents',
        ),
    );

    public function up()
    {
        $helper = new IblockHelper();

        foreach ($this->props as $lang => $prop) {
            $arSection = array("NAME" => $prop["NAME"], "CODE" => $prop["CODE"]);
            
            if ($helper->addSection(constant("DOCUMENTS_IBLOCK_ID_" . $lang), $arSection)) {
                $this->outSuccess('Раздел '. $prop['NAME'] . ' добавлен');
            } else {
                $this->outError("Не удалось добавить раздел " . $prop['NAME']);
            }
        }
    }

    public function down()
    {
        $arSectionsId = array();
        foreach ($this->props as $lang => $prop) {
            $dbSections = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang), "CODE" => $prop["CODE"]), false, array("ID"));
            while ($arSection = $dbSections->GetNext()) {
                $arSectionsId[] = $arSection["ID"];
            }
        }

        foreach ($arSectionsId as $sectionId) {
            if (\CIBlockSection::Delete($sectionId)) {
                $this->outSuccess('Раздел id='. $sectionId . ' удален');
            } else {
                $this->outError("Не удалось удалить раздел id=" . $sectionId);
            }
        }

    }

}
