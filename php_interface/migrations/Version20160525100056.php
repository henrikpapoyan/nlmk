<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160525100056 extends Version
{

	protected $description = "Добавление форм и вопросов (Отправить резюме/Send CV)";

	public function up()
	{
		\CModule::IncludeModule("form");

		// добавление англ формы
		$arFieldsEn = array(
			"NAME" => "Send CV",
			"SID" => "SEND_CV_EN",
			"BUTTON" => "Send",
			"DESCRIPTION" => '<legend>Fill out our online CV application</legend><p class="form__note">By submitting your CV application online, you are confirming your wish to join NLMK Group and providing your personal data to the Company’s HR department. Your information is confidential and is protected in accordance with NLMK Group’s Privacy Policy.</p>',
			"DESCRIPTION_TYPE" => "html",
			"STAT_EVENT1" => "form",
			"STAT_EVENT2" => "send_cv_en_form",
			"arSITE" => array("s2"),
			"arMENU" => array("ru" => "Send CV", "en" => "Send CV"),
			"arGROUP" => array("2" => "10"),
		);
		$sendCvFormEnId = \CForm::Set($arFieldsEn);
		if ($sendCvFormEnId > 0)
			echo "Добавлена веб-форма с ID=" . $sendCvFormEnId;
		else
			echo $GLOBALS["strError"];

		// добавление вопросов в англ форму
		$arFormFieldsEn = array(
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "CV",
				"TITLE_TYPE" => "text",
				"SID" => "CV_FILE",
				"REQUIRED" => "Y",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "file"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Work experience",
				"TITLE_TYPE" => "text",
				"SID" => "EXPERIENCE",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Education",
				"TITLE_TYPE" => "text",
				"SID" => "EDUCATION",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Please tell us why you want to join NLMK Group (optional)",
				"TITLE_TYPE" => "text",
				"SID" => "WHY",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "textarea"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Name",
				"TITLE_TYPE" => "text",
				"SID" => "NAME",
				"REQUIRED" => "Y",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "text"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Citizenship",
				"TITLE_TYPE" => "text",
				"SID" => "CITY",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "text"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Region",
				"TITLE_TYPE" => "text",
				"SID" => "REGION",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Region ID",
				"TITLE_TYPE" => "text",
				"SID" => "REGION_ID",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Functional direction",
				"TITLE_TYPE" => "text",
				"SID" => "DIRECTION",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Email",
				"TITLE_TYPE" => "text",
				"SID" => "EMAIL",
				"REQUIRED" => "Y",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "email"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Salary expectations",
				"TITLE_TYPE" => "text",
				"SID" => "SALARY",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "text"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Phone",
				"TITLE_TYPE" => "text",
				"SID" => "PHONE",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "text"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Date of birth",
				"TITLE_TYPE" => "text",
				"SID" => "BIRTHDAY",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Vacancy",
				"TITLE_TYPE" => "text",
				"SID" => "VACANCY",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormEnId,
				"ACTIVE" => "Y",
				"TITLE" => "Are you willing to relocate?",
				"TITLE_TYPE" => "text",
				"SID" => "READY_TO_MOVE",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => "yes",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "checkbox"
					)
				),
			),
		);
		foreach ($arFormFieldsEn as $arFormField)
		{
			\CFormField::Set($arFormField);
		}

		// добавление статуса для англ формы
		$arStatusFieldsEn = array(
			"FORM_ID" => $sendCvFormEnId,
			"ACTIVE" => "Y",
			"TITLE" => "DEFAULT",
			"DEFAULT_VALUE" => "Y",
			"CSS" => "statusgreen",
			"arPERMISSION_MOVE" => array(0)
		);
		$statusEnId = \CFormStatus::Set($arStatusFieldsEn);
		if ($statusEnId>0)
			echo "Статус успешно добавлен ID=".$statusEnId;
		else
			echo $GLOBALS["strError"];


		// добавление русской формы
		$arFieldsRu = array(
			"NAME" => "Отправить резюме",
			"SID" => "SEND_CV",
			"BUTTON" => "Отправить",
			"DESCRIPTION" => '<legend>Поиск подходящей вакансии</legend><p class="form__note">Вы можете отправить резюме для подходящей вам вакансии, выбрав нужные вам характеристики ниже.</p>',
			"DESCRIPTION_TYPE" => "html",
			"STAT_EVENT1" => "form",
			"STAT_EVENT2" => "send_cv_form",
			"arSITE" => array("s1"),
			"arMENU" => array("ru" => "Отправить резюме", "en" => "Отправить резюме"),
			"arGROUP" => array("2" => "10"),
		);
		$sendCvFormRuId = \CForm::Set($arFieldsRu);
		if ($sendCvFormRuId > 0)
			echo "Добавлена веб-форма с ID=" . $sendCvFormRuId;
		else
			echo $GLOBALS["strError"];

		// добавление вопросов в русскую форму
		$arFormFieldsRu = array(
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Резюме",
				"TITLE_TYPE" => "text",
				"SID" => "CV_FILE",
				"REQUIRED" => "Y",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "file"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Опыт работы",
				"TITLE_TYPE" => "text",
				"SID" => "EXPERIENCE",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Образование",
				"TITLE_TYPE" => "text",
				"SID" => "EDUCATION",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					),
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Почему вы хотите работать в компании",
				"TITLE_TYPE" => "text",
				"SID" => "WHY",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "textarea"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "ФИО",
				"TITLE_TYPE" => "text",
				"SID" => "NAME",
				"REQUIRED" => "Y",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "text"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Город пребывания",
				"TITLE_TYPE" => "text",
				"SID" => "CITY",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "text"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Регион",
				"TITLE_TYPE" => "text",
				"SID" => "REGION",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Регион ID",
				"TITLE_TYPE" => "text",
				"SID" => "REGION_ID",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Функциональное направление",
				"TITLE_TYPE" => "text",
				"SID" => "DIRECTION",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Электронная почта",
				"TITLE_TYPE" => "text",
				"SID" => "EMAIL",
				"REQUIRED" => "Y",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "email"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Желаемый уровень з/п",
				"TITLE_TYPE" => "text",
				"SID" => "SALARY",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "text"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Телефон",
				"TITLE_TYPE" => "text",
				"SID" => "PHONE",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "text"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Дата рождения",
				"TITLE_TYPE" => "text",
				"SID" => "BIRTHDAY",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Вакансия",
				"TITLE_TYPE" => "text",
				"SID" => "VACANCY",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => " ",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "hidden"
					)
				),
			),
			array(
				"FORM_ID" => $sendCvFormRuId,
				"ACTIVE" => "Y",
				"TITLE" => "Готов к переезду",
				"TITLE_TYPE" => "text",
				"SID" => "READY_TO_MOVE",
				"REQUIRED" => "N",
				"arANSWER" => array(
					array(
						"MESSAGE" => "да",
						"ACTIVE" => "Y",
						"FIELD_TYPE" => "checkbox"
					)
				),
			),
		);
		foreach ($arFormFieldsRu as $arFormField)
		{
			\CFormField::Set($arFormField);
		}

		// добавление статуса для русской формы
		$arStatusFieldsRu = array(
			"FORM_ID" => $sendCvFormRuId,
			"ACTIVE" => "Y",
			"TITLE" => "DEFAULT",
			"DEFAULT_VALUE" => "Y",
			"CSS" => "statusgreen",
			"arPERMISSION_MOVE" => array(0)
		);
		$statusRuId = \CFormStatus::Set($arStatusFieldsRu);
		if ($statusRuId>0)
			echo "Статус успешно добавлен ID=".$statusRuId;
		else
			echo $GLOBALS["strError"];
	}

	public function down()
	{
		\CModule::IncludeModule("form");

		$dbForms = \CForm::GetList(
			$by="s_id",
			$order="desc",
			array(
				"SID" => "SEND_CV_EN | SEND_CV",
				"SID_EXACT_MATCH" => "Y"
			),
			$isFiltered
		);
		while ($arForm = $dbForms->Fetch())
		{
			if (\CForm::Delete($arForm["ID"]))
				echo "Веб-форма #{$arForm["ID"]} удалена.";
			else
				echo $GLOBALS["strError"];
		}
	}

}
