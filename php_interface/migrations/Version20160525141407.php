<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160525141407 extends Version
{

	protected $description = "Добавление инфоблока \"Вакансии\" (ru/en)";

	public function up()
	{
		global $APPLICATION;
		$path = $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/migrations/xml";

		$jobsRu = $path . "/jobs_ru.xml";
		$strImportResultRu = ImportXMLFile($jobsRu, "career", "s1", "N", "N");
		if($strImportResultRu)
			$this->outSuccess("Инфоблок \"Вакансии\" добавлен");
		else
			$this->outError("Импорт завершен с ошибкой");

		$jobsEn = $path . "/jobs_en.xml";
		$strImportResultEn = ImportXMLFile($jobsEn, "career_en", "s2", "N", "N");
		if($strImportResultEn)
			$this->outSuccess("Инфоблок \"Vacancies\" добавлен");
		else
			$this->outError("Импорт завершен с ошибкой");
	}

	public function down()
	{
		$helper = new IblockHelper();
	}

}
