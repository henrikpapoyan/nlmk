<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160711114631 extends Version {

    protected $description = "Настройки инфоблоков \"Ключевые экологические показатели\" и \"Key environmental indicators\" ";

    public function up()
    {
        if (defined("KEY_INDICATORS_CHARTS_IBLOCK_ID_RU") && defined("KEY_INDICATORS_CHARTS_IBLOCK_ID_EN"))
        {
            $helper = new IblockHelper;

            $obIblock = new \CIBlock;
            $obIblock->Update(KEY_INDICATORS_CHARTS_IBLOCK_ID_RU, array("LIST_MODE" => "C"));
            $obIblock->Update(KEY_INDICATORS_CHARTS_IBLOCK_ID_EN, array("LIST_MODE" => "C"));

            $arFieldsRu = \CIBlock::getFields(KEY_INDICATORS_CHARTS_IBLOCK_ID_RU);
            $arFieldsRu["IBLOCK_SECTION"]["IS_REQUIRED"] = "Y";
            $arFieldsRu["SECTION_CODE"]["IS_REQUIRED"] = "Y";
            $arFieldsRu["SECTION_CODE"]["DEFAULT_VALUE"]["UNIQUE"] = "Y";
            \CIBlock::setFields(KEY_INDICATORS_CHARTS_IBLOCK_ID_RU, $arFieldsRu);

            $arFieldsEn = \CIBlock::getFields(KEY_INDICATORS_CHARTS_IBLOCK_ID_EN);
            $arFieldsEn["IBLOCK_SECTION"]["IS_REQUIRED"] = "Y";
            $arFieldsEn["SECTION_CODE"]["IS_REQUIRED"] = "Y";
            $arFieldsEn["SECTION_CODE"]["DEFAULT_VALUE"]["UNIQUE"] = "Y";
            \CIBlock::setFields(KEY_INDICATORS_CHARTS_IBLOCK_ID_EN, $arFieldsEn);

            $arIblocks = array(
                KEY_INDICATORS_CHARTS_IBLOCK_ID_EN => array(
                    array(
                        "CODE" => "edit1",
                        "TITLE" => "Element",
                        "FIELDS" => array(
                            array("NAME" => "ACTIVE", "TITLE" => "Active"),
                            array("NAME" => "SORT", "TITLE" => "Sorting"),
                            array("NAME" => "NAME", "TITLE" => "*Name"),
                            array("NAME" => "PROPERTY_" . $helper->getPropertyId(KEY_INDICATORS_CHARTS_IBLOCK_ID_EN, "VALUE"), "TITLE" => "Value"),
                            array("NAME" => "SECTIONS", "TITLE" => "*Sections"),
                        )
                    )
                ),
                KEY_INDICATORS_CHARTS_IBLOCK_ID_RU => array(
                    array(
                        "CODE" => "edit1",
                        "TITLE" => "Элемент",
                        "FIELDS" => array(
                            array("NAME" => "ACTIVE", "TITLE" => "Активность"),
                            array("NAME" => "SORT", "TITLE" => "Сортировка"),
                            array("NAME" => "NAME", "TITLE" => "*Название"),
                            array("NAME" => "PROPERTY_" . $helper->getPropertyId(KEY_INDICATORS_CHARTS_IBLOCK_ID_RU, "VALUE"), "TITLE" => "Значение"),
                            array("NAME" => "SECTIONS", "TITLE" => "*Разделы"),
                        )
                    )
                ),
            );

            foreach ($arIblocks as $iblockId => $arIblock)
            {
                $tabsString = "";
                foreach($arIblock as $arTab) {
                    $tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
                    foreach($arTab["FIELDS"] as $field) {
                        $tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
                        if (end($arTab["FIELDS"]) == $field) {
                            $tabsString .= "--;--";
                            continue;
                        }
                        $tabsString .= "--,--";
                    }
                }
                $arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
                \CUserOptions::SetOptionsFromArray($arOptions);
            }
        }
        else
        {
            $this->outError("Не определены константы инфоблоков");
        }
    }

    public function down()
    {
        $helper = new IblockHelper();
    }

}
