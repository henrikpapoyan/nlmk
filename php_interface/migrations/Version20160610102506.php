<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160610102506 extends Version
{

	protected $description = "Изменение метода автоматической рассылки на cron";

	public function up()
	{
		\COption::SetOptionString("subscribe", "subscribe_auto_method", "cron");
	}

	public function down()
	{
		\COption::SetOptionString("subscribe", "subscribe_auto_method", "agent");
	}

}
