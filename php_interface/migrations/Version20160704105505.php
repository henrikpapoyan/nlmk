<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160704105505 extends Version {

    protected $description = "Включение опции \"Использовать CAPTCHA\" для форм \"Отправить резюме\" и \"Send CV\"";

    public function up()
    {
        \CModule::IncludeModule("form");
        \CForm::Set(array("USE_CAPTCHA" => "Y"), SEND_CV_FORM_ID_RU);
        \CForm::Set(array("USE_CAPTCHA" => "Y"), SEND_CV_FORM_ID_EN);
    }

    public function down()
    {
        \CModule::IncludeModule("form");
        \CForm::Set(array("USE_CAPTCHA" => "N"), SEND_CV_FORM_ID_RU);
        \CForm::Set(array("USE_CAPTCHA" => "N"), SEND_CV_FORM_ID_EN);
    }

}
