<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171222154919 extends Version {

    protected $description = "Добавление типа меню для страницы \"Регулятивное раскрытие информации\"";

    public function up(){
        $menuTypes = GetMenuTypes();
        $menuTypes['subleft'] = "Страница регулятивное раскрытие";
        SetMenuTypes($menuTypes);
    }

    public function down(){
        $menuTypes = GetMenuTypes();
        unset($menuTypes['subleft']);
        SetMenuTypes($menuTypes);
    }

}
