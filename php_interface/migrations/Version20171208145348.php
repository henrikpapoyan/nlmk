<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171208145348 extends Version {

    protected $description = "Убираем неверные типы файлов из ИБ \"Документы\"";
    protected $badFileTypes = array(
        '17-o-raskrytii-emitentom-konsolidirovannoy-finansovoy-otchetnosti_-a-takzhe-o-predstavlenii-auditorskogo-zaklyucheniya_-podgotovlennogo-v-o',
        '17-o-svedeniyakh_-napravlyaemykh-ili-predostavlyaemykh-emitentom-sootvetstvuyushchemu-organu-_sootvetstvuyushchey-organizatsii_-inostrannogo-gosudar',
        '17-o-raskrytii-emitentom-ezhekvartalnogo-otcheta_-soobshcheniya-o-poryadke-dostupa-k-insayderskoy-informatsii_-soderzhashcheysya-v-dokumente-litsa',
        '17-o-svedeniyakh_-napravlyaemykh-ili-predostavlyaemykh-emitentom-sootvetstvuyushchemu-organu-_sootvetstvuyushchey-organizatsii_-inostrannogo-gosudar',
    );

    public function up(){
        $ob = \CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => DOCUMENTS_IBLOCK_ID_RU,
                'PROPERTY_FILE_TYPE' => $this->badFileTypes
            ),
            false,
            false,
            array('ID')
        );
        while ($el = $ob->fetch()) {
            if (\CIBlockElement::SetPropertyValues($el['ID'], DOCUMENTS_IBLOCK_ID_RU, 'pdf', 'FILE_TYPE')) {
                $this->outSuccess('Элемент '. $el['ID'] . ' исправлен');
            }
        }
    }

    public function down(){
        $helper = new IblockHelper();
    }

}
