<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
/**
 * Для запуска из консоли:
 * php local/php_interface/migrations/migrate.php execute Version20171219092630 --up
 * php local/php_interface/migrations/migrate.php execute Version20171219092630 --down
 */
class Version20171219092630 extends Version {
    protected $description = "Перенос подразделов из раздела \"Эмиссионные документы\" в корень ИБ \"Документы\" RU версия";
    protected $props = array(
        "RU" => array(
            'emiss-doc',
        )    
    );
    
    public function up(){
        $cIBlockElement = new \CIBlockElement();
        $cIBlockSection = new \CIBlockSection();
        $cFile = new \CFile();
        foreach ($this->props as $lang => $code) {
            $dump = array();
            $secId = false;
            //данные корневого раздела
            $rs = \CIBlockSection::GetList(
                array(), 
                array("IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang), "CODE" => $code),
                false,
                array("ID", "NAME", "CODE")
            );

            while ($section = $rs->Fetch()) {
                $secId = $section['ID'];
                $dump[$section["ID"]]["ID"] = $section["ID"];
                $dump[$section["ID"]]["CODE"] = $section["CODE"];
                $dump[$section["ID"]]["NAME"] = $section["NAME"];
            }
            //подразделы
            if ($secId) {
                $sections = array();
                $rs = \CIBlockSection::GetList(
                    array(), 
                    array("IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang), "SECTION_ID" => $secId),
                    false,
                    array("ID", "NAME", "CODE")
                );

                while ($section = $rs->Fetch()) {
                    $arFilds = array(
                        "ID" => $section["ID"],
                        "CODE" => $section["CODE"],
                        "NAME" => $section["NAME"],
                    );
                    $sections[$lang][$section["ID"]] = $arFilds;
                    $dump[$secId]["SUBSECTION"][$section["ID"]] = $arFilds;
                }
                if (count($sections[$lang]) > 0) {
                    foreach ($sections[$lang] as $id => $prop) {
                        $arSection = array("NAME" => $prop["NAME"], "CODE" => $prop["CODE"]. '-' . $lang, "IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang));
                        if ($sections[$lang][$id]["NEW_ID"] = $cIBlockSection->add($arSection)) {
                            $dump[$secId]["SUBSECTION"][$id]['NEW_ID'] = $sections[$lang][$id]["NEW_ID"];
                            $this->outSuccess('Раздел '. $prop['NAME'] . ' добавлен');
                        } else {
                            $this->outError($cIBlockSection->LAST_ERROR);
                            
                            return false;
                        }
                    }

                    foreach ($sections[$lang] as $section) {
                        $rs = $cIBlockElement->GetList(
                            array(),
                            array(
                                "IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang),
                                "SECTION_ID" => $section['ID']
                            ),
                            false,
                            false,
                            array(
                                'ID', 'NAME', 'CODE', 'ACTIVE', 'SORT', 'IBLOCK_SECTION_ID',
                                'DATE_ACTIVE_FROM', 'PROPERTY_FILE'
                            )
                        );

                        while ($element = $rs->Fetch()) {
                            $fileId = $cFile->SaveFile(\CFile::MakeFileArray($element['PROPERTY_FILE_VALUE']), 'iblock');
                            $fileType = pathinfo($cFile->GetPath($fileId), PATHINFO_EXTENSION);
                            $err = false;
                            $arFields = array(
                                'IBLOCK_ID' => constant("DOCUMENTS_IBLOCK_ID_" . $lang),
                                'IBLOCK_SECTION_ID' => $section['NEW_ID'],
                                'NAME' => $element['NAME'],
                                'CODE' => $element['CODE'],
                                'ACTIVE' => $element['ACTIVE'],
                                'DATE_ACTIVE_FROM' => $element['DATE_ACTIVE_FROM'],
                                'SORT' => $element['SORT'],
                                'PROPERTY_VALUES' => array(
                                    'FILE' => $fileId,
                                    'FILE_TYPE' => $fileType,
                                    'PAGE_URL' => ''
                                )
                            );

                            if ($cIBlockElement->Add($arFields)) {
                                $this->outSuccess('Элемент '. $element['NAME'] . ' перенесен');
                            } else {
                                $err = true;
                                $this->outError('Элемент '. $element['NAME'] . ' не удалось перенести');
                            }
                        }
                        if ($err === false) {
                            $this->outSuccess('Элементы перенесены в раздел '. $section['NEW_ID']);
                        }
                    }
                } else {
                    $this->outError("Не удалось найти нужные разделы");

                    return false;
                }
            } else {
                $this->outError("Не удалось найти нужный раздел");
                
                return false;
            }
            //дамп данных
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/migrations/migration_Version20171219092630_' . $lang . '.txt', var_export(json_encode($dump, JSON_UNESCAPED_UNICODE), true));
            
            //удаление раздела
            if (\CIBlockSection::delete($secId)) {
                $this->outSuccess("Исходный раздел удален");
            } else {
                $this->outError("Не удалось удалить исходный раздел");
            }
            
            //смена символьного кода
            foreach ($sections[$lang] as $id => $prop) {
                if ($cIBlockSection->Update($prop["NEW_ID"], array("CODE" => $prop["CODE"]))) {
                    $this->outSuccess('Раздел '. $prop['NAME'] . ' обновлен');
                } else {
                    $this->outError('Не удалось обновить раздел ' . $prop['NAME']);
                }
            }
        }
    }

    public function down(){
        $cIBlockElement = new \CIBlockElement();
        $cIBlockSection = new \CIBlockSection();
        $cFile = new \CFile();
        foreach ($this->props as $lang => $code) {
            $file = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/migrations/migration_Version20171219092630_' . $lang . '.txt');
            if ($file) {
                $dump = json_decode(substr($file, 1, -1), true);
                $reCode = array();
                foreach ($dump as $sec) {//по корневым разделам
                    $arSec = array("NAME" => $sec["NAME"], "CODE" => $sec["CODE"], "IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang));
                    if ($parentId = $cIBlockSection->add($arSec)) {//добавляем корневой раздел
                        $this->outSuccess('Раздел '. $sec['NAME'] . ' добавлен');
                        foreach ($sec['SUBSECTION'] as $subSection) {//по подразделам
                            $subSec = array("NAME" => $subSection["NAME"], "CODE" => $subSection["CODE"] . $lang, "IBLOCK_SECTION_ID" => $parentId, "IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang));
                            if ($nowSec = $cIBlockSection->add($subSec)) {//добавляем подразделы
                                $reCode[$nowSec] = $subSection["CODE"];
                                
                                $rs = $cIBlockElement->GetList(
                                    array(),
                                    array(
                                        "IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang),
                                        "SECTION_ID" => $subSection["NEW_ID"]
                                    ),
                                    false,
                                    false,
                                    array(
                                        'ID', 'NAME', 'CODE', 'ACTIVE', 'SORT', 'IBLOCK_SECTION_ID',
                                        'DATE_ACTIVE_FROM', 'PROPERTY_FILE'
                                    )
                                );

                                while ($element = $rs->Fetch()) {
                                    $fileId = $cFile->SaveFile(\CFile::MakeFileArray($element['PROPERTY_FILE_VALUE']), 'iblock');
                                    $fileType = pathinfo($cFile->GetPath($fileId), PATHINFO_EXTENSION);
                                    $err = false;
                                    $arFields = array(
                                        'IBLOCK_ID' => constant("DOCUMENTS_IBLOCK_ID_" . $lang),
                                        'IBLOCK_SECTION_ID' => $nowSec,
                                        'NAME' => $element['NAME'],
                                        'CODE' => $element['CODE'],
                                        'ACTIVE' => $element['ACTIVE'],
                                        'DATE_ACTIVE_FROM' => $element['DATE_ACTIVE_FROM'],
                                        'SORT' => $element['SORT'],
                                        'PROPERTY_VALUES' => array(
                                            'FILE' => $fileId,
                                            'FILE_TYPE' => $fileType,
                                            'PAGE_URL' => ''
                                        )
                                    );
                                    if ($cIBlockElement->Add($arFields)) {
                                        $this->outSuccess('Элемент '. $element['NAME'] . ' перенесен');
                                    } else {
                                        $err = true;
                                        $this->outError('Элемент '. $element['NAME'] . ' не удалось перенести');
                                    }
                                }
                            } else {
                                $this->outError($cIBlockSection->LAST_ERROR);

                                return false;
                            }
                            $this->outSuccess("удаляем раздел " . $subSection["NEW_ID"]);
                            if (\CIBlockSection::delete($subSection["NEW_ID"])) {
                                $this->outSuccess("Исходный раздел удален");
                            } else {
                                $this->outError("Не удалось удалить исходный раздел");
                            }
                            
                        }
                        foreach ($reCode as $id => $code) {
                            if ($cIBlockSection->Update($id, array("CODE" => $code))) {
                                $this->outSuccess('Раздел '. $code . ' обновлен');
                            } else {
                                $this->outError('Не удалось обновить раздел ' . $code);
                            }
                        }
                    } else {
                        $this->outError($cIBlockSection->LAST_ERROR);
                        
                        return false;
                    }
                }
                //удаляем дамп
                //unlink($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/migrations/migration_Version20171219092630_' . $lang . '.txt');
            } else {
                $this->outError('Файл migration_Version20171219092630_' . $lang . '.txt' . ' не удалось найти');
                
                return false;
            }
        }
    }
}
