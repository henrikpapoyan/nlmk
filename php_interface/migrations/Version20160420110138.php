<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160420110138 extends Version {

	protected $description = "Подразделы для разделов \"Презентации\" ИБ Документы (ru и en)";

	public function up()
	{
		$helper = new IblockHelper();

		$dbSectionsRu = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => DOCUMENTS_IBLOCK_ID_RU, "CODE" => "PRESENTATIONS"), false, array("ID", "IBLOCK_ID", "CODE"));
		if ($arSectionRu = $dbSectionsRu->GetNext())
			$presentationsIblockIdRu = $arSectionRu["ID"];

		$dbSectionsEn = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => DOCUMENTS_IBLOCK_ID_EN, "CODE" => "PRESENTATIONS"), false, array("ID", "IBLOCK_ID", "CODE"));
		if ($arSectionEn = $dbSectionsEn->GetNext())
			$presentationsIblockIdEn = $arSectionEn["ID"];

		if ($presentationsIblockIdRu)
		{
			$arSectionsRu = array(
				array("NAME" => "Мероприятия", "CODE" => "PRESENTATIONS_EVENTS", "IBLOCK_SECTION_ID" => $presentationsIblockIdRu),
				array("NAME" => "M&A", "CODE" => "PRESENTATIONS_MA", "IBLOCK_SECTION_ID" => $presentationsIblockIdRu),
				array("NAME" => "Результаты работы", "CODE" => "PRESENTATIONS_RESULTS", "IBLOCK_SECTION_ID" => $presentationsIblockIdRu),
				array("NAME" => "Презентации к визитам на производственные мощности", "CODE" => "PRESENTATIONS_VISITS", "IBLOCK_SECTION_ID" => $presentationsIblockIdRu),
			);

			foreach ($arSectionsRu as $arSection)
				$helper->addSection(DOCUMENTS_IBLOCK_ID_RU, $arSection);

		}

		if ($presentationsIblockIdEn)
		{
			$arSectionsEn = array(
				array("NAME" => "IR events", "CODE" => "PRESENTATIONS_EVENTS", "IBLOCK_SECTION_ID" => $presentationsIblockIdEn),
				array("NAME" => "M&A", "CODE" => "PRESENTATIONS_MA", "IBLOCK_SECTION_ID" => $presentationsIblockIdEn),
				array("NAME" => "Financial and operational results", "CODE" => "PRESENTATIONS_RESULTS", "IBLOCK_SECTION_ID" => $presentationsIblockIdEn),
				array("NAME" => "Visits", "CODE" => "PRESENTATIONS_VISITS", "IBLOCK_SECTION_ID" => $presentationsIblockIdEn),
			);

			foreach ($arSectionsEn as $arSection)
				$helper->addSection(DOCUMENTS_IBLOCK_ID_EN, $arSection);

		}

	}

	public function down()
	{
		$arSectionsId = array();
		$arSectionsCode = array("PRESENTATIONS_EVENTS", "PRESENTATIONS_MA", "PRESENTATIONS_RESULTS", "PRESENTATIONS_VISITS");

		$dbSectionsRu = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => DOCUMENTS_IBLOCK_ID_RU, "CODE" => $arSectionsCode), false, array("ID", "IBLOCK_ID"));
		while ($arSectionRu = $dbSectionsRu->GetNext())
			$arSectionsId[] = $arSectionRu["ID"];

		$dbSectionsEn = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => DOCUMENTS_IBLOCK_ID_EN, "CODE" => $arSectionsCode), false, array("ID", "IBLOCK_ID"));
		while ($arSectionEn = $dbSectionsEn->GetNext())
			$arSectionsId[] = $arSectionEn["ID"];

		foreach ($arSectionsId as $sectionId)
			\CIBlockSection::Delete($sectionId);

	}

}
