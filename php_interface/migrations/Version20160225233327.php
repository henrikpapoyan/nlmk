<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
use CIBlockProperty;

class Version20160225233327 extends Version {

	protected $description = "Изменение типа свойства \"Описание\" для ИБ Фотогалерея";

	public function up(){
		$arIblocks = array(
			PHOTOS_IBLOCK_ID_RU,
			PHOTOS_IBLOCK_ID_EN,
		);
		foreach ($arIblocks as $iblockId) {
			$dbProp = CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "DESCRIPTION")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("USER_TYPE" => "HTML"));
			}
		}
	}

	public function down(){
		$arIblocks = array(
			PHOTOS_IBLOCK_ID_RU,
			PHOTOS_IBLOCK_ID_EN,
		);
		foreach ($arIblocks as $iblockId) {
			$dbProp = CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "DESCRIPTION")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("USER_TYPE" => ""));
			}
		}
	}

}
