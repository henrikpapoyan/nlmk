<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171215164647 extends Version {

    protected $description = "Добавление типа меню \"Компании группы\"";

    public function up(){
        $menuTypes = GetMenuTypes();
        $menuTypes['group_companies'] = "Меню групп компании";
        SetMenuTypes($menuTypes);

    }

    public function down(){
        $menuTypes = GetMenuTypes();
        unset($menuTypes['group_companies']);
        SetMenuTypes($menuTypes);
    }

}
