<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
/**
 * Для запуска из консоли:
 * php local/php_interface/migrations/migrate.php execute Version20171215094504 --up
 * php local/php_interface/migrations/migrate.php execute Version20171215094504 --down
 */
class Version20171215094504 extends Version {
    protected $description = "Добавляем свойство \"Дата публикации\" в ИБ \"Документы\" RU и EN версии";

    protected $props = array(
        "RU" => array(
            "NAME" => "Дата публикации",
            "CODE" => "DATE_OF_PUBLICATION"
        ),
        "EN" => array(
            "NAME" => "Date of publication",
            "CODE" => "DATE_OF_PUBLICATION"
        )
    );

    public function up(){
        $iblockproperty = new \CIBlockProperty;
        foreach ($this->props as $lang => $prop) {
            $arFields = Array(
                "NAME" => $prop["NAME"],
                "ACTIVE" => "Y",
                "SORT" => "500",
                "CODE" => $prop["CODE"],
                "PROPERTY_TYPE" => "S",
                "IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang),
                "USER_TYPE" => "Date",
            );
            
            if ($iblockproperty->Add($arFields)) {
                $this->outSuccess("Свойство " . $prop["NAME"] . " создано");
            } else {
                $this->outError("Не удалось создать свойство" . $prop["NAME"]);
                
                return false;
            }
        }
    }

    public function down(){
        foreach ($this->props as $lang => $prop) {
            $arFilter = array(
                "IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang),
                "CODE" => $prop["CODE"]
            );
            $properties = \CIBlockProperty::GetList(array(), $arFilter);
            while ($arProp = $properties->GetNext())
            {
                if (\CIBlockProperty::Delete($arProp["ID"])) {
                    $this->outSuccess("Свойство " . $prop["NAME"] . " удалено");
                } else {
                    $this->outError("Не удалось удалить свойство" . $prop["NAME"]);
                    
                    return false;
                }
            }       
        }
    }

}
