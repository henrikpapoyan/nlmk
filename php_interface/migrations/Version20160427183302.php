<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160427183302 extends Version {

    protected $description = "Создание служебного ИБ для корпоративного журнала";

    public function up()
    {
        global $APPLICATION;
        $path = $_SERVER['DOCUMENT_ROOT']."/local/php_interface/migrations/xml";

        $xmlSRC = $path."/corporate_magazine.xml";
        $strImportResult = ImportXMLFile($xmlSRC, "mediacenter", ["s1", "s2"], "N", "N");

        if($strImportResult)
            $this->outSuccess('Инфоблок "Корпоративный журнал" добавлен');
        else
            $APPLICATION->ThrowException('Импорт завершен с ошибкой');

    }

    public function down(){
        $helper = new IblockHelper();
    }

}