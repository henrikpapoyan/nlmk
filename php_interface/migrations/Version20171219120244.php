<?
namespace Sprint\Migration;

use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171219120244 extends Version
{
    protected $description = 'Создание и заполнение ИБ со страницы "Структура группы снабжения"';
    protected $elements    = ['vice-president-po-snab'               => ['FIELDS'     => ['NAME' => 'ВИЦЕ-ПРЕЗИДЕНТ ПО СНАБЖЕНИЮ',
                                                                                          'CODE' => 'vice-president-po-snab'],
                                                                         'PROPERTIES' => ['SUBDIVISION_BUSINESS'         => '<p>
                                                                                                                            Руководство службой снабжения посредством организованных в единую структуру подразделений:
                                                                                                                        </p>
                                                                                                                        <ul class="list list_flat">
                                                                                                                            <li class="list__item">Дирекции по инвестиционным закупкам (CAPEX)</li>
                                                                                                                            <li class="list__item">Дирекции по операционным закупкам (OPEX)</li>
                                                                                                                            <li class="list__item">Дирекции по развитию снабжения</li>
                                                                                                                        </ul>',
                                                                                          'SUBORDINATE_SUBDIVISION_NAME' => [0 => 'Представительство ПАО «НЛМК» Москва']]],
                              'direkcia-po-operacionnim-zakupkam'    => ['FIELDS' => ['NAME' => 'ДИРЕКЦИЯ ПО ОПЕРАЦИОННЫМ ЗАКУПКАМ',
                                                                                      'CODE' => 'direkcia-po-operacionnim-zakupkam',
                                                                                      'SORT' => 100],
                                                                         'PARENT' => 'vice-president-po-snab'],
                              'direkcia-po-investicionnim-zakupkam'  => ['FIELDS' => ['NAME' => 'ДИРЕКЦИЯ ПО ИНВЕСТИЦИОННЫМ ЗАКУПКАМ',
                                                                                      'CODE' => 'direkcia-po-investicionnim-zakupkam',
                                                                                      'SORT' => 200],
                                                                         'PARENT' => 'vice-president-po-snab'],
                              'direkcia-po-razvitiuy-snab'           => ['FIELDS' => ['NAME' => 'ДИРЕКЦИЯ ПО РАЗВИТИЮ СНАБЖЕНИЯ',
                                                                                      'CODE' => 'direkcia-po-razvitiuy-snab',
                                                                                      'SORT' => 300],
                                                                         'PARENT' => 'vice-president-po-snab'],
                              'upravlenie-snabjenia'                 => ['FIELDS' => ['NAME' => 'УПРАВЛЕНИЕ СНАБЖЕНИЯ',
                                                                                      'CODE' => 'upravlenie-snabjenia',
                                                                                      'SORT' => 400],
                                                                         'PARENT' => 'vice-president-po-snab'],
                              'otdel-soprovogdenia-zakupok'          => ['FIELDS'     => ['NAME' => 'ОТДЕЛ СОПРОВОЖДЕНИЯ ЗАКУПОК',
                                                                                          'CODE' => 'otdel-soprovogdenia-zakupok',
                                                                                          'SORT' => 500],
                                                                         'PROPERTIES' => ['SUBDIVISION_BUSINESS'         => '<p>Оформление договоров, дополнительных соглашений, спецификаций,  а также проводит другие мероприятия, направленные на поддержку категорий в договорной работе.</p>',
                                                                                          'SUBORDINATE_SUBDIVISION_NAME' => [0 => 'Представительство ПАО «НЛМК» (Москва)',
                                                                                                                             1 => 'ПАО «НЛМК» (Липецк)']],
                                                                         'PARENT'     => 'vice-president-po-snab'],
                              'director-po-zakupke-orex'             => ['FIELDS'     => ['NAME' => 'ДИРЕКТОР ПО ЗАКУПКАМ OPEX',
                                                                                          'CODE' => 'director-po-zakupke-orex',
                                                                                          'SORT' => 100],
                                                                         'PROPERTIES' => ['SUBDIVISION_BUSINESS'         => '<p>Централизованное управление глобальными стратегическими категориями в операционных закупках.</p>',
                                                                                          'SUBORDINATE_SUBDIVISION_NAME' => [0 => 'Представительство ПАО «НЛМК» Москва']],
                                                                         'PARENT'     => 'direkcia-po-operacionnim-zakupkam',
                                                                         'COMPANIES'  => [0,
                                                                                          2,
                                                                                          1]],
                              'nachalnik-uprav-po-zakupke-orex'      => ['FIELDS' => ['NAME' => 'НАЧАЛЬНИК УПРАВЛЕНИЯ ЗАКУПОК OPEX',
                                                                                      'CODE' => 'nachalnik-uprav-po-zakupke-orex',
                                                                                      'SORT' => 200],
                                                                         'PARENT' => 'direkcia-po-operacionnim-zakupkam'],
                              'nachalnik-otdela-zakupok-orex'        => ['FIELDS' => ['NAME' => 'НАЧАЛЬНИК ОТДЕЛА ЗАКУПОК OPEX',
                                                                                      'CODE' => 'nachalnik-otdela-zakupok-orex',
                                                                                      'SORT' => 300],
                                                                         'PARENT' => 'direkcia-po-operacionnim-zakupkam'],
                              'specialist-po-zakupkam-1'             => ['FIELDS' => ['NAME' => 'СПЕЦИАЛИСТ ПО ЗАКУПКАМ',
                                                                                      'CODE' => 'specialist-po-zakupkam-1'],
                                                                         'PARENT' => 'direkcia-po-operacionnim-zakupkam'],
                              'director-po-zakupke-carex'            => ['FIELDS'     => ['NAME' => 'ДИРЕКТОР ПО ЗАКУПКАМ CAPEX',
                                                                                          'CODE' => 'director-po-zakupke-carex',
                                                                                          'SORT' => 100],
                                                                         'PROPERTIES' => ['SUBDIVISION_BUSINESS'         => '<p>Централизованное управление глобальными стратегическими категориями в инвестиционных закупках.</p>',
                                                                                          'SUBORDINATE_SUBDIVISION_NAME' => [0 => 'Представительство ПАО «НЛМК» Москва']],
                                                                         'PARENT'     => 'direkcia-po-investicionnim-zakupkam',
                                                                         'COMPANIES'  => [3,
                                                                                          5,
                                                                                          4]],
                              'nachalnik-upravlenia-zakupok-capex'   => ['FIELDS' => ['NAME' => 'НАЧАЛЬНИК УПРАВЛЕНИЯ ЗАКУПОК САРЕХ',
                                                                                      'CODE' => 'nachalnik-upravlenia-zakupok-capex',
                                                                                      'SORT' => 200],
                                                                         'PARENT' => 'direkcia-po-investicionnim-zakupkam'],
                              'nachalnik-otdela-zakupok-capex'       => ['FIELDS' => ['NAME' => 'НАЧАЛЬНИК ОТДЕЛА ЗАКУПОК САРЕХ',
                                                                                      'CODE' => 'nachalnik-otdela-zakupok-capex',
                                                                                      'SORT' => 300],
                                                                         'PARENT' => 'direkcia-po-investicionnim-zakupkam'],
                              'specialist-po-zakupkam-2'             => ['FIELDS' => ['NAME' => 'СПЕЦИАЛИСТ ПО ЗАКУПКАМ',
                                                                                      'CODE' => 'specialist-po-zakupkam-2'],
                                                                         'PARENT' => 'direkcia-po-investicionnim-zakupkam'],
                              'director-po-razvitiuy-snabgenia'      => ['FIELDS'     => ['NAME' => 'ДИРЕКТОР ПО РАЗВИТИЮ СНАБЖЕНИЯ',
                                                                                          'CODE' => 'director-po-razvitiuy-snabgenia'],
                                                                         'PROPERTIES' => ['SUBDIVISION_BUSINESS'         => '<p>Методология развития снабжения, мониторинг ключевых показателей  эффективности, отчетность, внедрение ИТ  проектов.</p>',
                                                                                          'SUBORDINATE_SUBDIVISION_NAME' => [0 => 'Представительство ПАО «НЛМК» Москва']],
                                                                         'PARENT'     => 'direkcia-po-razvitiuy-snab'],
                              'nachalnik-upravlenia-snabgenia'       => ['FIELDS'     => ['NAME' => 'НАЧАЛЬНИК УПРАВЛЕНИЯ СНАБЖЕНИЯ',
                                                                                          'CODE' => 'nachalnik-upravlenia-snabgenia'],
                                                                         'PROPERTIES' => ['SUBDIVISION_BUSINESS'         => '<p>Представительство снабжения на каждом предприятии группы НЛМК. Снабжение материалами, закупаемыми локально. Заказы и поставки.</p>',
                                                                                          'SUBORDINATE_SUBDIVISION_NAME' => [0 => 'НЛМК-Метиз',
                                                                                                                             1 => 'ПАО «НЛМК» (Липецк)',
                                                                                                                             2 => 'Стойленский ГОК (Старый Оскол)',
                                                                                                                             3 => 'НЛМК- Калуга (с.Ворсино)',
                                                                                                                             4 => 'Алтай-кокс НЛМК-Урал',
                                                                                                                             5 => 'НЛМК Урал-сервис',
                                                                                                                             6 => 'ВИЗ-Сталь']],
                                                                         'PARENT'     => 'upravlenie-snabjenia'],
                              'nachalnik-otdela-lokalnih-zakupok'    => ['FIELDS' => ['NAME' => 'НАЧАЛЬНИК ОТДЕЛА ЛОКАЛЬНЫХ ЗАКУПОК',
                                                                                      'CODE' => 'nachalnik-otdela-lokalnih-zakupok'],
                                                                         'PARENT' => 'nachalnik-upravlenia-snabgenia'],
                              'nachalnik-otdela-podderjki-snabjehia' => ['FIELDS'     => ['NAME' => 'НАЧАЛЬНИК ОТДЕЛА ПОДДЕРЖКИ СНАБЖЕНИЯ',
                                                                                          'CODE' => 'nachalnik-otdela-podderjki-snabjehia',
                                                                                          'SORT' => 100],
                                                                         'PROPERTIES' => ['SUBDIVISION_BUSINESS'         => '<p>Планирование поставок, размещение заказов и их мониторинг, входящую поставку и приемку.</p>',
                                                                                          'SUBORDINATE_SUBDIVISION_NAME' => [0 => 'НЛМК-Метиз',
                                                                                                                             1 => 'ПАО «НЛМК» (Липецк)',
                                                                                                                             2 => 'Стойленский ГОК (Старый Оскол)',
                                                                                                                             3 => 'НЛМК- Калуга (с.Ворсино)',
                                                                                                                             4 => 'Алтай-кокс НЛМК-Урал',
                                                                                                                             5 => 'НЛМК Урал-сервис',
                                                                                                                             6 => 'ВИЗ-Сталь']],
                                                                         'PARENT'     => 'nachalnik-upravlenia-snabgenia'],
                              'specialist-po-zakupkam-3'             => ['FIELDS' => ['NAME' => 'СПЕЦИАЛИСТ ПО ЗАКУПКАМ',
                                                                                      'CODE' => 'specialist-po-zakupkam-3'],
                                                                         'PARENT' => 'nachalnik-otdela-lokalnih-zakupok'],
                              'specialist-podderjki-snabjenia'       => ['FIELDS' => ['NAME' => 'СПЕЦИАЛИСТ ПОДДЕРЖКИ СНАБЖЕНИЯ',
                                                                                      'CODE' => 'specialist-podderjki-snabjenia'],
                                                                         'PARENT' => 'nachalnik-otdela-podderjki-snabjehia']];
    protected $companies   = [['FIELDS'     => ['NAME' => 'Представительство ПАО «НЛМК» (Москва)'],
                               'PROPERTIES' => ['CITY'        => 'Москва',
                                                'COUNTRY'     => 'Россия',
                                                'DESCRIPTION' => '<p>Элемент страницы "supply-service-structure"</p>'],
                               'INDEX'      => '1',
                               'PRODUCTION' => ['Валки листовые и сортовые',
                                                'ГСМ',
                                                'Электроды графитированные',
                                                'Кабельная продукция',
                                                'Насосная продукция',
                                                'Электродвигатели',
                                                'Огнеупорные материалы',
                                                'Покрытия',
                                                'Упаковочные материалы',
                                                'Пилопродукция',
                                                'Химические материалы технологические',
                                                'Ферросплавы',
                                                'Цветные металлы',
                                                'Алюминий',
                                                'Лом меди',
                                                'Свинец',
                                                'Лом и отходы титана',
                                                'Материалы верхнего строения пути',
                                                'Кислота серная',
                                                'Пековый кокс',
                                                'Пек гранулированный',
                                                'Бензол',
                                                'Шунгит',
                                                'Гидроантрацит',
                                                'Агломераты',
                                                'Плавиковый шпат',
                                                'Кварцит',],
                               'ADDITIONAL' => [2895,
                                                2892]],

                              ['FIELDS'     => ['NAME' => 'Стойленский ГОК (Старый Оскол)'],
                               'PROPERTIES' => ['CITY'        => 'Старый Оскол',
                                                'COUNTRY'     => 'Россия',
                                                'DESCRIPTION' => '<p>Элемент страницы "supply-service-structure"</p>'],
                               'INDEX'      => '1',
                               'PRODUCTION' => ['Автошины',
                                                'Мелющие тела']],

                              ['FIELDS'     => ['NAME' => 'ПАО «НЛМК» (Липецк)'],
                               'PROPERTIES' => ['CITY'        => 'Липецк',
                                                'COUNTRY'     => 'Россия',
                                                'DESCRIPTION' => '<p>Элемент страницы "supply-service-structure"</p>'],
                               'INDEX'      => '1',
                               'PRODUCTION' => ['Инструмент',
                                                'Аккумуляторы для инструмента',
                                                'Светотехнические материалы',
                                                'Высоковольтное оборудование',
                                                'Противопожарное оборудование и материалы',
                                                'Приборы и системы измерения параметров металла',
                                                'Контрольно-измерительные приборы и автоматика',
                                                'Лабораторное оборудование',
                                                'Электрические системы контроля',
                                                'Электроника и компоненты',
                                                'Спецодежда и СИЗ',
                                                'Подшипники',
                                                'РТИ',
                                                'Ленты конвейерные',
                                                'Рукава',
                                                'Метизы, цвет.прокат',
                                                'Цветной прокат',
                                                'Металлопрокат',
                                                'Трубы',
                                                'Строительные материалы',
                                                'Низковольтное ОБРД',
                                                'Системы весоизмерения',
                                                'Электромонтажные изделия',
                                                'ОБРД, запчасти для информ.технологий и програм. обеспечение',
                                                'Оборудование, запасные части для автоматизации',
                                                'Оборудование Ж/Д автоматизации',]],

                              ['FIELDS'     => ['NAME' => 'Представительство ПАО «НЛМК» (Москва)'],
                               'PROPERTIES' => ['CITY'        => 'Москва',
                                                'COUNTRY'     => 'Россия',
                                                'DESCRIPTION' => '<p>Элемент страницы "supply-service-structure"</p>'],
                               'INDEX'      => '2',
                               'PRODUCTION' => ['ТПА']],

                              ['FIELDS'     => ['NAME' => 'Стойленский ГОК (Старый Оскол)'],
                               'PROPERTIES' => ['CITY'        => 'Старый Оскол',
                                                'COUNTRY'     => 'Россия',
                                                'DESCRIPTION' => '<p>Элемент страницы "supply-service-structure"</p>'],
                               'INDEX'      => '2',
                               'PRODUCTION' => ['Дробильное оборудование',
                                                'Обогатительное оборудование',
                                                'Шахтное оборудование',
                                                'Оборудование для карьеров, карьерная техника',
                                                'ЗИП для карьерной техники и ОБРД',]],

                              ['FIELDS'     => ['NAME' => 'ПАО «НЛМК» (Липецк)'],
                               'PROPERTIES' => ['CITY'        => 'Липецк',
                                                'COUNTRY'     => 'Россия',
                                                'DESCRIPTION' => '<p>Элемент страницы "supply-service-structure"</p>'],
                               'INDEX'      => '2',
                               'PRODUCTION' => ['Нестандартные запчасти к технологическому ОБРД',
                                                'Общезаводское подъемно-транспортное оборудование',
                                                'Редукторы и запасные части к ним',
                                                'Станочное оборудование',
                                                'Гидравлическое и смазочное оборудование',
                                                'Пневматическое оборудование',
                                                'Технологический транспорт',
                                                'Ломоперерабатывающее оборудование',
                                                'Автомобильный транспорт',
                                                'Железнодорожный транспорт',
                                                'Водный транспорт',
                                                'Воздушный транспорт',
                                                'Аккум.для транспорта',
                                                'Краны технологические грузоподъёмные и запасные части к ним',
                                                'Конвейеры и запасные части к ним',
                                                'Фильтры газоочистные комплектные и запасные части к ним',
                                                'ОБРД, запчасти для информ.технологий и програм. обеспечение',
                                                'Вентиляторы и ЗЧ к ним',
                                                'Котельное и турбинное ОБРД и ОБРД водоочистки',
                                                'Нестандартизированные ЗЧ к технологич.и энергетическому ОБРД',
                                                'Теплообменное и емкостное оборудование',
                                                'Энергетическое ОБРД и трубопроводы ВД',
                                                'Компрессоры и запасные части к ним',
                                                'Промышленные кондиционеры и ЗЧ к ним',
                                                'Сменное оборудование',
                                                'Коксующиеся угли',
                                                'Кокс доменный',
                                                'Коксовая мелочь',
                                                'Коксовый орешек',
                                                'Технологическое ОБРД',],
                               'ADDITIONAL' => [5683]]];
    protected $production  = [//Представительство ПАО «НЛМК» (Москва)
                              'NLMK-Moscow-1'      => [['FIELDS'     => ['NAME' => 'Валки листовые и сортовые'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'ГСМ'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Электроды графитированные'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Кабельная продукция'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Насосная продукция'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Электродвигатели'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Огнеупорные материалы'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Покрытия'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Упаковочные материалы'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Пилопродукция'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Химические материалы технологические'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Ферросплавы'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Цветные металлы'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Алюминий'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Лом меди'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Свинец'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Лом и отходы титана'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Материалы верхнего строения пути'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Кислота серная'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Пековый кокс'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Пек гранулированный'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Бензол'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Шунгит'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Гидроантрацит'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Агломераты'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Плавиковый шпат'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Кварцит'],
                                                        'PROPERTIES' => ['TYPE' => 5678]]],
                              //endsПредставительство ПАО «НЛМК» (Москва)

                              //Стойленский ГОК (Старый Оскол)
                              'GOK-Stariy-Oskol-1' => [['FIELDS'     => ['NAME' => 'Автошины'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Мелющие тела'],
                                                        'PROPERTIES' => ['TYPE' => 5678]]],
                              //ends Стойленский ГОК (Старый Оскол)

                              //ПАО «НЛМК» (Липецк)
                              'NLMK-Lipeck-1'      => [['FIELDS'     => ['NAME' => 'Инструмент'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Аккумуляторы для инструмента'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Светотехнические материалы'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Высоковольтное оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Противопожарное оборудование и материалы'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Приборы и системы измерения параметров металла'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Контрольно-измерительные приборы и автоматика'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Лабораторное оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Электрические системы контроля'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Электроника и компоненты'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Спецодежда и СИЗ'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Подшипники'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'РТИ'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Ленты конвейерные'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Рукава'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Метизы, цвет.прокат'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Цветной прокат'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Металлопрокат'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Трубы'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Строительные материалы'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Низковольтное ОБРД'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Системы весоизмерения'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Электромонтажные изделия'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'ОБРД, запчасти для информ.технологий и програм. обеспечение'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Оборудование, запасные части для автоматизации'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Оборудование Ж/Д автоматизации'],
                                                        'PROPERTIES' => ['TYPE' => 5678]]],
                              //ends ПАО «НЛМК» (Липецк)

                              //Представительство ПАО «НЛМК» (Москва) - 2
                              'NLMK-Moscow-2'      => [['FIELDS'     => ['NAME' => 'ТПА'],
                                                        'PROPERTIES' => ['TYPE' => 5678]]],
                              //ends Представительство ПАО «НЛМК» (Москва) - 2

                              //Стойленский ГОК (Старый Оскол) - 2
                              'GOK-Stariy-Oskol-2' => [['FIELDS'     => ['NAME' => 'Дробильное оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Обогатительное оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Шахтное оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Оборудование для карьеров, карьерная техника'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'ЗИП для карьерной техники и ОБРД'],
                                                        'PROPERTIES' => ['TYPE' => 5678]]],
                              //ends Стойленский ГОК (Старый Оскол) - 2

                              //ПАО «НЛМК» (Липецк) - 2
                              'NLMK-Lipeck-2'      => [['FIELDS'     => ['NAME' => 'Нестандартные запчасти к технологическому ОБРД'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Общезаводское подъемно-транспортное оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Редукторы и запасные части к ним'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Станочное оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Гидравлическое и смазочное оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Пневматическое оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Технологический транспорт'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Ломоперерабатывающее оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Автомобильный транспорт'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Железнодорожный транспорт'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Водный транспорт'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Воздушный транспорт'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Аккум.для транспорта'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Краны технологические грузоподъёмные и запасные части к ним'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Конвейеры и запасные части к ним'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Фильтры газоочистные комплектные и запасные части к ним'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Вентиляторы и ЗЧ к ним'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Котельное и турбинное ОБРД и ОБРД водоочистки'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Нестандартизированные ЗЧ к технологич.и энергетическому ОБРД'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Теплообменное и емкостное оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Энергетическое ОБРД и трубопроводы ВД'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Компрессоры и запасные части к ним'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Промышленные кондиционеры и ЗЧ к ним'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Сменное оборудование'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Коксующиеся угли'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Кокс доменный'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Коксовая мелочь'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Коксовый орешек'],
                                                        'PROPERTIES' => ['TYPE' => 5678]],
                                                       ['FIELDS'     => ['NAME' => 'Технологическое ОБРД'],
                                                        'PROPERTIES' => ['TYPE' => 5678]]],
                              //ends ПАО «НЛМК» (Липецк) - 2
    ];

    const IB_NAME       = 'Структура группы снабжения';
    const IB_CODE       = 'SUPPLY_SERVICE_STRUCTURE_RU';
    const IB_COMPANIES  = 21;
    const IB_PRODUCTION = 41;

    public function up() {
        $IblockHelper = new IblockHelper();

        $iblockId = $IblockHelper->addIblockIfNotExists(['ACTIVE'          => 'Y',
                                                         'NAME'            => self::IB_NAME,
                                                         'CODE'            => self::IB_CODE,
                                                         'LIST_PAGE_URL'   => '',
                                                         'DETAIL_PAGE_URL' => '',
                                                         'IBLOCK_TYPE_ID'  => 'activities',
                                                         'SITE_ID'         => ['s1'],
                                                         'SORT'            => 500,
                                                         'DESCRIPTION'     => '']);

        if ($iblockId) {
            $this->outSuccess("Добавлен ИБ \"" . self::IB_NAME . "\"");
            //добавляем необходимую продукцию в ИБ 41
            foreach ($this->production as &$companyProducts) {
                foreach ($companyProducts as &$productInfo) {
                    $productInfo['FIELDS']['CODE']                = \CUtil::translit($productInfo['FIELDS']['NAME'], 'ru');
                    $productInfo['ID']                            = $IblockHelper->addElement(self::IB_PRODUCTION, $productInfo['FIELDS'], $productInfo['PROPERTIES']);
                    $productsList[$productInfo['FIELDS']['CODE']] = $productInfo;
                }
            }

            //новое св-во для ИБ компаний
            $companyIbProperty = ['IBLOCK_ID'      => self::IB_COMPANIES,
                                  'NAME'           => 'Ссылка на сайт компании',
                                  'ACTIVE'         => 'Y',
                                  'SORT'           => '500',
                                  'CODE'           => 'COMPANY_SITE_LINK',
                                  'PROPERTY_TYPE'  => 'S',
                                  'LINK_IBLOCK_ID' => self::IB_COMPANIES,
                                  'ROW_COUNT'      => '1',
                                  'COL_COUNT'      => '30',
                                  'LIST_TYPE'      => 'L',
                                  'MULTIPLE'       => 'N',
                                  'IS_REQUIRED'    => 'N',
                                  'FILTRABLE'      => 'N',];
            $IblockHelper->addPropertyIfNotExists(self::IB_COMPANIES, $companyIbProperty);

            //добавляем 6 компаний
            foreach ($this->companies as &$company) {
                $company['FIELDS']['CODE']            = \CUtil::translit($company['FIELDS']['NAME'], 'ru') . '_'
                                                        . $company['INDEX'];
                $company['FIELDS']['PREVIEW_PICTURE'] = \CFile::MakeFileArray('/local/templates/nlmk/images/procurement-mask.jpg');
                foreach ($company['PRODUCTION'] as $productName) {
                    $productCode                           = \CUtil::translit($productName, 'ru');
                    $company['PROPERTIES']['PRODUCTION'][] = $productsList[$productCode]['ID'];
                }
                if ($company['ADDITIONAL']) {
                    foreach ($company['ADDITIONAL'] as $prodId) {
                        if (!in_array($prodId, $company['PROPERTIES']['PRODUCTION'])) {
                            $company['PROPERTIES']['PRODUCTION'][] = $prodId;
                        }
                    }
                }
                $company['ID'] = $IblockHelper->addElement(self::IB_COMPANIES, $company['FIELDS'], $company['PROPERTIES']);
                $companiesList[] = $company;
            }

            //Свойства для нового ИБ
            $properties = [['IBLOCK_ID'     => $iblockId,
                            'NAME'          => 'Деятельность подразделения',
                            'ACTIVE'        => 'Y',
                            'SORT'          => '100',
                            'CODE'          => 'SUBDIVISION_BUSINESS',
                            'PROPERTY_TYPE' => 'S',
                            'USER_TYPE'     => 'HTML',
                            'ROW_COUNT'     => '3',
                            'COL_COUNT'     => '45',
                            'LIST_TYPE'     => 'L',
                            'MULTIPLE'      => 'N',
                            'IS_REQUIRED'   => 'N',
                            'FILTRABLE'     => 'N',],
                           ['IBLOCK_ID'     => $iblockId,
                            'NAME'          => 'Название подведомственного подразделения',
                            'ACTIVE'        => 'Y',
                            'SORT'          => '200',
                            'CODE'          => 'SUBORDINATE_SUBDIVISION_NAME',
                            'PROPERTY_TYPE' => 'S',
                            'ROW_COUNT'     => '1',
                            'COL_COUNT'     => '30',
                            'LIST_TYPE'     => 'L',
                            'MULTIPLE'      => 'Y',
                            'IS_REQUIRED'   => 'N',
                            'FILTRABLE'     => 'N',],
                           ['IBLOCK_ID'      => $iblockId,
                            'NAME'           => 'Родительское подразделение',
                            'ACTIVE'         => 'Y',
                            'SORT'           => '300',
                            'CODE'           => 'PARENT_SUBDIVISION',
                            'PROPERTY_TYPE'  => 'E',
                            'LINK_IBLOCK_ID' => $iblockId,
                            'ROW_COUNT'      => '1',
                            'COL_COUNT'      => '30',
                            'LIST_TYPE'      => 'L',
                            'MULTIPLE'       => 'N',
                            'IS_REQUIRED'    => 'N',
                            'FILTRABLE'      => 'N',],
                           ['IBLOCK_ID'      => $iblockId,
                            'NAME'           => 'Компании',
                            'ACTIVE'         => 'Y',
                            'SORT'           => '400',
                            'CODE'           => 'DIRECTION_COMPANIES',
                            'PROPERTY_TYPE'  => 'E',
                            'LINK_IBLOCK_ID' => self::IB_COMPANIES,
                            'ROW_COUNT'      => '1',
                            'COL_COUNT'      => '30',
                            'LIST_TYPE'      => 'L',
                            'MULTIPLE'       => 'Y',
                            'IS_REQUIRED'    => 'N',
                            'FILTRABLE'      => 'N',]];
            foreach ($properties as $property) {
                if ($IblockHelper->addPropertyIfNotExists($iblockId, $property)) {
                    $this->outSuccess("Добавлено свойство \"" . $property['NAME'] . "\" в ИБ маппинга статусов");
                }
            }

            //элементы для нового ИБ
            foreach ($this->elements as $code => &$element) {
                if ($element['PARENT']) {
                    $element['PROPERTIES']['PARENT_SUBDIVISION'] = $this->elements[$element['PARENT']]['ID'];
                }
                if ($element['COMPANIES']) {
                    foreach ($element['COMPANIES'] as $companyKey) {
                        $element['PROPERTIES']['DIRECTION_COMPANIES'][] = $companiesList[$companyKey]['ID'];
                    }
                }
                $element['ID'] = $IblockHelper->addElement($iblockId, $element['FIELDS'], $element['PROPERTIES']);
            }
        }
    }

    public function down() {
        $IblockHelper = new IblockHelper();

        if ($IblockHelper->deleteIblockIfExists(self::IB_CODE)) {
            $this->outSuccess("Удален ИБ \"Структура группы снабжения\"");
        }
        if ($IblockHelper->deleteProperty(self::IB_COMPANIES, 'COMPANY_SITE_LINK')) {
            $this->outSuccess("Удалено св-во ИБ \"COMPANY_SITE_LINK\" из IB #" . self::IB_COMPANIES);
        }

        //удаление компаний
        foreach ($this->companies as $company) {
            $compCodesList[] = \CUtil::translit($company['FIELDS']['NAME'], 'ru') . '_' . $company['INDEX'];
        }
        $companiesEnt = \CIBlockElement::GetList([], ['IBLOCK_ID' => self::IB_COMPANIES,
                                                      'CODE'      => $compCodesList], false, false, ['ID',
                                                                                                     'IBLOCK_ID',
                                                                                                     'CODE',
                                                                                                     'NAME']);
        while ($companyInfo = $companiesEnt->Fetch()) {
            \CIBlockElement::Delete($companyInfo['ID']);
        }

        //удаление продукции
        foreach ($this->production as &$companyProducts) {
            foreach ($companyProducts as &$productInfo) {
                $productCodesList[] = \CUtil::translit($productInfo['FIELDS']['NAME'], 'ru');
            }
        }
        $productEnt = \CIBlockElement::GetList([], ['IBLOCK_ID' => self::IB_PRODUCTION,
                                                    'CODE'      => $productCodesList], false, false, ['ID',
                                                                                                      'IBLOCK_ID',
                                                                                                      'CODE',
                                                                                                      'NAME']);
        while ($prodInfo = $productEnt->Fetch()) {
            \CIBlockElement::Delete($prodInfo['ID']);
        }
    }
}