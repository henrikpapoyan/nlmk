<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160415140353 extends Version {

	protected $description = "Перевод пользовательских настроек разделов";

	public function up(){
		$helper = new IblockHelper();
		$arIblocks = array(
			PURCHASES_AND_SALES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Section",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "IBLOCK_SECTION_ID", "TITLE" => "Parent level"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PICTURE", "TITLE" => "Picture"),
						array("NAME" => "DESCRIPTION", "TITLE" => "Description"),
					)
				),
			),
			DOCUMENTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Section",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "IBLOCK_SECTION_ID", "TITLE" => "Parent level"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "Mnemonic code"),
					)
				),
			),
			HISTORY_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Section",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "IBLOCK_SECTION_ID", "TITLE" => "Parent level"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "UF_PICTURES", "TITLE" => "Pictures of the year"),
						array("NAME" => "PICTURE", "TITLE" => "Picture"),
						array("NAME" => "DESCRIPTION", "TITLE" => "Description"),
					)
				),
			),
		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_section_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}
	}

	public function down() {

	}

}
