<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160413154418 extends Version {

	protected $description = "Изменение пользовательских настроек формы редактирования элементов инфоблоков (для англ. инфоблоков) (Contacts)";

	public function up()
	{
		$helper = new IblockHelper();
		$arIblocks = array(

			CONTACTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_IBLOCK_ID_EN, "PERSON_NAME"), "TITLE" => "Person name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_IBLOCK_ID_EN, "POST"), "TITLE" => "Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_IBLOCK_ID_EN, "PHONE"), "TITLE" => "Phone"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_IBLOCK_ID_EN, "FAX"), "TITLE" => "Fax"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_IBLOCK_ID_EN, "EMAIL"), "TITLE" => "Email"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_IBLOCK_ID_EN, "ADDRESS"), "TITLE" => "Address"),
						array("NAME" => "SECTIONS", "TITLE" => "Section"),
					)
				)
			),

		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}

	}

	public function down()
	{
	}

}

