<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171208165643 extends Version {

    protected $description = "Удаляем ИБ \"Корпоративные отчеты\", \"Governance reports\", \"Эмиссионные документы\", \"Финансовая отчетность\" , \"Financial statements\"";
    protected $iblocksId = array(
        DOCUMENTS_KORP_IBLOCK_ID_RU,
        DOCUMENTS_KORP_IBLOCK_ID_EN,
        DOCUMENTS_EMISS_IBLOCK_ID_RU
    );
    protected $iblocksCode = array(
        "RU" => "fin_report_ru",
        "EN" => "fin_report_en"
    );

    public function up(){
        foreach ($this->iblocksId as $iblock) {
            if (\CIBlock::Delete($iblock)) {
                $this->outSuccess('ИБ id='. $iblock . ' удален');
            } else {
                $this->outError("Не удалось удалить ИБ id=" . $iblock);
            }
        }
        foreach ($this->iblocksCode as $iblock) {
            $res = \CIBlock::GetList(array(), array("CODE" => $iblock));
            while ($ibres = $res->Fetch()) {
                if (\CIBlock::Delete($ibres["ID"])) {
                    $this->outSuccess('ИБ id='. $ibres["ID"] . ' удален');
                } else {
                    $this->outError("Не удалось удалить ИБ id=" . $ibres["ID"]);
                }
            }
        }
    }

    public function down(){
        $helper = new IblockHelper();
    }

}
