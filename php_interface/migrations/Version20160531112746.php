<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160531112746 extends Version
{

	protected $description = "Обновление привязки инфоблоков к сайтам";

	public function up()
	{
		$obIblock = new \CIBlock;

		$arFieldsEn = array("SITE_ID" => array("s2"));
		$arIblocksEn = array(DOCUMENTS_IBLOCK_ID_EN, NEWS_COMPANIES_IBLOCK_ID_EN, NEWS_GROUP_IBLOCK_ID_EN, MULTIMEDIA_IBLOCK_ID_EN);
		foreach ($arIblocksEn as $iblockId)
		{
			if ($obIblock->Update($iblockId, $arFieldsEn))
				$this->outSuccess("Инфоблок обновлен");
			else
				$this->outError($obIblock->LAST_ERROR);
		}

		$arFieldsRu = array("SITE_ID" => array("s1"));
		$arIblocksRu = array(DOCUMENTS_IBLOCK_ID_RU, NEWS_COMPANIES_IBLOCK_ID_RU, NEWS_GROUP_IBLOCK_ID_RU, MULTIMEDIA_IBLOCK_ID_RU);
		foreach ($arIblocksRu as $iblockId)
		{
			if ($obIblock->Update($iblockId, $arFieldsRu))
				$this->outSuccess("Инфоблок обновлен");
			else
				$this->outError($obIblock->LAST_ERROR);
		}

	}

	public function down()
	{
	}

}
