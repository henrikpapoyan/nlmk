<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160413155015 extends Version {

    protected $description = "Сделать необязательным поле \"Описание для анонса\" ИБ Слайдер: Медиацентр (главная страница)";

	public function up(){
		$arIblocks = array(
			MC_SLIDER_IBLOCK_ID_RU,
			MC_SLIDER_IBLOCK_ID_EN
		);
		$helper = new IblockHelper();
		foreach ($arIblocks as $iblockId) {
			$helper->mergeIblockFields($iblockId, array("PREVIEW_TEXT" => array("IS_REQUIRED" => "N")));
		}
	}

	public function down(){
		$arIblocks = array(
			MC_SLIDER_IBLOCK_ID_RU,
			MC_SLIDER_IBLOCK_ID_EN
		);
		$helper = new IblockHelper();
		foreach ($arIblocks as $iblockId) {
			$helper->mergeIblockFields($iblockId, array("PREVIEW_TEXT" => array("IS_REQUIRED" => "Y")));
		}
	}

}
