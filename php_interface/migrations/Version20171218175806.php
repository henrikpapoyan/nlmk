<?
namespace Sprint\Migration;

use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171218175806 extends Version
{
    protected $description = 'Добавление инфоблока SOURCE-TO-PAY';

    const IB_NAME = 'Описание процесса оплаты (SOURCE-TO-PAY)';
    const IB_CODE = 'PAYMENT_PROCEDURE_RU';

    public function up() {
        $IblockHelper = new IblockHelper();

        $iblockId = $IblockHelper->addIblockIfNotExists(['ACTIVE'          => 'Y',
                                                         'NAME'            => self::IB_NAME,
                                                         'CODE'            => self::IB_CODE,
                                                         'LIST_PAGE_URL'   => '',
                                                         'DETAIL_PAGE_URL' => '',
                                                         'IBLOCK_TYPE_ID'  => 'activities',
                                                         'SITE_ID'         => ['s1'],
                                                         'SORT'            => 500,
                                                         'DESCRIPTION'     => '']);

        if ($iblockId) {
            $this->outSuccess("Добавлен ИБ \"" . self::IB_NAME . "\"");
            $sectionsInfo = [['FIELDS'   => ['NAME' => 'Сорсинг',
                                             'SORT' => 100],
                              'ELEMENTS' => [['NAME'         => 'ФРОНТ-ОФИС',
                                              'PREVIEW_TEXT' => '<ul class="list list_dot">
                                                                <li class="list__item">Анализ потребности и спецификации</li>
                                                                <li class="list__item">Тендерный процесс</li>
                                                                <li class="list__item">Переговоры</li>
                                                                <li class="list__item">Формирование КЛ</li>
                                                             </ul>'],
                                             ['NAME' => 'УПРАВЛЕНИЕ СНАБЖЕНИЯ'],
                                             ['NAME' => 'ДИРЕКЦИЯ ПО ИНВЕСТИЦИОННЫМ ЗАКУПКАМ'],
                                             ['NAME' => 'ДИРЕКЦИЯ ПО ОПЕРАЦИОННЫМ ЗАКУПКАМ']]],
                             ['FIELDS'   => ['NAME' => 'Документальное оформление сделки',
                                             'SORT' => 200],
                              'ELEMENTS' => [['NAME'         => 'ФРОНТ-ОФИС',
                                              'PREVIEW_TEXT' => '<ul class="list list_dot">
                                                                 <li class="list__item">Оформление договора</li>
                                                             </ul>'],
                                             ['NAME'         => 'МИДЛ-ОФИС',
                                              'PREVIEW_TEXT' => '<ul class="list list_dot">
                                                                <li class="list__item">Формирование спецификаций</li>
                                                                <li class="list__item">Продление договоров</li>
                                                                <li class="list__item">Присвоение ИП в SAP</li>
                                                                <li class="list__item">Одобрение спецификации с поставщиком</li>
                                                                <li class="list__item">Оформление оригиналов</li>
                                                                <li class="list__item">Квалификация поставщиков</li>
                                                             </ul>'],
                                             ['NAME' => 'ОТДЕЛ СОПРОВОЖДЕНИЯ ЗАКУПОК']]],
                             ['FIELDS'   => ['NAME' => 'Заказ и доставка',
                                             'SORT' => 300],
                              'ELEMENTS' => [['NAME'         => 'БЭК-ОФИС',
                                              'PREVIEW_TEXT' => '<ul class="list list_dot">
                                                                <li class="list__item">Формирование заказа</li>
                                                                <li class="list__item">Мониторинг поставки</li>
                                                                <li class="list__item">Коммуникация с внутр.заказчиком</li>
                                                                <li class="list__item">Организация автомобильного и ЖД заезда</li>
                                                                <li class="list__item">Формирование пакета документов</li>
                                                                <li class="list__item">Претензионная работа</li>
                                                                <li class="list__item">Контроль прихода в цеха</li>
                                                             </ul>'],
                                             ['NAME' => 'ОТДЕЛ ПОДДЕРЖКИ СНАБЖЕНИЯ']]],
                             ['FIELDS'   => ['NAME' => 'Оплата',
                                             'SORT' => 400],
                              'ELEMENTS' => [['NAME'         => 'УЧЁТНЫЙ ЦЕНТР',
                                              'PREVIEW_TEXT' => '<ul class="list list_dot">
                                                                <li class="list__item">Оплата</li>
                                                                <li class="list__item">Сверка взаимозачетов</li>
                                                                <li class="list__item">Подписание возвратных накладных ТОРГ-12</li>
                                                             </ul>'],
                                             ['NAME' => 'ОТДЕЛ УЧЕТА ЗАКУПОК']]]];
            foreach ($sectionsInfo as $section) {
                if ($sectionId = $IblockHelper->addSection($iblockId, $section['FIELDS'])) {
                    foreach ($section['ELEMENTS'] as $element) {
                        $element['IBLOCK_SECTION_ID'] = $sectionId;
                        $IblockHelper->addElement($iblockId, $element);
                    }
                }
            }
        }
    }

    public function down() {
        $IblockHelper = new IblockHelper();

        if ($IblockHelper->deleteIblockIfExists(self::IB_CODE)) {
            $this->outSuccess('Удален ИБ SOURCE-TO-PAY');
        }
    }
}
