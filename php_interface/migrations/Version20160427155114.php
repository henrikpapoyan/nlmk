<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160427155114 extends Version {

	protected $description = "Добавление инфоблоков \"Контакты в сайдбаре (общие)\" (ru/en)";

	public function up()
	{
		global $APPLICATION;
		$path = $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/migrations/xml";

		$contactsSidebarRu = $path . "/contacts_sidebar_ru.xml";
		$strImportResultRu = ImportXMLFile($contactsSidebarRu, "contacts", "s1", "N", "N");
		if($strImportResultRu)
			$this->outSuccess("Инфоблок \"Контакты в сайдбаре (общие)\" добавлен");
		else
			$APPLICATION->ThrowException("Импорт завершен с ошибкой");

		$contactsSidebarEn = $path . "/contacts_sidebar_en.xml";
		$strImportResultEn = ImportXMLFile($contactsSidebarEn, "contacts_en", "s2", "N", "N");
		if($strImportResultEn)
			$this->outSuccess("Инфоблок \"Contacts in the sidebar (general)\" добавлен");
		else
			$APPLICATION->ThrowException("Импорт завершен с ошибкой");
	}

	public function down()
	{
	}

}
