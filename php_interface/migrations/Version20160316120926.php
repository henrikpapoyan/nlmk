<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160316120926 extends Version {

	protected $description = "Изменение сортировки в ИБ Географический дивизион";

	public function up() {
		$arDivisonsRU = array("russia" => array("SORT" => 1), "usa" => array("SORT" => 2), "europe" => array("SORT" => 3));
		$dbDivisionsRU = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => GEOGRAPHY_DIVISION_IBLOCK_ID_RU), false, false, array("ID", "IBLOCK_ID", "CODE"));
		while ($arDivision = $dbDivisionsRU->GetNext()) {
			$arDivisonsRU[$arDivision["CODE"]]["ID"] = $arDivision["ID"];
		}
		$arDivisonsEN = array("russia" => array("SORT" => 1), "usa" => array("SORT" => 2), "europe" => array("SORT" => 3));
		$dbDivisionsEN = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => GEOGRAPHY_DIVISION_IBLOCK_ID_EN), false, false, array("ID", "IBLOCK_ID", "CODE"));
		while ($arDivision = $dbDivisionsEN->GetNext()) {
			$arDivisonsEN[$arDivision["CODE"]]["ID"] = $arDivision["ID"];
		}

		$el = new \CIBlockElement();
		foreach ($arDivisonsRU as $arDivisionRU) {
			$el->Update($arDivisionRU["ID"], array("SORT" => $arDivisionRU["SORT"]));
		}
		foreach ($arDivisonsEN as $arDivisionEN) {
			$el->Update($arDivisionEN["ID"], array("SORT" => $arDivisionEN["SORT"]));
		}
	}

	public function down() {
		$arDivisonsRU = array("russia" => array("SORT" => 3), "usa" => array("SORT" => 1), "europe" => array("SORT" => 2));
		$dbDivisionsRU = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => GEOGRAPHY_DIVISION_IBLOCK_ID_RU), false, false, array("ID", "IBLOCK_ID", "CODE"));
		while ($arDivision = $dbDivisionsRU->GetNext()) {
			$arDivisonsRU[$arDivision["CODE"]]["ID"] = $arDivision["ID"];
		}
		$arDivisonsEN = array("russia" => array("SORT" => 3), "usa" => array("SORT" => 1), "europe" => array("SORT" => 2));
		$dbDivisionsEN = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => GEOGRAPHY_DIVISION_IBLOCK_ID_EN), false, false, array("ID", "IBLOCK_ID", "CODE"));
		while ($arDivision = $dbDivisionsEN->GetNext()) {
			$arDivisonsEN[$arDivision["CODE"]]["ID"] = $arDivision["ID"];
		}

		$el = new \CIBlockElement();
		foreach ($arDivisonsRU as $arDivisionRU) {
			$el->Update($arDivisionRU["ID"], array("SORT" => $arDivisionRU["SORT"]));
		}
		foreach ($arDivisonsEN as $arDivisionEN) {
			$el->Update($arDivisionEN["ID"], array("SORT" => $arDivisionEN["SORT"]));
		}
	}

}
