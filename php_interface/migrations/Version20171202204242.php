<?php

namespace Sprint\Migration;

use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

/**
 * Запускать из консоли:
 * php local/php_interface/migrations/migrate.php execute Version20171202204242 --up
 * php local/php_interface/migrations/migrate.php execute Version20171202204242 --down
 */
class Version20171202204242 extends Version
{
    protected $description = "Перенос ИБ \"Эмиссионные документы\" в ИБ \"Документы\"";
    
    protected $props = array(
        "RU" => array(
            'CODE' => 'emiss-doc',
            'NAME' => 'Эмиссионные документы',
        )    
    );

    public function up()
    {
        $helper = new IblockHelper();
        $cIBlockElement = new \CIBlockElement();
        $cFile = new \CFile();
        $newSecId = array();
        //new section
        foreach ($this->props as $lang => $prop) {
            $arSection = array("NAME" => $prop["NAME"], "CODE" => $prop["CODE"]);
            
            if ($newSecId[$lang] = $helper->addSection(constant("DOCUMENTS_IBLOCK_ID_" . $lang), $arSection)) {
                $this->outSuccess('Раздел '. $prop['NAME'] . ' добавлен');
            } else {
                $this->outError("Не удалось добавить раздел " . $prop['NAME']);
                
                return false;
            }
        }
        //iblocks -> section
        $sections = array();
        foreach($this->props as $lang => $prop){
            $rs = \CIBlockSection::GetList(
                array(), 
                array('IBLOCK_ID' => constant("DOCUMENTS_EMISS_IBLOCK_ID_" . $lang), 'DEPTH_LEVEL' => 1),
                false,
                array("ID", "NAME", "CODE")
            );

            while ($section = $rs->Fetch()) {
                $section['CODE'] = !empty($section['CODE']) ? $section['CODE'] : \CUtil::translit($section['NAME'], mb_strtolower($lang));
                $sections[$lang][$section["ID"]]["ID"] = $section["ID"];
                $sections[$lang][$section["ID"]]["CODE"] = $section["CODE"];
                $sections[$lang][$section["ID"]]["NAME"] = $section["NAME"];
            }
            
            if (count($sections[$lang]) > 0) {
                foreach ($sections[$lang] as $section) {
                    $arSection = array("NAME" => $section["NAME"], "CODE" => $section["CODE"], "IBLOCK_SECTION_ID" => $newSecId[$lang]);

                    if ($sections[$lang][$section["ID"]]["NEW_ID"] = $helper->addSection(constant("DOCUMENTS_IBLOCK_ID_" . $lang), $arSection)) {
                        $this->outSuccess('Раздел '. $section["NAME"] . ' добавлен');
                    } else {
                        $this->outError("Не удалось добавить раздел " . $section["NAME"]);
                        
                        return false;
                    }
                }
            } else {
                $this->outError("Не удалось найти разделы ИБ " . constant("DOCUMENTS_KORP_IBLOCK_ID_" . $lang));
                
                return false;
            }
        }
        
        //elems
        foreach ($this->props as $lang => $prop) {
            foreach ($sections[$lang] as $section) {
                $rs = \CIBlockElement::GetList(
                    array(),
                    array(
                        'IBLOCK_ID' => constant("DOCUMENTS_EMISS_IBLOCK_ID_" . $lang),
                        'SECTION_ID' => $section['ID'],
                        'INCLUDE_SUBSECTIONS' => 'Y'
                    ),
                    false,
                    false,
                    array(
                        'ID', 'NAME', 'CODE', 'ACTIVE', 'SORT', 'IBLOCK_SECTION_ID',
                        'DATE_ACTIVE_FROM', 'PROPERTY_FILE'
                    )
                );

                while ($element = $rs->Fetch()) {
                    $activeFrom = !empty($element['DATE_ACTIVE_FROM']) ? $element['DATE_ACTIVE_FROM'] : date('d.m.Y');
                    $fileId = $cFile->SaveFile(\CFile::MakeFileArray($element['PROPERTY_FILE_VALUE']), 'iblock');
                    $fileType = pathinfo($cFile->GetPath($fileId), PATHINFO_EXTENSION);
                    $err = false;

                    if ($cIBlockElement->Add(array(
                        'IBLOCK_ID' => constant("DOCUMENTS_IBLOCK_ID_" . $lang),
                        'IBLOCK_SECTION_ID' => $section['NEW_ID'],
                        'NAME' => $element['NAME'],
                        'CODE' => $element['CODE'],
                        'ACTIVE' => $element['ACTIVE'],
                        'DATE_ACTIVE_FROM' => $activeFrom,
                        'SORT' => $element['SORT'],
                        'PROPERTY_VALUES' => array(
                            'FILE' => $fileId,
                            'FILE_TYPE' => $fileType,
                            'PAGE_URL' => ''
                        )
                    ))) {
                        $this->outSuccess('Элемент '. $element['NAME'] . ' перенесен');
                    } else {
                        $err = true;
                        $this->outError('Элемент '. $element['NAME'] . ' не удалось перенести');
                    }
                }
                if ($err === false) {
                    $this->outSuccess('Элементы перенесены в раздел '. $section['NAME']);
                }
            }
        } 
    }

    public function down()
    {
        $arSectionsId = array();
        foreach ($this->props as $lang => $prop) {
            $dbSections = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => constant("DOCUMENTS_IBLOCK_ID_" . $lang), "CODE" => $prop["CODE"]), false, array("ID"));
            while ($arSection = $dbSections->GetNext()) {
                $arSectionsId[] = $arSection["ID"];
            }
        }

        foreach ($arSectionsId as $sectionId) {
            if (\CIBlockSection::Delete($sectionId)) {
                $this->outSuccess('Раздел id='. $sectionId . ' удален');
            } else {
                $this->outError("Не удалось удалить раздел id=" . $sectionId);
            }
        }
    }

}
