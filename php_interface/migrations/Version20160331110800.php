<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160331110800 extends Version {

    protected $description = "Поля \"Имя, Фамилия персоны\" и \"Должность\" сделать необязательными в слайдере \"Миссия и видение\"";

    public function up(){
	$arIblocks = array(
		MISSION_SLIDER_IBLOCK_ID_RU,
		MISSION_SLIDER_IBLOCK_ID_EN
	);
	foreach ($arIblocks as $iblockId) {
		$dbProp = \CIBlockProperty::GetList(
			array("SORT" => "ASC"),
			array("IBLOCK_ID" => $iblockId, "CODE" => "PERSON_NAME")
		);
		while($arProp = $dbProp->GetNext()) {
			$CIBlockProperty = new \CIBlockProperty;
			$CIBlockProperty->Update($arProp["ID"], array("IS_REQUIRED" => "N"));
		}
		$dbProp = \CIBlockProperty::GetList(
			array("SORT" => "ASC"),
			array("IBLOCK_ID" => $iblockId, "CODE" => "POST")
		);
		while($arProp = $dbProp->GetNext()) {
			$CIBlockProperty = new \CIBlockProperty;
			$CIBlockProperty->Update($arProp["ID"], array("IS_REQUIRED" => "N"));
		}
	}
    }

    public function down(){
	$arIblocks = array(
		MISSION_SLIDER_IBLOCK_ID_RU,
		MISSION_SLIDER_IBLOCK_ID_EN
	);
	foreach ($arIblocks as $iblockId) {
		$dbProp = \CIBlockProperty::GetList(
			array("SORT" => "ASC"),
			array("IBLOCK_ID" => $iblockId, "CODE" => "PERSON_NAME")
		);
		while($arProp = $dbProp->GetNext()) {
			$CIBlockProperty = new \CIBlockProperty;
			$CIBlockProperty->Update($arProp["ID"], array("IS_REQUIRED" => "Y"));
		}
		$dbProp = \CIBlockProperty::GetList(
			array("SORT" => "ASC"),
			array("IBLOCK_ID" => $iblockId, "CODE" => "POST")
		);
		while($arProp = $dbProp->GetNext()) {
			$CIBlockProperty = new \CIBlockProperty;
			$CIBlockProperty->Update($arProp["ID"], array("IS_REQUIRED" => "Y"));
		}
	}
    }

}
