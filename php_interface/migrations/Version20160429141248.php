<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;


class Version20160429141248 extends Version {

    protected $description = "Если код задан, то проверять на уникальность (для служебного инфоблока Корпоративный журнал)";

    public function up(){
        $helper = new IblockHelper();

        \Bitrix\Main\Loader::includeModule("iblock");

        $fields = \CIBlock::getFields(CORPORATE_MAGAZINE_IBLOCK_ID);
        $fields["CODE"]["IS_REQUIRED"] = "Y";
        $fields["CODE"]["DEFAULT_VALUE"]["UNIQUE"] = "Y";;

        \CIBlock::setFields(CORPORATE_MAGAZINE_IBLOCK_ID, $fields);
    }

    public function down(){
        $helper = new IblockHelper();
    }

}
