<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160413203103 extends Version {

	protected $description = "Изменение пользовательских настроек формы редактирования элементов инфоблоков (для англ. инфоблоков) Investor Relations";

	public function up(){
		$helper = new IblockHelper();
		$arIblocks = array(
			INVESTOR_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INVESTOR_SLIDER_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INVESTOR_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
					)
				),
			),
			AGM_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(AGM_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
					)
				),
			),
			REPORTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(REPORTS_IBLOCK_ID_EN, "PICTURE"), "TITLE" => "*Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(REPORTS_IBLOCK_ID_EN, "FILE"), "TITLE" => "*File"),
					)
				),
			),
			PRESENTATIONS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRESENTATIONS_IBLOCK_ID_EN, "FILE"), "TITLE" => "Presentation"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(PRESENTATIONS_IBLOCK_ID_EN, "FILE_TYPE"), "TITLE" => "File type"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "SECTIONS", "TITLE" => "Section")
					)
				),
			),
			INVESTOR_CONTACTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INVESTOR_CONTACTS_IBLOCK_ID_EN, "POST"), "TITLE" => "Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INVESTOR_CONTACTS_IBLOCK_ID_EN, "PHONE"), "TITLE" => "Phone, Fax"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INVESTOR_CONTACTS_IBLOCK_ID_EN, "EMAIL"), "TITLE" => "Email"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INVESTOR_CONTACTS_IBLOCK_ID_EN, "ADDRESS"), "TITLE" => "Address"),
						array("NAME" => "SECTIONS", "TITLE" => "Section")
					)
				),
			),
			FINANCIAL_CALENDAR_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "ACTIVE_FROM", "TITLE" => "*Activity Start"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FINANCIAL_CALENDAR_IBLOCK_ID_EN, "DATE"), "TITLE" => "Event start date"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(FINANCIAL_CALENDAR_IBLOCK_ID_EN, "DATE_END"), "TITLE" => "Event end date"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					)
				),
			),
			SHAREHOLDER_CENTRE_DOCS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(SHAREHOLDER_CENTRE_DOCS_IBLOCK_ID_EN, "FILE"), "TITLE" => "File"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(SHAREHOLDER_CENTRE_DOCS_IBLOCK_ID_EN, "PERIOD"), "TITLE" => "Period"),
						array("NAME" => "SECTIONS", "TITLE" => "Section"),
					)
				),
			),
			INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN, "POST"), "TITLE" => "Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN, "EMAIL"), "TITLE" => "Email"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN, "PHONE"), "TITLE" => "Phone"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN, "PAGE_URL"), "TITLE" => "Page url"),
						array("NAME" => "SECTIONS", "TITLE" => "Section"),
					)
				),
			),
			ANNUAL_REPORTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ANNUAL_REPORTS_IBLOCK_ID_EN, "REPORTS"), "TITLE" => "Reports"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ANNUAL_REPORTS_IBLOCK_ID_EN, "FULL_REPORT"), "TITLE" => "Annual repor (full version)"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ANNUAL_REPORTS_IBLOCK_ID_EN, "PERSON_NAME"), "TITLE" => "Person name"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ANNUAL_REPORTS_IBLOCK_ID_EN, "PHOTO"), "TITLE" => "Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ANNUAL_REPORTS_IBLOCK_ID_EN, "POST"), "TITLE" => "Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ANNUAL_REPORTS_IBLOCK_ID_EN, "COMMENT"), "TITLE" => "Comment"),
					)
				),
			),
		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}

    }

    public function down(){
        $helper = new IblockHelper();
    }

}
