<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

require($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/iblock/include.php");

class Version20160422114938 extends Version {

	protected $description = "Создание инфоблоков &laquo;Слайдер - Энергоэффективность&raquo; (RU и EN) и &laquo;Энергоэффективные кейсы&raquo; (RU и EN)";

	public function up()
	{

		global $APPLICATION;
		$path = $_SERVER['DOCUMENT_ROOT']."/local/php_interface/migrations/xml";

		$energy_cases_ru = $path."/energy_cases_ru.xml";
		$strImportResult = ImportXMLFile($energy_cases_ru, "sustainability", "s1", "N", "N");
		if($strImportResult)
			$this->outSuccess('Инфоблок "Энергоэффективные кейсы" добавлен');
		else
			$APPLICATION->ThrowException('Импорт завершен с ошибкой');

		$energy_cases_en = $path."/energy_cases_en.xml";
		$strImportResult2 = ImportXMLFile($energy_cases_en, "sustainability_en", "s2", "N", "N");
		if($strImportResult2)
			$this->outSuccess('Инфоблок "Energy efficiency cases" добавлен');
		else
			$APPLICATION->ThrowException('Импорт завершен с ошибкой');

		$energy_slider_ru = $path."/energy_slider_ru.xml";
		$strImportResult = ImportXMLFile($energy_slider_ru, "sustainability", "s1", "N", "N");
		if($strImportResult)
			$this->outSuccess('Инфоблок "Слайдер - Энергоэффективность" добавлен');
		else
			$APPLICATION->ThrowException('Импорт завершен с ошибкой');

		$energy_slider_en = $path."/energy_slider_en.xml";
		$strImportResult2 = ImportXMLFile($energy_slider_en, "sustainability_en", "s2", "N", "N");
		if($strImportResult2)
			$this->outSuccess('Инфоблок "Slider - Energy efficiency" добавлен');
		else
			$APPLICATION->ThrowException('Импорт завершен с ошибкой');

	}

	public function down()
	{
	}

}
