<?php
namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

require($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/iblock/include.php");

class Version20160413170346 extends Version {

	protected $description = "Создание инфоблоков &laquo;Слайдер - ответственное лидерство&raquo;";

	public function up()
	{
		global $APPLICATION;
		$path = $_SERVER['DOCUMENT_ROOT']."/local/php_interface/migrations/xml";

		$leadership = $path."/leadership.xml";
		$strImportResult = ImportXMLFile($leadership, "company", "s1", "N", "N");
	
		if($strImportResult)
			$this->outSuccess('Инфоблок "Слайдер - ответственное лидерство" добавлен');
		else
			$APPLICATION->ThrowException('Импорт завершен с ошибкой');
	
		$leadership_en = $path."/leadership_en.xml";
		$strImportResult2 = ImportXMLFile($leadership_en, "company_en", "s2", "N", "N");
	
		if($strImportResult2)
			$this->outSuccess('Инфоблок "Slider - Leadership" добавлен');
		else
			$APPLICATION->ThrowException('Импорт завершен с ошибкой');
	}

	public function down(){
	}

}
