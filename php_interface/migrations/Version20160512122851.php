<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160512122851 extends Version {

	protected $description = "изменение путей изображений в ИБ Ключевые факты (ru/en)";

	public function up()
	{
		$obElement = new \CIBlockElement;
		$dbElements = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => array(KEY_FACTORS_IBLOCK_ID_EN, KEY_FACTORS_IBLOCK_ID_RU)),
			false,
			false,
			array("ID", "IBLOCK_ID", "PREVIEW_TEXT")
		);
		while ($arElement = $dbElements->GetNext())
		{
			$previewText = $arElement["~PREVIEW_TEXT"];
			$previewText = str_replace('"/images/', '"/local/templates/nlmk/images/', $previewText);
			$obElement->Update($arElement["ID"], array("PREVIEW_TEXT" => $previewText));
		}
	}

	public function down()
	{
	}

}
