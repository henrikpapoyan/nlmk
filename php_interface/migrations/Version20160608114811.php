<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160608114811 extends Version {

	protected $description = "Активация свойств-чекбоксов \"Выводить элемент в списках (Раздел \"Руководство\")\" у текущих элементов";

	public function up()
	{
		$arIblocks = array(DIRECTORS_IBLOCK_ID_RU, DIRECTORS_IBLOCK_ID_EN, MANAGEMENT_IBLOCK_ID_RU, MANAGEMENT_IBLOCK_ID_EN);
		foreach ($arIblocks as $iblockID)
		{
			$propertyEnums = \CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID" => $iblockID, "CODE" => "SHOW_IN_LISTS"));
			if ($enumFields = $propertyEnums->Fetch())
			{
				$enumID = $enumFields["ID"];

				$dbElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => $iblockID), false, false, array("ID", "IBLOCK_ID"));
				while ($arElement = $dbElements->Fetch())
				{
					\CIBlockElement::SetPropertyValuesEx($arElement["ID"], false, array("SHOW_IN_LISTS" => $enumID));
				}
			}
		}
	}

	public function down()
	{
		$arIblocks = array(DIRECTORS_IBLOCK_ID_RU, DIRECTORS_IBLOCK_ID_EN, MANAGEMENT_IBLOCK_ID_RU, MANAGEMENT_IBLOCK_ID_EN);
		foreach ($arIblocks as $iblockID)
		{
			$dbElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => $iblockID), false, false, array("ID", "IBLOCK_ID"));
			while ($arElement = $dbElements->Fetch())
			{
				\CIBlockElement::SetPropertyValuesEx($arElement["ID"], false, array("SHOW_IN_LISTS" => ""));
			}
		}
	}

}
