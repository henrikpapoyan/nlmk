<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160304102556 extends Version {

    protected $description = "Добавление нового опроса совета директоров";

    public function up(){
        global $APPLICATION;
        $arQuestions = array(
            "<span>Board composition and directors’ skill set <br />/Состав СД и квалификация директоров/</span>
<p>Our board has world-class industry experts among its members</p>
<p>/Среди членов СД – отраслевые эксперты мирового класса</p>",
            "<p>Our board possess advanced expertise in the area of talent management</p>
<p>/СД обладает глубокой экспертизой в области управления талантами/</p>",
            "<p>Our board possess advanced expertise in the area of finance</p>
<p>/СД обладает глубокой экспертизой в области финансов/</p>",
            "<p>Our board possess advanced expertise in the area of risk mitigation</p>
<p>/СД обладает глубокой экспертизой в области управления рисками/</p>",
            "<p>All our directors have strong listening, speaking and feedback skills </p>
<p>/Все директора умеют слушать, четко формулировать свое мнение и реагировать на мнение других/</p>",
            "<p>All our directors have strong listening, speaking, analytical and synthetic skills</p>
<p>/Все директора умеют слушать, четко формулировать свое мнение, обладают синтетическими и аналитическими навыками/</p>",
            "<p>All directors come well-prepared to the board meetings</p>
<p>/Все директора готовятся к заседаниям СД/</p>",
            "<p>All directors dedicated enough time to their duties</p>
<p>/Все директора уделяют достаточно времени исполнению своих обязанностей/</p>",
            "<p>Our board is diverse in terms of directors’ backgrounds, practical experience and knowledge</p>
<p>/СД достаточно диверсифицирован в плане образования, практического опыта и навыков директоров/</p>",
            "<p>Our board has the optimal size</p>
<p>/Размер СД оптимален/</p>",
            "<p>Our board has a sufficient number of independent directors</p>
<p>/В состав СД входит достаточное число независимых директоров/</p>",

            "<span>Board agendas <br />/Повестки дня СД/</span>
<p>The board address issues which are relevant to its purpose and mission</p>
<p>/СД рассматривает вопросы, соответствующие его назначению и миссии/</p>",
            "<p>Our board address issue which are strategic and could not be dealt with elsewhere</p>
<p>/СД рассматривает вопросы стратегического значения, которые не могут быть решены другими органами компании/</p>",
            "<p>Our board does not leave out any items of strategic importance for the company</p>
<p>/СД не упускает никаких вопросов стратегического значения/</p>",
            "<p>Our board agendas allocate enough time for productive discussions</p>
<p>/Повесткой дня СД выделяется достаточно времени на продуктивные дискуссии/</p>",

            "<span>Board process <br />/Проведение СД/</span>
<p>Our board makes quality decisions</p>
<p>/СД принимает качественные решения/</p>",
            "<p>Our board process is effective and fair</p>
<p>/Заседания СД проходят эффективно и справедливо/</p>",
            "<p>Our board spends most of our time in constrictive discussions (not listening to presentations)</p>
<p>/СД уделяет больше времени конструктивному обсуждения, а не прослушиванию презентаций/</p>",
            "<p>We deal with procedural and compliance issues in effective and efficient ways</p>
<p>/Вопросы, связанные с процедурами и соответствием, решаются эффективно/</p>",
            "<p>Every director actively participates in discussions and contributes ideas</p>
<p>/Все директора активно участвуют в дискуссиях и генерируют идеи/</p>",
            "<p>The board thoroughly follows up on its decisions</p>
<p>/СД отслеживает реализацию принятых решений/</p>",
            "<p>The board  has a constructive two-ways dialogue with the CEO and management</p>
<p>/СД ведет конструктивный двусторонний диалог с президентом и менеджментом/</p>",
            "<p>We regularly conduct  in-camera board session (without executives being present)</p>
<p>/Регулярно проводятся закрытые заседания СД (без участия руководства)/</p>",
            "<p>The board materials are comprehensive, but easy to read and prepared and communicated in advance</p>
<p>/Материалы СД – исчерпывающи, но при это легко воспринимаются. Готовятся и рассылаются заранее/</p>",
            "<p>The Audit Committee increases the efficiency of the board’s activity</p>
<p>/Комитет по аудиту повышает эффективность работы СД/</p>",
            "<p>The HR, Remuneration and Social Policy Committee increases the efficiency of the board’s activity</p>
<p>/Комитет по кадрам, вознаграждениям и социальной политике повышает эффективность работы СД/</p>",
            "<p>The Strategic Planning Committee increases the efficiency of the board’s activity</p>
<p>/Комитет по стратегическому планированию повышает эффективность работы СД/</p>",
            "<p>The list of committees and their tasks in line with the company’s needs</p>
<p>/Список комитетов и их задачи соответствуют потребностям компании/</p>",
            "<p>The Corporate Secretary supports efficient operation of the board</p>
<p>/Корпоративный секретарь способствует эффективной работе СД/</p>",

            "<span>Chairman of the board <br />/Председатель Совета директоров/</span>
<p>Our chair sets strategic and comprehensive agendas for board meetings </p>
<p>/Председатель СД формирует стратегические и исчерпывающие повестки дня для СД/</p>",
            "<p>Our chair controls flow of board meetings and their timing </p>
<p>/Председатель СД контролирует течение заседаний СД и соответствие установленным временным рамкам/</p>",
            "<p>Our chair synthesizes director’s ideas and suggestions into clear comprehensive and actionable board decisions</p>
<p>/Председатель СД синтезирует идеи и предложения директоров, формулируя четкие, исчерпывающие и реализуемые решения СД/</p>",
            "<p>Our chair effectively manages conflict and disagreements among directors</p>
<p>/Председатель СД эффективно разрешает конфликты и споры директоров/</p>",
            "<p>Our chair provides support, feedback and coaching to directors on a regular basis</p>
<p>/Председатель СД оказывает поддержку и консультирует директоров на регулярной основе/</p>",
            "<p>Our chair effectively communicates on behalf of the board with its key stakeholders – shareholders, management, regulators, etc.</p>
<p>/Председатель СД активно взаимодействует от имени СД с ключевыми стейкхолдерами: акционерами, менеджментом, регуляторами и т.д./</p>"
        );
        $arAnswers = array(
            array("MESSAGE" => "Poor", "FIELD_TYPE" => 0),
            array("MESSAGE" => "Acceptable", "FIELD_TYPE" => 0),
            array("MESSAGE" => "Good", "FIELD_TYPE" => 0),
            array("MESSAGE" => "Excellent", "FIELD_TYPE" => 0),
            array("MESSAGE" => "Comment", "FIELD_TYPE" => 5, "FIELD_HEIGHT" => 3)
        );
        $arTextQuestions = array(
            "<span>What specific things our board should stop doing to improve its effectiveness?<br />/Что конкретно необходимо прекратить делать СД, чтобы повысить эффективность своей работы?/</span>",
            "<span>What specific things our board should start doing to improve its effectiveness?<br />/Что конкретно необходимо начать делать СД, чтобы повысить эффективность своей работы?/</span>",
            "<span>Other comments and recommendations:<br />/Прочие комментарии и рекомендации:/</span>"
        );

        if (\Bitrix\Main\Loader::includeModule("vote")) {
            $voteChannelID = 0;
            $by = "ID";
            $order = "ASC";
            $is_filtered = null;
            $rsVoteChannel = CVoteChannel::GetList($by, $order, array("SYMBOLIC_NAME" => "BOARD_POLLS", "SYMBOLIC_NAME_EXACT_MATCH" => "Y"), $is_filtered);
            if ($arVoteChannel = $rsVoteChannel->Fetch()) {
                $voteChannelID = $arVoteChannel["ID"];
            } else {
                $voteChannelFields = array(
                    'SYMBOLIC_NAME' => "BOARD_POLLS",
                    'C_SORT' => "200",
                    'ACTIVE' => "Y",
                    'HIDDEN' => "Y",
                    'TITLE' => "Опросы совета директоров",
                    'VOTE_SINGLE' => "Y",
                    'USE_CAPTCHA' => "N",
                    'SID' => "BOARD_POLLS",
                    'SITE' => array("s1", "s2"),
                    'FIRST_SITE_ID' => "s1",
                    'GROUP_ID' => array(2 => 2, 3 => 2, 4 => 2, 5 => 2)
                );
                $voteChannelID = CVoteChannel::Add($voteChannelFields);
            }
            if ($voteChannelID > 0) {
                $voteFields = array(
                    'CHANNEL_ID' => $voteChannelID,
                    'C_SORT' => 100,
                    'ACTIVE' => "Y",
                    'NOTIFY' => "N",
                    'AUTHOR_ID' => 1,
                    'DATE_START' => "01.03.2016 00:00:00",
                    'DATE_END' => "31.12.2016 00:00:00",
                    'URL' => "",
                    'TITLE' => "Board Evaluation Form /Форма оценки Совета директоров/",
                    'DESCRIPTION' => "<p>
Using the following criteria evaluate NLMK board of directors in the last period answering to which extend each of the following questions describes the board and its performance
</p>
<p>
/Используя следующие критерии, оцените Совет директоров НЛМК за последний период, отметив, в какой степени каждый из нижеприведенных вопросов характеризует Совет и его деятельность/
</p>",
                    'DESCRIPTION_TYPE' => "html",
                    'UNIQUE_TYPE' => 7,
                    'DELAY' => 0,
                    'DELAY_TYPE' => "D"
                );
                $voteID = CVote::Add($voteFields);

                if ($voteID > 0) {
                    $i = 0;
                    foreach ($arQuestions as $questionText) {
                        $voteQuestionFields = array(
                            'ACTIVE' => "Y",
                            'VOTE_ID' => $voteID,
                            'C_SORT' => ++$i * 10,
                            'QUESTION' => $questionText,
                            'QUESTION_TYPE' => "html",
                            'DIAGRAM' => "Y",
                            'REQUIRED' => "Y",
                            'DIAGRAM_TYPE' => "histogram"
                        );
                        $voteQuestionID = CVoteQuestion::Add($voteQuestionFields);
                        if ($voteQuestionID > 0) {
                            foreach ($arAnswers as $k => $arAnswer) {
                                $voteAnswerFields = array(
                                    "QUESTION_ID" => $voteQuestionID,
                                    "ACTIVE" => ($arAnswer["ACTIVE"] == 'N' ? 'N' : 'Y'),
                                    "C_SORT" => (intval($arAnswer["C_SORT"]) > 0 ? intval($arAnswer["C_SORT"]) : ($k + 1) * 100),
                                    "MESSAGE" => ($arAnswer["MESSAGE"] != ' ') ? trim($arAnswer["MESSAGE"]) : ' ',
                                    "FIELD_TYPE" => $arAnswer["FIELD_TYPE"],
                                    "FIELD_WIDTH" => intVal($arAnswer["FIELD_WIDTH"]),
                                    "FIELD_HEIGHT" => intVal($arAnswer["FIELD_HEIGHT"]),
                                    "FIELD_PARAM" => trim($arAnswer["FIELD_PARAM"]),
                                    "COLOR" => trim($arAnswer["COLOR"])
                                );
                                CVoteAnswer::Add($voteAnswerFields);
                            }
                        }
                    }
                    foreach ($arTextQuestions as $questionText) {
                        $voteQuestionFields = array(
                            'ACTIVE' => "Y",
                            'VOTE_ID' => $voteID,
                            'C_SORT' => ++$i * 10,
                            'QUESTION' => $questionText,
                            'QUESTION_TYPE' => "html",
                            'DIAGRAM' => "N",
                            'REQUIRED' => "Y",
                            'DIAGRAM_TYPE' => "histogram"
                        );
                        $voteQuestionID = CVoteQuestion::Add($voteQuestionFields);
                        if ($voteQuestionID > 0) {
                                $voteAnswerFields = array(
                                    "QUESTION_ID" => $voteQuestionID,
                                    "ACTIVE" => 'Y',
                                    "C_SORT" => 500,
                                    "MESSAGE" => ' ',
                                    "FIELD_TYPE" => 5,
                                    "FIELD_WIDTH" => 0,
                                    "FIELD_HEIGHT" => 5,
                                    "FIELD_PARAM" => "",
                                    "COLOR" => ""
                                );
                                CVoteAnswer::Add($voteAnswerFields);
                        }
                    }
                }
            } else {
                $this->outError($APPLICATION->GetException());
                return false;
            }
        } else {
            $this->outError("vote module is not installed");
            return false;
        }
        return true;
    }

    public function down(){
        return true;
    }

}
