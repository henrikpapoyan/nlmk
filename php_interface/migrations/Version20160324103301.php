<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160324103301 extends Version {

    protected $description = "Добавление свойства \"Ссылка\" в слайдер \"Миссия и видение\"";

    public function up(){
        $helper = new IblockHelper();
        $arFields = array(
            "NAME" => "Ссылка",
            "ACTIVE" => "Y",
            "SORT" => "100",
            "CODE" => "LINK",
            "PROPERTY_TYPE" => "S",
            "COL_COUNT" => "50"
        );
        $helper->addPropertyIfNotExists(MISSION_SLIDER_IBLOCK_ID_RU, $arFields);
        $helper->addPropertyIfNotExists(MISSION_SLIDER_IBLOCK_ID_EN, $arFields);
    }

    public function down(){
        $helper = new IblockHelper();
        $helper->deleteProperty(MISSION_SLIDER_IBLOCK_ID_RU, "LINK");
        $helper->deleteProperty(MISSION_SLIDER_IBLOCK_ID_EN, "LINK");
    }

}
