<?php

namespace Sprint\Migration;

use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171212131931 extends Version
{
    protected $description = "Убирает некорректные ссылки";

    protected $replaceLinks = [
        '/ru/about/management/board-of-directors/oleg-bagrin/' => '/ru/about/governance/board-of-directors/the-board-of-directors/oleg-bagrin/',
        '/en/about/management/management-board/grigory-fedorishin/' => '/en/about/governance/management-board/the-composition-of-the-board/grigory-fedorishin/',
        '/ru/about/management/management-board/oleg-bagrin/' => '/ru/about/governance/management-board/the-composition-of-the-board/oleg-bagrin/',
        '/en/about/management/management-board/oleg-bagrin/' => '/en/about/governance/management-board/the-composition-of-the-board/oleg-bagrin/',
        '/en/about/management/management-board/sergey-filatov/' => '/en/about/governance/management-board/the-composition-of-the-board/sergey-filatov/',
        '/ru/about/management/management-board/sergey-filatov/' => '/ru/about/governance/management-board/the-composition-of-the-board/sergey-filatov/',
        '/ru/about/management/management-board/grigoriy-fedorishin/' => '/ru/about/governance/management-board/the-composition-of-the-board/grigory-fedorishin/',
        '/en/about/governance/management-board/the-composition-of-the-board/grigoriy-fedorishin/' => '/en/about/governance/management-board/the-composition-of-the-board/grigory-fedorishin/',
        '/ru/about/governance/management-board/the-composition-of-the-board/grigoriy-fedorishin/' => '/ru/about/governance/management-board/the-composition-of-the-board/grigory-fedorishin/',
        '/en/about/management/board-of-directors/helmut-wieser/' => '/en/about/governance/board-of-directors/the-board-of-directors/helmut-wieser/',
        '/ru/about/management/board-of-directors/helmut-wieser/' => '/ru/about/governance/board-of-directors/the-board-of-directors/helmut-wieser/',
        '/about/management/management-board/oleg-bagrin/' => '/about/governance/management-board/the-composition-of-the-board/oleg-bagrin/',
    ];

    public function up()
    {
        foreach ($this->replaceLinks as $currentLink => $replacement) {
            $arFilter = Array("DETAIL_TEXT" => '%' . $currentLink . '%');
            $res = \CIBlockElement::GetList(
                [],
                $arFilter,
                false,
                false,
                ['ID', 'NAME', 'DETAIL_TEXT', 'LID', 'IBLOCK_ID']
            );
            $el = new \CIBlockElement;

            while($ob = $res->Fetch()){
                if ($currentLink == '/about/management/management-board/oleg-bagrin/' && $ob['LID'] == 's1') {
                    $arLoadProductArray = ["DETAIL_TEXT" => str_replace($currentLink, '/ru'.$replacement, $ob['DETAIL_TEXT'])];
                } elseif ($currentLink == '/about/management/management-board/oleg-bagrin/' && $ob['LID'] == 's2') {
                    $arLoadProductArray = ["DETAIL_TEXT" => str_replace($currentLink, '/en'.$replacement, $ob['DETAIL_TEXT'])];
                }  else {
                    $arLoadProductArray = ["DETAIL_TEXT" => str_replace($currentLink, $replacement, $ob['DETAIL_TEXT'])];
                }

                $el->Update($ob['ID'], $arLoadProductArray);
            }
        }
    }

    public function down()
    {
        $helper = new IblockHelper();
    }
}
