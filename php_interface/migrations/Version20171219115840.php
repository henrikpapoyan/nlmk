<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;


class Version20171219115840 extends Version {

    protected $description = "Создание почтовых шаблонов обратной связи";

    public function up(){
		$helper = new IblockHelper();
		$et = new \CEventType;

        $arr = array(
        	"EVENT_NAME"    => "FEEDBACK_FORM_NLMK",
        	"LID"           => 'ru',
        	"NAME"          => "Тип обратной связи НЛМК",
        	"DESCRIPTION"   => "#AUTHOR# - Автор сообщения
#AUTHOR_EMAIL# - Email автора сообщения
#TEXT# - Текст сообщения
#EMAIL_FROM# - Email отправителя письма
#EMAIL_TO# - Email получателя письма"
        );

 		if ($et->Add($arr)) {
        	$this->outSuccess('Добавлен тип почтового события');
		} else {
			$this->outError('Не удалось добавить тип почтового события');
        }

		$emess = new \CEventMessage;
		/*  $arFilter = Array(
            "EVENT_NAME"  => "FEEDBACK_FORM_NLMK",
            "LID"         => "s1",
        );
        $rsMess = $emess->GetList($by="site_id", $order="desc", $arFilter);
        while($arMess = $rsMess->GetNext()) {
            $emess->Delete(intval($arMess["ID"]));
}*/

        $arr = array(
            "ACTIVE" => "Y",
            "EVENT_NAME" => "FEEDBACK_FORM_NLMK",
            "LID" => "s1",
            "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
            "EMAIL_TO" => "#AUTHOR_EMAIL#",
            "SUBJECT" => "#SITE_NAME#: Уведомление автору по вопросу #TYPE_ISSUES#",
            "BODY_TYPE" => "text",
            "MESSAGE" => "Информационное сообщение сайта #SITE_NAME#
                ------------------------------------------
                
                Автор: #AUTHOR#
                E-mail автора: #AUTHOR_EMAIL#
                
                Текст сообщения:
                #TEXT#
                
                Спасибо за обращение!",
        );
        $emess->Add($arr);

        $arr = array(
            "ACTIVE" => "Y",
            "EVENT_NAME" => "FEEDBACK_FORM_NLMK",
            "LID" => "s1",
            "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
            "EMAIL_TO" => "#EMAIL_TO#",
            "SUBJECT" => "#SITE_NAME#: Сообщение обратной связи по вопросу #TYPE_ISSUES#",
            "BODY_TYPE" => "text",
            "MESSAGE" => "Информационное сообщение сайта #SITE_NAME#
                ------------------------------------------
                
                Вам было отправлено сообщение через форму обратной связи
                
                Мои ФИО: #AUTHOR#
                Мой E-mail: #AUTHOR_EMAIL#
                Название моей компании: #NAME_COMPANY#
                Моя компания: #RESIDENT#
                
                Текст сообщения:
                #TEXT#",
        );
        $emess->Add($arr);
    }

    public function down(){

		$et = new \CEventType;
		$et->Delete("FEEDBACK_FORM_NLMK");

        $emess = new \CEventMessage;
        $arFilter = Array(
            "EVENT_NAME"  => "FEEDBACK_FORM_NLMK",
            "LID"         => "s1",
        );
        $rsMess = $emess->GetList($by="site_id", $order="desc", $arFilter);
        while($arMess = $rsMess->GetNext()) {
            $emess->Delete(intval($arMess["ID"]));
        }
    }

}
