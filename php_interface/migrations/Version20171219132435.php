<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171219132435 extends Version {

    protected $description = "Создание инфоблоков обратной связи";

    public function up(){

        $helper = new IblockHelper();

        $arFieldsType = array(
            'ID' => 'feedback',
            'LANG' => array(
                'en' => array('NAME' => 'Feedback'),
                'ru' => array('NAME' => 'Обратная связь')
            )
        );
        $iblockType = $helper->addIblockTypeIfNotExists($arFieldsType);

        if ($iblockType == "feedback") {
            $this->outSuccess('Добавлен тип инфоблока #' . $iblockType);
            // добавление первого инфоблока
            $arFields =  array(
                "ACTIVE" => "Y",
                "NAME" => "Список email для обратной связи",
                "CODE" => "list_email_feedback",
                "IBLOCK_TYPE_ID"   => "feedback",
                "SITE_ID" => array("s1"),
                "SORT" => "500",
                "GROUP_ID" => array( // Доступ
                    "1" => "X", // Админам всё
                    "2" => "R", // Пользователям чтение
                ),
                'DESCRIPTION' => 'Список email для обратной связи',
                "FIELDS" => array(
                    "CODE" => array(
                        "IS_REQUIRED" => "N",
                        "DEFAULT_VALUE" => array()
                    ),
                )
            );
            $iblockId1 = $helper->addIblockIfNotExists($arFields);
            if ($iblockId1 > 0) {
                $this->outSuccess('Добавлен инфоблок #' . $iblockId1);

                // Добавляем свойство "email"
                $propName = 'Email';
                $arPropFields = array(
                    'IBLOCK_ID' => $iblockId1,
                    'NAME' => $propName,
                    'ACTIVE' => 'Y',
                    'SORT' => 10,
                    'CODE' => 'EMAIL',
                    'PROPERTY_TYPE' => 'S',
                    'ROW_COUNT' => 1,
                    'COL_COUNT' => 30,
                    'MULTIPLE' => 'N',
                    'WITH_DESCRIPTION' => 'N',
                    'SEARCHABLE' => 'N',
                    'FILTRABLE' => 'N',
                    'IS_REQUIRED' => 'N',
                    'VERSION' => 1
                );
                if ($helper->addPropertyIfNotExists($iblockId1, $arPropFields)) {
                    $this->outSuccess('Добавлено свойство ' . $propName);

                } else {
                    $this->outError('Не удалось добавить свойство ' . $propName);
                }

                $propName = 'Типы вопросов';
                $arPropFields = array(
                    'IBLOCK_ID' => $iblockId1,
                    'NAME' => $propName,
                    'ACTIVE' => 'Y',
                    'SORT' => 10,
                    'CODE' => 'LIST_TYPE_ISSUES',
                    'PROPERTY_TYPE' => 'S',
					'LIST_TYPE' => 'C',
                    'ROW_COUNT' => 1,
                    'COL_COUNT' => 30,
                    'MULTIPLE' => 'N',
                    'WITH_DESCRIPTION' => 'N',
                    'SEARCHABLE' => 'N',
                    'FILTRABLE' => 'N',
                    'IS_REQUIRED' => 'N',
                    'VERSION' => 1
                );
                if ($helper->addPropertyIfNotExists($iblockId1, $arPropFields)) {
                    $this->outSuccess('Добавлено свойство ' . $propName);
                } else {
                    $this->outError('Не удалось добавить свойство ' . $propName);
                }
                $listTypeIssues = array(
                    "Выбор поставщика (ТМЦ)" => array(
                        0 => "Выбор поставщика",
                        1 => "viselskaya_en@nlmk.com"
                    ),
                    "Выбор поставщика (Услуги)" => array(
                        0 => "Выбор поставщика",
                        1 => "komarova_ov@nlmk.com"
                    ),
                    "Размещение заказов и поставок (ТМЦ)" => array(
                        0 => "Размещение заказов и поставок",
                        1 => "viselskaya_en@nlmk.com"
                    ),
                    "Размещение заказов и поставок (Услуги)" => array(
                        0 => "Размещение заказов и поставок",
                        1 => "komarova_ov@nlmk.com"
                    ),
                    "Оформление документов (ТМЦ)" => array(
                        0 => "Оформление документов",
                        1 => "viselskaya_en@nlmk.com"
                    ),
                    "Оформление документов (Услуги)" => array(
                        0 => "Оформление документов",
                        1 => "komarova_ov@nlmk.com"
                    ),
                    "Техническая поддержка" => array(
                        0 => "Технические вопросы",
                        1 => "srm@nlmk.ru"
                    ),
                    "Регистрация SRM" => array(
                        0 => "Технические вопросы",
                        1 => "srm@nlmk.ru"
                    ),
                );

                $sort = 1;
                $theme = "Выбор поставщика";
                foreach ($listTypeIssues as $key => $item) {
                    if ($item[0] != $theme){
                        $theme = $item[0];
                        $this->outError($sort);
                        $sort++;
                    } else {
                        $theme = $item[0];
                    }
                    if($helper->addElement($iblockId1,
                        array('NAME' => $item[0],
                              'ACTIVE' => 'Y',
                              'SORT' => $sort,
                              "PROPERTY_VALUES" => array(
                                  "EMAIL" => $item[1],
                                  "LIST_TYPE_ISSUES" => $key
                              )
                        ))) {
                        $this->outSuccess("Создан тип вопроса ".$key);
                        $this->outError($item[0]);
                        $this->outSuccess($theme);



                    } else {
                        $this->outError("Не удалось создать тип вопроса ".$key);
                    }
                }

            } else {
                $this->outError('Не удалось создать инфоблок');
            }

            // добавление второго инфоблока
            $arFields =  array(
                "ACTIVE" => "Y",
                "NAME" => "Отчеты обратной связи",
                "CODE" => "report_feedback",
                "IBLOCK_TYPE_ID"   => "feedback",
                "SITE_ID" => array("s1"),
                "SORT" => "600",
                "GROUP_ID" => array( // Доступ
                    "1" => "X", // Админам всё
                    "2" => "R", // Пользователям чтение
                ),
                'DESCRIPTION' => 'Отчеты обратной связи',
                "FIELDS" => array(
                    "CODE" => array(
                        "IS_REQUIRED" => "N",
                        "DEFAULT_VALUE" => array()
                    ),
                )
            );
            $iblockId = $helper->addIblockIfNotExists($arFields);
            if ($iblockId > 0) {
                $this->outSuccess('Добавлен инфоблок #' . $iblockId);

                $propName = 'Email';
                $arPropFields = array(
                    'IBLOCK_ID' => $iblockId,
                    'NAME' => $propName,
                    'ACTIVE' => 'Y',
                    'SORT' => 10,
                    'CODE' => 'AUTHOR_EMAIL',
                    'PROPERTY_TYPE' => 'S',
                    'ROW_COUNT' => 1,
                    'COL_COUNT' => 30,
                    'MULTIPLE' => 'N',
                    'WITH_DESCRIPTION' => 'N',
                    'SEARCHABLE' => 'N',
                    'FILTRABLE' => 'N',
                    'IS_REQUIRED' => 'Y',
                    'VERSION' => 1
                );
                if ($helper->addPropertyIfNotExists($iblockId, $arPropFields)) {
                    $this->outSuccess('Добавлено свойство ' . $propName);
                } else {
                    $this->outError('Не удалось добавить свойство ' . $propName);
                }

                $propName = 'Название компании';
                $arPropFields = array(
                    'IBLOCK_ID' => $iblockId,
                    'NAME' => $propName,
                    'ACTIVE' => 'Y',
                    'SORT' => 20,
                    'CODE' => 'NAME_COMPANY',
                    'PROPERTY_TYPE' => 'S',
                    'ROW_COUNT' => 1,
                    'COL_COUNT' => 30,
                    'MULTIPLE' => 'N',
                    'WITH_DESCRIPTION' => 'N',
                    'SEARCHABLE' => 'N',
                    'FILTRABLE' => 'N',
                    'IS_REQUIRED' => 'Y',
                    'VERSION' => 1
                );
                if ($helper->addPropertyIfNotExists($iblockId, $arPropFields)) {
                    $this->outSuccess('Добавлено свойство ' . $propName);
                } else {
                    $this->outError('Не удалось добавить свойство ' . $propName);
                }

                $propName = 'Резидент';
                $arPropFields = array(
                    'IBLOCK_ID' => $iblockId,
                    'NAME' => $propName,
                    'ACTIVE' => 'Y',
                    'SORT' => 30,
                    'CODE' => 'RESIDENT',
                    'PROPERTY_TYPE' => 'S',
                    'ROW_COUNT' => 1,
                    'COL_COUNT' => 30,
                    'MULTIPLE' => 'N',
                    'WITH_DESCRIPTION' => 'N',
                    'SEARCHABLE' => 'N',
                    'FILTRABLE' => 'N',
                    'IS_REQUIRED' => 'Y',
                    'VERSION' => 1
                );
                if ($helper->addPropertyIfNotExists($iblockId, $arPropFields)) {
                    $this->outSuccess('Добавлено свойство ' . $propName);
                } else {
                    $this->outError('Не удалось добавить свойство ' . $propName);
                }
                $propName = 'Сообщение';
                $arPropFields = array(
                    'IBLOCK_ID' => $iblockId,
                    'NAME' => $propName,
                    'ACTIVE' => 'Y',
                    'SORT' => 30,
                    'CODE' => 'TEXT',
                    'PROPERTY_TYPE' => 'S',
                    'ROW_COUNT' => 1,
                    'COL_COUNT' => 50,
                    'MULTIPLE' => 'N',
                    'WITH_DESCRIPTION' => 'N',
                    'SEARCHABLE' => 'N',
                    'FILTRABLE' => 'N',
                    'IS_REQUIRED' => 'Y',
                    'VERSION' => 1
                );
                if ($helper->addPropertyIfNotExists($iblockId, $arPropFields)) {
                    $this->outSuccess('Добавлено свойство ' . $propName);
                } else {
                    $this->outError('Не удалось добавить свойство ' . $propName);
                }

                $propName = 'Тип вопроса';
                $arPropFields = array(
                    'IBLOCK_ID' => $iblockId,
                    'NAME' => $propName,
                    'ACTIVE' => 'Y',
                    'SORT' => 30,
                    'CODE' => 'TYPE_ISSUES',
                    'PROPERTY_TYPE' => 'S',
                    'ROW_COUNT' => 1,
                    'COL_COUNT' => 50,
                    'MULTIPLE' => 'N',
                    'WITH_DESCRIPTION' => 'N',
                    'SEARCHABLE' => 'N',
                    'FILTRABLE' => 'N',
                    'IS_REQUIRED' => 'Y',
                    'VERSION' => 1
                );
                if ($helper->addPropertyIfNotExists($iblockId, $arPropFields)) {
                    $this->outSuccess('Добавлено свойство ' . $propName);
                } else {
                    $this->outError('Не удалось добавить свойство ' . $propName);
                }

                if ($iblockId1 > 0) {
                    $propName = 'Тема вопроса';
                    $arPropFields = [
                        'IBLOCK_ID' => $iblockId,
                        'NAME' => $propName,
                        'ACTIVE' => 'Y',
                        'SORT' => 40,
                        'CODE' => 'EMAIL_TO',
                        'PROPERTY_TYPE' => 'E',
                        'ROW_COUNT' => 1,
                        'MULTIPLE' => 'N',
                        'SEARCHABLE' => 'N',
                        'FILTRABLE' => 'N',
                        'IS_REQUIRED' => 'Y',
                        'VERSION' => 1,
                        'DISPLAY_TYPE' => 'F',
                        'LINK_IBLOCK_ID' => $iblockId1
                    ];
                    if ($helper->addPropertyIfNotExists($iblockId, $arPropFields)) {
                        $this->outSuccess('Добавлено свойство ' . $propName);
                    } else {
                        $this->outError('Не удалось добавить свойство ' . $propName);
                    }
                } else {
                    $this->outError('Не удалось определить код инфоблока "Каталог" по коду "catalog"');
                }

            } else {
                $this->outError('Не удалось создать инфоблок');
            }

        } else {
            $this->outError('Не удалось создать тип инфоблока');
        }
    }

    public function down(){
        $helper = new IblockHelper();

        $iblockCode = 'report_feedback';
        if ($helper->deleteIblockIfExists($iblockCode, $iblockTypeId = '')) {
            $this->outSuccess('Инфоблок "' . $iblockCode . '" удалён');
        } else {
            $this->outError('Инфоблок "' . $iblockCode . '" не удалось удалить.');
        }

        $iblockCode = 'list_email_feedback';
        if ($helper->deleteIblockIfExists($iblockCode, $iblockTypeId = '')) {
            $this->outSuccess('Инфоблок "' . $iblockCode . '" удалён');
        } else {
            $this->outError('Инфоблок "' . $iblockCode . '" не удалось удалить.');
        }
    }
}
