<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160530181236 extends Version {

	protected $description = "Добавление вакансий";

	public function up(){
		$companyId = "";
		$sectionId = "";

		$dbCompanies = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => COMPANIES_IBLOCK_ID_RU, "ACTIVE" => "Y", "CODE" => "novolipetsk"),
			false,
			false,
			array("ID", "IBLOCK_ID", "NAME")
		);
		if ($arCompany = $dbCompanies->GetNext())
			$companyId = $arCompany["ID"];

		$dbSections = \CIBlockSection::GetList(
			array(),
			array("IBLOCK_ID" => JOBS_IBLOCK_ID_RU, "NAME" => "Продажи", "ACTIVE" => "Y"),
			false,
			array(),
			false
		);
		if ($arSection = $dbSections->GetNext())
			$sectionId = $arSection["ID"];

		$el = new \CIBlockElement;

		$arProperties_1 = array();
		$arProperties_1["REGION"] = $companyId;
		$arProperties_1["RESPONSIBILITIES"] = Array(
			"VALUE" => Array (
				"TEXT" => '<ul>
<li>Планирование продаж и операций (S&OP - Sales and Operations Planning) с целью максимизации прибыльности Компании.</li>
<li>Разработка и реализация мероприятий по повышению эффективности Интегрированного планирования;</li>
<li>Активное участие в работе проектных команд Программы планирования: Календарное планирование и графикование производства, Order Dressing, КСУ НСИ;</li>
<li>Персональная ответственность за процесс Планирования продаж и операций (S&OP) и его непрерывное совершенствование;</li>
<li>Проведение план-факт анализа на регулярной основе, анализ причин расхождений;</li>
<li>Проведение анализа сегментации клиентов, повторяемости заказов, стоимости обслуживания (Cost-to-Serve), разработка каталога услуг (Service Catalogue);</li>
<li>Разработка и мониторинг KPI процесса Планирования продаж и операций (S&OP).</li>
</ul>',
				"TYPE" => "html"
			)
		);
		$arProperties_1["REQUIREMENTS"] = Array(
			"VALUE" => Array (
				"TEXT" => '<ul>
<li>Образование: Высшее образование (желательно техническое)</li>
<li>Возраст: Предпочтительно от 35 до 45 лет</li>
<li>Иностранный язык: Владение английским языком (уровень upper intermediate)</li>
<li>Опыт работы в Бизнес-консалтинге и планировании (от 5 лет)</li>
<li>Опыт работы в системе SAP APO, SCM приветствуется</li>
<li>Высокий уровень владения программной Excel, включая написание макросов и программирование на Visual Basic</li>
<li>SQL</li>
<li>Знание (обучение, сертификация) систем календарного планирования и графикования производства (Quintiq, PSI и подобных)</li>
<li>Преимуществом является опыт работы в планировании (очень желательно в металлургии, но можно рассмотреть FMCG и Retail)</li>
<li>Опыт участия в проектах внедрения ИТ-систем для планирования.</li>
</ul>',
				"TYPE" => "html"
			)
		);
		$arProperties_1["OTHER_QUALIFICATIONS"] = Array(
			"VALUE" => Array (
				"TEXT" => '<ul>
<li>Стабильные навыки выстраивания эффективного взаимодействия с различными подразделениями компании</li>
<li>Высокий уровень самостоятельности при решении поставленных задач</li>
<li>Развитые аналитические способности, включая работу с большими объемами информации</li>
<li>Умение работать в ситуации неопределенности</li>
<li>Инициативность, нацеленность на результат.</li>
<li>Амбициозность.</li>
</ul>',
				"TYPE" => "html"
			)
		);
		$arProperties_1["TYPE_OF_EMPLOYMENT"] = Array("VALUE" => Array ("TEXT" => '<p>Полный рабочий день/ Неполный рабочий день</p>', "TYPE" => "html"));

		$arFields_1 = Array(
			"ACTIVE_FROM" =>  ConvertTimeStamp(time(), "FULL"),
			"IBLOCK_SECTION_ID" => (!empty($sectionId) ? $sectionId : false),
			"IBLOCK_ID"      => JOBS_IBLOCK_ID_RU,
			"PROPERTY_VALUES"=> $arProperties_1,
			"NAME"           => "Главный специалист",
			"CODE" => "glavnyy-spetsialist",
			"ACTIVE"         => "Y",
		);

		if($elementId = $el->Add($arFields_1))
			$this->outSuccess("Элемент добавлен");
		else
		  echo $this->outError($el->LAST_ERROR);

		$arProperties_2 = array();
		$arProperties_2["REGION"] = $companyId;
		$arProperties_2["RESPONSIBILITIES"] = Array(
			"VALUE" => Array (
				"TEXT" => '<ul>
<li>Обеспечение реализации и развития продаж продукции Q&T (износостойкий и высокопрочный прокат) в заданном регионе (СЗФО)</li>
<li>Поддержание продаж действующим Заказчикам в заданном регионе</li>
<li>Поиск новых Заказчиков и расширение клиентской базы в заданном регионе</li>
<li>Подготовка и защита плана продаж в заданном регионе</li>
<li>Подготовка и защита плана развития ключевых Заказчиков в заданном регионе</li>
<li>Работа в CRM</li>
<li>Отслеживание дебиторской задолженности</li>
<li>Развитие складской программы в заданном регионе</li>
<li>Техническая поддержка Заказчиков (совместно с техническими специалистами)</li>
<li>Участие и предложения по маркетинговым мероприятиям в заданном регионе</li>
</ul>',
				"TYPE" => "html"
			)
		);
		$arProperties_2["REQUIREMENTS"] = Array(
			"VALUE" => Array (
				"TEXT" => '<ul>
<li>Высшее образование (желательно техническое/экономическое)</li>
<li>Предпочтительно от 35 до 45 лет</li>
<li>Владение английским языком (уровень intermediate)</li>
<li>Коммерсант с глубоким пониманием отрасли: понимание специфики продаж металлопродукции</li>
<li>На 100% незапятнанная репутация (отсутствие коррупционной составляющей в продажах)</li>
<li>Активная жизненная позиция: нам необходим человек «с горящими глазами», который не устал от работы, который хочет расти, демонстрировать результат, заражать своей энергией сотрудников</li>
<li>Активное перемещение и личное посещение клиентов </li>
</ul>',
				"TYPE" => "html"
			)
		);
		$arProperties_2["OTHER_QUALIFICATIONS"] = Array(
			"VALUE" => Array (
				"TEXT" => '<ul>
<li>Порядочность и лояльность</li>
<li>Структурированность и системность</li>
<li>Способность к планированию и анализу</li>
<li>Нацеленность на результат</li>
<li>Самостоятельность</li>
<li>Инициативность, нацеленность на результат</li>
<li>Амбициозность</li>
</ul>',
				"TYPE" => "html"
			)
		);
		$arProperties_2["TYPE_OF_EMPLOYMENT"] = Array("VALUE" => Array ("TEXT" => '<p>Полный рабочий день/ Неполный рабочий день</p>', "TYPE" => "html"));
		$arFields_2 = Array(
			"ACTIVE_FROM" =>  ConvertTimeStamp(time(), "FULL"),
			"IBLOCK_SECTION_ID" => (!empty($sectionId) ? $sectionId : false),
			"IBLOCK_ID"      => JOBS_IBLOCK_ID_RU,
			"PROPERTY_VALUES"=> $arProperties_1,
			"NAME"           => "Главный специалист",
			"CODE" => "glavnyy-spetsialist-2",
			"ACTIVE"         => "Y",
		);

		if($elementId = $el->Add($arFields_2))
			$this->outSuccess("Элемент добавлен");
		else
		  echo $this->outError($el->LAST_ERROR);

	}

	public function down(){
		$dbElements = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => JOBS_IBLOCK_ID_RU, "ACTIVE" => "Y", "CODE" => array("glavnyy-spetsialist-2", "glavnyy-spetsialist")),
			false,
			false,
			array("ID", "IBLOCK_ID", "NAME")
		);
		while ($arElement = $dbElements->GetNext())
		{
			if (!\CIBlockElement::Delete($arElement["ID"]))
				$this->outError("Ошибка удаления элемента");
			else
				$this->outSuccess("Элемент удален");
		}
	}

}
