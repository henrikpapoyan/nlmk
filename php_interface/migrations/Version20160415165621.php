<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160415165621 extends Version {

    protected $description = "исправление Biography in dockx format и настройка формы редактирования элемента ИБ Слайдер - отечественное производство";

    public function up(){

		$arIblockId = array(
			// 177, 178
			MISSION_SLIDER_LEADERSHIP_IBLOCK_ID_RU,
			MISSION_SLIDER_LEADERSHIP_IBLOCK_ID_EN
		);
		$arFields = array("LIST_MODE" => "C");
		$iblock = new \CIBlock;
		foreach ($arIblockId as $iblockId)
		{
			$iblock->Update($iblockId, $arFields);
		}

		$helper = new IblockHelper();
		$arIblocks = array(
			// 177 => array(
			MISSION_SLIDER_LEADERSHIP_IBLOCK_ID_RU => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Элемент",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Активность"),
						array("NAME" => "NAME", "TITLE" => "*Название"),
						array("NAME" => "SORT", "TITLE" => "Сортировка"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Картинка для анонса"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Описание для анонса")
					)
				),
			),
			// 178 => array(
			MISSION_SLIDER_LEADERSHIP_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Preview text")
					)
				),
			),
			MISSION_SLIDER_LEADERSHIP_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Preview text")
					)
				),
			),
			MANAGEMENT_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "ROLE"), "TITLE" => "Chairman of the Board"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "FILTER"), "TITLE" => "Filter"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "BIOGRAPHY_PDF"), "TITLE" => "Biography in pdf format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "BIOGRAPHY_DOCKX"), "TITLE" => "Biography in docx format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "MULTIMEDIA"), "TITLE" => "Multimedia"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(MANAGEMENT_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					)
				),
			),
			DIRECTORS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Photo"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "ROLE"), "TITLE" => "Role"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "FILTER"), "TITLE" => "Filter"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "BIOGRAPHY_PDF"), "TITLE" => "Biography in pdf format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "BIOGRAPHY_DOCKX"), "TITLE" => "Biography in docx format"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "MULTIMEDIA"), "TITLE" => "Multimedia"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(DIRECTORS_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text")
					),
				)
			),

		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}
    }

    public function down(){
        $helper = new IblockHelper();
    }

}
