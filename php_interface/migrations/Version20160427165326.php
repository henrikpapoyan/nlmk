<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160427165326 extends Version {

    protected $description = "Создание агента для обработки PDF";

    public function up(){
        $helper = new IblockHelper();

        \CAgent::AddAgent("CParsePDF::FindAndParse();", "main",
          "N", 3600, "", "Y", "", 30
        );
    }

    public function down(){
        $helper = new IblockHelper();

        \CAgent::RemoveAgent("CParsePDF::FindAndParse();", "main");
    }

}