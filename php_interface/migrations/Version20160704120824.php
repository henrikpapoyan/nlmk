<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160704120824 extends Version
{

    protected $description = "Добавление скрытого вопроса в формы \"Отправить резюме\" и \"Send CV\" ";

    public function up()
    {
        \CModule::IncludeModule("form");
        \CFormField::Set(array(
            "FORM_ID" => SEND_CV_FORM_ID_RU,
            "ACTIVE" => "Y",
            "TITLE" => "ANTISPAM",
            "TITLE_TYPE" => "text",
            "SID" => "ANTISPAM",
            "REQUIRED" => "N",
            "arANSWER" => array(
                array(
                    "MESSAGE" => " ",
                    "ACTIVE" => "Y",
                    "FIELD_TYPE" => "hidden"
                )
            )
        ));
        \CFormField::Set(array(
            "FORM_ID" => SEND_CV_FORM_ID_EN,
            "ACTIVE" => "Y",
            "TITLE" => "ANTISPAM",
            "TITLE_TYPE" => "text",
            "SID" => "ANTISPAM",
            "REQUIRED" => "N",
            "arANSWER" => array(
                array(
                    "MESSAGE" => " ",
                    "ACTIVE" => "Y",
                    "FIELD_TYPE" => "hidden"
                )
            )
        ));
    }

    public function down()
    {
        $helper = new IblockHelper();
    }

}
