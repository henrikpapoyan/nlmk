<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160428164658 extends Version {

    protected $description = "Пользовательские настройки изменения элемента для ИБ \"Контакты в сайдбаре (общие)\" (ru/en) ";

    public function up()
    {
        $helper = new IblockHelper();
		if (defined("CONTACTS_SIDEBAR_IBLOCK_ID_RU") && defined("CONTACTS_SIDEBAR_IBLOCK_ID_EN"))
		{
			$obIblock = new \CIBlock;
			$obIblock->Update(CONTACTS_SIDEBAR_IBLOCK_ID_RU, array("LIST_MODE" => "C"));
			$obIblock->Update(CONTACTS_SIDEBAR_IBLOCK_ID_EN, array("LIST_MODE" => "C"));

			$arIblocks = array(
				CONTACTS_SIDEBAR_IBLOCK_ID_RU => array(
					array(
						"CODE" => "edit1",
						"TITLE" => "Элемент",
						"FIELDS" => array(
							array("NAME" => "ACTIVE", "TITLE" => "Активность"),
							array("NAME" => "SORT", "TITLE" => "Сортировка"),
							array("NAME" => "NAME", "TITLE" => "*Имя, Фамилия"),
							array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Фото"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_SIDEBAR_IBLOCK_ID_RU, "POST"), "TITLE" => "*Должность"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_SIDEBAR_IBLOCK_ID_RU, "EMAIL"), "TITLE" => "*Адрес электронной почты"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_SIDEBAR_IBLOCK_ID_RU, "PHONE"), "TITLE" => "Контактные телефоны"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_SIDEBAR_IBLOCK_ID_RU, "PAGE_URL"), "TITLE" => "Страница")
						)
					)
				),
				CONTACTS_SIDEBAR_IBLOCK_ID_EN => array(
					array(
						"CODE" => "edit1",
						"TITLE" => "Element",
						"FIELDS" => array(
							array("NAME" => "ACTIVE", "TITLE" => "Active"),
							array("NAME" => "SORT", "TITLE" => "Sorting"),
							array("NAME" => "NAME", "TITLE" => "*Person name"),
							array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Photo"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_SIDEBAR_IBLOCK_ID_EN, "POST"), "TITLE" => "*Post"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_SIDEBAR_IBLOCK_ID_EN, "EMAIL"), "TITLE" => "*Email"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_SIDEBAR_IBLOCK_ID_EN, "PHONE"), "TITLE" => "Phone"),
							array("NAME" => "PROPERTY_" . $helper->getPropertyId(CONTACTS_SIDEBAR_IBLOCK_ID_EN, "PAGE_URL"), "TITLE" => "Page")
						)
					)
				)
			);

			foreach ($arIblocks as $iblockId => $arIblock)
			{
				$tabsString = "";
				foreach($arIblock as $arTab) {
					$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
					foreach($arTab["FIELDS"] as $field) {
						$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
						if (end($arTab["FIELDS"]) == $field) {
							$tabsString .= "--;--";
							continue;
						}
						$tabsString .= "--,--";
					}
				}
				$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
				\CUserOptions::SetOptionsFromArray($arOptions);
			}
		}
		else
		{

		}
    }

    public function down()
    {
        $helper = new IblockHelper();
    }

}
