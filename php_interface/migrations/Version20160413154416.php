<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160413154416 extends Version {

	protected $description = "Изменение пользовательских настроек формы редактирования элементов инфоблоков (для англ. инфоблоков) Sustainability";

	public function up()
	{
		$helper = new IblockHelper();
		$arIblocks = array(
			CHARITY_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Title1"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(CHARITY_SLIDER_IBLOCK_ID_EN, "TITLE2"), "TITLE" => "*Title2"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(CHARITY_SLIDER_IBLOCK_ID_EN, "TITLE3"), "TITLE" => "Title3"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(CHARITY_SLIDER_IBLOCK_ID_EN, "TITLE4"), "TITLE" => "Title4"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
					)
				),
			),

			ECOLOGY_SLIDER_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Title"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ECOLOGY_SLIDER_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ECOLOGY_SLIDER_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),

					)
				),
			),

			ECOLOGY_CASES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text"),
					)
				),
			),


			CHARITY_PROJECTS_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Title"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(CHARITY_PROJECTS_IBLOCK_ID_EN, "SUBTITLE"), "TITLE" => "Subtitle"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text"),
					)
				),
			),


			CHARITY_PROGRAM_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text"),
					)
				),
			),


			ENV_PROGRAMME_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ENV_PROGRAMME_IBLOCK_ID_EN, "STATUS"), "TITLE" => "Status"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(ENV_PROGRAMME_IBLOCK_ID_EN, "YEAR"), "TITLE" => "Year"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "Picture"),
						array("NAME" => "PREVIEW_TEXT", "TITLE" => "Description"),
						array("NAME" => "DETAIL_TEXT", "TITLE" => "Detail Text"),
					)
				),
			),



		);


		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}

	}

	public function down()
	{
	}

}

