<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160328124433 extends Version {

	protected $description = "Обновление свойств ИБ Видео, Аудио";

	public function up(){
		$helper = new IblockHelper();
		$arFields = array(
			"NAME" => "Ссылка на видео с Youtube",
			"ACTIVE" => "Y",
			"SORT" => "100",
			"CODE" => "YOUTUBE_URL",
			"PROPERTY_TYPE" => "S"
		);
		$helper->addPropertyIfNotExists(VIDEOS_IBLOCK_ID_RU, $arFields);
		$helper->addPropertyIfNotExists(VIDEOS_IBLOCK_ID_EN, $arFields);

		$arIblocks = array(
			VIDEOS_IBLOCK_ID_RU,
			VIDEOS_IBLOCK_ID_EN
		);
		foreach ($arIblocks as $iblockId) {
			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "FILE")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new \CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("IS_REQUIRED" => "N"));
			}
		}
	}

	public function down(){
		$helper = new IblockHelper();
		$helper->deleteProperty(VIDEOS_IBLOCK_ID_RU, "YOUTUBE_URL");
		$helper->deleteProperty(VIDEOS_IBLOCK_ID_EN, "YOUTUBE_URL");

		$arIblocks = array(
			VIDEOS_IBLOCK_ID_RU,
			VIDEOS_IBLOCK_ID_EN
		);
		foreach ($arIblocks as $iblockId) {
			$dbProp = \CIBlockProperty::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId, "CODE" => "FILE")
			);
			while($arProp = $dbProp->GetNext()) {
				$CIBlockProperty = new \CIBlockProperty;
				$CIBlockProperty->Update($arProp["ID"], array("IS_REQUIRED" => "Y"));
			}
		}
	}

}
