<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160525105138 extends Version
{

	protected $description = "Почтовые шаблоны для отправки резюме";

	public function up()
	{
		$arMessageAdminRu = array(
			"ACTIVE" => "Y",
			"EVENT_NAME" => "FORM_FILLING_SEND_CV",
			"LID" => array("s1"),
			"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
			"EMAIL_TO" => "job@nlmk.com",
			"CC" => "#REGION_EMAIL#",
			"SUBJECT" => "nlmk.com: Заполнена веб-форма \"Отправить резюме\"",
			"BODY_TYPE" => "html",
			"MESSAGE" => '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>nlmk резюме</title>
	</head>
	<body style="background-color:#ffffff; margin:0;" bgcolor="#ffffff">
		<table style="border-collapse:collapse; margin-bottom:0; font-family:Verdana,sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none; background-color:#ffffff; text-align:center" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="886">
					<table style="border-collapse:collapse; margin-bottom:0; font-family:Verdana,sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none; text-align:left; background-color:#f8f9fa;" bgcolor="#f8f9fa" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="3" height="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td width="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								&nbsp;
							</td>
							<td style="vertical-align:top; padding:0; border:none;" valign="top">
								<table width="886" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none;" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
												&nbsp;
											</td>
											<td width="152" height="106" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
												<a href="http://nlmk.ru/" title="nlmk" target="_blank">
													<img src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/logo.jpg" width="152" height="106" alt="nlmk" style="display:block; line-height:1px; border:none;" />
												</a>
											</td>
											<td width="40" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
												&nbsp;
											</td>
											<td width="486" style="vertical-align:middle; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#007ec7" valign="middle">
												<span style="font-family:Arial, FreeSans, sans-serif; font-size:13px; line-height:21px; color:#333333; text-transform:uppercase; letter-spacing:0.065px;"><a href="http://nlmk.ru/" title="nlmk.ru" target="_blank" style="color:#333333; text-decoration:none">вакансии</a></span>
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
												&nbsp;
											</td>
										</tr>
									</tbody>
								</table>

								<table width="886" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; line-height:20px; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none;" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td colspan="3" height="45" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
											<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
												<span style="font-family:Arial, FreeSans, sans-serif; font-size:26px; line-height:40px; color:#333333;">#VACANCY#</span>
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td colspan="3" height="12" style="vertical-align:top; padding:0; border:none; line-height:12px;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td colspan="3" height="45" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
											<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
												<table width="552" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; line-height:20px; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none;" cellpadding="0" cellspacing="0">
													<tbody>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Регион</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Функциональное направление</span>
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">#REGION#</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">#DIRECTION#</span>
															</td>
														</tr>
														<tr>
															<td colspan="2" height="25" style="vertical-align:top; padding:0; border:none;" valign="top">
																&nbsp;
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">ФИО</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Город пребывания</span>
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">#NAME#</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">#CITY#</span>
															</td>
														</tr>
														<tr>
															<td colspan="2" height="25" style="vertical-align:top; padding:0; border:none;" valign="top">
																&nbsp;
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Дата рождения</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Телефон пребывания</span>
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#666666;">#BIRTHDAY#</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#666666;">#PHONE#</span>
															</td>
														</tr>
														<tr>
															<td colspan="2" height="25" style="vertical-align:top; padding:0; border:none;" valign="top">
																&nbsp;
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Электронная почта</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Желаемый уровень з/п</span>
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">#EMAIL#</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#666666;">#SALARY#</span>
															</td>
														</tr>
														<tr>
															<td colspan="2" height="25" style="vertical-align:top; padding:0; border:none;" valign="top">
																&nbsp;
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Готов к переезду</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">&nbsp;</span>
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">#READY_TO_MOVE#</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#e20000;">&nbsp;</span>
															</td>
														</tr>
														<tr>
															<td colspan="2" height="61" style="vertical-align:middle; padding:0; border:none;" valign="middle">
																<img src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/bg-line.png" width="550" height="1" alt="" style="display:block; line-height:1px; border:none;" />
															</td>
														</tr>
														<tr>
															<td colspan="2" style="vertical-align:middle; padding:0; padding-bottom:20px; border:none;" valign="middle">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:15px; line-height:21px; text-transform:uppercase; letter-spacing:1.2px; font-weight:bold; color:#6e7e92;">Образование</span>
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Годы обучения</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Учебное заведение</span>
															</td>
														</tr>
														#EDUCATION#
														<tr>
															<td colspan="2" height="61" style="vertical-align:middle; padding:0; border:none;" valign="middle">
																<img src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/bg-line.png" width="550" height="1" alt="" style="display:block; line-height:1px; border:none;" />
															</td>
														</tr>
														<tr>
															<td colspan="2" style="vertical-align:middle; padding:0; padding-bottom:20px; border:none;" valign="middle">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:15px; line-height:21px; text-transform:uppercase; letter-spacing:1.2px; font-weight:bold; color:#6e7e92;">Опыт работы</span>
															</td>
														</tr>
														<tr>
															<td width="240" style="vertical-align:top; padding:0; padding-right:40px; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Годы работы</span>
															</td>
															<td width="312" style="vertical-align:top; padding:0; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Место работы</span>
															</td>
														</tr>
														#EXPERIENCE#
														<tr>
															<td colspan="2" height="61" style="vertical-align:middle; padding:0; border:none;" valign="middle">
																<img src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/bg-line.png" width="550" height="1" alt="" style="display:block; line-height:1px; border:none;" />
															</td>
														</tr>
														<tr>
															<td colspan="2" width="552" style="vertical-align:top; padding:0; padding-right:40px; padding-bottom:10px; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#666666;">Почему вы хотите работать в компании</span>
															</td>
														</tr>
														<tr>
															<td colspan="2" width="552" style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">#WHY#</span>
															</td>
														</tr>
														<tr>
															<td colspan="2" height="61" style="vertical-align:middle; padding:0; border:none;" valign="middle">
																<img src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/bg-line.png" width="550" height="1" alt="" style="display:block; line-height:1px; border:none;" />
															</td>
														</tr>
														<tr>
															<td colspan="2" style="vertical-align:middle; padding:0; padding-bottom:20px; border:none;" valign="middle">
																<span style="font-family:Arial, FreeSans, sans-serif; font-size:15px; line-height:21px; text-transform:uppercase; letter-spacing:1.2px; font-weight:bold; color:#6e7e92;">резюме</span>
															</td>
														</tr>
														<tr>
															<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
																<table style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; line-height:20px; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none;" cellpadding="0" cellspacing="0">
																	<tbody>
																		<tr>
																			<td width="25" height="30" style="vertical-align:top; padding:0; border:none;" valign="top">
																				<img src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/ico-pdf.png" width="25" height="30" alt="" style="display:block; line-height:1px; border:none;" />
																			</td>
																			<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
																				<a href="#CV_FILE#" style="display:block; font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:16px; color:#333333; letter-spacing:0.18px; padding-left:12px; padding-bottom:4px;">#CV_FILE_NAME#</span>
																				<span style="display:block; font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:15px; color:#6f6f6f; letter-spacing:0.18px; padding-left:12px;">#CV_FILE_SIZE#</span>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td colspan="3" height="65" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td width="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="3" height="25" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
			'
		);

		$arMessageUserRu = array(
			"ACTIVE" => "Y",
			"EVENT_NAME" => "FORM_FILLING_SEND_CV",
			"LID" => array("s1"),
			"EMAIL_FROM" => "НЛМК <job@nlmk.com>",
			"EMAIL_TO" => "#EMAIL#",
			"SUBJECT" => "nlmk.com: Отправлено резюме",
			"BODY_TYPE" => "html",
			"MESSAGE" => '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>nlmk резюме</title>
	</head>
	<body style="background-color:#ffffff; margin:0;" bgcolor="#ffffff">
		<table style="border-collapse:collapse; margin-bottom:0; font-family:Verdana,sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none; background-color:#ffffff; text-align:center" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="886">
					<table style="border-collapse:collapse; margin-bottom:0; font-family:Verdana,sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none; text-align:left; background-color:#f8f9fa;" bgcolor="#f8f9fa" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="3" height="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td width="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								&nbsp;
							</td>
							<td style="vertical-align:top; padding:0; border:none;" valign="top">
								<table width="886" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none;" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
												&nbsp;
											</td>
											<td width="152" height="106" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
												<a href="http://nlmk.ru/" title="nlmk" target="_blank">
													<img src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/logo.jpg" width="152" height="106" alt="nlmk" style="display:block; line-height:1px; border:none;" />
												</a>
											</td>
											<td width="40" style="vertical-align:top; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#f2f4f5" valign="top">
												&nbsp;
											</td>
											<td width="486" style="vertical-align:middle; background-color:#f2f4f5; padding:0; border:none;" bgcolor="#007ec7" valign="middle">
												<span style="font-family:Arial, FreeSans, sans-serif; font-size:13px; line-height:21px; color:#333333; text-transform:uppercase; letter-spacing:0.065px;"><a href="http://nlmk.ru/" title="nlmk.ru" target="_blank" style="color:#333333; text-decoration:none">вакансии</a></span>
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none; background-color:#f2f4f5;" bgcolor="#f2f4f5" valign="top">
												&nbsp;
											</td>
										</tr>
									</tbody>
								</table>

								<table width="886" style="border-collapse:collapse; margin-bottom:0; font-family: Arial, FreeSans, sans-serif; line-height:20px; -premailer-cellspacing:0; -premailer-cellpadding:0; border:none;" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td colspan="3" height="50" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
											<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
												<span style="font-family:Arial, FreeSans, sans-serif; font-size:20px; line-height:40px; color:#333333;">Уважаемый (ая) #NAME#!</span>
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td colspan="3" height="40" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
											<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
												<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#595959;">Мы признательны за проявленный интерес к Группе НЛМК. Благодарим Вас за направленное резюме. Мы с внимательно с ним ознакомимся и будем сохраним его в нашей базе данных.</span>
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td colspan="3" height="20" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
											<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
												<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#595959;">Как только у нас появится вакансия, соответствующая Вашим навыкам и квалификации, мы обязательно свяжемся с Вами.</span>
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td colspan="3" height="20" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
											<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
												<span style="font-family:Arial, FreeSans, sans-serif; font-size:14px; line-height:20px; color:#595959;">Для получения более детальной информации о Группе НЛМК и открытых вакансиях посетите наш сайт: <a href="http://nlmk.com/ru/career/jobs/" title="nlmk.ru" target="_blank" style="color:#0c54a0; text-decoration:none">http://nlmk.com/ru/career/jobs/</a></span>
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td colspan="3" height="50" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
											<td height="35" style="vertical-align:top; padding:0; border:none;" valign="top">
												<img src="http://#SERVER_NAME#/local/templates/nlmk/images/mail/bg-line.png" width="60" height="2" alt="" style="display:block; line-height:1px; border:none;" />
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
											<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
												<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:18px; color:#747474;">С наилучшими пожеланиями,</span>
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td colspan="3" height="12" style="vertical-align:top; padding:0; border:none; line-height:12px" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
											<td style="vertical-align:top; padding:0; border:none;" align="left" valign="top">
												<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:18px; color:#747474;">Аппарат Вице-Президента по кадрам и системе управления<br />«Подбор персонала»</span>
											</td>
											<td width="104" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td colspan="3" height="65" style="vertical-align:top; padding:0; border:none;" valign="top">
												&nbsp;
											</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td width="20" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="3" height="25" style="vertical-align:top; padding:0; border:none; background-color:#ffffff;" bgcolor="#ffffff" valign="top">
								&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
			'
		);

		$arMessageAdminEn = $arMessageAdminRu;
		$arMessageUserEn = $arMessageUserRu;
		$arMessageAdminEn["EVENT_NAME"] = "FORM_FILLING_SEND_CV_EN";
		$arMessageAdminEn["LID"] = array("s2");
		$arMessageAdminEn["SUBJECT"] = "nlmk.com: Заполнена веб-форма \"Send CV\"";
		$arMessageUserEn["EVENT_NAME"] = "FORM_FILLING_SEND_CV_EN";
		$arMessageUserEn["LID"] = array("s2");

		$obMessage = new \CEventMessage;
		$obMessage->Add($arMessageAdminRu);
		$obMessage->Add($arMessageAdminEn);
		$obMessage->Add($arMessageUserRu);
		$obMessage->Add($arMessageUserEn);
	}

	public function down()
	{
		$obMess = new \CEventMessage;
		$dbMess = \CEventMessage::GetList($by="site_id", $order="desc", array("TYPE_ID" => array("FORM_FILLING_SEND_CV", "FORM_FILLING_SEND_CV_EN")));
		while ($arMess = $dbMess->GetNext())
		{
			$obMess->Delete(intval($arMess["ID"]));
		}
	}

}
