<?

namespace Sprint\Migration;

use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171227114313  extends Version
{

    const IB_NAME = 'Инфографика закупок службы снабжения';
    const IB_CODE = 'SUPPLY_SERVICE_PURCHASE_SCHEDULE_RU';
    protected $description = 'Создание ИБ "Инфографика закупок" для supply-service';
    protected $elements    = [['FIELDS'     => ['NAME' => 'Закупки 2013-го',
                                                'ACTIVE' => 'N'],
                               'PROPERTIES' => ['PURCHASE_VALUE' => '248,5',
                                                'PURCHASE_YEAR'  => '2013']],
                              ['FIELDS'     => ['NAME' => 'Закупки 2014-го',
                                                'ACTIVE' => 'N'],
                               'PROPERTIES' => ['PURCHASE_VALUE' => '301,2',
                                                'PURCHASE_YEAR'  => '2014']],
                              ['FIELDS'     => ['NAME' => 'Закупки 2015-го',
                                                'ACTIVE' => 'N'],
                               'PROPERTIES' => ['PURCHASE_VALUE' => '380,8',
                                                'PURCHASE_YEAR'  => '2015']],
                              ['FIELDS'     => ['NAME' => 'Закупки 2016-го',
                                                'ACTIVE' => 'N'],
                               'PROPERTIES' => ['PURCHASE_VALUE' => '410,9',
                                                'PURCHASE_YEAR'  => '2016']]
    ];

    public function up() {
        $IblockHelper = new IblockHelper();

        $iblockId = $IblockHelper->addIblockIfNotExists(
            [
                'ACTIVE' => 'Y',
                'NAME' => self::IB_NAME,
                'CODE' => self::IB_CODE,
                'LIST_PAGE_URL' => '',
                'DETAIL_PAGE_URL' => '',
                'IBLOCK_TYPE_ID' => 'activities',
                'SITE_ID' => ['s1'],
                'SORT' => 500,
                'DESCRIPTION' => ''
            ]
        );

        if ($iblockId) {
            $this->outSuccess("Добавлен ИБ \"" . self::IB_NAME . "\"");
            $properties = [
                [
                    'IBLOCK_ID' => $iblockId,
                    'NAME' => 'Объём закупок',
                    'ACTIVE' => 'Y',
                    'SORT' => '100',
                    'CODE' => 'PURCHASE_VALUE',
                    'PROPERTY_TYPE' => 'S',
                    'ROW_COUNT' => '1',
                    'COL_COUNT' => '15',
                    'LIST_TYPE' => 'L',
                    'MULTIPLE' => 'N',
                    'IS_REQUIRED' => 'Y',
                    'FILTRABLE' => 'N'
                ],
                [
                    'IBLOCK_ID' => $iblockId,
                    'NAME' => 'Год',
                    'ACTIVE' => 'Y',
                    'SORT' => '100',
                    'CODE' => 'PURCHASE_YEAR',
                    'PROPERTY_TYPE' => 'S',
                    'ROW_COUNT' => '1',
                    'COL_COUNT' => '15',
                    'LIST_TYPE' => 'L',
                    'MULTIPLE' => 'N',
                    'IS_REQUIRED' => 'Y',
                    'FILTRABLE' => 'N'
                ],
            ];

            foreach ($properties as $property) {
                if ($IblockHelper->addPropertyIfNotExists($iblockId, $property)) {
                    $this->outSuccess("Добавлено свойство \"" . $property['NAME'] . "\" в ИБ " . self::IB_NAME);
                }
            }

            foreach ($this->elements as $element) {
                if ($elemId = $IblockHelper->addElement($iblockId, $element['FIELDS'], $element['PROPERTIES'])) {
                    $this->outSuccess("Добавлен элемент \"" . $elemId . " элемент в ИБ " . self::IB_NAME);
                }
            }
        }
    }

    public function down()
    {
        $IblockHelper = new IblockHelper();

        if ($IblockHelper->deleteIblockIfExists(self::IB_CODE)) {
            $this->outSuccess("Удален ИБ \"" . self::IB_NAME . "\"");
        }
    }
}