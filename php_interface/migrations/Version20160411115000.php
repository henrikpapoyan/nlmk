<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
use Bitrix\Main\Config\Option;

class Version20160411115000 extends Version {

	protected $description = "Включение сжатия и объединения css и js и перенос js в конец страницы";

	public function up(){
		Option::set("main", "optimize_css_files", "Y");
		Option::set("main", "optimize_js_files", "Y");
		Option::set("main", "use_minified_assets", "Y");
		Option::set("main", "move_js_to_body", "Y");
		Option::set("main", "compres_css_js_files", "Y");
	}

	public function down(){
		Option::set("main", "optimize_css_files", "N");
		Option::set("main", "optimize_js_files", "N");
		Option::set("main", "use_minified_assets", "N");
		Option::set("main", "move_js_to_body", "N");
		Option::set("main", "compres_css_js_files", "N");
	}

}
