<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160203153209 extends Version {

    protected $description = "Добавление свойства \"Дата окончания события\" в финансовый календарь";

    public function up(){
        $helper = new IblockHelper();
        $arFields = array(
            "NAME" => "Дата окончания события",
            "ACTIVE" => "Y",
            "SORT" => "100",
            "CODE" => "DATE_END",
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "Date"
        );
        $helper->addPropertyIfNotExists(FINANCIAL_CALENDAR_IBLOCK_ID_RU, $arFields);
        $helper->addPropertyIfNotExists(FINANCIAL_CALENDAR_IBLOCK_ID_EN, $arFields);
    }

    public function down(){
        $helper = new IblockHelper();
        $helper->deleteProperty(FINANCIAL_CALENDAR_IBLOCK_ID_RU, "DATE_END");
        $helper->deleteProperty(FINANCIAL_CALENDAR_IBLOCK_ID_EN, "DATE_END");
    }

}
