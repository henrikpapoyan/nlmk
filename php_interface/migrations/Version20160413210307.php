<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
use CIBlockProperty;


class Version20160413210307 extends Version {

    protected $description = "Обновление свойства \"Статус\" для инфоблока \"Охрана атмосферного воздуха\" EN";

    public function up(){
		$arIblocks = array(ENV_PROGRAMME_IBLOCK_ID_EN);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = CIBlockProperty::GetPropertyEnum(
				"STATUS",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["XML_ID"] == "DONE") {
					$arEnumProp['VALUE'] = "Done";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
    }

    public function down(){
		$arIblocks = array(ENV_PROGRAMME_IBLOCK_ID_EN);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = CIBlockProperty::GetPropertyEnum(
				"STATUS",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch()) {
				if ($arEnumProp["XML_ID"] == "DONE") {
					$arEnumProp['VALUE'] = "Выполнено";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
    }

}
