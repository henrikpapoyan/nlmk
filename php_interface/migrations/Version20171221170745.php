<?

namespace Sprint\Migration;

use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20171221170745 extends Version
{

    const IB_NAME = 'Слайдер: служба снабжения';
    const IB_CODE = 'SUPPLY_SERVICE_SLIDER_RU';
    protected $description = 'Создание ИБ и эл-та со страницы "Служба снабжения"';

    public function up()
    {
        $IblockHelper = new IblockHelper();

        $iblockId = $IblockHelper->addIblockIfNotExists(
            [
                'ACTIVE' => 'Y',
                'NAME' => self::IB_NAME,
                'CODE' => self::IB_CODE,
                'LIST_PAGE_URL' => '',
                'DETAIL_PAGE_URL' => '',
                'IBLOCK_TYPE_ID' => 'activities',
                'SITE_ID' => ['s1'],
                'SORT' => 500,
                'DESCRIPTION' => ''
            ]
        );

        if ($iblockId) {
            $this->outSuccess("Добавлен ИБ \"" . self::IB_NAME . "\"");
            $properties = [
                [
                    'IBLOCK_ID' => $iblockId,
                    'NAME' => 'Описание',
                    'ACTIVE' => 'Y',
                    'SORT' => '100',
                    'CODE' => 'SUBTITLE',
                    'PROPERTY_TYPE' => 'S',
                    'ROW_COUNT' => '5',
                    'COL_COUNT' => '30',
                    'LIST_TYPE' => 'L',
                    'MULTIPLE' => 'N',
                    'IS_REQUIRED' => 'Y',
                    'FILTRABLE' => 'Y'
                ],
                [
                    'IBLOCK_ID' => $iblockId,
                    'NAME' => 'Ссылка',
                    'ACTIVE' => 'Y',
                    'SORT' => '100',
                    'CODE' => 'LINK',
                    'PROPERTY_TYPE' => 'S',
                    'ROW_COUNT' => '1',
                    'COL_COUNT' => '30',
                    'LIST_TYPE' => 'L',
                    'MULTIPLE' => 'N',
                    'IS_REQUIRED' => 'N',
                    'FILTRABLE' => 'N'
                ]
            ];

            foreach ($properties as $property) {
                if ($IblockHelper->addPropertyIfNotExists($iblockId, $property)) {
                    $this->outSuccess("Добавлено свойство \"" . $property['NAME'] . "\" в ИБ " . self::IB_NAME);
                }
            }

            $element = ['FIELDS' => ['NAME' => 'Служба снабжения группы НЛМК',
                'PREVIEW_PICTURE' => \CFile::MakeFileArray('/local/templates/nlmk/images/procurement-mask.jpg')],
                'PROPERTIES' => ['LINK' => '/ru/media-center/news-groups/nlmk-group-delivers-on-interim-strategy-2017-targets-for-2015/',
                    'SUBTITLE' => 'Мы стремимся выстроить хорошие деловые отношения с поставщиками, используя современные IT решения, сокращающие бумажный документооборот и увеливающие прозрачность процесса закупок']];

            if ($elemId = $IblockHelper->addElement($iblockId, $element['FIELDS'], $element['PROPERTIES'])) {
                $this->outSuccess("Добавлен элемент \"" . $elemId . " элемент в ИБ " . self::IB_NAME);
            }
        }
    }

    public function down()
    {
        $IblockHelper = new IblockHelper();

        if ($IblockHelper->deleteIblockIfExists(self::IB_CODE)) {
            $this->outSuccess("Удален ИБ \"" . self::IB_NAME . "\"");
        }
    }
}