<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160415153030 extends Version {

	protected $description = "перевод значений свойства TITLE_COLOR для ИБ Companies";

	public function up(){
		$arIblocks = array(COMPANIES_IBLOCK_ID_EN);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = \CIBlockProperty::GetPropertyEnum(
				"TITLE_COLOR",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch())
			{
				if ($arEnumProp["XML_ID"] == "BLACK")
				{
					$arEnumProp["VALUE"] = "dark";
				}
				if ($arEnumProp["XML_ID"] == "WHITE")
				{
					$arEnumProp["VALUE"] = "light";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new \CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
	}

	public function down()
	{
		$arIblocks = array(COMPANIES_IBLOCK_ID_EN);

		foreach ($arIblocks as $iblockId) {
			$dbEnumList = \CIBlockProperty::GetPropertyEnum(
				"TITLE_COLOR",
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $iblockId)
			);
			$arValues = array();
			while($arEnumProp = $dbEnumList->Fetch())
			{
				if ($arEnumProp["XML_ID"] == "BLACK")
				{
					$arEnumProp["VALUE"] = "темный";
				}
				if ($arEnumProp["XML_ID"] == "WHITE")
				{
					$arEnumProp["VALUE"] = "светлый";
				}
				$arValues[$arEnumProp["ID"]] = array("VALUE" => $arEnumProp["VALUE"]);
				$propertyId = $arEnumProp["PROPERTY_ID"];
			}

			$CIBlockProperty = new \CIBlockProperty;
			if (isset($propertyId)) {
				$CIBlockProperty->UpdateEnum($propertyId, $arValues);
			}
		}
		$helper = new IblockHelper();
	}

}
