<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;

class Version20160525115733 extends Version
{

	protected $description = "Добавление свойста \"Email\" в ИБ Предприятия (ru/en)";

	public function up()
	{
		$helper = new IblockHelper();
		$arFields = array(
			"NAME" => "Email",
			"ACTIVE" => "Y",
			"SORT" => "100",
			"CODE" => "EMAIL",
			"PROPERTY_TYPE" => "S",
			"COL_COUNT" => "50"
		);
		$helper->addPropertyIfNotExists(COMPANIES_IBLOCK_ID_RU, $arFields);
		$helper->addPropertyIfNotExists(COMPANIES_IBLOCK_ID_EN, $arFields);

		$arIblocks = array(
			COMPANIES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "TITLE_COLOR"), "TITLE" => "Color header of detailed page"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "HIDE_LINK"), "TITLE" => "Not show URL"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "CITY"), "TITLE" => "City"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "COUNTRY"), "TITLE" => "Country"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "REGION"), "TITLE" => "Region"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "INFOGRAPHIC"), "TITLE" => "Infographic"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "PRODUCTION"), "TITLE" => "Production"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "FILES"), "TITLE" => "Files"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "COORDINATES"), "TITLE" => "Coordinates"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "COMPANY_ON_MAP"), "TITLE" => "Company on map"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "EMAIL"), "TITLE" => "Email"),
					)
				)
			),
			COMPANIES_IBLOCK_ID_RU => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Предприятие",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Активность"),
						array("NAME" => "SORT", "TITLE" => "Сортировка"),
						array("NAME" => "NAME", "TITLE" => "*Название"),
						array("NAME" => "CODE", "TITLE" => "*Символьный код"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "TITLE_COLOR"), "TITLE" => "Цвет заголовка на детальной странице"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "HIDE_LINK"), "TITLE" => "Не отображать ссылку предприятие"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Изображение"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "LINK"), "TITLE" => "Ссылка"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "CITY"), "TITLE" => "*Город расположения"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "COUNTRY"), "TITLE" => "*Страна расположения"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "REGION"), "TITLE" => "Регион"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "DESCRIPTION"), "TITLE" => "*Описание"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "INFOGRAPHIC"), "TITLE" => "Инфографика"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "PRODUCTION"), "TITLE" => "*Продукция"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "FILES"), "TITLE" => "Файлы"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "COORDINATES"), "TITLE" => "Координаты на карте"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "COMPANY_ON_MAP"), "TITLE" => "Предприятие на карте"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "EMAIL"), "TITLE" => "Email"),
					)
				)
			),
		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}


	}

	public function down()
	{
		$helper = new IblockHelper();
		$helper->deleteProperty(COMPANIES_IBLOCK_ID_RU, "EMAIL");
		$helper->deleteProperty(COMPANIES_IBLOCK_ID_EN, "EMAIL");

		$arIblocks = array(
			COMPANIES_IBLOCK_ID_EN => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Element",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Active"),
						array("NAME" => "SORT", "TITLE" => "Sorting"),
						array("NAME" => "NAME", "TITLE" => "*Name"),
						array("NAME" => "CODE", "TITLE" => "*Mnemonic code"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "TITLE_COLOR"), "TITLE" => "Color header of detailed page"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "HIDE_LINK"), "TITLE" => "Not show URL"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Picture"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "LINK"), "TITLE" => "Link"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "CITY"), "TITLE" => "City"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "COUNTRY"), "TITLE" => "Country"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "REGION"), "TITLE" => "Region"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "DESCRIPTION"), "TITLE" => "Description"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "INFOGRAPHIC"), "TITLE" => "Infographic"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "PRODUCTION"), "TITLE" => "Production"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "FILES"), "TITLE" => "Files"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "COORDINATES"), "TITLE" => "Coordinates"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_EN, "COMPANY_ON_MAP"), "TITLE" => "Company on map"),
					)
				)
			),
			COMPANIES_IBLOCK_ID_RU => array(
				array(
					"CODE" => "edit1",
					"TITLE" => "Предприятие",
					"FIELDS" => array(
						array("NAME" => "ACTIVE", "TITLE" => "Активность"),
						array("NAME" => "SORT", "TITLE" => "Сортировка"),
						array("NAME" => "NAME", "TITLE" => "*Название"),
						array("NAME" => "CODE", "TITLE" => "*Символьный код"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "TITLE_COLOR"), "TITLE" => "Цвет заголовка на детальной странице"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "HIDE_LINK"), "TITLE" => "Не отображать ссылку предприятие"),
						array("NAME" => "PREVIEW_PICTURE", "TITLE" => "*Изображение"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "LINK"), "TITLE" => "Ссылка"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "CITY"), "TITLE" => "*Город расположения"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "COUNTRY"), "TITLE" => "*Страна расположения"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "REGION"), "TITLE" => "Регион"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "DESCRIPTION"), "TITLE" => "*Описание"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "INFOGRAPHIC"), "TITLE" => "Инфографика"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "PRODUCTION"), "TITLE" => "*Продукция"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "FILES"), "TITLE" => "Файлы"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "COORDINATES"), "TITLE" => "Координаты на карте"),
						array("NAME" => "PROPERTY_" . $helper->getPropertyId(COMPANIES_IBLOCK_ID_RU, "COMPANY_ON_MAP"), "TITLE" => "Предприятие на карте"),
					)
				)
			),
		);

		foreach ($arIblocks as $iblockId => $arIblock)
		{
			$tabsString = "";
			foreach($arIblock as $arTab) {
				$tabsString .= $arTab["CODE"] . "--#--" . $arTab["TITLE"] . "--,--";
				foreach($arTab["FIELDS"] as $field) {
					$tabsString .= $field["NAME"] . "--#--" . $field['TITLE'];
					if (end($arTab["FIELDS"]) == $field) {
						$tabsString .= "--;--";
						continue;
					}
					$tabsString .= "--,--";
				}
			}
			$arOptions = array(array("c" => "form", "n" => "form_element_" . $iblockId, "d" => "Y", "v" => array("tabs" => $tabsString)));
			\CUserOptions::SetOptionsFromArray($arOptions);
		}

	}

}
