<?php

namespace Sprint\Migration;
use \Sprint\Migration\Helpers\IblockHelper;
use \Sprint\Migration\Helpers\EventHelper;
use \Sprint\Migration\Helpers\UserTypeEntityHelper;
use Bitrix\Main\Config\Option;

class Version20160415114912 extends Version {

	protected $description = "добавление в \"Тип меню для нулевого уровня карты сайта\" - меню header";

	public function up()
	{
		Option::set("main", "map_top_menu_type", "top, header");
	}

	public function down()
	{
		Option::set("main", "map_top_menu_type", "top");
	}

}
