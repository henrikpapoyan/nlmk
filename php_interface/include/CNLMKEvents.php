<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);


define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/events.log");
define("LOG_FILENAME", __DIR__ . "/CNLMKEvents.txt");

AddEventHandler("iblock", "OnAfterIBlockElementAdd", array("CNLMKEvents", "onAfterIBlockElementHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", array("CNLMKEvents", "onAfterIBlockElementHandler"));

AddEventHandler("iblock", "OnAfterIBlockElementAdd", array("CNLMKEvents", "createGalleryArchive"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", array("CNLMKEvents", "createGalleryArchive"));

AddEventHandler("form", "onBeforeResultAdd", array("CNLMKEvents", "onBeforeResultAddHandler"));
AddEventHandler("form", "onAfterResultAdd", array("CNLMKEvents", "onAfterResultAddHandler"));

AddEventHandler("main", "OnEpilog", array("CNLMKEvents", "Redirect404"));
AddEventHandler("main", "OnAdminTabControlBegin", array("CNLMKEvents", "onAdminTabControlBegin"));

AddEventHandler("search", "BeforeIndex", array("CNLMKEvents", "BeforeIndexHandler"));

AddEventHandler("subscribe", "BeforePostingSendMail", array("CNLMKEvents", "BeforePostingSendMailHandler"));
AddEventHandler("main", "OnBeforeEventAdd", array("CNLMKEvents", "OnBeforeEventAddHandler"));


class CNLMKEvents
{
	private $file_abs_path = "";

	public function BeforeIndexHandler($arFields)
	{
		$io = CBXVirtualIo::GetInstance();

		if($arFields["MODULE_ID"] == "main")
		{
			list($site_id, $file_rel_path) = explode("|", $arFields["ITEM_ID"]);
			$file_doc_root = CSite::GetSiteDocRoot($site_id);
			$file_abs_path = preg_replace("#[\\\\\\/]+#", "/", $file_doc_root."/".$file_rel_path);
			$f = $io->GetFile($file_abs_path);
			$sFile = $f->GetContents();
			if(isset($sFile) && $sFile != "")
			{
				$replacer = new CNLMKEvents;
				$replacer->file_abs_path = $file_abs_path;
				$sFile = preg_replace_callback("/<\\?\\\$APPLICATION->IncludeComponent\\(\\s*\"bitrix:main.include\",(.*?)\\?>/mis", array($replacer, "replace"), $sFile);
				$arFields["BODY"] = CSearch::KillTags($sFile);
			}
		}
		return $arFields;
	}

	public function replace($matchParams)
	{
		$io = CBXVirtualIo::GetInstance();

		if (preg_match("/\"AREA_FILE_SUFFIX\"\\s*=>\\s*\"(.*?)\",/", $matchParams[1], $match))
		{
			$slash_pos = strrpos($this->file_abs_path, "/");
			$sFilePath = substr($this->file_abs_path, 0, $slash_pos+1);
			$sFileName = substr($this->file_abs_path, $slash_pos+1);
			$sFileName = substr($sFileName, 0, strlen($sFileName)-4)."_".$match[1].".php";

			$f = $io->GetFile($sFilePath.$sFileName);
			return $f->GetContents();
		}
		return "";
	}

	public function Redirect404()
	{
		 if (!defined('ADMIN_SECTION') && defined("ERROR_404")) {
			global $APPLICATION;

			/*
			 * редирект в общий раздел со списком элементов только при переключении языковой версии
			 */
			if ($_REQUEST["ELEMENT_CODE"] && isset($_REQUEST["from"]) &&
				(strpos($APPLICATION->GetCurPage(), SITE_DIR . "media-center/news-groups/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "media-center/news-companies/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "media-center/interviews-and-speeches/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "media-center/multimedia/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "responsibility/charity/projects/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "responsibility/ecology/key-cases/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "responsibility/ecology/environmental-programme-2020/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "ir/financial-calendar/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "about/mission/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "about/governance/board-of-directors/the-board-of-directors/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "about/governance/management-board/the-composition-of-the-board/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "about/map-of-assets/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "career/our-professions/") !== false
				|| strpos($APPLICATION->GetCurPage(), SITE_DIR . "career/jobs/") !== false)) {
				$returnURL = str_replace($_REQUEST["ELEMENT_CODE"]."/", "", $APPLICATION->GetCurPage());
				LocalRedirect($returnURL);
			} else {
				$APPLICATION->RestartBuffer();
				$APPLICATION->SetDirProperty("hide_title", "Y");
				include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/header.php";
				if (file_exists($_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "404.php"))
					include $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "404.php";
				include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/footer.php";
			}

		}
	}

	public function onAfterResultAddHandler($webFormId, $resultId)
	{
		if ($webFormId == FAQ_FORM_ID) {

			$arAnswer = CFormResult::GetDataByID(
				$resultId,
				array("NAME", "EMAIL", "QUESTION", "LANGUAGE"),
				$arResult,
				$arAnswer2
			);
			$name = $arAnswer["NAME"][0]["USER_TEXT"];
			$email = $arAnswer["EMAIL"][0]["USER_TEXT"];
			$question = $arAnswer["QUESTION"][0]["USER_TEXT"];
			$language = $arAnswer["LANGUAGE"][0]["USER_TEXT"];

			if (CModule::IncludeModule("iblock")) {
				$obQuestion = new CIBlockElement;
				$properties = array(
					"NAME" => $name,
					"EMAIL" => $email,
					"QUESTION" => $question
				);
				$arQuestion = array(
					"IBLOCK_SECTION_ID" => false,
					"IBLOCK_ID"      => constant("QUESTIONS_IBLOCK_ID_" . strtoupper($language)),
					"PROPERTY_VALUES"=> $properties,
					"NAME"           => Loc::getMessage("ADD_QUESTION") . " $name ($email)",
					"ACTIVE"         => "Y"
				);
				$questionId = $obQuestion->Add($arQuestion);
			}
		}
		if (in_array($webFormId, array(SEND_CV_FORM_ID_RU, SEND_CV_FORM_ID_EN)))
		{
			$arAnswer = CFormResult::GetDataByID(
				$resultId,
				array(),
				$arResult,
				$arAnswer2
			);
			$strExperience = "";
			$strEducation = "";
			$fileCv = CFile::GetPath($arAnswer["CV_FILE"][0]["USER_FILE_ID"]);

			foreach ($arAnswer["EXPERIENCE"] as $arAnswerExp)
			{
				if (!empty($arAnswerExp["USER_TEXT"]))
				{
					$arExpEntry = explode('|', $arAnswerExp["USER_TEXT"]);
					$strExperience .= '<tr>' .
										'<td width="240" style="vertical-align:top; padding:0; padding-right:40px; border:none;" align="left" valign="top">' .
											'<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">' . trim($arExpEntry[0]) . '</span>' .
										'</td>' .
										'<td width="312" style="vertical-align:top; padding:0; border:none;" align="left" valign="top">' .
											'<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">' . trim($arExpEntry[1]) . '</span>' .
										'</td>' .
									'</tr>';
				}
			}
			if (empty($strExperience))
			{
				$strExperience .= '<tr>' .
									'<td width="240" style="vertical-align:top; padding:0; padding-right:40px; border:none;" align="left" valign="top">' .
										'<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">' . Loc::getMessage("CV_NOT_SPECIFIED") . '</span>' .
									'</td>' .
									'<td width="312" style="vertical-align:top; padding:0; border:none;" align="left" valign="top">' .
										'<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">' . Loc::getMessage("CV_NOT_SPECIFIED") . '</span>' .
									'</td>' .
								'</tr>';
			}

			foreach ($arAnswer["EDUCATION"] as $arAnswerEdu)
			{
				if (!empty($arAnswerEdu["USER_TEXT"]))
				{
					$arEduEntry = explode('|', $arAnswerEdu["USER_TEXT"]);
					$strEducation .= '<tr>' .
										'<td width="240" style="vertical-align:top; padding:0; padding-right:40px; border:none;" align="left" valign="top">' .
											'<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">' . trim($arEduEntry[0]) . '</span>' .
										'</td>' .
										'<td width="312" style="vertical-align:top; padding:0; border:none;" align="left" valign="top">' .
											'<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">' . trim($arEduEntry[1]) . '</span>' .
										'</td>' .
									'</tr>';
				}
			}
			if (empty($strEducation))
			{
				$strEducation .= '<tr>' .
									'<td width="240" style="vertical-align:top; padding:0; padding-right:40px; border:none;" align="left" valign="top">' .
										'<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">' . Loc::getMessage("CV_NOT_SPECIFIED") . '</span>' .
									'</td>' .
									'<td width="312" style="vertical-align:top; padding:0; border:none;" align="left" valign="top">' .
										'<span style="font-family:Arial, FreeSans, sans-serif; font-size:12px; line-height:20px; color:#202020;">' . Loc::getMessage("CV_NOT_SPECIFIED") . '</span>' .
									'</td>' .
								'</tr>';
			}

			if (!empty($arAnswer["REGION_ID"][0]["USER_TEXT"]))
			{
				$dbCompanies = CIBlockElement::GetList(
					array(),
					array("ID" => $arAnswer["REGION_ID"][0]["USER_TEXT"]),
					false,
					false,
					array("ID", "IBLOCK_ID", "PROPERTY_EMAIL")
				);
				if ($arCompany = $dbCompanies->GetNext())
				{
					if (!empty($arCompany["PROPERTY_EMAIL_VALUE"]))
						$email = $arCompany["PROPERTY_EMAIL_VALUE"];
				}
			}

			$arEventFields = array(
				"REGION_EMAIL" => isset($email) ? $email : "",
				"VACANCY" => $arAnswer["VACANCY"][0]["USER_TEXT"],
				"REGION" => $arAnswer["REGION"][0]["USER_TEXT"]
					? $arAnswer["REGION"][0]["USER_TEXT"]
					: Loc::getMessage("CV_NOT_SPECIFIED"),
				"DIRECTION" => $arAnswer["DIRECTION"][0]["USER_TEXT"]
					? $arAnswer["DIRECTION"][0]["USER_TEXT"]
					: Loc::getMessage("CV_NOT_SPECIFIED"),
				"NAME" => $arAnswer["NAME"][0]["USER_TEXT"],
				"CITY" => $arAnswer["CITY"][0]["USER_TEXT"]
					? $arAnswer["CITY"][0]["USER_TEXT"]
					: Loc::getMessage("CV_NOT_SPECIFIED"),
				"BIRTHDAY" => $arAnswer["BIRTHDAY"][0]["USER_TEXT"]
					? $arAnswer["BIRTHDAY"][0]["USER_TEXT"]
					: Loc::getMessage("CV_NOT_SPECIFIED"),
				"PHONE" => $arAnswer["PHONE"][0]["USER_TEXT"]
					? $arAnswer["PHONE"][0]["USER_TEXT"]
					: Loc::getMessage("CV_NOT_SPECIFIED"),
				"EMAIL" => $arAnswer["EMAIL"][0]["USER_TEXT"],
				"SALARY" => $arAnswer["SALARY"][0]["USER_TEXT"]
					? $arAnswer["SALARY"][0]["USER_TEXT"]
					: Loc::getMessage("CV_NOT_SPECIFIED"),
				"READY_TO_MOVE" => !empty($arAnswer["READY_TO_MOVE"])
					? $arAnswer["READY_TO_MOVE"][0]["ANSWER_TEXT"]
					: Loc::getMessage("CV_NOT_READY_TO_MOVE"),
				"EDUCATION" => $strEducation,
				"EXPERIENCE" => $strExperience,
				"WHY" => $arAnswer["WHY"][0]["USER_TEXT"]
					? $arAnswer["WHY"][0]["USER_TEXT"]
					: Loc::getMessage("CV_NOT_SPECIFIED"),
				"CV_FILE" => $fileCv,
				"CV_FILE_NAME" => $arAnswer["CV_FILE"][0]["USER_TEXT"],
				"CV_FILE_SIZE" => CFile::FormatSize($arAnswer["CV_FILE"][0]["USER_FILE_SIZE"])
			);
			if ($webFormId == SEND_CV_FORM_ID_RU)
				CEvent::Send("FORM_FILLING_SEND_CV", "s1", $arEventFields, "N");
			elseif ($webFormId == SEND_CV_FORM_ID_EN)
				CEvent::Send("FORM_FILLING_SEND_CV_EN", "s2", $arEventFields, "N");
		}
	}

	public function onAfterIBlockElementHandler(&$arFields)
	{
		if ($arFields["IBLOCK_ID"] == MULTIMEDIA_IBLOCK_ID_RU || $arFields["IBLOCK_ID"] == MULTIMEDIA_IBLOCK_ID_EN)
		{
			$dbElementProps = CIBlockElement::GetProperty(
				$arFields["IBLOCK_ID"],
				$arFields["ID"],
				array(),
				array("CODE" => "MULTIMEDIA")
			);
			if ($arProp = $dbElementProps->GetNext())
			{
				if (!empty($arProp["VALUE"]))
				{
					$dbMultimedia = CIBlockElement::GetList(array(), array("ID" => $arProp["VALUE"]), false, false, array());
					if ($obMultimedia = $dbMultimedia->GetNextElement())
					{
						$arProperties = $obMultimedia->GetProperties();
						if (is_array($arProperties["DESCRIPTION"]["VALUE"]))
							CIBlockElement::SetPropertyValuesEx($arFields["ID"], $arFields["IBLOCK_ID"], array("DESCRIPTION" => $arProperties["DESCRIPTION"]["VALUE"]["TEXT"]));
						else
							CIBlockElement::SetPropertyValuesEx($arFields["ID"], $arFields["IBLOCK_ID"], array("DESCRIPTION" => $arProperties["DESCRIPTION"]["VALUE"]["TEXT"]));
					}
				}
			}
		}

		if ($arFields["IBLOCK_ID"] == PHOTOS_IBLOCK_ID_RU || $arFields["IBLOCK_ID"] == PHOTOS_IBLOCK_ID_EN)
		{
			$dbElementProps = CIBlockElement::GetProperty(
				$arFields["IBLOCK_ID"],
				$arFields["ID"],
				array(),
				array("CODE" => "DESCRIPTION")
			);
			if ($arProp = $dbElementProps->GetNext())
			{
				$dbMultimedia = CIBlockElement::GetList(
					array(),
					array("IBLOCK_ID" => array(MULTIMEDIA_IBLOCK_ID_RU, MULTIMEDIA_IBLOCK_ID_EN), "PROPERTY_MULTIMEDIA" => $arFields["ID"]),
					false,
					false,
					array("ID", "IBLOCK_ID")
				);
				while ($arMultimedia = $dbMultimedia->GetNext())
				{
					CIBlockElement::SetPropertyValuesEx($arMultimedia["ID"], $arMultimedia["IBLOCK_ID"], array("DESCRIPTION" => $arProp["VALUE"]["TEXT"]));
				}
			}
		}

		if ($arFields["IBLOCK_ID"] == VIDEOS_IBLOCK_ID_RU || $arFields["IBLOCK_ID"] == VIDEOS_IBLOCK_ID_EN)
		{
			$dbElementProps = CIBlockElement::GetProperty(
				$arFields["IBLOCK_ID"],
				$arFields["ID"],
				array(),
				array("CODE" => "DESCRIPTION")
			);
			if ($arProp = $dbElementProps->GetNext())
			{
				$dbMultimedia = CIBlockElement::GetList(
					array(),
					array("IBLOCK_ID" => array(MULTIMEDIA_IBLOCK_ID_RU, MULTIMEDIA_IBLOCK_ID_EN), "PROPERTY_MULTIMEDIA" => $arFields["ID"]),
					false,
					false,
					array("ID", "IBLOCK_ID")
				);
				while ($arMultimedia = $dbMultimedia->GetNext())
				{
					CIBlockElement::SetPropertyValuesEx($arMultimedia["ID"], $arMultimedia["IBLOCK_ID"], array("DESCRIPTION" => $arProp["VALUE"]));
				}
			}
		}

		if ($arFields["IBLOCK_ID"] == DOCUMENTS_IBLOCK_ID_RU || $arFields["IBLOCK_ID"] == DOCUMENTS_IBLOCK_ID_EN) {

			$id = $arFields["ID"];

			$resProperties = CIBlockElement::GetProperty(
				$arFields["IBLOCK_ID"],
				$id,
				array(),
				array("CODE" => array("FILE", "FILE_TYPE"))
			);

			while ($arProperty = $resProperties->GetNext()) {
				if ($arProperty["CODE"] == "FILE") {
					$fileId = $arProperty["VALUE"];
					$fileSrc = CFile::GetPath($fileId);
					$fileExt = pathinfo($fileSrc, PATHINFO_EXTENSION);
				}
				if ($arProperty["CODE"] == "FILE_TYPE") {
					$propertyId = $arProperty["ID"];
				}
			}

			if (isset($propertyId) && isset($fileExt)) {
				CIBlockElement::SetPropertyValuesEx($id, $arFields["IBLOCK_ID"], array("FILE_TYPE" => $fileExt));
			}

		}

		if ($arFields["IBLOCK_ID"] == FIVE_YEAR_RESULTS_IBLOCK_ID_RU || $arFields["IBLOCK_ID"] == FIVE_YEAR_RESULTS_IBLOCK_ID_EN) {


			$dbElement = CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "ID" => $arFields["ID"]),
				false,
				false,
				array("ID", "IBLOCK_ID", "NAME")
			);
			while ($obElement = $dbElement->GetNextElement()) {

				$arElementFields = $obElement->GetFields();
				$arElementProperties = $obElement->GetProperties();

				foreach ($arElementProperties["DATA"]["VALUE"] as $rowData) {
					$arRowData = explode("|", $rowData);
					$arRowData = array_map("trim", $arRowData);
					$arElementProperties["DATA_FORMATED"][] = $arRowData;
				}

				$html = "<html><head>";
				$html .= "<style>
								table{border-collapse:collapse;border-spacing:0;border-width:0;width:100%;empty-cells:hide}
								table td,table th{text-align:left;vertical-align:top}
								table th{font-weight:normal;vertical-align:middle}
								table{line-height:16px;margin-bottom:40px}
								table td,table th{padding:14px 10px}
								table th{font:11px/20px Arial,FreeSans,sans-serif;font-weight:normal;border-bottom:1px solid #d2d2d2;color:#999}
								table tbody td{font:11px/20px Arial,FreeSans,sans-serif;font-weight:normal;border-bottom:1px solid #e5e5e5;color:#333}
								table tbody p{margin:2px 0}
						  </style>";
				$html .= "</head><body><table><thead><tr>";
				$html .= "<th>" . $arElementProperties["DESCRIPTION"]["VALUE"] . '</th>';
				foreach ($arElementProperties["COLUMNS"]["VALUE"] as $column) {
					$html .= "<th>" . $column . "</th>";
				}
				$html .= "</tr></thead><tbody>";
				foreach ($arElementProperties["ROWS"]["VALUE"] as $index =>  $row) {
					$html .= "<tr>";
					$html .= "<td>" . $row . "</td>";
					foreach ($arElementProperties["DATA_FORMATED"][$index] as $rowData) {
							$html .= "<td>" . $rowData . "</td>";
					}
					$html .= "</tr>";
				}
				$html .= "</tbody></table>";

			}

			$tmp = $_SERVER["DOCUMENT_ROOT"] . "/upload/tmp/"  . Cutil::translit($arElementFields["NAME"], "ru") . ".pdf";

			require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/dompdf/dompdf_config.inc.php");

			$dompdf = new DOMPDF();
			$dompdf->load_html($html, "UTF-8");
			$dompdf->render();

			file_put_contents($tmp, $dompdf->output());

			$propertyValues = array("PDF" => CFile::MakeFileArray($tmp));
			CIBlockElement::SetPropertyValuesEx(
				$arFields["ID"],
				$arFields["IBLOCK_ID"],
				$propertyValues
			);

		}

	}

	/* --- создание архива галереи ---------------------------------------------------------------------------------- */
	public function createGalleryArchive(&$arFields)
	{
		if ($arFields["IBLOCK_ID"] == PHOTOS_IBLOCK_ID_RU || $arFields["IBLOCK_ID"] == PHOTOS_IBLOCK_ID_EN) {

			$dbProperty = CIBlockElement::GetProperty(
				$arFields["IBLOCK_ID"],
				$arFields["ID"],
				array(),
				array(
					"CODE" => "PICTURES"
				)
			);

			$arPackFiles = array();
			while ($arProperty = $dbProperty->GetNext()) {
				$arPackFiles[] = $_SERVER["DOCUMENT_ROOT"] . CFile::GetPath($arProperty["VALUE"]);
			}

			/* --- Под архивируемые файлы выделяется директория в /upload/tmp/ -------------------------------------------------- */
			$sDirTmpName = randString();                                                                 // Имя временной папки
			$sDirTmpPath = $_SERVER["DOCUMENT_ROOT"] . "/upload/tmp/{$sDirTmpName}/";                    // Серверный путь до временной папки

			if(mkdir($sDirTmpPath)) {                                                                    // Создание временной папки

				foreach($arPackFiles as $k => $sFileArcPath) {                                           // Перебор всех файлов для архивирования
					$arPackFiles[$k] = $sDirTmpPath.basename($sFileArcPath);                             // Новый путь архивируемого файла
					copy($sFileArcPath, $arPackFiles[$k]);                                               // И копирование во временную папку
				}

				$archiveName = CUtil::translit($arFields["NAME"] . " - "  . $arFields["ID"], "ru");
				$packarc = CBXArchive::GetArchive($_SERVER["DOCUMENT_ROOT"] . "/upload/{$archiveName}.zip");
				$packarc->SetOptions(
					array(
						"REMOVE_PATH" => $sDirTmpPath,
					)
				);
				$pRes = $packarc->Pack($arPackFiles);


				$propertyValues = array(
					"ARCHIVE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . "/upload/{$archiveName}.zip")
				);

				CIBlockElement::SetPropertyValuesEx(
					$arFields["ID"],
					$arFields["IBLOCK_ID"],
					$propertyValues
				);

				foreach($arPackFiles as $file) if(is_file($file)) unlink($file);                        // Удаление скопированных файлов
				rmdir($sDirTmpPath);                                                                    // Удаление временной папки

			}

		}
	}

	/* --- вывод пользовательских полей у подписчиков --------------------------------------------------------------- */
	public function onAdminTabControlBegin(&$form)
	{
		if (strpos($GLOBALS["APPLICATION"]->GetCurPage(), "/bitrix/admin/subscr_edit.php") !==false ) {
			$userProperties = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields(
				"NLMK_SUBSCRIPTION",
				$_REQUEST["ID"],
				LANGUAGE_ID
			);
			if ($_REQUEST["ID"]):
				$form->tabs[] = array(
					"DIV" => "nlmk_edit",
					"TAB" => Loc::getMessage("TAB_CONTROL_ADDITIONAL_INFO"),
					"ICON" => "main_user_edit",
					"TITLE" => Loc::getMessage("TAB_CONTROL_SUBSCRIBER_ADDITIONAL_INFO"),
					"CONTENT" =>
						'<tr>
						<td>' . Loc::getMessage("TAB_CONTROL_NAME") . ':</td>
						<td>
							<input type="text" name="UF_NAME" value="'.$userProperties["UF_NAME"]["VALUE"].'" size="30">
						</td>
					</tr>
					<tr>
						<td>' . Loc::getMessage("TAB_CONTROL_POST") . ':</td>
						<td>
							<input type="text" name="UF_POST" value="'.$userProperties["UF_POST"]["VALUE"].'" size="30">
						</td>
					</tr>
					<tr>
						<td>' . Loc::getMessage("TAB_CONTROL_COMPANY") . ':</td>
						<td>
							<input type="text" name="UF_COMPANY" value="'.$userProperties["UF_COMPANY"]["VALUE"].'" size="30">
						</td>
					</tr>'
				);
			endif;
		}
	}

	public function BeforePostingSendMailHandler($arFields)
	{
		$obCache = new CPHPCache();
		$cacheLifetime = 3600;
		$cacheID = "posting" . $arFields["POSTING_ID"];
		$cachePath = "/subscribers/" . $cacheID;

		if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath))
		{
			$arResult = $obCache->GetVars();
		}
		elseif ($obCache->StartDataCache())
		{
			$arSubscriptions = array();
			$dbSubscriptions = CSubscription::GetList(array(), array("CONFIRMED" => "Y", "ACTIVE" => "Y"));
			while ($arSubscription = $dbSubscriptions->GetNext())
			{
				$arSubscriptions[$arSubscription["EMAIL"]] = $arSubscription;
			}

			$arSites = array();
			$dbSites = CSite::GetList($by="sort", $order="desc", array());
			while ($arSite = $dbSites->Fetch())
			{
				$arSites[$arSite["LID"]] = $arSite;
			}

			$arResult = array(
				"SITES" => $arSites,
				"SUBSCRIPTIONS" => $arSubscriptions,
			);

			$dbPosting = CPosting::GetRubricList($arFields["POSTING_ID"]);
			if ($arPosting = $dbPosting->Fetch())
				$arResult["POSTING"] = $arPosting;

			$obCache->EndDataCache($arResult);
		}

		$unsubscribeUrl = "http://" . $arResult["SITES"][$arResult["POSTING"]["LID"]]["SERVER_NAME"] . $arResult["SITES"][$arResult["POSTING"]["LID"]]["DIR"] . "media-center/subscription/?ID=" . $arResult["SUBSCRIPTIONS"][$arFields["EMAIL"]]["ID"] . "&CONFIRM_CODE=" . $arResult["SUBSCRIPTIONS"][$arFields["EMAIL"]]["CONFIRM_CODE"] . "&action=unsubscribe";
		$arFields["BODY"] = str_replace("#UNSUBSCRIBE_URL#", $unsubscribeUrl, $arFields["BODY"]);

		return $arFields;
	}

	public function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
	{
		if ($event == "SUBSCRIBE_CONFIRM")
		{
			$arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("NLMK_SUBSCRIPTION", $arFields["ID"]);
			// отмена события вызываемого в CSubscription::Add, т.к. в момент его вызова еще не сохранено Имя подписчика
			if (!empty($arUserFields["UF_NAME"]["VALUE"]))
				$arFields["UF_NAME"] = $arUserFields["UF_NAME"]["VALUE"];
			else
				return false;
		}
	}

	public function onBeforeResultAddHandler($formID, &$arFields, &$arrVALUES)
	{
		if (in_array($formID, array(SEND_CV_FORM_ID_RU, SEND_CV_FORM_ID_EN)))
		{
			$dbFields = CFormField::GetBySID("ANTISPAM");
			while ($arField = $dbFields->Fetch())
			{
				if ($arField["FORM_ID"] == $formID)
					$questionID = $arField["ID"];
			}

			if (isset($questionID))
			{
				$dbAnswers = CFormAnswer::GetList(
					$questionID,
					$by="s_id",
					$order="desc",
					array("ACTIVE" => "Y"),
					$isFiltered
				);
				if ($arAnswer = $dbAnswers->Fetch())
				{
					// если антиспам значение заполнено - результат формы не добавляем
					if (!empty($arrVALUES["form_" . $arAnswer["FIELD_TYPE"] . "_" . $arAnswer["ID"]]))
						$GLOBALS["APPLICATION"]->ThrowException();
				}
			}
		}
	}

}


?>
