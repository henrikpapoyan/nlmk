<?php
define("CONTACTS_DEFAULT_SECTION_ID_RU", 11);
define("CONTACTS_DEFAULT_SECTION_ID_EN", 317);

/* ------ about ----------------------------------------------------------------------------------------------------- */

define("HOME_SLIDER_IBLOCK_ID_RU", 90);
define("HOME_SLIDER_IBLOCK_ID_EN", 127);

define("DOCUMENTS_IBLOCK_ID_RU", 96);
define("DOCUMENTS_IBLOCK_ID_EN", 151);

define("DOCUMENTS_BOTTOM_IBLOCK_ID_RU", 98);
define("DOCUMENTS_BOTTOM_IBLOCK_ID_EN", 125);

define("DOCUMENTS_KORP_IBLOCK_ID_RU", 201);	
define("DOCUMENTS_KORP_IBLOCK_ID_EN", 199);	

define("DOCUMENTS_EMISS_IBLOCK_ID_RU", 205);
define("DOCUMENTS_EMISS_IBLOCK_ID_EN", 200);

define("FIVE_YEAR_RESULTS_IBLOCK_ID_RU", 154);
define("FIVE_YEAR_RESULTS_IBLOCK_ID_EN", 155);

define("HISTORY_IBLOCK_ID_RU", 105);
define("HISTORY_IBLOCK_ID_EN", 117);

define("KEY_FACTORS_SLIDER_IBLOCK_ID_RU", 113);
define("KEY_FACTORS_SLIDER_IBLOCK_ID_EN", 122);

define("KEY_FACTORS_IBLOCK_ID_RU", 112);
define("KEY_FACTORS_IBLOCK_ID_EN", 121);

define("DIRECTORS_IBLOCK_ID_RU", 18);
define("DIRECTORS_IBLOCK_ID_EN", 124);

define("MANAGEMENT_IBLOCK_ID_RU", 17);
define("MANAGEMENT_IBLOCK_ID_EN", 120);

define("GOVMANAGEMENT_IBLOCK_ID_RU", 194);
define("GOVMANAGEMENT_IBLOCK_ID_EN", 197);

define("COMPANIES_IBLOCK_ID_RU", 21);
define("COMPANIES_IBLOCK_ID_EN", 123);

define("INFOGRAPHIC_IBLOCK_ID_RU", 111);
define("INFOGRAPHIC_IBLOCK_ID_EN", 119);

define("GEOGRAPHY_DIVISION_IBLOCK_ID_RU", 22);
define("GEOGRAPHY_DIVISION_IBLOCK_ID_EN", 132);

define("FUNCTIONAL_DIVISION_IBLOCK_ID_RU", 23);
define("FUNCTIONAL_DIVISION_IBLOCK_ID_EN", 131);

define("MISSION_SLIDER_IBLOCK_ID_RU", 88);
define("MISSION_SLIDER_IBLOCK_ID_EN", 126);

define("MISSION_SLIDER_LEADERSHIP_IBLOCK_ID_RU", 176);
define("MISSION_SLIDER_LEADERSHIP_IBLOCK_ID_EN", 177);

define("MISSION_STORIES_IBLOCK_ID_RU", 25);
define("MISSION_STORIES_IBLOCK_ID_EN", 129);

define("PURCHASES_AND_SALES_IBLOCK_ID_RU" ,106);
define("PURCHASES_AND_SALES_IBLOCK_ID_EN", 118);

define("STRATEGY_IBLOCK_ID_RU", 114);
define("STRATEGY_IBLOCK_ID_EN", 115);

/* ------------------------------------------------------------------------------------------------------------------ */


/* ------ career ---------------------------------------------------------------------------------------------------- */

define("CAREER_SLIDER_IBLOCK_ID_RU", 102);
define("CAREER_SLIDER_IBLOCK_ID_EN", 171);

define("OUR_PROFESSIONS_IBLOCK_ID_RU", 9);
define("OUR_PROFESSIONS_IBLOCK_ID_EN", 82);

define("COMPETITIONS_IBLOCK_ID_RU", 8);
define("COMPETITIONS_IBLOCK_ID_EN", 81);

define("CAREER_CONTACTS_SIDEBAR_IBLOCK_ID_RU", 172);
define("CAREER_CONTACTS_SIDEBAR_IBLOCK_ID_EN", 173);

define("RESUME_IBLOCK_ID", 109);

define("JOBS_IBLOCK_ID_RU", 185);
define("JOBS_IBLOCK_ID_EN", 186);

/*
 * TODO
 * ИБ карьера EN
 * ИБ резюме
 */

/* ------------------------------------------------------------------------------------------------------------------ */


/* ------ contacts -------------------------------------------------------------------------------------------------- */

define("CONTACTS_IBLOCK_ID_RU", 19);
define("CONTACTS_IBLOCK_ID_EN", 83);

define("CONTACTS_SIDEBAR_IBLOCK_ID_RU", 183);
define("CONTACTS_SIDEBAR_IBLOCK_ID_EN", 184);

/*
 * TODO
 * ИБ резюме
 */

/* ------------------------------------------------------------------------------------------------------------------ */


/* ------ investor-relations ---------------------------------------------------------------------------------------- */

define("FINANCIAL_CALENDAR_IBLOCK_ID_RU", 37);
define("FINANCIAL_CALENDAR_IBLOCK_ID_EN", 62);

define("AGM_SLIDER_IBLOCK_ID_RU", 84);
define("AGM_SLIDER_IBLOCK_ID_EN", 85);

define("SHAREHOLDER_CENTRE_DOCS_IBLOCK_ID_RU", 156);
define("SHAREHOLDER_CENTRE_DOCS_IBLOCK_ID_EN", 157);

define("INVESTOR_SLIDER_IBLOCK_ID_RU", 99);
define("INVESTOR_SLIDER_IBLOCK_ID_EN", 158);

define("PRESENTATIONS_IBLOCK_ID_RU", 31);
define("PRESENTATIONS_IBLOCK_ID_EN", 60);

define("ANNUAL_REPORTS_IBLOCK_ID_RU", 94);
define("ANNUAL_REPORTS_IBLOCK_ID_EN", 159);

define("REPORTS_IBLOCK_ID_RU", 95);
define("REPORTS_IBLOCK_ID_EN", 160);

define("INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_RU", 165);
define("INVESTOR_CONTACTS_SIDEBAR_IBLOCK_ID_EN", 166);

define("INVESTOR_CONTACTS_IBLOCK_ID_RU", 167);
define("INVESTOR_CONTACTS_IBLOCK_ID_EN", 168);

/* ------------------------------------------------------------------------------------------------------------------ */


/* ------ media-center ---------------------------------------------------------------------------------------------- */

define("MC_SLIDER_IBLOCK_ID_RU", 174);
define("MC_SLIDER_IBLOCK_ID_EN", 175);

define("AWARDS_IBLOCK_ID_RU", 153);
define("AWARDS_IBLOCK_ID_EN", 161);

define("EVENTS_IBLOCK_ID_RU", 20);
define("EVENTS_IBLOCK_ID_EN", 137);

define("EVENTS_SLIDER_IBLOCK_ID_RU", 107);
define("EVENTS_SLIDER_IBLOCK_ID_EN", 150);

define("GLOSSARY_IBLOCK_ID_RU", 92);
define("GLOSSARY_IBLOCK_ID_EN", 148);

define("PERFORMANSES_IBLOCK_ID_RU", 26);
define("PERFORMANSES_IBLOCK_ID_EN", 138);

define("NEWS_SMI_IBLOCK_ID_RU", 27);
define("NEWS_SMI_IBLOCK_ID_EN", 139);

define("MULTIMEDIA_IBLOCK_ID_RU", 34);
define("MULTIMEDIA_IBLOCK_ID_EN", 145);

define("NEWS_COMPANIES_IBLOCK_ID_RU", 29);
define("NEWS_COMPANIES_IBLOCK_ID_EN", 141);

define("NEWS_GROUP_IBLOCK_ID_RU", 28);
define("NEWS_GROUP_IBLOCK_ID_EN", 140);

define("PRESS_KIT_IBLOCK_ID_RU", 162);
define("PRESS_KIT_IBLOCK_ID_EN", 149);

define("PRESS_CONTACTS_IBLOCK_ID_RU", 10);
define("PRESS_CONTACTS_IBLOCK_ID_EN", 133);

define("FAQ_IBLOCK_ID_RU", 12);
define("FAQ_IBLOCK_ID_EN", 135);

define("PHOTOS_IBLOCK_ID_RU", 32);
define("PHOTOS_IBLOCK_ID_EN", 143);

define("VIDEOS_IBLOCK_ID_RU", 33);
define("VIDEOS_IBLOCK_ID_EN", 144);

define("MC_FILES_IBLOCK_ID_RU", 35);
define("MC_FILES_IBLOCK_ID_EN", 146);

define("MC_CONTACTS_IBLOCK_ID_RU", 13);
define("MC_CONTACTS_IBLOCK_ID_EN", 136);

define("QUESTIONS_IBLOCK_ID_RU", 11);
define("QUESTIONS_IBLOCK_ID_EN", 134);

define("CORPORATE_MAGAZINE_IBLOCK_ID", 182);

/* ------------------------------------------------------------------------------------------------------------------ */


/* ------ our-business ---------------------------------------------------------------------------------------------- */

define("PRODUCTION_SLIDER_IBLOCK_ID_RU", 39);
define("PRODUCTION_SLIDER_IBLOCK_ID_EN", 49);

define("PRODUCTION_TYPE_IBLOCK_ID_RU", 42);
define("PRODUCTION_TYPE_IBLOCK_ID_EN", 46);

define("APPLICATION_SCOPE_IBLOCK_ID_RU", 40);
define("APPLICATION_SCOPE_IBLOCK_ID_EN", 51);

define("PRODUCTS_SLIDER_IBLOCK_ID_RU", 44);
define("PRODUCTS_SLIDER_IBLOCK_ID_EN", 50);

define("PRODUCTS_IBLOCK_ID_RU", 41);
define("PRODUCTS_IBLOCK_ID_EN", 47);

define("INNOVATIONS_SLIDER_IBLOCK_ID_RU", 43);
define("INNOVATIONS_SLIDER_IBLOCK_ID_EN", 48);

/* ------------------------------------------------------------------------------------------------------------------ */


/* ------ responsibility -------------------------------------------------------------------------------------------- */

define("CHARITY_SLIDER_IBLOCK_ID_RU", 86);
define("CHARITY_SLIDER_IBLOCK_ID_EN", 87);

define("CHARITY_PROGRAM_IBLOCK_ID_RU", 152);
define("CHARITY_PROGRAM_IBLOCK_ID_EN", 163);

define("CHARITY_PROJECTS_IBLOCK_ID_RU", 15);
define("CHARITY_PROJECTS_IBLOCK_ID_EN", 63);

define("ENV_PROGRAMME_IBLOCK_ID_RU", 16);
define("ENV_PROGRAMME_IBLOCK_ID_EN", 64);

define("ECOLOGY_CASES_IBLOCK_ID_RU", 14);
define("ECOLOGY_CASES_IBLOCK_ID_EN", 65);

define("ECOLOGY_SLIDER_IBLOCK_ID_RU", 103);
define("ECOLOGY_SLIDER_IBLOCK_ID_EN", 164);

define("ENERGY_CASES_IBLOCK_ID_RU", 178);
define("ENERGY_CASES_IBLOCK_ID_EN", 179);

define("ENERGY_SLIDER_IBLOCK_ID_RU", 180);
define("ENERGY_SLIDER_IBLOCK_ID_EN", 181);

/* ------------------------------------------------------------------------------------------------------------------ */


define("FAQ_FORM_ID", 1);



define("SEND_CV_FORM_ID_RU", 3);
define("SEND_CV_FORM_ID_EN", 2);
	

define("SOCIAL_NETWORKS_IBLOCK_ID_RU", 189);
define("SOCIAL_NETWORKS_IBLOCK_ID_EN", 190);


define("KEY_INDICATORS_CHARTS_IBLOCK_ID_RU", 187);
define("KEY_INDICATORS_CHARTS_IBLOCK_ID_EN", 188);

