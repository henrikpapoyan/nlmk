<?php
class CNLMK
{
	public static function convertUrlToFileName($arUrls)
	{
		$arResult = array();
		foreach ($arUrls as $page)
		{
			$arResult[$page] = str_replace("/", ".", trim($page, "/")) . ".pdf";
		}
		return $arResult;
	}

	public static function getUrlPagesForPdf()
	{
		$arBaseUrls = array(
			"about/key-factors/",
			"about/strategy/",
			"about/strategy/strategy-in-action/",
			"about/strategy/investment-program/",
			"about/management/",
			"about/group-structure/",
			"about/five-year-highlights/",
			"our-business/production/",
			"ir/financial-results/income-statement/",
			"ir/financial-results/balance/",
			"ir/financial-results/cash-flow/",
			"ir/financial-results/key-ratios/",
			"ir/financial-results/key-financial-and-operating-data/",
			"ir/governance/governance-system/",
			"ir/governance/board-of-directors/",
			"ir/governance/board-committees/",
			"ir/governance/directors-independent/",
			"ir/governance/management-board/",
			"ir/governance/corporate-secretary/",
			"ir/governance/audit-internal-control/",
			"ir/governance/risk-management/",
			"responsibility/ecology/key-indicators/",
			"responsibility/ecology/environmental-management-system/",
			"responsibility/ecology/environmental-programme-2020/",
			"responsibility/social-responsibility/industrial-safety/",
			"career/",
			"career/selection-and-motivation/",
			"career/personnel-development/",
			"career/students/",
			"career/young-professionals/"
		);

		return $arBaseUrls;
	}

	public static $multimediaIblocks = array(
		MULTIMEDIA_IBLOCK_ID_RU,
		MULTIMEDIA_IBLOCK_ID_EN
	);

	public static $smiIblocks = array(
		NEWS_SMI_IBLOCK_ID_RU,
		NEWS_SMI_IBLOCK_ID_EN
	);

	public static $performansesIblocks = array(
		PERFORMANSES_IBLOCK_ID_RU,
		PERFORMANSES_IBLOCK_ID_EN
	);

	public static $newsIblocks = array(
		NEWS_COMPANIES_IBLOCK_ID_RU,
		NEWS_COMPANIES_IBLOCK_ID_EN,
		NEWS_GROUP_IBLOCK_ID_RU,
		NEWS_GROUP_IBLOCK_ID_EN
	);

	public static $personIblocks = array(
		CONTACTS_IBLOCK_ID_RU,
		CONTACTS_IBLOCK_ID_EN,

		MC_CONTACTS_IBLOCK_ID_RU,
		MC_CONTACTS_IBLOCK_ID_EN,

		INVESTOR_CONTACTS_IBLOCK_ID_RU,
		INVESTOR_CONTACTS_IBLOCK_ID_EN
	);

	public static $filesIblocks = array(
		DOCUMENTS_IBLOCK_ID_RU,
		DOCUMENTS_IBLOCK_ID_EN,
	);

	public static function getQuarterDates($quarter, $year) {

		switch ($quarter) {
			case 1:
				return array(
					ConvertTimeStamp(strtotime("01-01-" . $year), "FULL"),
					ConvertTimeStamp(strtotime("31-03-" . $year), "FULL")
				);
				break;

			case 2:
				return array(
					ConvertTimeStamp(strtotime("01-04-" . $year), "FULL"),
					ConvertTimeStamp(strtotime("30-06-" . $year), "FULL")
				);
				break;

			case 3:
				return array(
					ConvertTimeStamp(strtotime("01-07-" . $year), "FULL"),
					ConvertTimeStamp(strtotime("30-09-" . $year), "FULL")
				);
				break;

			case 4:
				return array(
					ConvertTimeStamp(strtotime("01-10-" . $year), "FULL"),
					ConvertTimeStamp(strtotime("31-12-" . $year), "FULL")
				);
				break;

			default:
				return false;
				break;

		}

	}

	public static function youtubeSubscribers()
	{
		$key = "AIzaSyAcra2UawaHRRRSVNktCMdNxf0qHhtYKrM";
		$channelId = "UCVK-WsaF7c5CsG5Tx1WDi8Q";
		$jsonData = file_get_contents("https://www.googleapis.com/youtube/v3/channels?part=statistics&id=$channelId&key=$key");

		if ($jsonData != false) {
			$data = json_decode($jsonData);
			if ($data->items[0]) {
				$subscriberCount = $data->items[0]->statistics->subscriberCount;
				return $subscriberCount;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static function isHomePage($APPLICATION)
	{
		return $APPLICATION->GetCurPage() == SITE_DIR;
	}

	public static function isDetailNews($APPLICATION)
	{
		if (!empty($_REQUEST["ELEMENT_CODE"]) && strpos($APPLICATION->GetCurPage(), SITE_DIR . "media-center/news-companies/") !== false
			|| !empty($_REQUEST["ELEMENT_CODE"]) && strpos($APPLICATION->GetCurPage(), SITE_DIR . "media-center/news-groups/") !== false)
		{
			return true;
		}
		return false;
	}

	public static function detailMultimedia($APPLICATION)
	{
		if (strpos($APPLICATION->GetCurPage(), SITE_DIR . "media-center/multimedia/") !== false && $APPLICATION->GetCurPage() != SITE_DIR . "media-center/multimedia/") {
			return true;
		}
		return false;
	}

	public static function jobsFilter($arQuery)
	{
		$arFilter = array();
		if (!empty($arQuery["region"]))
			$arFilter["PROPERTY_REGION"] = filter_var($arQuery["region"], FILTER_SANITIZE_NUMBER_INT);
		if (!empty($arQuery["direction"]))
			$arFilter["SECTION_ID"] = filter_var($arQuery["direction"], FILTER_SANITIZE_NUMBER_INT);
		return $arFilter;
	}

	public static function galleryFilter($elementCode, $curPage)
	{
		$obCache = new CPHPCache();
		$cacheLifetime = 36000000;
		$cacheID = LANGUAGE_ID . filter_var($elementCode, FILTER_SANITIZE_STRING);
		$cachePath = "/filters/" . $cacheID;

		if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath))
		{
			$galleryFilter = $obCache->GetVars();
		}
		elseif ($obCache->StartDataCache())
		{
			$iblockId = strpos($curPage, SITE_DIR . "media-center/news-companies/") !== false
				? constant("NEWS_COMPANIES_IBLOCK_ID_" . strtoupper(LANGUAGE_ID))
				: constant("NEWS_GROUP_IBLOCK_ID_" . strtoupper(LANGUAGE_ID));

			if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS["CACHE_MANAGER"]))
			{
				$GLOBALS["CACHE_MANAGER"]->StartTagCache($cachePath);
				$GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_" . $iblockId);
				$GLOBALS["CACHE_MANAGER"]->EndTagCache();
			}

			$dbElement = CIBlockElement::GetList(
				array(),
				array(
					"IBLOCK_ID" => $iblockId,
					"CODE" => filter_var($elementCode, FILTER_SANITIZE_STRING)
				),
				false,
				false,
				array("ID", "IBLOCK_ID", "PROPERTY_COMPANY")
			);
			if ($arElement = $dbElement->Fetch())
			{
				$galleryFilter = !empty($arElement["PROPERTY_COMPANY_VALUE"])
					? array("PROPERTY_COMPANY" => $arElement["PROPERTY_COMPANY_VALUE"])
					: array();
			}
			else
			{
				$galleryFilter = array();
			}

			$obCache->EndDataCache($galleryFilter);
		}

		return $galleryFilter;
	}

	public static function financialReleasesFilter($arQuery)
	{
		$arFilter = array("!PROPERTY_FINANCIAL_RELEASES" => false);

		if (!empty($arQuery["filterYear"]))
		{
			$arFilter["><DATE_ACTIVE_FROM"] = array(
				ConvertTimeStamp(mktime(0, 0, 0, 1, 1, (int) $arQuery["filterYear"]), "FULL"),
				ConvertTimeStamp(mktime(0, 0, 0, 1, 1, (int) $arQuery["filterYear"] + 1), "FULL"),
			);
		}

		return $arFilter;
	}

	public static function tradingUpdatesFilter($arQuery)
	{
		$arFilter = array("!PROPERTY_TRADING_UPDATES" => false);

		if (!empty($arQuery["filterYear"]))
		{
			$arFilter["><DATE_ACTIVE_FROM"] = array(
				ConvertTimeStamp(mktime(0, 0, 0, 1, 1, (int) $arQuery["filterYear"]), "FULL"),
				ConvertTimeStamp(mktime(0, 0, 0, 1, 1, (int) $arQuery["filterYear"] + 1), "FULL"),
			);
		}

		return $arFilter;
	}


	public static function calendarFilter($arQuery)
	{
		$arQuery = array_map("htmlspecialchars", $arQuery);
		$filter = array();

		if ($arQuery["dateStart"] || $arQuery["dateEnd"]) {
			if ($arQuery["dateStart"] && $arQuery["dateEnd"]) {
				$filter[] = array(
					">=PROPERTY_DATE" => date("Y-m-d 00:00:00", strtotime($arQuery["dateStart"])),
					"<=PROPERTY_DATE" => date("Y-m-d 23:59:59", strtotime($arQuery["dateStart"])),
				);
			} else {
				if ($arQuery["dateStart"]) {
					$filter[">=PROPERTY_DATE"] = date("Y-m-d 00:00:00", strtotime($arQuery["dateStart"]));
				}
				if ($arQuery["dateEnd"]) {
					$filter["<=PROPERTY_DATE"] = date("Y-m-d 23:59:59", strtotime($arQuery["dateEnd"]));
				}
			}
		}

		if (!empty($arQuery["type"])) {
			if ($arQuery["type"] == "archive") {
				$filter["<PROPERTY_DATE"] = date("Y-m-d 00:00:00", time());
			}
		} else {
			if (empty($arQuery["dateStart"]) && empty($arQuery["dateEnd"])) {
				$filter[">=PROPERTY_DATE"] = date("Y-m-d 00:00:00", time());
			}
		}

		return $filter;
	}

	public static function multimediaFilter($arQuery)
	{
		$multimediaFilter = array();

		$obCache = new CPHPCache();
		$cacheLifetime = 36000000;
		$cacheID = LANGUAGE_ID . "multimedia" . filter_var($arQuery["company"], FILTER_SANITIZE_NUMBER_INT);
		$cachePath = "/filtersMultimedia/" . $cacheID;

		if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath))
		{
			$multimediaFilter = $obCache->GetVars();
		}
		elseif ($obCache->StartDataCache())
		{
			if (defined("BX_COMP_MANAGED_CACHE") && is_object($GLOBALS["CACHE_MANAGER"]))
			{
				$GLOBALS["CACHE_MANAGER"]->StartTagCache($cachePath);
				$GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_" . constant("PHOTOS_IBLOCK_ID_" . strtoupper(LANGUAGE_ID)));
				$GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_" . constant("VIDEOS_IBLOCK_ID_" . strtoupper(LANGUAGE_ID)));
				$GLOBALS["CACHE_MANAGER"]->EndTagCache();
			}

			$multimediaFilter["PROPERTY_MULTIMEDIA"] = array();
			$dbElement = CIBlockElement::GetList(
				array(),
				array(
					"IBLOCK_ID" => array(
						constant("PHOTOS_IBLOCK_ID_" . strtoupper(LANGUAGE_ID)),
						constant("VIDEOS_IBLOCK_ID_" . strtoupper(LANGUAGE_ID))
					),
					"PROPERTY_COMPANY" => filter_var($arQuery["company"], FILTER_SANITIZE_NUMBER_INT)
				),
				false,
				false,
				array("ID", "IBLOCK_ID")
			);
			while ($arElement = $dbElement->Fetch())
			{
				$multimediaFilter["PROPERTY_MULTIMEDIA"][] = $arElement["ID"];
			}

			$obCache->EndDataCache($multimediaFilter);
		}

		// если нет фото- и видео- галерей для указанного предприятия
		if (empty($multimediaFilter["PROPERTY_MULTIMEDIA"]))
			$multimediaFilter["PROPERTY_MULTIMEDIA"] = false;

		if ($arQuery["type"])
			$multimediaFilter["PROPERTY_TYPE_VALUE"] = filter_var($arQuery["type"], FILTER_SANITIZE_STRING);

		if ($arQuery["keywords"])
			$multimediaFilter["NAME"] =  "%" . filter_var($arQuery["keywords"], FILTER_SANITIZE_STRING) . "%";

		return $multimediaFilter;

	}

	public static function eventsFilter($arQuery)
	{
		$arQuery = array_map("htmlspecialchars", $arQuery);
		$performancesFilter = array();

		if ($arQuery["dateEnd"]) {
			$performancesFilter["><PROPERTY_DATE"] = array(
				date("Y-m-d"),
				date_format(date_create($arQuery["dateEnd"]), "Y-m-d")
			);
		} else {
			$performancesFilter[">=PROPERTY_DATE"] = date("Y-m-d");
		}

		if ($arQuery["company"]) {
			$performancesFilter["PROPERTY_COMPANY"] = htmlspecialchars($arQuery["company"]);
		}

		if ($arQuery["keywords"]) {
			$performancesFilter[] = array(
				"LOGIC" => "OR",
				array("NAME" => "%" . htmlspecialchars($arQuery["keywords"]) . "%"),
				array("PROPERTY_SUBTITLE" => "%" . htmlspecialchars($arQuery["keywords"]) . "%")
			);
		}

		return $performancesFilter;
	}

	public static function pastEventsFilter($arQuery)
	{
		$arQuery = array_map("htmlspecialchars", $arQuery);
		$performancesFilter = array();

		if ($arQuery["dateStart"]) {
			$performancesFilter["><PROPERTY_DATE"] = array(
				date_format(date_create($arQuery["dateStart"]), "Y-m-d"),
				date("Y-m-d")
			);
		} else {
			$performancesFilter["<PROPERTY_DATE"] = date("Y-m-d");
		}

		if ($arQuery["company"]) {
			$performancesFilter["PROPERTY_COMPANY"] = htmlspecialchars($arQuery["company"]);
		}

		if ($arQuery["keywords"]) {
			$performancesFilter[] = array(
				"LOGIC" => "OR",
				array("NAME" => "%" . htmlspecialchars($arQuery["keywords"]) . "%"),
				array("PROPERTY_SUBTITLE" => "%" . htmlspecialchars($arQuery["keywords"]) . "%")
			);
		}

		return $performancesFilter;
	}

	public static function performancesFilter($arQuery)
	{
		$arQuery = array_map("htmlspecialchars", $arQuery);
		$performancesFilter = array();

		if ($arQuery["dateStart"] || $arQuery["dateEnd"]) {
			if ($arQuery["dateStart"] && $arQuery["dateEnd"]) {
				$performancesFilter["><DATE_ACTIVE_FROM"] = array(
					ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["dateStart"])), "FULL"),
					ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["dateEnd"])), "FULL")
				);
			} else {
				if ($arQuery["dateStart"]) {
					$performancesFilter[">=DATE_ACTIVE_FROM"] = ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["dateStart"])), "FULL");
				}
				if ($arQuery["dateEnd"]) {
					$performancesFilter["<=DATE_ACTIVE_FROM"] = ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["dateEnd"])), "FULL");
				}
			}
		}

		if ($arQuery["company"]) {
			$performancesFilter["PROPERTY_COMPANY"] = htmlspecialchars($arQuery["company"]);
		}

		if ($arQuery["keywords"]) {
			$performancesFilter[] = array(
				"LOGIC" => "OR",
				array("PROPERTY_PERSON_NAME" => "%" . htmlspecialchars($arQuery["keywords"]) . "%"),
				array("PROPERTY_SUBTITLE" => "%" . htmlspecialchars($arQuery["keywords"]) . "%")
			);
		}

		return $performancesFilter;
	}

	public static function newsFilter($arQuery)
	{
		$arQuery = array_map("htmlspecialchars", $arQuery);
		$newsFilter = array();

		if ($arQuery["keywords"]) {
			$newsFilter[] = array(
				"LOGIC" => "OR",
				array("NAME" => "%" . $arQuery["keywords"] . "%"),
				array("PROPERTY_SUBTITLE" => "%" . $arQuery["keywords"] . "%")
			);
		}

		if ($arQuery["dateStart"] || $arQuery["dateEnd"]) {
			if ($arQuery["dateStart"] && $arQuery["dateEnd"]) {
				$newsFilter["><DATE_ACTIVE_FROM"] = array(
					ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["dateStart"])), "FULL"),
					ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["dateEnd"])), "FULL")
				);
			} else {
				if ($arQuery["dateStart"]) {
					$newsFilter[">=DATE_ACTIVE_FROM"] = ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["dateStart"])), "FULL");
				}
				if ($arQuery["dateEnd"]) {
					$newsFilter["<=DATE_ACTIVE_FROM"] = ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["dateEnd"])), "FULL");
				}
			}
		}

		if ($arQuery["company"]) {
			$newsFilter["PROPERTY_COMPANY"] = $arQuery["company"];
		}

		return $newsFilter;
	}

	public static function archiveFilter($arQuery)
	{
		$arQuery = array_map("htmlspecialchars", $arQuery);
		$documentsFilter = array();

		if ($arQuery["keywords"]) {
			$documentsFilter["NAME"] = "%" . $arQuery["keywords"]. "%";
		}

		if (!$arQuery["section"]) {
			$documentsFilter["SECTION_CODE"] = array("ANNUAL_REPORTS", "TRADING_UPDATES", "DISCLOSURE", "DISCLOSURE1", "DISCLOSURE2", "DISCLOSURE3");
		}

		if ($arQuery["year"]) {
			$documentsFilter["><DATE_ACTIVE_FROM"] = array(
				ConvertTimeStamp(mktime(0, 0, 0, 1, 1, (int) $arQuery["year"]), "FULL"),
				ConvertTimeStamp(mktime(0, 0, 0, 1, 1, (int) $arQuery["year"] + 1), "FULL"),
			);
			if ($arQuery["quarter"]) {
				$documentsFilter["><DATE_ACTIVE_FROM"] = CNLMK::getQuarterDates($arQuery["quarter"], $arQuery["year"]);
			}
		}

		return $documentsFilter;
	}

	public static function documentsFilter($arQuery)
	{
		$arQuery = array_map("htmlspecialchars", $arQuery);
		$documentsFilter = array(
			"SECTION_ACTIVE" => "Y"
		);

		if ($arQuery["keywords"]) {
			$documentsFilter["NAME"] = "%" . $arQuery["keywords"]. "%";
		}

		if ($arQuery["section"]) {
			$documentsFilter["SECTION_ID"] = $arQuery["section"];
		}

		if ($arQuery["year"]) {
			$documentsFilter["><DATE_ACTIVE_FROM"] = array(
				ConvertTimeStamp(mktime(0, 0, 0, 1, 1, (int) $arQuery["year"]), "FULL"),
				ConvertTimeStamp(mktime(0, 0, 0, 1, 1, (int) $arQuery["year"] + 1), "FULL"),
			);
		}

		if ($arQuery["date-start"] || $arQuery["date-finish"]) {
			if ($arQuery["date-start"] && $arQuery["date-finish"]) {
				$documentsFilter["><DATE_ACTIVE_FROM"] = array(
					ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["date-start"])), "FULL"),
					ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["date-finish"])), "FULL")
				);
			} else {
				if ($arQuery["date-start"]) {
					$documentsFilter[">=DATE_ACTIVE_FROM"] = ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["date-start"])), "FULL");
				}
				if ($arQuery["date-finish"]) {
					$documentsFilter["<=DATE_ACTIVE_FROM"] = ConvertTimeStamp(strtotime(htmlspecialchars($arQuery["date-finish"])), "FULL");
				}
			}
		}

		if ($arQuery["type"]) {
			$documentsFilter["PROPERTY_FILE_TYPE"] = $arQuery["type"];
		}

		return $documentsFilter;
	}

	public static function isActiveLanguage($language)
	{
		return LANGUAGE_ID == $language;
	}

	public static function bodyClass($APPLICATION)
	{
		$class = "";

		if ($APPLICATION->GetCurPage() == SITE_DIR . "about/strategy/purchases-and-sales/") {
			$class = "page-container-thumb-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "ir/overview/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "responsibility/energy-efficiency/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "responsibility/ecology/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "media-center/exhibitions-and-conferences/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "responsibility/charity/projects/") {
			$class = "page-container-middle-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "ir/shareholder-centre/agm/") {
			$class = "page-container-slider";
		}

		if (strpos($APPLICATION->GetCurPage(), SITE_DIR . "media-center/multimedia/") !== false
			&& $APPLICATION->GetCurPage() != SITE_DIR . "media-center/multimedia/") {
			$class = "page-title-bg";
		}

		if (strpos($APPLICATION->GetCurPage(), SITE_DIR . "about/map-of-assets/") !== false
			&& $APPLICATION->GetCurPage() != SITE_DIR . "about/map-of-assets/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "our-business/products-and-innovations/innovations/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "career/selection-and-motivation/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "our-business/products-and-innovations/products/") {
			$class = "page-container-slider page-aside-h_478";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "our-business/products-and-innovations/application-fields/") {
			$class = "page-container-slider page-aside-h_478";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "about/mission/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "our-business/production/") {
			$class = "page-container-big-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "about/history/") {
			$class = "page-container-thumb-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "media-center/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "about/key-factors/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "career/") {
			$class = "page-container-slider";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "contacts/") {
			$class = 'page-title-step';
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "search/") {
			$class = "page-title-step";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "sitemap/") {
			$class = "page-title-step";
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR) {
			$class = "page-index";
		}

		return $class;
	}

	public static function getFileIcon($extension)
	{
		switch ($extension) {
			case "pdf":
				$icon = "i-ico_pdf";
				break;
			case "png":
				$icon = "i-ico_png";
				break;
			case "jpg":
				$icon = "i-ico_png";
				break;
			case "ai":
				$icon = "i-ico_ai";
				break;
			case "doc":
			case "docx":
				$icon = "i-ico_doc";
				break;
			case "xls":
			case "xlsx":
				$icon = "i-ico_xls";
				break;
			default:
				$icon = "";
		}
		return $icon;
	}

	public static function initDirProperties()
	{
		global $APPLICATION;

		if (strpos($APPLICATION->GetCurPage(), SITE_DIR . "media-center/exhibitions-and-conferences/") !== false
			&& $APPLICATION->GetCurPage() != SITE_DIR . "media-center/exhibitions-and-conferences/") {
			$APPLICATION->SetDirProperty("hide_title", "Y");
		}

		if (strpos($APPLICATION->GetCurPage(), SITE_DIR . "responsibility/ecology/environmental-programme-2020/") !== false
			&& $APPLICATION->GetCurPage() != SITE_DIR . "responsibility/ecology/environmental-programme-2020/") {
			$APPLICATION->SetDirProperty("hide_title", "Y");
		}

		if (strpos($APPLICATION->GetCurPage(), SITE_DIR . "responsibility/energy-efficiency/cases/") !== false
			&& $APPLICATION->GetCurPage() != SITE_DIR . "responsibility/energy-efficiency/cases/") {
			$APPLICATION->SetDirProperty("hide_title", "Y");
		}

		if (strpos($APPLICATION->GetCurPage(), SITE_DIR . "responsibility/ecology/key-cases/") !== false
			&& $APPLICATION->GetCurPage() != SITE_DIR . "responsibility/ecology/key-cases/") {
			$APPLICATION->SetDirProperty("hide_title", "Y");
		}

		if (strpos($APPLICATION->GetCurPage(), SITE_DIR . "responsibility/charity/projects/") !== false
			&& $APPLICATION->GetCurPage() != SITE_DIR . "responsibility/charity/projects/") {
			$APPLICATION->SetDirProperty("hide_title", "Y");
		}

		if (strpos($APPLICATION->GetCurPage(), SITE_DIR . "about/mission/") !== false
			&& $APPLICATION->GetCurPage() != SITE_DIR . "about/mission/") {
			$APPLICATION->SetDirProperty("hide_title", "N");
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "ir/financial-calendar/") {
			$APPLICATION->SetDirProperty("hide_title", "N");
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "media-center/news-groups/") {
			$APPLICATION->SetDirProperty("hide_title", "N");
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "about/map-of-assets/") {
			$APPLICATION->SetDirProperty("hide_title", "N");
		}

		if ($APPLICATION->GetCurPage() == SITE_DIR . "media-center/interviews-and-speeches/") {
			$APPLICATION->SetDirProperty("hide_title", "N");
		}

	}

}?>
<?
AddEventHandler("main", "OnAfterUserLogin", Array("BadAuthorization", "OnAfterUserLoginHandler"));
class BadAuthorization
{
    function OnAfterUserLoginHandler(&$fields)
    {
		$fields['RESULT_MESSAGE']['MESSAGE']='Неверный логин или пароль / Invalid login or password';
    } 
	
}
?>