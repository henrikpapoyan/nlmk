<?
if(!$_SERVER['DOCUMENT_ROOT'])
  $_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__.'/../../../../');

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
Loader::includeModule("iblock");

define('NEEDED_ACCESS_LEVEL', 0777);
define('FOLDER_PATH', $_SERVER['DOCUMENT_ROOT'].'/upload/pdf_pics/');
define('PIC_HEIGHT', 1600);


/**
* class for parsing PDF
*/
class CParsePDF
{
  protected static $folder   = FOLDER_PATH;
  protected static $str      = '';
  protected static $updID    = false;
  protected static $elExists = false;

  protected static function CheckFolder(){
    if(!file_exists(self::$folder)){
      mkdir(self::$folder, NEEDED_ACCESS_LEVEL);
      self::AddMsg(self::$folder." created");
    } else {
      self::AddMsg(self::$folder."  exists");
    }
  }

  public static function GetPageNum($file){
    if(!file_exists($file))return;

    $ex = "pdfinfo '{$file}' | grep Pages: | awk '{print \$2}'";
    exec($ex, $res);

    $num = current($res);

    self::AddMsg('count pictures: ' . print_r($num,true));
    self::Log();

    return $num;
  }

  public static function __GetPageNum($file){
    if(!file_exists($file))return;
    self::AddMsg('[identify_count] start: '.date('c'));
    self::Log();

    $ex = "identify -format %n '{$file}'";
    exec($ex, $res);
    $num = current($res);

    self::AddMsg('count pictures: ' . print_r($num,true) . ', [' . date('c') . ']');
    self::Log();
    return $num;
  }

  public static function isCMYK($file){
    if(!file_exists($file))return $false;

    $ex = "identify -verbose '{$file}[0]' | grep CMYK";
    exec($ex, $res);
    $ans = !empty($res) ? true : false;

    self::AddMsg('is CMYK: ' . ($ans ? 'yes' : 'no'));
    return $ans;
  }

  public static function ConvertPdfToPics($param=array()){
    // drfault parameters
    $dpi       = 300;
    $path      = $_SERVER['DOCUMENT_ROOT'].'/upload/pdf/';
    $file      = 'publication.pdf';
    $qual      = 80;
    $width     = 1024;
    $height    = 1024;
    $resizeImg = false;
    $cmyk      = __DIR__.'/USWebUncoated.icc';
    $srgb      = __DIR__.'/sRGB.icm';
    extract($param);

    $time = time();
    //$is_cmyk = self::isCMYK($file);
    $num = self::GetPageNum($file);

    $arSrc = array();

    for ($i=0; $i < $num; $i++) {
      $strInd = str_pad($i, 3, "0", STR_PAD_LEFT);
      $src = "{$path}picture_{$time}_{$strInd}.jpg";
      // $src = "{$path}picture_{$strInd}.jpg";

      /*if($is_cmyk){
        //$ex = "convert -profile '{$cmyk}' -density {$dpi} '{$file}[{$i}]' -background '#FFFFFF' -flatten -resample 150 -profile '{$srgb}' {$src}";
        // раскомментировать, если первый вариант не пподойдет
        $ex = "convert -colorspace RGB -density {$dpi} '{$file}[{$i}]' -background '#FFFFFF' -flatten -resample 150 {$src}";
        // $ex = "convert -colorspace CMYK -density {$dpi} '{$file}[{$i}]' -background '#FFFFFF' -flatten -resample 150 -colorspace RGB {$src}";
      }else{
        $ex = "convert -density {$dpi} '{$file}[{$i}]' -background '#FFFFFF' -flatten -resample 150 {$src}";
      }*/
      $ex = "convert -colorspace RGB -density {$dpi} '{$file}[{$i}]' -background '#FFFFFF' -flatten -resample 150 '{$src}'";
      self::AddMsg("Running: ".$ex);

      $out = 0;
      exec($ex, $out);

      if(empty($out)){
        $arSrc[] = $src;
      }

      self::Log();
    }

    self::AddMsg("Pics done. ".date('c'));
    self::AddMsg("Last saved pic: {$src}");

    return $arSrc;
  }

  protected static function FindServiceElement($id, $md5){
    self::$updID    = false;
    self::$elExists = false;

    $upSelect = array('ID', 'IBLOCK_ID', 'NAME', 'CODE', 'DATE_ACTIVE_FROM');
    $upFilter = array(
      'IBLOCK_ID'=>CORPORATE_MAGAZINE_IBLOCK_ID,
      'ACTIVE_DATE'=>'Y', 'ACTIVE'=>'Y',
      'CODE'=>$id
    );
    $upRes = CIBlockElement::GetList(array(), $upFilter, false, array('nTopCount'=>1), $upSelect);
    if($up = $upRes->GetNextElement()){
      self::$elExists = true;

      $arFields = $up->GetFields();
      $arFields['PROPERTIES'] = $up->GetProperties();

      if($arFields['PROPERTIES']['MD5']['VALUE']!=$md5 || empty($arFields['PROPERTIES']['PDF_PAGES']['VALUE'])){
        self::$updID = $arFields['ID'];
        self::AddMsg("Текущий элемент [{$arFields['~NAME']}] будет обновлен");
      } else {
        self::AddMsg("Текущий элемент [{$arFields['~NAME']}] актуален");
      }
      self::AddMsg("el_MD5: {$arFields['PROPERTIES']['MD5']['VALUE']}");
      self::AddMsg("page_count: ".count($arFields['PROPERTIES']['PDF_PAGES']['VALUE']));
    }

    self::Log();
  }

  protected static function GetFile($id){
    $arFile = CFile::GetFileArray($id);
    return "{$_SERVER['DOCUMENT_ROOT']}{$arFile['SRC']}";
  }

  protected static function GetPics($file){
    self::AddMsg("updating");

    $arSrc = self::ConvertPdfToPics(array(
      'file'      => $file,
      'path'      => self::$folder,
      'resizeImg' => 'height',
      'height'    => PIC_HEIGHT
    ));

    $arFiles = array();

    // подготовить массив файлов для загрузки в ИБ
    foreach ($arSrc as $src) {
      $arPic = CFile::MakeFileArray($src);
      $arFiles[] = $arPic;
    }

    return array(
      'files' => $arFiles,
      'srcs'  => $arSrc
    );
  }

  public static function UpdElement($arElement){
    $el = new CIBlockElement;

    $file = self::GetFile($arElement['PROPERTIES']['FILE']['VALUE']);
    $md5 = md5_file($file);
    self::AddMsg("md5 {$md5}");

    // искать элемент в служебном инфоблоке
    self::FindServiceElement($arElement['ID'], $md5);

    if(self::$updID || !self::$elExists){
      $arPics = self::GetPics($file);
      $arFiles = $arPics['files'];
      // установить первую картинку в качестве preview_picture в ИБ Документы
      self::AddMsg('first pic array: '.print_r($arFiles[0],true));
      $el->Update($arElement['ID'], array('PREVIEW_PICTURE' => $arFiles[0]));
    }

    if(self::$updID && !empty($arFiles)){
      // обновить мд5 и загрузить новые картинки, если элемент существует, но не актуален
      CIBlockElement::SetPropertyValuesEx(self::$updID, false, array(
        'MD5'       => $md5,
        'PDF_PAGES' => $arFiles
      ));
    }

    if(!self::$elExists){
      // добавить новый элемент, если его еще нет
      $arLoadProductarray = array(
        'IBLOCK_SECTION_ID' => false,
        'IBLOCK_ID'         => CORPORATE_MAGAZINE_IBLOCK_ID,
        'PROPERTY_VALUES'   => array(
          'MD5'       => $md5,
          'PDF_PAGES' => $arFiles
        ),
        'NAME'              => $arElement['~NAME'],
        'ACTIVE'            => 'Y',
        'CODE'              => $arElement['ID']
      );
      if($PRODUCT_ID = $el->Add($arLoadProductarray)){
         self::AddMsg('New ID: '.$PRODUCT_ID);
      }else{
         self::AddMsg('Error: '.$el->LAST_ERROR);
      }
    }

    if(!empty($arPics['srcs'])) self::ClearFolder($arPics['srcs']);
  }

  protected static function AddMsg($msg=''){
    self::$str .= $msg . PHP_EOL;
  }

  protected static function ClearFolder($arSrc=array()){
    if(empty($arSrc))
      $arSrc = glob(self::$folder."*");
    array_map('unlink', $arSrc);
    self::AddMsg(count($arSrc).' pictures removed');
  }

  protected static function Log($clear=false, $path=''){
    if(empty($path))$path=__DIR__.'/log.txt';
    if($clear){
      file_put_contents($path, '');
    }
    else{
      file_put_contents($path, self::$str, FILE_APPEND | LOCK_EX);
      self::$str = '';
    }
  }

  public static function FindAndParse(){
    self::Log(true);
    self::AddMsg("Started at ".date('d.M.Y:H:i:s'));

    // проверить наличие папки для картинок
    self::CheckFolder();

    $arSelect = array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM");
    $arFilter = array(
      "ACTIVE_DATE"=>"Y",
      "ACTIVE"=>"Y",
      array(
        "LOGIC"=>"OR",
        ["IBLOCK_ID"=>DOCUMENTS_IBLOCK_ID_RU],
        ["IBLOCK_ID"=>DOCUMENTS_IBLOCK_ID_EN],
      ),
      "SECTION_CODE"=>"NLMK_COMPANY"
    );
    $arCount = false;
    // $arCount = array("nTopCount"=>1);

    $res = CIBlockElement::GetList(Array(), $arFilter, false, $arCount, $arSelect);

    while($ob = $res->GetNextElement()) {
      $arElement = $ob->GetFields();
      $arElement['PROPERTIES'] = $ob->GetProperties();

      self::AddMsg("{$arElement['ID']}, {$arElement['~NAME']}");
      self::UpdElement($arElement);
      self::AddMsg(PHP_EOL);

      self::Log();
    }

    // очистить папку с картинками
    self::ClearFolder();

    self::AddMsg("Successfully ended at ".date('d.M.Y:H:i:s'));

    self::Log();

    return "CParsePDF::FindAndParse();";
  }
}