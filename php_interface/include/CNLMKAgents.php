<?php

// добавить выполнение агентов на крон (например, /usr/bin/php -f /var/www/bitrix/modules/main/tools/cron_events.php)

class CNLMKAgents
{
	public static function clearPdfFolder()
	{
		$uploadPdfDir = "/../../../upload/tmp/pdf/";
		if (is_dir(dirname(__FILE__) . $uploadPdfDir)) {
			$files = glob(dirname(__FILE__) . $uploadPdfDir . "*");
			if (!empty($files)) {
				foreach($files as $file) {
					if(is_file($file)) {
						unlink($file);
					}
				}
			}
		}

		return "CNLMKAgents::clearPdfFolder();";
	}

	public static function generatePdfFiles()
	{
		$rPath = dirname(__FILE__) . "/phantomjs/rasterize.js";
		$arUrls = CNLMK::getUrlPagesForPdf();

		if (!is_dir(dirname(__FILE__) . "/../../../upload/pdf_pages/"))
		{
			if (!mkdir(dirname(__FILE__) . "/../../../upload/pdf_pages/", 0775))
			{
				// -- не удалось создать папку
			}
		}

		$arSites = array();
		$dbSites = CSite::GetList($by="sort", $order="desc", array("ACTIVE" => "Y"));
		while ($arSite = $dbSites->Fetch())
		{
			$arSites[] = $arSite;
		}

		$defaultServerName = COption::GetOptionString("main", "server_name");

		foreach ($arUrls as $url)
		{
			foreach ($arSites as $arSite)
			{
				$serverName = $arSite["SERVER_NAME"]
					? $arSite["SERVER_NAME"]
					: $defaultServerName;
				$pageUrl = "http://" . $serverName . $arSite["DIR"] . $url;
				$fPath = dirname(__FILE__) . "/../../../upload/pdf_pages/" . str_replace("/", ".", trim($arSite["DIR"] . $url, "/")) . ".pdf";
				exec("phantomjs --debug=true " . $rPath . " " . $pageUrl . "?pdf=Y" . " "  . $fPath . " 2>&1", $output);
				AddMessage2Log($output);
			}
		}

		return "CNLMKAgents::generatePdfFiles();";
	}
}
