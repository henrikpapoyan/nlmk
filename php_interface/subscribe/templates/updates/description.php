<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arTemplate = array(
	"NAME" => Loc::getMessage("PRESS_RELEASES_TMPL_NAME"),
	"DESCRIPTION" => Loc::getMessage("PRESS_RELEASES_TMPL_DESC")
);
?>